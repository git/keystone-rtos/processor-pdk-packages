#!/bin/bash
#   ============================================================================
#   @file   pdk_test_auto.sh
#
#   @desc   Script to Trigger the test execution using testlink
#
#   ============================================================================
#   Revision History
#   23-Aug-2017 Ankur         Initial draft.
#
#   ============================================================================

# Command: pdk_test_auto.sh --pdk_tag=$pdk_tag --binary_dir=$binary_dir --build_name=$build_name

start_time=`date +%s`

date=`date '+%b_%d_%Y_%I_%M_%p'`
date_print=`date '+%d-%b-%Y %I:%M:%S %p'`
month=`date '+%b_%Y'`
year=`date '+%Y'`
git_path="/usr/bin"

#Get user input
while [ $# -gt 0 ]; do
  case "$1" in
    --pdk_tag=*)
      pdk_tag="${1#*=}"
      ;;
    --binary_dir=*)
      binary_dir="${1#*=}"
      ;;
    --build_name=*)
      build_name="${1#*=}"
      ;;
    --mcpif_dir=*)
      mcpif_dir="${1#*=}"
      ;;
    --test_pc_ip=*)
      test_pc_ip="${1#*=}"
      ;;
    --tda3xx_tee=*)
      tda3xx_tee="${1#*=}"
      ;;
    --tda2xx_tee=*)
      tda2xx_tee="${1#*=}"
      ;;
    --tda2ex_tee=*)
      tda2ex_tee="${1#*=}"
      ;;
    --tda2px_tee=*)
      tda2px_tee="${1#*=}"
      ;;
    *)
      printf "Error: Invalid argument $1!!\n"
  esac
  shift
done

#Default value if not provided
: ${pdk_tag:="NA"}
: ${binary_dir:="\dbdsamba03/data/datalocal1_videoapps01/user/pdp_jenkin_build/pdk_daily_build/executables/pdk"}
: ${build_name:="test_build"}
: ${mcpif_dir:="/c/ti/mcpi_tf/mcpi_testframework_1_12_00_01"}
: ${test_pc_ip:="NA"}
: ${tda3xx_tee:="MCPI_TEE2@a0132235pc01_TDA3xx"}
: ${tda2xx_tee:="NA"}
: ${tda2ex_tee:="NA"}
: ${tda2px_tee:="NA"}

if [ "$working_dir" == "" ]; then
  working_dir=`pwd`
  #clone in same directory
  pdk_dir=$working_dir/pdk
else
  pdk_dir=$working_dir/pdk
fi

#Make required directories
rm -rf $working_dir/pdk

# Clone the scripts

git_server_bb="ssh://git@bitbucket.itg.ti.com/processor-sdk"

echo "=== Cloning PDK Repositories ..."
echo Cloning $git_server_bb/processor-pdk-packages.git ...
# git clone -q $git_server_bb/processor-pdk-packages.git pdk

if [ "$test_pc_ip" == "NA" ]; then
  test_pc_ip="$(ipconfig | grep 'IPv4' | tail -1 | cut -d ':' -f 2 | cut -d ' ' -f 2)"
fi

cd $mcpif_dir/opentest

# Create testlink build and trigger the test execution
if [ "$tda3xx_tee" != "NA" ]; then
  # Build for tda3xx
  des_str="var_TestRootPath="$binary_dir";pdk_commit_id="$pdk_tag";assign_to_tees="$tda3xx_tee" on "$test_pc_ip

  # Execution for more platform can be added and needs to run parallely
  Java.exe -jar opentest_cli.jar --buildid $build_name --report MATRIX_BUILD --testplan EP_PDK:PDK_Test_Plan_TDA3xx --hw platform=tda3xx --sw "$des_str"
fi
if [ "$tda2xx_tee" != "NA" ]; then
  # Build for tda2xx
  des_str="var_TestRootPath="$binary_dir";pdk_commit_id="$pdk_tag";assign_to_tees="$tda2xx_tee" on "$test_pc_ip

  # Execution for more platform can be added and needs to run parallely
  Java.exe -jar opentest_cli.jar --buildid $build_name --report MATRIX_BUILD --testplan EP_PDK:PDK_Test_Plan_TDA2xx --hw platform=tda2xx --sw "$des_str"
fi
if [ "$tda2ex_tee" != "NA" ]; then
  echo Entering tda2ex
  # Build for tda2ex
  des_str="var_TestRootPath="$binary_dir";pdk_commit_id="$pdk_tag";assign_to_tees="$tda2ex_tee" on "$test_pc_ip

  # Execution for more platform can be added and needs to run parallely
  Java.exe -jar opentest_cli.jar --buildid $build_name --report MATRIX_BUILD --testplan EP_PDK:PDK_Test_Plan_TDA2ex --hw platform=tda2ex --sw "$des_str"
fi
if [ "$tda2px_tee" != "NA" ]; then
  # Build for tda2px
  des_str="var_TestRootPath="$binary_dir";pdk_commit_id="$pdk_tag";assign_to_tees="$tda2ex_tee" on "$test_pc_ip

  # Execution for more platform can be added and needs to run parallely
  Java.exe -jar opentest_cli.jar --buildid $build_name --report MATRIX_BUILD --testplan EP_PDK:PDK_Test_Plan_TDA2ex --hw platform=tda2ex --sw "$des_str"
fi


end_time=`date +%s`
let deltatime=end_time-start_time
let hours=deltatime/3600
let minutes=deltatime/60
let minutes=minutes%60
let seconds=deltatime%60
printf "Execution Time: %d:%02d:%02d\n" $hours $minutes $seconds

exit
