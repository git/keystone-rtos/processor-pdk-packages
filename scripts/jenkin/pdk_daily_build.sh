#!/bin/bash
#   ============================================================================
#   @file   pdk_daily_build.sh
#
#   @desc   Script to do a daily build of the PDK package
#
#   ============================================================================
#   Revision History
#   11-Sep-2015 Sivaraj         Initial draft.
#   24-Dec-2015 Sivaraj         Added package build
#   01-Jun-2016 Sivaraj         Added build support from within pdk cloned repos
#   16-Nov-2016 Sivaraj         Added named argument and clone tag inputs.
#   07-Mar-2017 Sivaraj         Added c++ build support
#   08-Nov-2017 Sivaraj         Changes for UDMA LLD repo
#   04-Jan-2018 Sivaraj         Changes for Sciclient and VHWA repos
#   03-Apr-2018 Sivaraj         Changes for IPC repos
#   21-May-2018 Sivaraj         Changes for all Maxwell repos - EMAC, PRUSS, USB, FATFS
#
#   ============================================================================

#   ============================================================================
#   CAUTION: DO NOT INVOKE THE SCRIPTS AS 'gitadmin'.
#            IT WILL ADD OBJECT FILES IN BASE REPOSITORY; WHICH SHOULD IDEALLY
#            BE CLEAN.
#   ============================================================
# Command: pdk_daily_build.sh --working_dir=$working_dir --clone_enable=$clone_enable --tools_path=$tools_path --exe_cp_dir=$exe_cp_dir --gmake_path=$gmake_path
# Example (Linux): /datalocal/daily_build/scripts/pdk_daily_build.sh --working_dir=/datalocal/daily_build/pdk_build --clone_enable="yes" --exe_cp_dir=/datalocal/daily_build/pdk_build/executables
# Example (Windows):                              pdk_daily_build.sh --working_dir="/d/build/pdk_build"             --clone_enable="yes" --exe_cp_dir="/d/build/pdk_build/executables"
# Example (to build from already setup pdk location use): pdk_daily_build.sh

start_time=`date +%s`
: ${OS:="linux"}

#For MSHIELD-DK build
export PATH=$PATH:/bin:/data/datalocal1_videoapps01/ti_components/os_tools/linux/linaro/gcc-linaro-arm-linux-gnueabihf-4.7-2013.03-20130313_linux/bin
date=`date '+%b_%d_%Y_%I_%M_%p'`
date_print=`date '+%d-%b-%Y %I:%M:%S %p'`
month=`date '+%b_%Y'`
year=`date '+%Y'`
git_path="/usr/bin"

#Get user input
while [ $# -gt 0 ]; do
  case "$1" in
    --working_dir=*)
      working_dir="${1#*=}"
      ;;
    --clone_enable=*)
      clone_enable="${1#*=}"
      ;;
    --clone_config=*)
      clone_config="${1#*=}"
      ;;
    --quick_check=*)
      quick_check="${1#*=}"
      ;;
    --board_list=*)
      board_list="${1#*=}"
      ;;
    --doxygen=*)
      doxygen="${1#*=}"
      ;;
    --doxygen_install_path=*)
      doxygen_install_path="${1#*=}"
      ;;
    --tools_path=*)
      tools_path="${1#*=}"
      ;;
    --exe_cp_dir=*)
      exe_cp_dir="${1#*=}"
      ;;
    --gmake_path=*)
      gmake_path="${1#*=}"
      ;;
    --pdk_top_tag=*)
      pdk_top_tag="${1#*=}"
      ;;
    --docs_tag=*)
      docs_tag="${1#*=}"
      ;;
    --csl_tag=*)
      csl_tag="${1#*=}"
      ;;
    --build_tag=*)
      build_tag="${1#*=}"
      ;;
    --board_tag=*)
      board_tag="${1#*=}"
      ;;
    --osal_tag=*)
      osal_tag="${1#*=}"
      ;;
    --sbl_tag=*)
      sbl_tag="${1#*=}"
      ;;
    --i2c_tag=*)
      i2c_tag="${1#*=}"
      ;;
    --uart_tag=*)
      uart_tag="${1#*=}"
      ;;
    --spi_tag=*)
      spi_tag="${1#*=}"
      ;;
    --mcasp_tag=*)
      mcasp_tag="${1#*=}"
      ;;
    --mmcsd_tag=*)
      mmcsd_tag="${1#*=}"
      ;;
    --pcie_tag=*)
      pcie_tag="${1#*=}"
      ;;
    --sa_tag=*)
      sa_tag="${1#*=}"
      ;;
    --pm_tag=*)
      pm_tag="${1#*=}"
      ;;
    --vps_tag=*)
      vps_tag="${1#*=}"
      ;;
    --fvid2_tag=*)
      fvid2_tag="${1#*=}"
      ;;
    --dss_tag=*)
      dss_tag="${1#*=}"
      ;;
    --cal_tag=*)
      cal_tag="${1#*=}"
      ;;
    --csirx_tag=*)
      csirx_tag="${1#*=}"
      ;;
    --ipc_lite_tag=*)
      ipc_lite_tag="${1#*=}"
      ;;
    --fw_l3l4_tag=*)
      fw_l3l4_tag="${1#*=}"
      ;;
    --diag_tag=*)
      diag_tag="${1#*=}"
      ;;
    --security_tag=*)
      security_tag="${1#*=}"
      ;;
    --stw_lld_tag=*)
      stw_lld_tag="${1#*=}"
      ;;
    --bsp_lld_tag=*)
      bsp_lld_tag="${1#*=}"
      ;;
    --udma_tag=*)
      udma_tag="${1#*=}"
      ;;
    --ipc_tag=*)
      ipc_tag="${1#*=}"
      ;;
    --sciclient_tag=*)
      sciclient_tag="${1#*=}"
      ;;
    --vhwa_tag=*)
      vhwa_tag="${1#*=}"
      ;;
    --emac_tag=*)
      emac_tag="${1#*=}"
      ;;
    --pruss_tag=*)
      pruss_tag="${1#*=}"
      ;;
    --gpio_tag=*)
      gpio_tag="${1#*=}"
      ;;
    --usb_tag=*)
      usb_tag="${1#*=}"
      ;;
    --fatfs_tag=*)
      fatfs_tag="${1#*=}"
      ;;
    *)
      printf "Error: Invalid argument $1!!\n"
  esac
  shift
done

#Default value if not provided
: ${clone_enable:="no"}
: ${clone_config:="prsdk_vision"}
: ${quick_check:="false"}
if [ "$quick_check" == "false" ]; then
  : ${board_list:="tda2xx-evm tda2ex-evm tda3xx-evm tda2px-evm tda2xx-evm-radar tda2xx-cascade-radar tda2px-evm-radar tda3xx-evm-radar tda3xx-ar12-booster tda3xx-ar12-alps tda3xx-ar12-rvp tda2ex-eth-srv tda2xx-rvp"}
else
  : ${board_list:="tda2xx-evm tda2ex-evm tda3xx-evm tda2px-evm"}
fi
#: ${catalog_board_list:="evmDRA75x evmDRA72x evmDRA78x evmAM572x idkAM572x idkAM571x"}
: ${catalog_board_list:=""}
: ${doxygen:="false"}
if [ "$working_dir" == "" ]; then
  working_dir=`pwd`
  if [ "$clone_enable" == "yes" ]; then
    #clone in same directory
    pdk_dir=$working_dir/pdk
    log_dir=$working_dir/logs
  else
    #Script called from within PDK git for build
    cd ..
    pdk_dir=`pwd`
    log_dir=$pdk_dir/logs
  fi
else
  pdk_dir=$working_dir/pdk
  log_dir=$working_dir/logs/$year/$month/$date
fi
if [ "$quick_check" == "true" ]; then
  profile_list="debug"
else
  profile_list="debug release"
fi

sdk_install_path=/data/datalocal1_videoapps01

jobs_option="-j"
if [ "$gmake_path" == "" ]; then
  if [ "$OS" == "linux" ]; then
    gmake_cmd=make
  else
    gmake_cmd=gmake
  fi
else
  gmake_cmd=$gmake_path/gmake
fi

echo "PDK Daily Build for $date_print"
echo "Build Logs could be found at $log_dir"
echo ""

#Make required directories
mkdir -p $log_dir
if [ -n "$exe_cp_dir" ]; then
  rm -rf $exe_cp_dir/pdk
  rm -rf $exe_cp_dir/sbl_images
  rm -rf $exe_cp_dir/app_images
  mkdir -p $exe_cp_dir/pdk
  mkdir -p $exe_cp_dir/sbl_images
  mkdir -p $exe_cp_dir/app_images
fi

###################### Clone all required repos ######################
if [ "$clone_enable" == "yes" ]; then
  cd $working_dir

  git archive --remote=ssh://git@bitbucket.itg.ti.com/processor-sdk/processor-pdk-packages.git master scripts/pdk_git.sh | tar -xO > $working_dir/pdk_git.sh
  chmod 777 $working_dir/pdk_git.sh
  $working_dir/pdk_git.sh --clone --clone_config=$clone_config --pdk_top_tag=$pdk_top_tag --docs_tag=$docs_tag --board_tag=$board_tag --sbl_tag=$sbl_tag --build_tag=$build_tag --csl_tag=$csl_tag --osal_tag=$osal_tag --i2c_tag=$i2c_tag --mcasp_tag=$mcasp_tag --mmcsd_tag=$mmcsd_tag --pcie_tag=$pcie_tag --sa_tag=$sa_tag --pm_tag=$pm_tag --ipc_lite_tag=$ipc_lite_tag --fw_l3l4_tag=$fw_l3l4_tag --diag_tag=$diag_tag --security_tag=$security_tag --spi_tag=$spi_tag --uart_tag=$uart_tag --vps_tag=$vps_tag --fvid2_tag=$fvid2_tag --dss_tag=$dss_tag --cal_tag=$cal_tag --csirx_tag=$csirx_tag --stw_lld_tag=$stw_lld_tag --bsp_lld_tag=$bsp_lld_tag --udma_tag=$udma_tag --ipc_tag=$ipc_tag --sciclient_tag=$sciclient_tag --vhwa_tag=$vhwa_tag --emac_tag=$emac_tag --pruss_tag=$pruss_tag --gpio_tag=$gpio_tag --usb_tag=$usb_tag --fatfs_tag=$fatfs_tag
  rm -f $working_dir/pdk_git.sh

  #Change Rules.make
  # Replace the tools path with actual path
  cd $pdk_dir/packages
  if [ "$OS" == "Windows_NT" ]; then
    working_dir_colan=`echo $working_dir | sed -e "s|/||" | sed -e "s|/|:/|"`
    sed -i -e "s|PDK_INSTALL_PATH          ?= \$(SDK_INSTALL_PATH)/ti_components/drivers/pdk_\$(PDK_VERSION)/packages|PDK_INSTALL_PATH          ?= $working_dir_colan/pdk/packages|g"   ti/build/Rules.make
    if [ -n "$tools_path" ]; then
      tools_path_colan=` echo $tools_path  | sed -e "s|/||" | sed -e "s|/|:/|"`
      sed -i "s|TOOLS_INSTALL_PATH ?= \$(SDK_INSTALL_PATH)|TOOLS_INSTALL_PATH ?= $tools_path_colan|g"       ti/build/Rules.make
    fi
  else
    sed -i "s:SDK_INSTALL_PATH ?= \$(abspath \.\./\.\./\.\./\.\./):SDK_INSTALL_PATH ?= $sdk_install_path:g"  ti/build/Rules.make
    sed -i -e "s|PDK_INSTALL_PATH          ?= \$(SDK_INSTALL_PATH)/ti_components/drivers/pdk_\$(PDK_VERSION)/packages|PDK_INSTALL_PATH          ?= $working_dir/pdk/packages|g"         ti/build/Rules.make
    sed -i -e "s|PDK_INSTALL_PATH          ?= \$(SDK_INSTALL_PATH)/pdk\$(PDK_VERSION_STR)/packages|PDK_INSTALL_PATH          ?= $working_dir/pdk/packages|g"                   ti/build/Rules.make
  fi
fi

print_error() {
    if [ "$OS" == "Windows_NT" ]; then
      err_fs=$( wc -c "$2" | cut -f 4 -d ' ')
      log_fs=$( wc -c "$3" | cut -f 4 -d ' ')
    else
      err_fs=$( wc -c "$2" | cut -f 1 -d ' ')
      log_fs=$( wc -c "$3" | cut -f 1 -d ' ')
    fi
    let deltatime=$5-$4
    let hours=deltatime/3600
    let minutes=deltatime/60
    let minutes=minutes%60
    let seconds=deltatime%60

    echo ==============================================================
    printf "$1 Build Log: (Time: %d:%02d:%02d)\n" $hours $minutes $seconds
    if [ "$err_fs" == "0" ] || [ "$err_fs" == "" ]; then
      if [ "$log_fs" == "0" ] || [ "$log_fs" == "" ]; then
        echo "Warning: Looks like $1 was not Build Properly!!!"
      else
        echo "Build Passed!!!"
      fi
    fi
    cat "$2"
}

recur_copy() {
  #echo "Copying $1 to $2"
  find ./ -iname $1  | \
  while read filepath; do
    cp --parent --target-directory=$2 "$filepath"
  done
}

recur_del() {
  #echo "Deleting $1 recursively from $2...."
  find $2 -iname $1  | \
  while read filepath; do
    rm -f "$filepath"
  done
}

###################### PDK build ######################
pdk_build() {
    pdk_build_target=$1
    pdk_board=$2
    pdk_profile=$3
    pdk_print_msg=$4
    pdk_misc=$5

    if [ "$pdk_misc" == "" ]; then
        pdk_build_log="$log_dir""/build_pdk_""$pdk_build_target""_""$pdk_board""_""$pdk_profile"".log"
        pdk_build_err_log="$log_dir""/build_err_pdk_""$pdk_build_target""_""$pdk_board""_""$pdk_profile"".log"
    else
        pdk_build_log="$log_dir""/build_pdk_""$pdk_build_target""_""$pdk_board""_""$pdk_profile""_""$pdk_misc"".log"
        pdk_build_err_log="$log_dir""/build_err_pdk_""$pdk_build_target""_""$pdk_board""_""$pdk_profile""_""$pdk_misc"".log"
    fi

    build_start_time=`date +%s`
    $gmake_cmd -s $jobs_option $pdk_build_target BOARD=$pdk_board BUILD_PROFILE=$pdk_profile $pdk_misc 1>"$pdk_build_log" 2>"$pdk_build_err_log"
    build_end_time=`date +%s`
    print_error "$pdk_print_msg" "$pdk_build_err_log" "$pdk_build_log" $build_start_time $build_end_time
}

cd $pdk_dir/packages/ti/build

# Build PDK
$gmake_cmd -s allclean
if [ "$quick_check" == "false" ]; then
  #Onetime build of MSHIELD-DK required for TDA2xx Secure boot
  $gmake_cmd -C $pdk_dir/packages/ti/boot/sbl_auto/sbl_app -fmakefile_mshield.mk PDK_INSTALL_PATH=$pdk_dir/packages BUILD_HS=yes 2>/dev/null >/dev/null
fi
for profile in $profile_list
do
  for board in $board_list
  do
    pdk_build "allcores" "$board" "$profile" "PDK $board ($profile)"          ""
    if [ "$quick_check" == "false" ]; then
      pdk_build "allcores" "$board" "$profile" "PDK $board HS build ($profile)" "BUILD_HS=yes"
    fi
  done
done

if [ "$quick_check" == "false" ]; then
  cd $pdk_dir/packages/ti/binary
  if [ -n "$exe_cp_dir" ]; then
    #Copy the executables to a separate folder
    recur_copy '*.xa15fg'   "$exe_cp_dir/pdk"
    recur_copy '*.xe66'     "$exe_cp_dir/pdk"
    recur_copy '*.xem4'     "$exe_cp_dir/pdk"
    #Map files
    recur_copy '*.map'      "$exe_cp_dir/pdk"
    #SBL Images
    cd $pdk_dir/packages/ti/binary/sbl
    recur_copy '*.bin'      "$exe_cp_dir/sbl_images"
    recur_copy '*.tiimage'  "$exe_cp_dir/sbl_images"
    recur_copy '*.chimage'  "$exe_cp_dir/sbl_images"
    recur_copy 'MLO'        "$exe_cp_dir/sbl_images"
    recur_copy '*.xloader'  "$exe_cp_dir/sbl_images"
    #Delete unsigned SBL images for HS build
    recur_del  '*unsigned*' "$exe_cp_dir/sbl_images"
    #SBL App Images
    cd $pdk_dir/packages/ti
    recur_copy '*.rprc'     "$exe_cp_dir/app_images"
    recur_copy '*.appimage' "$exe_cp_dir/app_images"
  fi

  # Test various clean options
  cd $pdk_dir/packages/ti/build
  echo ""
  echo "Testing various clean options..."
  for board in $board_list
  do
    $gmake_cmd -s clean BOARD=$board
  done
  $gmake_cmd -s allall_clean
  $gmake_cmd -s profiles_clean
  $gmake_cmd -s allboards_clean
  $gmake_cmd -s allcores_clean
  echo "Testing various clean options completed!!"

  ###################### Baremetal build ######################
  cd $pdk_dir/packages/ti/build
  $gmake_cmd -s allclean
  for board in $board_list
  do
    pdk_build "allcores" "$board" "debug" "PDK $board baremetal (Debug)" "BUILD_OS_TYPE=baremetal"
  done
fi    # Quick build

###################### DRA/AM build ######################
# Test other platforms - do after exe copy to avoid getting these binaries in exe folder
cd $pdk_dir/packages/ti/build
$gmake_cmd -s allclean
# change DSP tool path as older version has issues for DRA/AM builds/components
sed -i "s:C6000_7\.4\.2:ti-cgt-c6000_8\.1\.3:g" Rules.make
for board in $catalog_board_list
do
  pdk_build "allcores" "$board" "release" "PDK $board (Release)" ""
done

###################### C++ build ######################
cd $pdk_dir/packages/ti/build
$gmake_cmd -s allclean
#C++ build not yet supported by RTOS LLD, so remove those directories
rm -rf $pdk_dir/packages/ti/drv/i2c
rm -rf $pdk_dir/packages/ti/drv/spi
rm -rf $pdk_dir/packages/ti/drv/uart
# revert DSP tool path for TDA C++ builds
sed -i "s:ti-cgt-c6000_8\.1\.3:C6000_7\.4\.2:g" Rules.make
for board in $board_list
do
  pdk_build "pdk_libs" "$board" "debug" "PDK Libs $board C++ (Debug)" "CPLUSPLUS_BUILD=yes"
done

if [ "$doxygen" == "true" ]; then
  echo ""
  echo "Starting Doxygen Build..."
  doxy_warn_log="$log_dir""/doxygen.log"
  $working_dir/pdk/scripts/pdk_git.sh --doxygen --working_dir=$working_dir --doxygen_install_path=$doxygen_install_path > "$doxy_warn_log"
  cat "$doxy_warn_log"
  echo "Doxygen Build Completed..."
  echo "Output file @ $working_dir/doxygen_log"
fi

end_time=`date +%s`
let deltatime=end_time-start_time
let hours=deltatime/3600
let minutes=deltatime/60
let minutes=minutes%60
let seconds=deltatime%60
printf "Execution Time: %d:%02d:%02d\n" $hours $minutes $seconds

exit
