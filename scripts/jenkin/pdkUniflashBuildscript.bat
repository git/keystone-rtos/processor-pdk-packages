@REM ***********************************************************************************************************
@REM * FILE PURPOSE: Downloading and building Uniflash Host Application
@REM ***********************************************************************************************************
@REM * FILE NAME: script.bat
@REM *
@REM * DESCRIPTION: 
@REM *  This batch file downloads, installs and builds Uniflash Host Application.
@REM *
@REM *
@REM * USAGE:
@REM *  script.bat [platform] [url]
@REM *  platform - Platform under build. Refer platform_list for supported platforms
@REM *  url      - URL to download PDK package
@REM * 
@REM * Copyright (C) 2019, Texas Instruments, Inc.
@REM **********************************************************************************************************
@echo OFF

@REM List of valid platform input arguments
set platform_list=am335x am437x am57xx k2g am65xx

set validation_err=0
@REM Input paramters that needs to be updated
set JENKINS_DIR=C:\ti\uniflashBuild
set JENKINS_GIT_DIR=%cd%
@REM proxy settings
set http_proxy=http://webproxy.ext.ti.com:80/
set https_proxy=https://webproxy.ext.ti.com:80/

:main
    call :check_arguments %*
    if %validation_err% == 0 (
        call :install_prerequisites
        call :install_processor_sdk
        call :copy_host_binary
    )
exit /B 0

:check_arguments 
echo Validating the Inputs...
@REM Check that platform argument is passed in
    if "%1%" == "" (
    echo Usage: script.bat [platform] [url]
    set validation_err=1
    exit /B 0
    ) else (
        set PLATFORM=%1%
    )

@REM Check for a URL argument
    if "%2%" == "" (
    echo Usage: script.bat [platform] [url]
    set validation_err=1
    exit /B 0
    ) else (
        set URL=%2%
    )

@REM Check that platform is in platform list
    set in_list=false
    for %%p in (%platform_list%) do (
        if "%PLATFORM%" == "%%p" (
            set in_list=true
        )
        
        )
    if  %in_list% == false (
        echo ERROR: %PLATFORM% is not a valid platform name. Exiting...
        set validation_err=1
    )
exit /B 0

:install_prerequisites
if not exist "%JENKINS_DIR%\downloads\cygwin" (
	@REM Installing Cygwin for dependant (wget and tar) utilities
	echo Installing prerequisites...
	mkdir %JENKINS_DIR%\downloads 2>nul
	pushd %JENKINS_DIR%\downloads 2>nul
	echo Installing cygwin...
	pushd %JENKINS_DIR%\downloads
	wget -nc -e http_proxy=http://webproxy.ext.ti.com:80/ http://www.cygwin.com/setup-x86.exe
	mkdir %JENKINS_DIR%\downloads\cygwin 2>nul
	setup-x86.exe --quiet-mode --root %JENKINS_DIR%\downloads\cygwin --site http://cygwin.mirror.constant.com --wait --packages tar
	@REM Installing MingW Compiler
	pushd %JENKINS_DIR%\downloads
) else (
	echo "--------------------"
	echo "Cygwin already exist"
	echo "--------------------"
)

set PATH=%PATH%;%JENKINS_DIR%\downloads\cygwin\bin

if not exist "%JENKINS_DIR%\downloads\msys64" (
	echo Installing MingW...
	pushd %JENKINS_DIR%\downloads
	wget -nc -e http_proxy=http://webproxy.ext.ti.com:80/ http://repo.msys2.org/distrib/x86_64/msys2-base-x86_64-20180531.tar.xz
	tar xvf msys2-base-x86_64-20180531.tar.xz
	mingw64.exe
	echo Initialisation in progress...
	@REM Wait until the initialization is complete
	SLEEP 200
	echo y | pacman -Syy mingw-w64-x86_64-gcc
) else (
	echo "-------------------"
	echo "MingW already exist"
	echo "-------------------"
)

set PATH=%PATH%;%JENKINS_DIR%\downloads\msys64\usr\bin;%JENKINS_DIR%\downloads\msys64;%JENKINS_DIR%\downloads\msys64\mingw64\bin

exit /B 0

:install_processor_sdk
@REM Create a directory for the platform
rmdir /q /s C:\ti\processor-sdk-rtos-%PLATFORM% 2>nul
mkdir C:\ti\processor-sdk-rtos-%PLATFORM%
set PDK_PATH=C:\ti\processor-sdk-rtos-%PLATFORM%
pushd %PDK_PATH%
@REM Download the PDK package
echo Downloading PDK package...
wget --no-proxy -nc %URL%
@set downloadPath=%URL%
@for %%a in ("%downloadPath%\.") do set "fileName=%%~nxa"
@REM Install the package
@echo Installing PDK package %fileName%...
%fileName% --mode unattended --prefix %PDK_PATH%

@REM Enter the PDK package folder
pushd %PDK_PATH%\pdk*%PLATFORM%*\packages

@REM Set the Environment variable
call pdksetupenv.bat

@REM Enter the Uniflash folder and build the host application
pushd %PDK_PATH%\pdk*%PLATFORM%*\packages\ti\board\utils\uniflash\host
echo Building Uniflash Host Application...
gmake clean
gmake all
exit /B 0

:copy_host_binary
@REM Create Jenkins directories
echo Copying Output file...
rmdir /s /q artifacts 2>NUL
mkdir %JENKINS_GIT_DIR%\artifacts\output 2>nul
@REM Copy the binaries to the artifacts\output
pushd %PDK_PATH%\pdk*%PLATFORM%*\packages\ti\board\utils\uniflash\host\bin
copy *.exe %JENKINS_GIT_DIR%\artifacts\output
echo Creating Build Targets file...
pushd %PDK_PATH%\pdk*%PLATFORM%*\packages\ti\board\utils\uniflash\host\bin
if exist *.exe (
    echo ProcessorSDKSerialFlash.exe:PASSED >> %JENKINS_GIT_DIR%\artifacts\output\build_targets
    )

@REM Create output tarball
echo Creating Output tar file...
pushd %JENKINS_GIT_DIR%
echo "JENKINS_GIT_DIR: %JENKINS_GIT_DIR%"
set PATH=C:\Program Files (x86)\Git\bin;%PATH%
echo|set /p=processor-pdk-packages: > artifacts/repo-revs.txt
git rev-parse HEAD > temp.txt
set /p COMMIT_ID=<temp.txt
del temp.txt
echo|set /p=%COMMIT_ID%: >> artifacts/repo-revs.txt
git log -1 --pretty=%%B|head -1 > temp.txt
set /p COMMIT_COMMENT=<temp.txt
del temp.txt
echo %COMMIT_COMMENT% >> artifacts/repo-revs.txt
del /Q c:%HOMEPATH%\artifacts.tgz c:\artifacts 2>NUL
tar -cvzf artifacts.tgz ./artifacts
cp artifacts.tgz c:%HOMEPATH%\artifacts.tgz
exit /B 0