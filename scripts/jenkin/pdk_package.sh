#!/bin/bash
#   ============================================================================
#   @file   pdk_package.sh
#
#   @desc   Script to do PDK packaging
#
#   ============================================================================
#   Revision History
#   31-May-2017 Sivaraj         Initial draft.
#   04-Jan-2017 Sivaraj         Changes for Sciclient and VHWA repos
#   04-Jul-2018 Sivaraj         Updated for j6_master
#
#   ============================================================================
# Windows Example: pdk_package.sh <List of PDK Tags of format --<repo>_tag="" --pdk_version_with_dot="01.00.00.02" --board_list="tda2xx-evm tda2ex-evm tda3xx-evm" --package_dir=/d/build/pdk_release_build/package
# Linux Example: ./pdk_package.sh <List of PDK Tags of format --<repo>_tag="" --pdk_version_with_dot="01.00.00.02" --board_list="tda2xx-evm tda2ex-evm tda3xx-evm" --package_dir=/data/datalocal1_videoapps01/user/pdp_jenkin_build/pdk_release_build/package

start_time=`date +%s`
: ${OS:="linux"}

git_path="/usr/bin"

#Get user input
while [ $# -gt 0 ]; do
  case "$1" in
    --pdk_version_with_dot=*)
      pdk_version_with_dot="${1#*=}"
      ;;
    --board_list=*)
      board_list="${1#*=}"
      ;;
    --package_dir=*)
      package_dir="${1#*=}"
      ;;
    --secure_package_dir=*)
      secure_package_dir="${1#*=}"
      ;;
    --gmake_path=*)
      gmake_path="${1#*=}"
      ;;
    --pdk_top_tag=*)
      pdk_top_tag="${1#*=}"
      ;;
    --docs_tag=*)
      docs_tag="${1#*=}"
      ;;
    --csl_tag=*)
      csl_tag="${1#*=}"
      ;;
    --build_tag=*)
      build_tag="${1#*=}"
      ;;
    --board_tag=*)
      board_tag="${1#*=}"
      ;;
    --osal_tag=*)
      osal_tag="${1#*=}"
      ;;
    --sbl_tag=*)
      sbl_tag="${1#*=}"
      ;;
    --i2c_tag=*)
      i2c_tag="${1#*=}"
      ;;
    --uart_tag=*)
      uart_tag="${1#*=}"
      ;;
    --spi_tag=*)
      spi_tag="${1#*=}"
      ;;
    --mcasp_tag=*)
      mcasp_tag="${1#*=}"
      ;;
    --mmcsd_tag=*)
      mmcsd_tag="${1#*=}"
      ;;
    --pcie_tag=*)
      pcie_tag="${1#*=}"
      ;;
    --pm_tag=*)
      pm_tag="${1#*=}"
      ;;
    --vps_tag=*)
      vps_tag="${1#*=}"
      ;;
    --fvid2_tag=*)
      fvid2_tag="${1#*=}"
      ;;
    --dss_tag=*)
      dss_tag="${1#*=}"
      ;;
    --cal_tag=*)
      cal_tag="${1#*=}"
      ;;
    --ipc_lite_tag=*)
      ipc_lite_tag="${1#*=}"
      ;;
    --fw_l3l4_tag=*)
      fw_l3l4_tag="${1#*=}"
      ;;
    --diag_tag=*)
      diag_tag="${1#*=}"
      ;;
    --security_tag=*)
      security_tag="${1#*=}"
      ;;
    --stw_lld_tag=*)
      stw_lld_tag="${1#*=}"
      ;;
    --bsp_lld_tag=*)
      bsp_lld_tag="${1#*=}"
      ;;
    --udma_tag=*)
      udma_tag="${1#*=}"
      ;;
    --sciclient_tag=*)
      sciclient_tag="${1#*=}"
      ;;
    --vhwa_tag=*)
      vhwa_tag="${1#*=}"
      ;;
    *)
      printf "Error: Invalid argument $1!!\n"
  esac
  shift
done

#Default value if not provided
: ${pdk_version_with_dot:="03.01.00.00"}
: ${board_list:="tda2xx-evm tda2ex-evm tda3xx-evm tda2px-evm"}
if [ "$package_dir" == "" ]; then
  package_dir=`pwd`/package
fi
: ${pdk_top_tag:="j6_master"}
clone_dir=$package_dir/clone

pdk_version_with_underscore=`echo $pdk_version_with_dot | sed -e "s|\.|_|g"`
clone_dir_colan=`echo $clone_dir | sed -e "s|/||" | sed -e "s|/|:/|"`
if [ "$gmake_path" == "" ]; then
  if [ "$OS" == "Windows_NT" ]; then
    gmake_cmd=gmake
  else
    gmake_cmd=make
  fi
else
  if [ "$OS" == "Windows_NT" ]; then
    gmake_cmd=$gmake_path/gmake
  else
    gmake_cmd=$gmake_path/make
  fi
fi
doxygen_install_path=/data/datalocal1_videoapps01/user/nightly/doxygen-1.8.9

#######################################################
# Remove the packages and
# repositories created during last build and create new ones
#######################################################
rm -rf $package_dir
rm -rf $clone_dir
mkdir -p $package_dir
mkdir -p $clone_dir
mkdir -p $clone_dir/logs

###################### Clone all required repos ######################
cd $clone_dir
git archive --remote=ssh://git@bitbucket.itg.ti.com/processor-sdk/processor-pdk-packages.git $pdk_top_tag scripts/pdk_git.sh | tar -xO > $clone_dir/pdk_git.sh
chmod 777 $clone_dir/pdk_git.sh
$clone_dir/pdk_git.sh --clone --clone_config="prsdk_vision" --pdk_top_tag=$pdk_top_tag --docs_tag=$docs_tag --board_tag=$board_tag --sbl_tag=$sbl_tag --build_tag=$build_tag --csl_tag=$csl_tag --osal_tag=$osal_tag --i2c_tag=$i2c_tag --mcasp_tag=$mcasp_tag --mmcsd_tag=$mmcsd_tag --pcie_tag=$pcie_tag --pm_tag=$pm_tag --ipc_lite_tag=$ipc_lite_tag --fw_l3l4_tag=$fw_l3l4_tag --diag_tag=$diag_tag --security_tag=$security_tag --spi_tag=$spi_tag --uart_tag=$uart_tag --vps_tag=$vps_tag --fvid2_tag=$fvid2_tag --dss_tag=$dss_tag --cal_tag=$cal_tag --stw_lld_tag=$stw_lld_tag --bsp_lld_tag=$bsp_lld_tag --udma_tag=$udma_tag --sciclient_tag=$sciclient_tag --vhwa_tag=$vhwa_tag
rm -f $clone_dir/pdk_git.sh

###################### Package build ######################
#Change Rules.make
# Replace the tools path with actual path
cd $clone_dir/pdk/packages/ti
if [ "$OS" == "Windows_NT" ]; then
  sed -i -e "s|PDK_INSTALL_PATH          ?= \$(SDK_INSTALL_PATH)/ti_components/drivers/pdk_\$(PDK_VERSION)/packages|PDK_INSTALL_PATH          ?= $clone_dir_colan/pdk/packages|g"   build/Rules.make
else
  sdk_install_path=/data/datalocal1_videoapps01
  sed -i "s:SDK_INSTALL_PATH ?= \$(HOME)/ti:SDK_INSTALL_PATH ?= $sdk_install_path:g"  build/Rules.make
  sed -i -e "s|PDK_INSTALL_PATH          ?= \$(SDK_INSTALL_PATH)/ti_components/drivers/pdk_\$(PDK_VERSION)/packages|PDK_INSTALL_PATH          ?= $clone_dir/pdk/packages|g"         build/Rules.make
fi
sed -i -e "s|..\...\...\...|$pdk_version_with_dot|g"                                        build/makefile

#Package
cd $clone_dir/pdk/packages/ti/build
for board in $board_list
do
  echo "Packaging files for $board ..."
  $gmake_cmd -s package PACKAGE_SELECT=all BOARD=$board 1>>$clone_dir/logs/package_build.log
  echo "Packaging files for $board completed!!"
done
#Copy non-source items not packaged through make package
cp -rf $clone_dir/pdk/docs $clone_dir/pdk/packages/ti/binary/package/all/pdk_
rm -rf $clone_dir/pdk/packages/ti/binary/package/all/pdk_/docs/.git
#remove internal folder
rm -rf $clone_dir/pdk/packages/ti/binary/package/all/pdk_/docs/internal

#Run doxygen and generate API guide
#Temporarily copy the script folder so that we can run doxygen on the packaged folder
cp -rf $clone_dir/pdk/scripts $clone_dir/pdk/packages/ti/binary/package/all/pdk_
cd $clone_dir/pdk/packages/ti/binary/package/all/pdk_/scripts
./pdk_git.sh --doxygen --doxygen_install_path=$doxygen_install_path --doxygen_silent_mode
#Replace daily build path with relative path
sed -i -e "s|file://///dbdsamba03/data/datalocal1_videoapps01/user/pdp_jenkin_build/pdk_daily_build/pdk|\.\.|g" $clone_dir/pdk/packages/ti/binary/package/all/pdk_/docs/API_Documentation.html

#Remove script folder
rm -rf $clone_dir/pdk/packages/ti/binary/package/all/pdk_/scripts

#Revert Rules.make changes
cd $clone_dir/pdk/packages/ti/build
$git_path/git checkout Rules.make
cp -f $clone_dir/pdk/packages/ti/build/Rules.make $clone_dir/pdk/packages/ti/binary/package/all/pdk_/packages/ti/build

# Move security package to a separate folder if asked for
if [ "$secure_package_dir" != "" ]; then
  cd $clone_dir/pdk/packages/ti/binary/package/all
  mkdir -p $secure_package_dir/
  cp -rf --parent pdk_/packages/ti/boot/sbl_auto/security $secure_package_dir/
  mv $secure_package_dir/pdk_ $secure_package_dir/pdk_$pdk_version_with_underscore
fi
#Delete what is cloned
rm -rf $clone_dir/pdk/packages/ti/binary/package/all/pdk_/packages/ti/boot/sbl_auto/security

#Copy the packaged files to package folder
cp -rf $clone_dir/pdk/packages/ti/binary/package/all/pdk_               $package_dir/
sed -i -e "s|PDK_VERSION=.._.._.._..|PDK_VERSION=$pdk_version_with_underscore|g"        $package_dir/pdk_/packages/ti/build/Rules.make
mv $package_dir/pdk_ $package_dir/pdk_$pdk_version_with_underscore

#Clean up
rm -rf $clone_dir

###################### End of build ######################

end_time=`date +%s`
let deltatime=end_time-start_time
let hours=deltatime/3600
let minutes=deltatime/60
let minutes=minutes%60
let seconds=deltatime%60
printf "Execution Time: %d:%02d:%02d\n" $hours $minutes $seconds

exit
