#!/bin/bash
#   ============================================================================
#   @file   pdk_kw_build.sh
#
#   @desc   Script to do a KW build of the PDK package
#
#   ============================================================================
#   Revision History
#   16-Nov-2016 Sivaraj         Initial draft.
#   14-Dec-2016 Sivaraj         Added K3 support
#   08-Nov-2017 Sivaraj         Changes for UDMA LLD repo
#   04-Jan-2018 Sivaraj         Changes for Sciclient and VHWA repos
#   03-Apr-2018 Sivaraj         Changes for IPC repos
#   21-May-2018 Sivaraj         Changes for all Maxwell repos - EMAC, PRUSS, USB, FATFS
#   17-Feb-2019 Sivaraj         Removed K3 support as this is done in k3 daily build script now
#
#   ============================================================================

#   ============================================================================
#   CAUTION: DO NOT INVOKE THE SCRIPTS AS 'gitadmin'.
#            IT WILL ADD OBJECT FILES IN BASE REPOSITORY; WHICH SHOULD IDEALLY
#            BE CLEAN.
#   ============================================================
# Command: pdk_kw_build.sh --build_label=$build_label --release_build="false" --his_metrics_build="true" --working_dir=$working_dir
# Example (Linux): pdk_kw_build.sh --working_dir=/data/adasuser_bangvideoapps02/pdk_jenkin_build/pdk_kw_build

start_time=`date +%s`
: ${OS:="linux"}

export PATH=$PATH:/bin
date=`date '+%b_%d_%Y_%I_%M_%p'`
date_print=`date '+%d-%b-%Y %I:%M:%S %p'`
month=`date '+%b_%Y'`
year=`date '+%Y'`

git_path="/usr/bin"
cgtools_path=/datalocal/ti_components/cg_tools/linux
bios_path=/datalocal/ti_components/os_tools
xdc_path=/datalocal/ti_components/os_tools/linux
linaro_path=/datalocal/ti_components/cg_tools/linux/gcc
edma_path=/datalocal/ti_components/drivers
radarlink_path=/datalocal/ti_components/radar

#Get user input
while [ $# -gt 0 ]; do
  case "$1" in
    --build_label=*)
      build_label="${1#*=}"
      ;;
    --release_build=*)
      release_build="${1#*=}"
      ;;
    --his_metrics_build=*)
      his_metrics_build="${1#*=}"
      ;;
    --working_dir=*)
      working_dir="${1#*=}"
      ;;
    --k3_build=*)
      k3_build="${1#*=}"
      ;;
    --pdk_top_tag=*)
      pdk_top_tag="${1#*=}"
      ;;
    --docs_tag=*)
      docs_tag="${1#*=}"
      ;;
    --csl_tag=*)
      csl_tag="${1#*=}"
      ;;
    --build_tag=*)
      build_tag="${1#*=}"
      ;;
    --board_tag=*)
      board_tag="${1#*=}"
      ;;
    --osal_tag=*)
      osal_tag="${1#*=}"
      ;;
    --sbl_tag=*)
      sbl_tag="${1#*=}"
      ;;
    --i2c_tag=*)
      i2c_tag="${1#*=}"
      ;;
    --uart_tag=*)
      uart_tag="${1#*=}"
      ;;
    --spi_tag=*)
      spi_tag="${1#*=}"
      ;;
    --mcasp_tag=*)
      mcasp_tag="${1#*=}"
      ;;
    --mmcsd_tag=*)
      mmcsd_tag="${1#*=}"
      ;;
    --pcie_tag=*)
      pcie_tag="${1#*=}"
      ;;
    --pm_tag=*)
      pm_tag="${1#*=}"
      ;;
    --vps_tag=*)
      vps_tag="${1#*=}"
      ;;
    --fvid2_tag=*)
      fvid2_tag="${1#*=}"
      ;;
    --dss_tag=*)
      dss_tag="${1#*=}"
      ;;
    --cal_tag=*)
      cal_tag="${1#*=}"
      ;;
    --csirx_tag=*)
      csirx_tag="${1#*=}"
      ;;
    --ipc_lite_tag=*)
      ipc_lite_tag="${1#*=}"
      ;;
    --fw_l3l4_tag=*)
      fw_l3l4_tag="${1#*=}"
      ;;
    --diag_tag=*)
      diag_tag="${1#*=}"
      ;;
    --security_tag=*)
      security_tag="${1#*=}"
      ;;
    --stw_lld_tag=*)
      stw_lld_tag="${1#*=}"
      ;;
    --bsp_lld_tag=*)
      bsp_lld_tag="${1#*=}"
      ;;
    --udma_tag=*)
      udma_tag="${1#*=}"
      ;;
    --ipc_tag=*)
      ipc_tag="${1#*=}"
      ;;
    --sciclient_tag=*)
      sciclient_tag="${1#*=}"
      ;;
    --vhwa_tag=*)
      vhwa_tag="${1#*=}"
      ;;
    --emac_tag=*)
      emac_tag="${1#*=}"
      ;;
    --pruss_tag=*)
      pruss_tag="${1#*=}"
      ;;
    --gpio_tag=*)
      gpio_tag="${1#*=}"
      ;;
    --usb_tag=*)
      usb_tag="${1#*=}"
      ;;
    --fatfs_tag=*)
      fatfs_tag="${1#*=}"
      ;;
    *)
      printf "Error: Invalid argument $1!!\n"
  esac
  shift
done

#Default value if not provided
: ${build_label:="PDK_KW_BUILD"}
: ${build_tag:="master"}
: ${csl_tag:="master"}
if [ "$working_dir" == "" ]; then
  working_dir=`pwd`
  #clone in same directory
  pdk_dir=$working_dir/pdk
  log_dir=$working_dir/logs
else
  pdk_dir=$working_dir/pdk
  log_dir=$working_dir/logs/$year/$month/$date
fi

if [ "$OS" == "linux" ]; then
  gmake_cmd=make
else
  gmake_cmd=gmake
fi
kw_path="/datalocal/ti_components/klocwork/bin"
kw_url="https://klocwork.india.ti.com:8095"
kw_user="a0393606"
# To get Klocwork ltoken use: cat ~/.klocwork/ltoken and paste only token from right KW/Port entry
kw_ltoken="fca073757fb3b8307568fb1a3de8faf50cc85d38b454e471014242f7bc4585c7"

kw_label=$build_label"_"$date
kwauth_cmd=$kw_path"/kwauth"
kwinject_cmd=$kw_path"/kwinject"
kwbuild_cmd=$kw_path"/kwbuildproject"
kwadmin_cmd=$kw_path"/kwadmin"
kwreport_cmd=$kw_path"/kwinspectreport"

#Below login needs to be enabled one time for the first time for a new machine
#$kwauth_cmd --url $kw_url/

echo "PDK Klocwork Build!!"
kw_project="EP_PDK"
clone_cfg="prsdk_vision"
echo "Date              : "$date_print
echo "Klocwork Label    : "$kw_label
echo "Build Log @       : "$log_dir
echo ""

#Make required directories
mkdir -p $log_dir

###################### Clone all required repos ######################
cd $working_dir
$git_path/git archive --remote=ssh://git@bitbucket.itg.ti.com/processor-sdk/processor-pdk-packages.git master scripts/pdk_git.sh | tar -xO > $working_dir/pdk_git.sh
chmod 777 $working_dir/pdk_git.sh
$working_dir/pdk_git.sh --clone --clone_config=$clone_cfg --pdk_top_tag=$pdk_top_tag --docs_tag=$docs_tag --board_tag=$board_tag --sbl_tag=$sbl_tag --build_tag=$build_tag --csl_tag=$csl_tag --osal_tag=$osal_tag --i2c_tag=$i2c_tag --mcasp_tag=$mcasp_tag --mmcsd_tag=$mmcsd_tag --pcie_tag=$pcie_tag --pm_tag=$pm_tag --ipc_lite_tag=$ipc_lite_tag --fw_l3l4_tag=$fw_l3l4_tag --diag_tag=$diag_tag --security_tag=$security_tag --spi_tag=$spi_tag --uart_tag=$uart_tag --vps_tag=$vps_tag --fvid2_tag=$fvid2_tag --dss_tag=$dss_tag --cal_tag=$cal_tag --csirx_tag=$csirx_tag --stw_lld_tag=$stw_lld_tag --bsp_lld_tag=$bsp_lld_tag --udma_tag=$udma_tag --ipc_tag=$ipc_tag --sciclient_tag=$sciclient_tag --vhwa_tag=$vhwa_tag --emac_tag=$emac_tag --pruss_tag=$pruss_tag --gpio_tag=$gpio_tag --usb_tag=$usb_tag --fatfs_tag=$fatfs_tag
rm -f $working_dir/pdk_git.sh

sdk_install_path=/datalocal

#Change Rules.make
#Replace the tools path with actual path
cd $pdk_dir/packages
if [ "$OS" == "Windows_NT" ]; then
  working_dir_colan=`echo $working_dir | sed -e "s|/||" | sed -e "s|/|:/|"`
  sed -i -e "s|PDK_INSTALL_PATH          ?= \$(SDK_INSTALL_PATH)/ti_components/drivers/pdk_\$(PDK_VERSION)/packages|PDK_INSTALL_PATH          ?= $working_dir_colan/pdk/packages|g"     ti/build/Rules.make
else
sed -i "s:SDK_INSTALL_PATH ?= \$(HOME)/ti:SDK_INSTALL_PATH ?= $sdk_install_path:g"  ti/build/Rules.make
  sed -i -e "s|PDK_INSTALL_PATH          ?= \$(SDK_INSTALL_PATH)/ti_components/drivers/pdk_\$(PDK_VERSION)/packages|PDK_INSTALL_PATH          ?= $pdk_dir/packages|g"                   ti/build/Rules.make
  sed -i -e "s|PDK_INSTALL_PATH          ?= \$(SDK_INSTALL_PATH)/pdk\$(PDK_VERSION_STR)/packages|PDK_INSTALL_PATH          ?= $pdk_dir/packages|g"                             ti/build/Rules.make
fi

print_error() {
    if [ "$OS" == "Windows_NT" ]; then
      err_fs=$( wc -c "$2" | cut -f 4 -d ' ')
      log_fs=$( wc -c "$3" | cut -f 4 -d ' ')
    else
      err_fs=$( wc -c "$2" | cut -f 1 -d ' ')
      log_fs=$( wc -c "$3" | cut -f 1 -d ' ')
    fi
    let deltatime=$5-$4
    let hours=deltatime/3600
    let minutes=deltatime/60
    let minutes=minutes%60
    let seconds=deltatime%60

    echo ==============================================================
    printf "$1 Build Log: (Time: %d:%02d:%02d)\n" $hours $minutes $seconds
    if [ "$err_fs" == "0" ] || [ "$err_fs" == "" ]; then
      if [ "$log_fs" == "0" ] || [ "$log_fs" == "" ]; then
        echo "Warning: Looks like $1 was not Build Properly!!!"
      else
        echo "Build Passed!!!"
      fi
    fi
    cat "$2"
}

###################### PDK build ######################

pdk_build() {
    pdk_build_target=$1
    pdk_board=$2
    pdk_profile=$3
    pdk_print_msg=$4
    pdk_misc=$5
    pdk_build_kw_out=$6

    if [ "$pdk_misc" == "" ]; then
        pdk_build_log="$log_dir""/build_pdk_""$pdk_build_target""_""$pdk_board""_""$pdk_profile"".log"
        pdk_build_err_log="$log_dir""/build_err_pdk_""$pdk_build_target""_""$pdk_board""_""$pdk_profile"".log"
    else
        pdk_build_log="$log_dir""/build_pdk_""$pdk_build_target""_""$pdk_board""_""$pdk_profile""_""$pdk_misc"".log"
        pdk_build_err_log="$log_dir""/build_err_pdk_""$pdk_build_target""_""$pdk_board""_""$pdk_profile""_""$pdk_misc"".log"
    fi

    build_start_time=`date +%s`
    $kwinject_cmd -o $pdk_build_kw_out $gmake_cmd -j -s $pdk_build_target BOARD=$pdk_board BUILD_PROFILE=$pdk_profile $pdk_misc 1>"$pdk_build_log" 2>"$pdk_build_err_log"
    build_end_time=`date +%s`
    print_error "$pdk_print_msg" "$pdk_build_err_log" "$pdk_build_log" $build_start_time $build_end_time
}

# Build project for Klocwork inject
cd $pdk_dir/packages/ti/build
mkdir -p $log_dir/kw_tables
kw_pdk_tda2xx_out="$log_dir""/kw_pdk_tda2xx.out"
kw_pdk_tda2ex_out="$log_dir""/kw_pdk_tda2ex.out"
kw_pdk_tda3xx_out="$log_dir""/kw_pdk_tda3xx.out"
kw_pdk_tda2px_out="$log_dir""/kw_pdk_tda2px.out"
kw_pdk_tda3xx_radar_out="$log_dir""/kw_pdk_tda3xx_radar.out"
kw_pdk_tda2xx_radar_out="$log_dir""/kw_pdk_tda2xx_radar.out"
pdk_build "allcores" "tda2xx-evm" "debug"   "PDK TDA2xx KW (Debug)" "" $kw_pdk_tda2xx_out
pdk_build "allcores" "tda2ex-evm" "debug"   "PDK TDA2Ex KW (Debug)" "" $kw_pdk_tda2ex_out
pdk_build "allcores" "tda3xx-evm" "debug"   "PDK TDA3xx KW (Debug)" "" $kw_pdk_tda3xx_out
pdk_build "allcores" "tda2px-evm" "debug"   "PDK TDA2Px KW (Debug)" "" $kw_pdk_tda2px_out
pdk_build "allcores" "tda3xx-ar12-booster" "debug"   "PDK TDA3xx Radar KW (Debug)" "" $kw_pdk_tda3xx_radar_out
pdk_build "allcores" "tda2xx-cascade-radar" "debug"   "PDK TDA2xx Radar KW (Debug)" "" $kw_pdk_tda2xx_radar_out
#Klocwork build
$kwbuild_cmd -o $log_dir/kw_tables --ssl --url $kw_url/$kw_project -j auto -f $kw_pdk_tda2xx_out $kw_pdk_tda2ex_out $kw_pdk_tda3xx_out $kw_pdk_tda2px_out $kw_pdk_tda3xx_radar_out $kw_pdk_tda2xx_radar_out 1>>$log_dir/kw_output.log

#Klocwork inject
$kwadmin_cmd --ssl --url $kw_url load --force $kw_project $log_dir/kw_tables --name $kw_label 1>>$log_dir/kw_output.log

# Make the build permanent for release build
cd $working_dir
if [ "$release_build" == "true" ]; then
  curl_action="action=update_build&user=$kw_user&project=$kw_project&name=$kw_label&keepit=true&ltoken=$kw_ltoken"
  curl -sS --data "$curl_action" $kw_url/review/api
fi

# HIS metrics build
cd $working_dir
if [ "$his_metrics_build" == "true" ]; then
  echo "Generating HIS Metrics..."
  curl -sS --noproxy "*" "http://www-open.india.ti.com/~pspcm/data_pspdocs/PDP/MCAL/Klocwork/generate_kw_metrics.pl" > generate_kw_metrics.pl
  curl -sS --noproxy "*" "http://www-open.india.ti.com/~pspcm/data_pspdocs/PDP/MCAL/Klocwork/input.txt" > input.txt
  chmod 777 generate_kw_metrics.pl
  ./generate_kw_metrics.pl $kw_user $kw_ltoken $kw_project
  rm -f generate_kw_metrics.pl input.txt
  echo "HIS Metrics Generation Completed!!"
  echo "Output file @ $working_dir/output/output.log!!"
fi

end_time=`date +%s`
let deltatime=end_time-start_time
let hours=deltatime/3600
let minutes=deltatime/60
let minutes=minutes%60
let seconds=deltatime%60
printf "Execution Time: %d:%02d:%02d\n" $hours $minutes $seconds

exit
