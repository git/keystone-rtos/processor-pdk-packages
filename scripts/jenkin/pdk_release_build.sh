#!/bin/bash
#   ============================================================================
#   @file   pdk_release_build.sh
#
#   @desc   Script to do a release build of the PDK package
#
#   ============================================================================
#   Revision History
#   6-May-2016 Sivaraj          Initial draft.
#   16-Nov-2016 Sivaraj         Added named argument and clone tag inputs.
#   31-May-2017 Sivaraj         Split into two scripts - package (used in VSDK) and build
#
#   ============================================================================
# Windows Example: pdk_release_build.sh <List of PDK Tags of format --<repo>_tag="" --pdk_version_with_dot="01.00.00.02" --board_list="tda2xx-evm tda2ex-evm tda3xx-evm tda2px-evm" --board_list_string="TDA2xx, TDA2Ex, TDA3xx and TDA2Px Platforms" --working_dir=/d/build/pdk_build
# Linux Example: ./pdk_release_build.sh <List of PDK Tags of format --<repo>_tag="" --pdk_version_with_dot="01.00.00.02" --board_list="tda2xx-evm tda2ex-evm tda3xx-evm tda2px-evm" --board_list_string="TDA2xx, TDA2Ex, TDA3xx and TDA2Px Platforms" --working_dir=/data/datalocal1_videoapps01/user/pdp_jenkin_build/pdk_release_build

start_time=`date +%s`
: ${OS:="linux"}

#For MSHIELD-DK build
export PATH=$PATH:/bin:/data/datalocal1_videoapps01/ti_components/os_tools/linux/linaro/gcc-linaro-arm-linux-gnueabihf-4.7-2013.03-20130313_linux/bin
date_print=`date '+%d-%b-%Y %I:%M:%S %p'`
git_path="/usr/bin"

#Get user input
while [ $# -gt 0 ]; do
  case "$1" in
    --pdk_version_with_dot=*)
      pdk_version_with_dot="${1#*=}"
      ;;
    --board_list=*)
      board_list="${1#*=}"
      ;;
    --board_list_string=*)
      board_list_string="${1#*=}"
      ;;
    --working_dir=*)
      working_dir="${1#*=}"
      ;;
    --gmake_path=*)
      gmake_path="${1#*=}"
      ;;
    --pdk_top_tag=*)
      pdk_top_tag="${1#*=}"
      ;;
    --docs_tag=*)
      docs_tag="${1#*=}"
      ;;
    --csl_tag=*)
      csl_tag="${1#*=}"
      ;;
    --build_tag=*)
      build_tag="${1#*=}"
      ;;
    --board_tag=*)
      board_tag="${1#*=}"
      ;;
    --osal_tag=*)
      osal_tag="${1#*=}"
      ;;
    --sbl_tag=*)
      sbl_tag="${1#*=}"
      ;;
    --i2c_tag=*)
      i2c_tag="${1#*=}"
      ;;
    --uart_tag=*)
      uart_tag="${1#*=}"
      ;;
    --spi_tag=*)
      spi_tag="${1#*=}"
      ;;
    --mcasp_tag=*)
      mcasp_tag="${1#*=}"
      ;;
    --mmcsd_tag=*)
      mmcsd_tag="${1#*=}"
      ;;
    --pcie_tag=*)
      pcie_tag="${1#*=}"
      ;;
    --pm_tag=*)
      pm_tag="${1#*=}"
      ;;
    --vps_tag=*)
      vps_tag="${1#*=}"
      ;;
    --ipc_lite_tag=*)
      ipc_lite_tag="${1#*=}"
      ;;
    --fw_l3l4_tag=*)
      fw_l3l4_tag="${1#*=}"
      ;;
    --diag_tag=*)
      diag_tag="${1#*=}"
      ;;
    --security_tag=*)
      security_tag="${1#*=}"
      ;;
    --stw_lld_tag=*)
      stw_lld_tag="${1#*=}"
      ;;
    --bsp_lld_tag=*)
      bsp_lld_tag="${1#*=}"
      ;;
    *)
      printf "Error: Invalid argument $1!!\n"
  esac
  shift
done

#Default value if not provided
: ${pdk_version_with_dot:="01.07.00.00"}
: ${board_list:="tda2xx-evm tda2ex-evm tda3xx-evm tda2px-evm"}
: ${board_list_string:="TDA2xx, TDA2Ex, TDA3xx and TDA2Px Platforms"}
if [ "$working_dir" == "" ]; then
  working_dir=`pwd`
fi
: ${pdk_top_tag:="j6_master"}

if [ "$OS" == "Windows_NT" ]; then
  installjammer_path="/c/PROGRA~2/InstallJammer"
else
  installjammer_path="/datalocal1/release-package/installjammer_1.2.7"
fi
pdk_version=`echo $pdk_version_with_dot | sed -e "s|\.|_|g"`
working_dir_colan=`echo $working_dir | sed -e "s|/||" | sed -e "s|/|:/|"`
jobs_option="-j"
if [ "$gmake_path" == "" ]; then
  if [ "$OS" == "Windows_NT" ]; then
    gmake_cmd=gmake
  else
    gmake_cmd=make
  fi
else
  if [ "$OS" == "Windows_NT" ]; then
    gmake_cmd=$gmake_path/gmake
  else
    gmake_cmd=$gmake_path/make
  fi
fi

echo "PDK Release Build Version:$pdk_version_with_dot Date:$date_print"
echo ""

#######################################################
# Remove the packages and
# repositories created during last build and create new ones
#######################################################
rm -rf $working_dir/package
rm -rf $working_dir/clone
mkdir -p $working_dir/package
mkdir -p $working_dir/package/build
mkdir -p $working_dir/package/final
mkdir -p $working_dir/package/logs
mkdir -p $working_dir/clone

###################### Package ######################
cd $working_dir/clone
git archive --remote=ssh://git@bitbucket.itg.ti.com/processor-sdk/processor-pdk-packages.git $pdk_top_tag scripts/jenkin/pdk_package.sh | tar -xO > $working_dir/clone/pdk_package.sh
chmod 777 $working_dir/clone/pdk_package.sh
$working_dir/clone/pdk_package.sh --pdk_version_with_dot=$pdk_version_with_dot --board_list="$board_list" --package_dir=$working_dir/clone --secure_package_dir=$working_dir/clone/secure --pdk_top_tag=$pdk_top_tag --docs_tag=$docs_tag --board_tag=$board_tag --sbl_tag=$sbl_tag --build_tag=$build_tag --csl_tag=$csl_tag --osal_tag=$osal_tag --i2c_tag=$i2c_tag --mcasp_tag=$mcasp_tag --mmcsd_tag=$mmcsd_tag --pcie_tag=$pcie_tag --pm_tag=$pm_tag --ipc_lite_tag=$ipc_lite_tag --fw_l3l4_tag=$fw_l3l4_tag --diag_tag=$diag_tag --security_tag=$security_tag --spi_tag=$spi_tag --uart_tag=$uart_tag --vps_tag=$vps_tag --stw_lld_tag=$stw_lld_tag --bsp_lld_tag=$bsp_lld_tag
rm -f $working_dir/clone/pdk_package.sh

#Copy the packaged files to build and final installer folder
cp -rf $working_dir/clone/* $working_dir/package/build/
cp -rf $working_dir/clone/* $working_dir/package/final/

###################### Build after packaging ######################

#Change Rules.make
# Replace the tools path with actual path
cd $working_dir/package/build/pdk_$pdk_version/packages/ti
if [ "$OS" == "Windows_NT" ]; then
  sed -i -e "s|PDK_INSTALL_PATH          ?= \$(SDK_INSTALL_PATH)/ti_components/drivers|PDK_INSTALL_PATH          ?= $working_dir_colan/package/build|g"   build/Rules.make
else
  sdk_install_path=/data/datalocal1_videoapps01
  sed -i "s:SDK_INSTALL_PATH ?= \$(HOME)/ti:SDK_INSTALL_PATH ?= $sdk_install_path:g"  build/Rules.make
  sed -i -e "s|PDK_INSTALL_PATH          ?= \$(SDK_INSTALL_PATH)/ti_components/drivers|PDK_INSTALL_PATH          ?= $working_dir/package/build|g"               build/Rules.make
  sed -i -e "s|PDK_INSTALL_PATH          ?= \$(SDK_INSTALL_PATH)/pdk_\$(PDK_SOC)_\$(PDK_VERSION)|PDK_INSTALL_PATH          ?= $working_dir/package/build/pdk|g" build/Rules.make
fi

# Build PDK
cd $working_dir/package/build/pdk_$pdk_version/packages/ti/build
for board in $board_list
do
  echo "Building for $board ..."
  $gmake_cmd -s $jobs_option allcores     BOARD=$board BUILD_PROFILE=debug     1>>$working_dir/package/logs/build.log
  $gmake_cmd -s $jobs_option allcores     BOARD=$board BUILD_PROFILE=release   1>>$working_dir/package/logs/build.log
  echo "Building for $board completed!!"
done

# Copy SBL images
mkdir -p $working_dir/package/final/pdk_$pdk_version/packages/ti/boot/sbl_auto/prebuilt_binaries/sbl_images
cd $working_dir/package/build/pdk_$pdk_version/packages/ti/binary/sbl
cp -f --parent nor/opp_nom/*/*_release.bin          $working_dir/package/final/pdk_$pdk_version/packages/ti/boot/sbl_auto/prebuilt_binaries/sbl_images
cp -f --parent qspi/opp_nom/*/*_release.tiimage     $working_dir/package/final/pdk_$pdk_version/packages/ti/boot/sbl_auto/prebuilt_binaries/sbl_images
cp -f --parent qspi_sd/opp_nom/*/*_release.tiimage  $working_dir/package/final/pdk_$pdk_version/packages/ti/boot/sbl_auto/prebuilt_binaries/sbl_images
cp -f --parent sd/opp_nom/*/MLO                     $working_dir/package/final/pdk_$pdk_version/packages/ti/boot/sbl_auto/prebuilt_binaries/sbl_images

###################### Install Jammer Packaging ######################

# Copying mpi files from scripts folder
rm -rf $working_dir/package/IJammer
mkdir $working_dir/package/IJammer
git archive --remote=ssh://git@bitbucket.itg.ti.com/processor-sdk/processor-pdk-packages.git $pdk_top_tag scripts/jenkin/pdk_installer.mpi | tar -xO > $working_dir/clone/pdk_installer.mpi
git archive --remote=ssh://git@bitbucket.itg.ti.com/processor-sdk/processor-pdk-packages.git $pdk_top_tag scripts/jenkin/pdk_security_installer.mpi | tar -xO > $working_dir/clone/pdk_security_installer.mpi
cp $working_dir/clone/pdk_installer.mpi             $working_dir/package/IJammer/pdk_installer_source.mpi
cp $working_dir/clone/pdk_security_installer.mpi    $working_dir/package/IJammer

#Copy webgen file
git archive --remote=ssh://git@bitbucket.itg.ti.com/processor-sdk/processor-pdk-packages.git $pdk_top_tag scripts/jenkin/pdk_webgen_makefile | tar -xO > $working_dir/clone/pdk_webgen_makefile
cp $working_dir/clone/pdk_webgen_makefile           $working_dir/package
# Updating webgen with with correct version
sed -i "s|01\...\...\...|$pdk_version_with_dot|g"   $working_dir/package/pdk_webgen_makefile
sed -i "s|01_.._.._..|$pdk_version|g"               $working_dir/package/pdk_webgen_makefile

# Updating the mpi file with correct tag and path info
chmod 777 $working_dir/package/IJammer/pdk_installer_source.mpi
chmod 777 $working_dir/package/IJammer/pdk_security_installer.mpi
sed -i "s|..\...\...\...|$pdk_version_with_dot|g"           $working_dir/package/IJammer/pdk_installer_source.mpi
sed -i "s|..\...\...\...|$pdk_version_with_dot|g"           $working_dir/package/IJammer/pdk_security_installer.mpi
sed -i "s|.._.._.._..|$pdk_version|g"                       $working_dir/package/IJammer/pdk_installer_source.mpi
sed -i "s|.._.._.._..|$pdk_version|g"                       $working_dir/package/IJammer/pdk_security_installer.mpi
if [ "$OS" == "Windows_NT" ]; then
  sed -i "s|D:/pdk_/|$working_dir_colan/package/final/pdk_$pdk_version/|ig"         $working_dir/package/IJammer/pdk_installer_source.mpi
  sed -i "s|D:/pdk/|$working_dir_colan/package/final/secure/pdk_$pdk_version/|ig"   $working_dir/package/IJammer/pdk_security_installer.mpi
else
  sed -i "s|D:/pdk_/|$working_dir/package/final/pdk_$pdk_version/|ig"               $working_dir/package/IJammer/pdk_installer_source.mpi
  sed -i "s|D:/pdk/|$working_dir/package/final/secure/pdk_$pdk_version/|ig"         $working_dir/package/IJammer/pdk_security_installer.mpi
fi
sed -i "s|for TDAxxx platforms|for $board_list_string|ig"   $working_dir/package/IJammer/pdk_installer_source.mpi
dos2unix $working_dir/package/IJammer/pdk_installer_source.mpi
dos2unix $working_dir/package/IJammer/pdk_security_installer.mpi

# Create MPI file for lib package
cp $working_dir/package/IJammer/pdk_installer_source.mpi $working_dir/package/IJammer/pdk_installer_lib.mpi
sed -i '0,/AppName/{s/AppName/AppName\r\{PDK_Lib\}/}'    $working_dir/package/IJammer/pdk_installer_lib.mpi
sed -i '0,/{PDK}/ s///'                                  $working_dir/package/IJammer/pdk_installer_lib.mpi
sed -i -e 's|_setupwin32_|_lib_setupwin32_|'             $working_dir/package/IJammer/pdk_installer_lib.mpi
# Create MPI file for binary package
cp $working_dir/package/IJammer/pdk_installer_source.mpi $working_dir/package/IJammer/pdk_installer_binary.mpi
sed -i '0,/AppName/{s/AppName/AppName\r\{PDK_Binary\}/}' $working_dir/package/IJammer/pdk_installer_binary.mpi
sed -i '0,/{PDK}/ s///'                                  $working_dir/package/IJammer/pdk_installer_binary.mpi
sed -i -e 's|_setupwin32_|_binary_setupwin32_|'          $working_dir/package/IJammer/pdk_installer_binary.mpi

# Generating the source package
echo "Install Jammer packaging for Source..."
$installjammer_path/installjammer --build-for-release $working_dir/package/IJammer/pdk_installer_source.mpi 1>>$working_dir/package/IJammer/ijammer_build.log
echo "Creating tar for Source..."
cd $working_dir/package/final
tar -zcf pdk_$pdk_version.tar.gz pdk_$pdk_version

recur_copy() {
  #echo "Copying $1 to $2"
  find ./ -iname $1  | \
  while read filepath; do
    cp --parent --target-directory=$2 "$filepath"
  done
}

recur_del() {
  #echo "Deleting $1 recursively from $2...."
  find $2 -iname $1  | \
  while read filepath; do
    rm -f "$filepath"
  done
}

# Copying library files (except generated sysbios lib) to generate lib package
cd $working_dir/package/build/pdk_$pdk_version/packages/ti
recur_del  'sysbios.a*' $working_dir/package/build/pdk_$pdk_version/packages/ti
recur_copy '*.aa15fg'   $working_dir/package/final/pdk_$pdk_version/packages/ti
recur_copy '*.ae66'     $working_dir/package/final/pdk_$pdk_version/packages/ti
recur_copy '*.aem4'     $working_dir/package/final/pdk_$pdk_version/packages/ti
recur_copy '*.aearp32F' $working_dir/package/final/pdk_$pdk_version/packages/ti
# Generating the lib package
#echo "Install Jammer packaging for Source+Library..."
#$installjammer_path/installjammer --build-for-release $working_dir/package/IJammer/pdk_installer_lib.mpi    1>>$working_dir/package/IJammer/ijammer_build.log
echo "Creating tar for Source+Library..."
cd $working_dir/package/final
tar -zcf pdk_lib_$pdk_version.tar.gz pdk_$pdk_version

# Copying binary files to generate binary package
# Copy all exe and map files.
cd $working_dir/package/build/pdk_$pdk_version/packages/ti
recur_copy '*.xa15fg'   $working_dir/package/final/pdk_$pdk_version/packages/ti
recur_copy '*.xe66'     $working_dir/package/final/pdk_$pdk_version/packages/ti
recur_copy '*.xem4'     $working_dir/package/final/pdk_$pdk_version/packages/ti
recur_copy '*.xearp32F' $working_dir/package/final/pdk_$pdk_version/packages/ti
recur_copy '*.map'      $working_dir/package/final/pdk_$pdk_version/packages/ti
recur_copy '*.bin'      $working_dir/package/final/pdk_$pdk_version/packages/ti
recur_copy '*.tiimage'  $working_dir/package/final/pdk_$pdk_version/packages/ti
recur_copy '*.chimage'  $working_dir/package/final/pdk_$pdk_version/packages/ti
recur_copy 'MLO'        $working_dir/package/final/pdk_$pdk_version/packages/ti
#HS build disabled in release build
#recur_copy '.xloader'   $working_dir/package/final/pdk_$pdk_version/packages/ti
recur_copy '*.rprc'     $working_dir/package/final/pdk_$pdk_version/packages/ti
recur_copy '*.appimage' $working_dir/package/final/pdk_$pdk_version/packages/ti
# Generating the binary package
#echo "Install Jammer packaging for Source+Library+Executables..."
#$installjammer_path/installjammer --build-for-release $working_dir/package/IJammer/pdk_installer_binary.mpi 1>>$working_dir/package/IJammer/ijammer_build.log
echo "Creating tar for Source+Library+Executables..."
cd $working_dir/package/final
tar -zcf pdk_binary_$pdk_version.tar.gz pdk_$pdk_version

# Generating the secure package
echo "Install Jammer packaging for Security addon..."
$installjammer_path/installjammer --build-for-release $working_dir/package/IJammer/pdk_security_installer.mpi 1>>$working_dir/package/IJammer/ijammer_build.log

cp $working_dir/package/IJammer/output/*.exe               $working_dir/package
cp $working_dir/package/IJammer/output/*.bin               $working_dir/package
cp $working_dir/package/final/*.gz                         $working_dir/package
cp $working_dir/package/final/pdk_$pdk_version/docs/*.pdf  $working_dir/package

###################### End of build ######################

end_time=`date +%s`
let deltatime=end_time-start_time
let hours=deltatime/3600
let minutes=deltatime/60
let minutes=minutes%60
let seconds=deltatime%60
printf "Execution Time: %d:%02d:%02d\n" $hours $minutes $seconds

exit
