#!/bin/bash
#   ============================================================================
#   @file   tisci_system_firmware_migrate.sh
#
#   @desc   Script to copy the sciclient header files from system-firmware to
#           sciclient
#           Usage:
#           > cd PDK_INSTALL_PATH/scripts/misc
#           > ./tisci_system_firmware_migrate.sh <absolute path to system-firmware> PDK_INSTALL_PATH/packages/  <tag of the sysfw you want to migrate to> <device: am65xx or j721e>
#
#   ============================================================================
#   Revision History
#   15-Jul-2018 Piyali         Initial draft.
#   ============================================================================
sysfirmware_src_path=$(echo "$1" | sed 's:/*$::')
pdk_src_path=$(echo $2 | sed 's:/*$::')
sysfw_ver="$3"
soc="$4"
year=`date '+%Y'`
currDir=`pwd`

echo "Migrating $sysfirmware_src_path to $pdk_src_path"
echo "------------------------------------------------------------------------"
echo ""
echo "Removing $pdk_src_path/ti/drv/sciclient/include/tisci"
rm -fr $pdk_src_path/ti/drv/sciclient/include/tisci

# Set the right version of the sysfirmware
if [ $soc == "am65xx" ]
then
  echo "------------------------------------------------------------------------"
  echo "Checking out the $sysfw_ver of the system firmware releases"
  cd $sysfirmware_src_path/../..
  git fetch origin
  git checkout master
  git rebase origin/master
  git branch -D $sysfw_ver
  git checkout -b $sysfw_ver $sysfw_ver
else
echo "------------------------------------------------------------------------"
echo "Checking out the $sysfw_ver of the system firmware source"
cd $sysfirmware_src_path
git fetch origin
git checkout master
git rebase origin/master
git branch -D $sysfw_ver
git checkout -b $sysfw_ver $sysfw_ver
fi
cd $currDir
echo "------------------------------------------------------------------------"
echo "Cleaning sciclient directory of any unchecked out files."
cd $pdk_src_path/ti/drv/sciclient/
git clean -fdx
cd $currDir
rm -fr $pdk_src_path/ti/binary/
cd $currDir

echo "------------------------------------------------------------------------"
echo "Copying Files..."
cp -fr $sysfirmware_src_path/include/tisci $pdk_src_path/ti/drv/sciclient/include/
cp -fr $sysfirmware_src_path/include/boardcfg/boardcfg_data.h $pdk_src_path/ti/drv/sciclient/include/tisci/tisci_boardcfg.h
if [ $soc == "am65xx" ]
then
  cp -fr $sysfirmware_src_path/include/soc/am6x/boardcfg_constraints.h $pdk_src_path/ti/drv/sciclient/include/tisci/tisci_boardcfg_constraints.h
  cp -fr $sysfirmware_src_path/include/soc/am6x/resasg_types.h $pdk_src_path/ti/drv/sciclient/include/tisci/tisci_resasg_types.h
  cp -fr $sysfirmware_src_path/include/soc/am6x/hosts.h $pdk_src_path/ti/drv/sciclient/include/tisci/tisci_hosts.h
  cp -fr $sysfirmware_src_path/include/soc/am6x/sec_proxy.h $pdk_src_path/ti/drv/sciclient/include/tisci/tisci_sec_proxy.h
  cp -fr $sysfirmware_src_path/../../binaries/ti-sci-firmware-am65x-gp.bin $pdk_src_path/ti/drv/sciclient/soc/V0
  # Copy the public Documentation
  cp -fr $sysfirmware_src_path/../../binaries/system-firmware-public-documentation/ $pdk_src_path/ti/drv/sciclient/docs/
else
  cp -fr $sysfirmware_src_path/include/soc/j721e/boardcfg_constraints.h $pdk_src_path/ti/drv/sciclient/include/tisci/tisci_boardcfg_constraints.h
  cp -fr $sysfirmware_src_path/include/soc/j721e/resasg_types.h $pdk_src_path/ti/drv/sciclient/include/tisci/tisci_resasg_types.h
  cp -fr $sysfirmware_src_path/include/soc/j721e/hosts.h $pdk_src_path/ti/drv/sciclient/include/tisci/tisci_hosts.h
  cp -fr $sysfirmware_src_path/include/soc/j721e/sec_proxy.h $pdk_src_path/ti/drv/sciclient/include/tisci/tisci_sec_proxy.h
  echo "Downloading J7 prebuilt from Jenkins. Please make sure the path is correct!!!"
  wget http://gtjenkins.itg.ti.com/nightly_builds/processor-system-firmware/latest/artifacts/output/binaries/ti-sci-firmware-j721e-gp.bin
  mv ti-sci-firmware-j721e-gp.bin $pdk_src_path/ti/drv/sciclient/soc/V1
  wget -r -np -R "index.html*" http://gtjenkins.itg.ti.com/nightly_builds/processor-system-firmware/latest/artifacts/output/binaries/system-firmware-public-documentation/
  mv -f system-firmware-public-documentation $pdk_src_path/ti/drv/sciclient/docs/
  #cp -fr $sysfirmware_src_path/../../binaries/ti-sci-firmware-j721e-gp-vlab.bin $pdk_src_path/ti/drv/sciclient/soc/V1
fi

echo "------------------------------------------------------------------------"
echo "Modifying files to match PDK convention..."
cd $pdk_src_path/ti/drv/sciclient/include/
allfiles=`find $pdk_src_path/ti/drv/sciclient/include/tisci -name "*.h" -print`

# Create a common tisci_includes.h which includes the tisci_includes.headers 
echo "/*" > tisci_includes.h
echo " *  Copyright (C) 2017-$year Texas Instruments Incorporated" >> tisci_includes.h
echo " *" >> tisci_includes.h
echo " *  Redistribution and use in source and binary forms, with or without" >> tisci_includes.h
echo " *  modification, are permitted provided that the following conditions" >> tisci_includes.h
echo " *  are met:" >> tisci_includes.h 
echo " *" >> tisci_includes.h
echo " *    Redistributions of source code must retain the above copyright" >> tisci_includes.h
echo " *    notice, this list of conditions and the following disclaimer." >> tisci_includes.h
echo " *" >> tisci_includes.h
echo " *    Redistributions in binary form must reproduce the above copyright" >> tisci_includes.h
echo " *    notice, this list of conditions and the following disclaimer in the" >> tisci_includes.h
echo " *    documentation and/or other materials provided with the" >> tisci_includes.h
echo " *    distribution." >> tisci_includes.h
echo " *" >> tisci_includes.h
echo " *    Neither the name of Texas Instruments Incorporated nor the names of" >> tisci_includes.h
echo " *    its contributors may be used to endorse or promote products derived" >> tisci_includes.h
echo " *    from this software without specific prior written permission." >> tisci_includes.h
echo " *" >> tisci_includes.h
echo " *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS" >> tisci_includes.h
echo " *  \"AS IS\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT" >> tisci_includes.h
echo " *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR" >> tisci_includes.h
echo " *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT" >> tisci_includes.h
echo " *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL," >> tisci_includes.h
echo " *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT" >> tisci_includes.h
echo " *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE," >> tisci_includes.h
echo " *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY" >> tisci_includes.h
echo " *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT" >> tisci_includes.h
echo " *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE" >> tisci_includes.h
echo " *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE." >> tisci_includes.h
echo " *" >> tisci_includes.h
echo " */" >> tisci_includes.h
echo "/**" >> tisci_includes.h
echo " * \ingroup TISCI" >> tisci_includes.h
# The Doxyen group is based on the file name.
echo " * \defgroup tisci_includes.h">> tisci_includes.h
echo " *" >> tisci_includes.h
echo " * DMSC controls the power management, security and resource management" >> tisci_includes.h
echo " * of the device." >> tisci_includes.h
echo " *" >> tisci_includes.h
echo " *" >> tisci_includes.h
echo " * @{" >> tisci_includes.h
echo " */" >> tisci_includes.h
echo "/**" >> tisci_includes.h
echo " *  \file   tisci_includes.h" >> tisci_includes.h
echo " *" >> tisci_includes.h
echo " *  \brief  This file includes the other tisci_* headers." >> tisci_includes.h
echo " *          WARNING!!: Autogenerated file from SYSFW. DO NOT MODIFY!!" >> tisci_includes.h
echo " */" >> tisci_includes.h
echo "" >> tisci_includes.h
echo "#ifndef TISCI_INCLUDES_H" >> tisci_includes.h
echo "#define TISCI_INCLUDES_H" >> tisci_includes.h
echo "" >> tisci_includes.h
echo "/* ========================================================================== */" >> tisci_includes.h
echo "/*                             Include Files                                  */" >> tisci_includes.h
echo "/* ========================================================================== */" >> tisci_includes.h
echo "" >> tisci_includes.h
echo "/** This macro is used by the TISCI headers to indicate bit positions */"  >> tisci_includes.h
echo "#define TISCI_BIT(n)  (1UL << (n))"  >> tisci_includes.h
echo "" >> tisci_includes.h
echo "#if defined (SOC_AM65XX)" >> tisci_includes.h
echo "#include <ti/drv/sciclient/soc/V0/tisci_resasg_types.h>" >> tisci_includes.h    
echo "#include <ti/drv/sciclient/soc/V0/tisci_hosts.h>" >> tisci_includes.h    
echo "#include <ti/drv/sciclient/soc/V0/tisci_sec_proxy.h>" >> tisci_includes.h    
echo "#endif" >> tisci_includes.h
echo "#if defined (SOC_J721E)" >> tisci_includes.h
echo "#include <ti/drv/sciclient/soc/V1/tisci_resasg_types.h>" >> tisci_includes.h    
echo "#include <ti/drv/sciclient/soc/V1/tisci_hosts.h>" >> tisci_includes.h    
echo "#include <ti/drv/sciclient/soc/V1/tisci_sec_proxy.h>" >> tisci_includes.h    
echo "#endif" >> tisci_includes.h

for filename in $allfiles; do
  echo "Filename is " $filename
  first_comment_start=`grep -n '\/\*' $filename | cut -d: -f 1 | head -1`
  first_comment_end=`grep -n '\*\/' $filename | cut -d: -f 1 | head -1`
  first_copyright_end=`grep -n 'Copyright' $filename | cut -d: -f 1 | head -1`
  first_comment_start=$((first_comment_start + 1))
  first_copyright_end=$((first_copyright_end - 1))
  # Copy the standard header to the file.
  echo "/*" > "$filename""_temp.h"
  echo " *  Copyright (C) 2017-$year Texas Instruments Incorporated" >> "$filename""_temp.h"
  echo " *" >> "$filename""_temp.h"
  echo " *  Redistribution and use in source and binary forms, with or without" >> "$filename""_temp.h"
  echo " *  modification, are permitted provided that the following conditions" >> "$filename""_temp.h"
  echo " *  are met:" >> "$filename""_temp.h"
  echo " *" >> "$filename""_temp.h"
  echo " *    Redistributions of source code must retain the above copyright" >> "$filename""_temp.h"
  echo " *    notice, this list of conditions and the following disclaimer." >> "$filename""_temp.h"
  echo " *" >> "$filename""_temp.h"
  echo " *    Redistributions in binary form must reproduce the above copyright" >> "$filename""_temp.h"
  echo " *    notice, this list of conditions and the following disclaimer in the" >> "$filename""_temp.h"
  echo " *    documentation and/or other materials provided with the" >> "$filename""_temp.h"
  echo " *    distribution." >> "$filename""_temp.h"
  echo " *" >> "$filename""_temp.h"
  echo " *    Neither the name of Texas Instruments Incorporated nor the names of" >> "$filename""_temp.h"
  echo " *    its contributors may be used to endorse or promote products derived" >> "$filename""_temp.h"
  echo " *    from this software without specific prior written permission." >> "$filename""_temp.h"
  echo " *" >> "$filename""_temp.h"
  echo " *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS" >> "$filename""_temp.h"
  echo " *  \"AS IS\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT" >> "$filename""_temp.h"
  echo " *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR" >> "$filename""_temp.h"
  echo " *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT" >> "$filename""_temp.h"
  echo " *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL," >> "$filename""_temp.h"
  echo " *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT" >> "$filename""_temp.h"
  echo " *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE," >> "$filename""_temp.h"
  echo " *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY" >> "$filename""_temp.h"
  echo " *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT" >> "$filename""_temp.h"
  echo " *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE" >> "$filename""_temp.h"
  echo " *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE." >> "$filename""_temp.h"
  echo " *" >> "$filename""_temp.h"
  echo " */" >> "$filename""_temp.h"
  echo "/**" >> "$filename""_temp.h"
  echo " * \ingroup TISCI" >> "$filename""_temp.h"
  # The Doxyen group is based on the file name.
  echo " * \defgroup $(basename "$filename" .h)" >> "$filename""_temp.h"
  echo " *" >> "$filename""_temp.h"
  echo " * DMSC controls the power management, security and resource management" >> "$filename""_temp.h"
  echo " * of the device." >> "$filename""_temp.h"
  echo " *" >> "$filename""_temp.h"
  echo " *" >> "$filename""_temp.h"
  echo " * @{" >> "$filename""_temp.h"
  echo " */" >> "$filename""_temp.h"
  echo "/**" >> "$filename""_temp.h"
  # Get the file name from the file name.
  echo " *  \file   `basename $filename`" >> "$filename""_temp.h"
  echo " *" >> "$filename""_temp.h"
  echo " *  \brief  This file contains:" >> "$filename""_temp.h"
  echo " *" >> "$filename""_temp.h"
  echo " *          WARNING!!: Autogenerated file from SYSFW. DO NOT MODIFY!!" >> "$filename""_temp.h"
  command="$first_comment_start"",""$first_copyright_end""p"
  # Copy the comment from the original file to the new file.
  sed -n $command $filename >> "$filename""_temp.h"
  echo " */" >> "$filename""_temp.h"
  command="1,"$first_comment_end"d"
  sed $command $filename >> "$filename""_temp.h"
  echo "" >> "$filename""_temp.h"
  echo "/* @} */" >> "$filename""_temp.h"
  # Remove headers which do not exist in PDK.
  # Have to do a copy to another file because sed seems to fail inplace editting
  # on windows 10.
  sed 's/types\/short_types.h/stdint.h/g' "$filename""_temp.h" > "$filename""_temp2.h"
  sed '/bitops.h/d' "$filename""_temp2.h" > "$filename""_temp.h"
  # Fix Types in the header and tabs to spaces
  sed 's/\t/    /g' "$filename""_temp.h" > "$filename""_temp2.h"
  sed 's/u64/uint64_t/g' "$filename""_temp2.h" > "$filename""_temp.h"
  sed 's/s64/int64_t/g' "$filename""_temp.h" > "$filename""_temp2.h"
  sed 's/u32/uint32_t/g' "$filename""_temp2.h" > "$filename""_temp.h"
  sed 's/s32/int32_t/g' "$filename""_temp.h" > "$filename""_temp2.h"
  sed 's/u16/uint16_t/g' "$filename""_temp2.h" > "$filename""_temp.h"
  sed 's/s16/int16_t/g' "$filename""_temp.h" > "$filename""_temp2.h"
  sed 's/u8/uint8_t/g' "$filename""_temp2.h" > "$filename""_temp.h"
  sed 's/s8/int8_t/g' "$filename""_temp.h" > "$filename""_temp2.h"
  sed '/\bhdr/d' "$filename""_temp2.h" > "$filename""_temp.h"
  sed 's/<tisci/<ti\/drv\/sciclient\/include\/tisci/' "$filename""_temp.h" > "$filename""_temp2.h"
  sed 's/short_types.h/stdint.h/g' "$filename""_temp2.h" > "$filename""_temp.h"
  sed 's/soc_phys_addr_t/uint64_t/g' "$filename""_temp.h" > "$filename""_temp2.h"
  sed '/address_types.h/d' "$filename""_temp2.h" > "$filename""_temp.h"
  sed 's/\bBIT\b/TISCI_BIT/g' "$filename""_temp.h" > "$filename""_temp2.h"
  sed 's/\bBOARD/TISCI_BOARD/g' "$filename""_temp2.h" > "$filename""_temp.h"
  sed '/ftbool.h/d' "$filename""_temp.h" > "$filename""_temp2.h"
  sed '/hosts.h/d' "$filename""_temp2.h" > "$filename""_temp.h"
  sed 's/ftbool/uint8_t/g' "$filename""_temp.h" > "$filename""_temp2.h"
  sed 's/\bstruct boardcfg/struct tisci_boardcfg/g' "$filename""_temp2.h" > "$filename""_temp.h"
  sed 's/AM6_BOARD/TISCI_BOARD/g' "$filename""_temp.h" > "$filename""_temp2.h"
  sed '/resasg_types.h/d' "$filename""_temp2.h" > "$filename""_temp.h"
  sed 's/\bRESASG/TISCI_RESASG/g' "$filename""_temp.h" > "$filename""_temp2.h"
  sed 's/enum resasg_/enum tisci_resasg_/g' "$filename""_temp2.h" > "$filename""_temp.h"
  sed 's/boardcfg_constraints.h/ti\/drv\/sciclient\/include\/tisci\/tisci_includes.h/g' "$filename""_temp.h" > "$filename""_temp2.h"
  sed 's/AM6_HOSTS/TISCI_HOSTS/g' "$filename""_temp2.h" > "$filename""_temp.h"
  sed 's/HOST_ID/TISCI_HOST_ID/g' "$filename""_temp.h" > "$filename""_temp2.h"
  sed 's/J721E_SEC/TISCI_SEC/g' "$filename""_temp2.h" > "$filename""_temp.h"
  sed 's/AM6_SEC/TISCI_SEC/g' "$filename""_temp.h" > "$filename""_temp2.h"
  sed 's/SEC_PROXY/TISCI_SEC_PROXY/g' "$filename""_temp2.h" > "$filename""_temp.h"
  sed 's/TISCI_DEV_NAVSS512L_MAIN_0_KSDMA_RINGACC_0/TISCI_DEV_NAVSS0_RINGACC0/g' "$filename""_temp.h" > "$filename""_temp2.h"
  sed 's/TISCI_DEV_NAVSS_MCU_J7_MCU_0_KSDMA_RINGACC_0/TISCI_DEV_MCU_NAVSS0_RINGACC0/g' "$filename""_temp2.h" > "$filename""_temp.h"
  sed 's/TISCI_DEV_NAVSS512L_MAIN_0_KSDMA_UDMAP_0/TISCI_DEV_NAVSS0_UDMAP0/g' "$filename""_temp.h" > "$filename""_temp2.h"
  sed 's/TISCI_DEV_NAVSS_MCU_J7_MCU_0_KSDMA_UDMAP_0/TISCI_DEV_MCU_NAVSS0_UDMAP0/g' "$filename""_temp2.h" > "$filename""_temp.h"
  sed 's/TISCI_DEV_NAVSS512L_MAIN_0_KSDMA_INTAGGR_2/TISCI_DEV_NAVSS0_UDMASS_INTA0/g' "$filename""_temp.h" > "$filename""_temp2.h"
  sed 's/TISCI_DEV_NAVSS_MCU_J7_MCU_0_KSDMA_INTAGGR_0/TISCI_DEV_MCU_NAVSS0_INTR_AGGR_0/g' "$filename""_temp2.h" > "$filename""_temp.h"
  sed 's/TISCI_DEV_COMPUTE_CLUSTER_J7ES_TB_VDC_MAIN_0_GIC500SS/TISCI_DEV_GIC0/g' "$filename""_temp.h" > "$filename""_temp2.h"
  sed 's/TISCI_DEV_PULSAR_SL_MCU_0_CPU0/TISCI_DEV_MCU_ARMSS0_CPU0/g' "$filename""_temp2.h" > "$filename""_temp.h"
  sed 's/TISCI_DEV_PULSAR_SL_MCU_0_CPU1/TISCI_DEV_MCU_ARMSS0_CPU1/g' "$filename""_temp.h" > "$filename""_temp2.h"
  sed 's/TISCI_DEV_NAVSS_MCU_J7_MCU_0/TISCI_DEV_MCU_NAVSS0/g' "$filename""_temp2.h" > "$filename""_temp.h"
  sed 's/TISCI_HOST_ID_MCU_0_R5_3/TISCI_HOST_ID_R5_3/g' "$filename""_temp.h" > "$filename""_temp2.h"
  sed 's/TISCI_HOST_ID_MCU_0_R5_2/TISCI_HOST_ID_R5_2/g' "$filename""_temp2.h" > "$filename""_temp.h"
  sed 's/TISCI_HOST_ID_MCU_0_R5_1/TISCI_HOST_ID_R5_1/g' "$filename""_temp.h" > "$filename""_temp2.h"
  sed 's/TISCI_HOST_ID_MCU_0_R5_0/TISCI_HOST_ID_R5_0/g' "$filename""_temp2.h" > "$filename""_temp.h"
  sed 's/types\/sbool.h/stdbool.h/g' "$filename""_temp.h" > "$filename""_temp2.h"
  cat "$filename""_temp2.h" > $filename
  rm "$filename""_temp.h"
  rm "$filename""_temp2.h"
  if [ `basename $filename` = "tisci_resasg_types.h" ]
  then
    if [ $soc == "am65xx" ]
    then
      mv $pdk_src_path/ti/drv/sciclient/include/tisci/tisci_resasg_types.h $pdk_src_path/ti/drv/sciclient/soc/V0/tisci_resasg_types.h
    else
      mv $pdk_src_path/ti/drv/sciclient/include/tisci/tisci_resasg_types.h $pdk_src_path/ti/drv/sciclient/soc/V1/tisci_resasg_types.h
    fi
  else
    if [ `basename $filename` = "tisci_hosts.h" ]
    then
      if [ $soc == "am65xx" ]
      then
        mv $pdk_src_path/ti/drv/sciclient/include/tisci/tisci_hosts.h $pdk_src_path/ti/drv/sciclient/soc/V0/tisci_hosts.h
      else
        mv $pdk_src_path/ti/drv/sciclient/include/tisci/tisci_hosts.h $pdk_src_path/ti/drv/sciclient/soc/V1/tisci_hosts.h
      fi
    else
      if [ `basename $filename` = "tisci_sec_proxy.h" ]
      then
        if [ $soc == "am65xx" ]
        then
          mv $pdk_src_path/ti/drv/sciclient/include/tisci/tisci_sec_proxy.h $pdk_src_path/ti/drv/sciclient/soc/V0/tisci_sec_proxy.h
        else
          mv $pdk_src_path/ti/drv/sciclient/include/tisci/tisci_sec_proxy.h $pdk_src_path/ti/drv/sciclient/soc/V1/tisci_sec_proxy.h
        fi
      else
        srcFolder=`pwd`
        target=$filename
        common_part=$srcFolder 
        result="${target#$common_part}"
        echo "#include <ti/drv/sciclient/include$result>" >> tisci_includes.h
      fi
    fi
  fi
done

echo "" >> tisci_includes.h
echo "#endif /* TISCI_INCLUDES_H */"  >> tisci_includes.h
echo "" >> tisci_includes.h
echo "/* @} */" >> tisci_includes.h
mv tisci_includes.h $pdk_src_path/ti/drv/sciclient/include/tisci
cd -

# Convert Binary to Header for the firmware
echo "------------------------------------------------------------------------"
echo "Generating the firmware header files..."
cd $pdk_src_path/ti/drv/sciclient/tools/
if [ $soc == "am65xx" ]
then
source firmwareHeaderGen.sh am65xx
else
source firmwareHeaderGen.sh j721e
fi
cd -

# Build the CCS executable
echo "------------------------------------------------------------------------"
echo "Generating the CCS load executable... "
cd $pdk_src_path/ti/build
if [ $soc == "am65xx" ]
then
  make -j -s sciclient_ccs_init BOARD=am65xx_evm
  cp $pdk_src_path/ti/binary/sciclient_ccs_init/bin/am65xx/sciclient_ccs_init_mcu1_0_release.xer5f $pdk_src_path/ti/drv/sciclient/tools/ccsLoadDmsc/am65xx/
else
  make -j -s sciclient_ccs_init BOARD=j721e_sim
  cp $pdk_src_path/ti/binary/sciclient_ccs_init/bin/j721e_sim/sciclient_ccs_init_mcu1_0_release.xer5f $pdk_src_path/ti/drv/sciclient/tools/ccsLoadDmsc/j721e/
fi
cd -

echo "========================================================================"
echo "Migration Script finished. Check for errors. If no errors push code changes in pdk/packages/ti/drv/sciclient"
echo "========================================================================"
