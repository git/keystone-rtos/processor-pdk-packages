#!/bin/bash
#   ============================================================================
#   @file   cm_status_accounting.sh
#
#   @desc   Script to do status accounting for a given git repo
#
#   ============================================================================
#   Revision History
#   12-Jul-2017 Sivaraj     Initial draft.
#
#   ============================================================================

#Get user input
while [ $# -gt 0 ]; do
  case "$1" in
    --old_tag=*)
      old_tag="${1#*=}"
      ;;
    --new_tag=*)
      new_tag="${1#*=}"
      ;;
    --project=*)
      project="${1#*=}"
      ;;
    --git_repo=*)
      git_repo="${1#*=}"
      ;;
    *)
      printf "Error: Invalid argument $1!!\n"
  esac
  shift
done

#Default value if not provided
: ${old_tag:=""}
# . is for latest commit
: ${new_tag:="."}
: ${project:="PDK"}
: ${git_repo:=`pwd`}

if [ x"" == x$old_tag ]; then
    printf "Need to provide old tag!!\n"
    printf "Usage: \n    ./cm_status_accounting.sh --project=\"PDK\" --old_tag=\"REL.PDK.TDA.01.07.00.04\" --new_tag=\"<. if not provided>\" --git_repo=\"<git path. pwd if not provided>\" \n"
    exit
fi

cd $git_repo

date_print=`date '+%d-%b-%Y %I:%M:%S %p'`
printf "\n"
printf "CM Status Accounting for $project ($git_repo) from release/tag $old_tag to $new_tag ...\n"
printf "Date: $date_print"
printf "\n"

printf "Number of Files Added   : "
git log --pretty --oneline --name-status "$old_tag".."$new_tag" | sort -u | grep $'A\t' | cut -f 2 | wc -l
printf "Number of Files Modified: "
git log --pretty --oneline --name-status "$old_tag".."$new_tag" | sort -u | grep $'M\t' | cut -f 2 | wc -l
printf "Number of Files Deleted : "
git log --pretty --oneline --name-status "$old_tag".."$new_tag" | sort -u | grep $'D\t' | cut -f 2 | wc -l

printf "\n"
printf "List of Files Added:\n"
git log --pretty --oneline --name-status "$old_tag".."$new_tag" | sort -u | grep $'A\t' | cut -f 2

printf "\n"
printf "List of Files Modified:\n"
git log --pretty --oneline --name-status "$old_tag".."$new_tag" | sort -u | grep $'M\t' | cut -f 2

printf "\n"
printf "List of Files Deleted:\n"
git log --pretty --oneline --name-status "$old_tag".."$new_tag" | sort -u | grep $'D\t' | cut -f 2

exit
