#!/bin/bash
#   ============================================================================
#   @file   package_test_k3.sh
#
#   @desc   Script to do GIT clone, remove .git and other internal folders for
#           comparing with PDK release package - difference should be the removed
#           files during packaging for K3 PDK
#
#   ============================================================================
#   Revision History
#   28-Mar-2019 Sivaraj         Initial draft.
#
#   ============================================================================
# Example: ./package_test_k3.sh --pdk_manifest_xml_with_path="releases/00_08_00/pdk.xml"

#Get user input
while [ $# -gt 0 ]; do
  case "$1" in
    --pdk_manifest_xml_with_path=*)
      pdk_manifest_xml_with_path="${1#*=}"
      ;;
    --working_dir=*)
      working_dir="${1#*=}"
      ;;
    *)
      printf "Error: Invalid argument $1!!\n"
  esac
  shift
done

#Default value if not provided
: ${pdk_manifest_xml_with_path:="pdk_j721e.xml"}
if [ "$working_dir" == "" ]; then
  working_dir=`pwd`
fi
clone_dir=$working_dir/pdk_package_test
pdk_dir=$clone_dir/pdk

rm -rf $clone_dir
mkdir -p $clone_dir
cd $clone_dir
repo init -u ssh://git@bitbucket.itg.ti.com/processor-sdk-vision/repo_manifests.git -m $pdk_manifest_xml_with_path
repo sync -q
repo start dev --all

#Remove .git folder
rm -rf $pdk_dir/.git/
rm -rf $pdk_dir/*/.git/
rm -rf $pdk_dir/*/*/.git/
rm -rf $pdk_dir/*/*/*/.git/
rm -rf $pdk_dir/*/*/*/*/.git/
rm -rf $pdk_dir/*/*/*/*/*/.git/

#Remove temp dir still the component is enabled
rm -rf $pdk_dir/packages/ti/board/diag
rm -rf $pdk_dir/packages/ti/board/src/am65*

#Remove other not used folder we know for sure
rm -rf $clone_dir/.repo
rm -rf $pdk_dir/internal_docs
rm -rf $pdk_dir/scripts
rm -rf $pdk_dir/release
rm -rf $pdk_dir/packages/ti/board/src/bbb*
rm -rf $pdk_dir/packages/ti/board/src/evmAM*
rm -rf $pdk_dir/packages/ti/board/src/evmTDA*
rm -rf $pdk_dir/packages/ti/board/src/evmC*
rm -rf $pdk_dir/packages/ti/board/src/evmDRA*
rm -rf $pdk_dir/packages/ti/board/src/evmK2*
rm -rf $pdk_dir/packages/ti/board/src/evmKeystone/
rm -rf $pdk_dir/packages/ti/board/src/evmOMAP*
rm -rf $pdk_dir/packages/ti/board/src/ice*
rm -rf $pdk_dir/packages/ti/board/src/idkAM*
rm -rf $pdk_dir/packages/ti/board/src/lcdk*
rm -rf $pdk_dir/packages/ti/board/src/skAM*
rm -rf $pdk_dir/packages/ti/board/src/flash/platform_flash
rm -rf $pdk_dir/packages/ti/board/src/flash/nand
rm -rf $pdk_dir/packages/ti/board/utils
rm -rf $pdk_dir/packages/ti/boot/sbl/soc/a15
rm -rf $pdk_dir/packages/ti/boot/sbl/soc/k2*
rm -rf $pdk_dir/packages/ti/boot/sbl/soc/am*
rm -rf $pdk_dir/packages/ti/boot/sbl/soc/c6*
rm -rf $pdk_dir/packages/ti/boot/sbl/soc/omap*
rm -rf $pdk_dir/packages/ti/boot/sbl/board/evm*
rm -rf $pdk_dir/packages/ti/boot/sbl/board/ice*
rm -rf $pdk_dir/packages/ti/boot/sbl/board/idk*
rm -rf $pdk_dir/packages/ti/boot/sbl/board/lcdk*
rm -rf $pdk_dir/packages/ti/build/am*
rm -rf $pdk_dir/packages/ti/build/k2*
rm -rf $pdk_dir/packages/ti/build/pruss
rm -rf $pdk_dir/packages/ti/build/dra*
rm -rf $pdk_dir/packages/ti/build/omap*
rm -rf $pdk_dir/packages/ti/build/tda*
rm -rf $pdk_dir/packages/ti/csl/soc/tda*
rm -rf $pdk_dir/packages/ti/csl/soc/common
rm -rf $pdk_dir/packages/ti/csl/soc/am*
rm -rf $pdk_dir/packages/ti/csl/soc/c6*
rm -rf $pdk_dir/packages/ti/csl/soc/dra*
rm -rf $pdk_dir/packages/ti/csl/soc/k2*
rm -rf $pdk_dir/packages/ti/csl/soc/omap*

exit
