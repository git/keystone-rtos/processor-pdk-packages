#!/bin/bash
#   ============================================================================
#   @file   cm_status_accounting_clearcase.sh
#
#   @desc   Script to do status accounting for clearcase
#
#   ============================================================================
#   Revision History
#   25-Jul-2017 Sivaraj     Initial draft.
#
#   ============================================================================

#Get user input
while [ $# -gt 0 ]; do
  case "$1" in
    --old_tag=*)
      old_tag="${1#*=}"
      ;;
    --new_tag=*)
      new_tag="${1#*=}"
      ;;
    --proj_relpath=*)
      proj_relpath="${1#*=}"
      ;;
    --cc_path=*)
      cc_path="${1#*=}"
      ;;
    --cc_view=*)
      cc_view="${1#*=}"
      ;;
    --checkout_relpath=*)
      checkout_relpath="${1#*=}"
      ;;
    --checkout_limit=*)
      checkout_limit="${1#*=}"
      ;;
    --mld_file=*)
      mld_file="${1#*=}"
      ;;
    *)
      printf "Error: Invalid argument $1!!\n"
  esac
  shift
done

#Default value if not provided
working_dir=`pwd`
: ${cc_path:="/m"}
: ${cc_view:="a0393606_omap35xx_dev1"}
: ${checkout_relpath:="data_pspdocs/PDP"}
: ${checkout_limit:="14"}
: ${proj_relpath:="data_pspdocs/PDP/PDK"}
: ${mld_file:="$working_dir/mld.txt"}

sed_cmd_fs_to_2bs='s|\/|\\\\|g'
sed_cmd_bs_to_2bs='s|\\|\\\\|g'
temp_file=$working_dir/temp.txt
proj_relpath_windows=`echo $proj_relpath | sed -e $sed_cmd_fs_to_2bs`
checkout_relpath_windows=`echo $checkout_relpath | sed -e $sed_cmd_fs_to_2bs`

date_print=`date '+%d-%b-%Y %I:%M:%S %p'`
printf "CM Status Accounting (Clearecase) for $checkout_relpath and $proj_relpath\n"
printf "Date: $date_print\n"

cd $cc_path/$cc_view/$checkout_relpath
echo ""
printf "List of all checkout files under $checkout_relpath...\n"
cleartool lscheckout -s -all | grep "$checkout_relpath_windows" | sort > $temp_file
#Replace \ with \\
sed -i $sed_cmd_bs_to_2bs $temp_file
printf "Checkout Date   Open Days    File\n"
echo "---------------------------------"
while read line
do
    time=`cleartool lscheckout -l $line | grep -B 1 "checkout" | grep -v "checkout" | cut -d "T" -f 1`
    delta=$(($(date +%s) - $(date -d "$time" +%s)))
    open_days=$((delta / 86400))
    if [ "$open_days" -gt "$checkout_limit" ]; then
        printf "%-16s%-13s%s(warning!!!)\n" $time $open_days $line
    else
        printf "%-16s%-13s%s\n" $time $open_days $line
    fi
done <$temp_file
printf "\n"
rm -f $temp_file
echo ""

cd $cc_path/$cc_view/$proj_relpath
echo ""
printf "Status of all Master List of Documents...\n"
printf "%-80s%-10s%-15s\n" "Document" "Version" "Latest Label"
echo "------------------------------------------------------------------------------------------------------"
while read line
do
    version=`cleartool lshistory $line | head -n 1 | cut -d @ -f 3 | cut -d '\' -f 3 | cut -d '"' -f 1`
    label=`  cleartool lshistory $line | head -n 1 | cut -d @ -f 3 | cut -d ' ' -f 2 | cut -d '(' -f 2 | cut -d ')' -f 1 | cut -d ',' -f 1`
    printf "%-80s%-10s%-15s\n" $line $version $label
done <$mld_file
printf "\n"
echo ""

if [ "$old_tag" != "" ] && [ "$new_tag" != "" ] ; then
  cd $cc_path/$cc_view/$proj_relpath
  mod_cmd1="{lbtype_sub($old_tag) && lbtype_sub($new_tag)}"
  mod_cmd2="{!(lbtype($old_tag) && lbtype($new_tag)) && (lbtype($new_tag) || lbtype($old_tag))}"
  del_cmd="{lbtype_sub($old_tag) && !lbtype_sub($new_tag)}"
  add_cmd="{!lbtype_sub($old_tag) && lbtype_sub($new_tag)}"
  printf "List of all files under $proj_relpath modified between $old_tag and $new_tag tag...\n"
  cleartool find . -element "$mod_cmd1" -version "$mod_cmd2" -print | sort
  printf "\n"
  printf "List of all files under $proj_relpath deleted between $old_tag and $new_tag tag...\n"
  cleartool find . -element "$del_cmd" -print | sort
  printf "\n"
  printf "List of all files under $proj_relpath added between $old_tag and $new_tag tag...\n"
  cleartool find . -element "$add_cmd" -print | sort
  printf "\n"
fi

exit
