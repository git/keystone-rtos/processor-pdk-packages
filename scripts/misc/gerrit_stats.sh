#   ============================================================================
#   @file   gerrit_stats.sh
#
#   @desc   Script to print statistics from gerrit
#
#   ============================================================================
#   Revision History
#   5-Nov-2015 Sivaraj         Initial draft.
#
#   Documentation on gerrit query:
#   https://gerrit01.dal.design.ti.com:8445/Documentation/cmd-query.html
#   https://www.mediawiki.org/wiki/Gerrit/Navigation#Reports
#
#   ============================================================================

#   ============================================================================
#   CAUTION: DO NOT INVOKE THE SCRIPTS AS 'gitadmin'.
#            IT WILL ADD OBJECT FILES IN BASE REPOSITORY; WHICH SHOULD IDEALLY
#            BE CLEAN.
#   ============================================================
#!/bin/bash
#ssh -p 29420 gerrit01.dal.design.ti.com gerrit query 'status:merged project:ADAS/starterware --comments --patch-sets' > stw_all.txt
#ssh -p 29420 gerrit01.dal.design.ti.com gerrit query 'status:merged project:ADAS/MCAL --comments --patch-sets' > mcal_all.txt
#ssh -p 29423 gerrit01.dal.design.ti.com gerrit query 'status:merged project:ducati/vayu_drivers --comments --patch-sets' > bsp_all.txt
#ssh -p 29423 gerrit01.dal.design.ti.com gerrit query 'status:merged project:ADAS/vision_sdk --comments --patch-sets' > vsdk_all.txt
#Month wise
#ssh -p 29420 gerrit01.dal.design.ti.com gerrit query 'status:merged project:ADAS/starterware after:2015-10-01 before:2015-11-01 --comments --patch-sets' > stw_Oct15.txt

query_result_file=$1

total_commits=`grep -c  'project: ' $query_result_file`
#total_uploaded_patchsets=`grep -c 'Uploaded patch set' $query_result_file`
#avg_patchset_per_commit_1=$(awk -v m=$total_uploaded_patchsets -v n=$total_commits 'BEGIN { print m/n }')
total_patchsets=`grep -c 'patchSets:' $query_result_file`
total_rebased_patchsets=`grep -c 'was rebased' $query_result_file`
avg_patchset_per_commit=$(awk -v m=$total_patchsets -v m1=$total_rebased_patchsets -v n=$total_commits 'BEGIN { print (m-m1)/n }')
total_comments=`grep 'comments)' $query_result_file | cut --delimiter='(' -f 2 | cut --delimiter=' ' -f 1 | awk '{ SUM += $1} END { print SUM }'`
avg_comments_per_commit=$(awk -v m=$total_comments -v n=$total_commits 'BEGIN { print m/n }')
total_line_inserted=`grep -A 25 'patchSets:' $query_result_file | grep 'sizeInsertions' | cut --delimiter=':' -f 2 | awk '{ SUM += $1} END { print SUM }'`
total_line_deleted=` grep -A 25 'patchSets:' $query_result_file | grep 'sizeDeletions'  | cut --delimiter=':' -f 2 | awk '{ SUM += $1} END { print SUM }'`

printf "%-40s: %-7d\n" "Total Commits Merged" $total_commits
printf "%-40s: %-7d\n" "Total Patch Sets (Excluding rebased)" `expr $total_patchsets - $total_rebased_patchsets`
printf "%-40s: %-3.2f\n" "Average Patch Sets per Commit" $avg_patchset_per_commit
printf "%-40s: %-7d\n" "Total Review Comments" $total_comments
printf "%-40s: %-3.2f\n" "Average Comments per Commit" $avg_comments_per_commit
printf "%-40s: %-7d\n" "Total Lines Inserted" $total_line_inserted
printf "%-40s: %-7d\n" "Total Lines Deleted" $total_line_deleted
