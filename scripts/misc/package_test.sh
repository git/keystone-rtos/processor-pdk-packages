#!/bin/bash
#   ============================================================================
#   @file   package_test.sh
#
#   @desc   Script to do GIT clone, remove .git and other internal folders for
#           comparing with PDK release package - difference should be the removed
#           files during packaging
#
#   ============================================================================
#   Revision History
#   04-Jul-2017 Sivaraj         Initial draft.
#   14-Oct-2017 Sivaraj         Added option to clone both master and release tag
#                               Added added diff option at the end
#   04-Jul-2018 Sivaraj         Updated for j6_master
#
#   ============================================================================

#Get user input
while [ $# -gt 0 ]; do
  case "$1" in
    --tag=*)
      tag="${1#*=}"
      ;;
    *)
      printf "Error: Invalid argument $1!!\n"
  esac
  shift
done

#Default value if not provided
: ${rel_tag:="REL.PDK.TDA.01.08.00.15"}

package() {
    tag=$1

    rm -rf pdk_$tag/

    #clone
    git archive --remote=ssh://git@bitbucket.itg.ti.com/processor-sdk/processor-pdk-packages.git $tag scripts/pdk_git.sh | tar -xO > pdk_git.sh
    chmod 777 pdk_git.sh
    ./pdk_git.sh --clone --clone_config="prsdk_vision" --pdk_top_tag=$tag --docs_tag=$tag --sbl_tag=$tag --build_tag=$tag --csl_tag=$tag --pm_tag=$tag --ipc_lite_tag=$tag --fw_l3l4_tag=$tag --diag_tag=$tag --security_tag=$tag --vps_tag=$tag --stw_lld_tag=$tag --bsp_lld_tag=$tag
    rm -f pdk_git.sh

    mv -f pdk pdk_$tag

    #Remove .git folder
    rm -rf pdk_$tag/.git*
    rm -rf pdk_$tag/*/.git*
    rm -rf pdk_$tag/*/*/.git*
    rm -rf pdk_$tag/*/*/*/.git*
    rm -rf pdk_$tag/*/*/*/*/.git*
    rm -rf pdk_$tag/*/*/*/*/*/.git*

    #Remove other not used folder we know for sure
    rm -rf pdk_$tag/release
    rm -rf pdk_$tag/scripts
    rm -rf pdk_$tag/docs/internal
    rm -rf pdk_$tag/packages/ti/build/am*
    rm -rf pdk_$tag/packages/ti/build/k2*
    rm -rf pdk_$tag/packages/ti/build/pruss
    rm -rf pdk_$tag/packages/ti/csl/soc/am*
    rm -rf pdk_$tag/packages/ti/csl/soc/c6*
    rm -rf pdk_$tag/packages/ti/csl/soc/dra*
    rm -rf pdk_$tag/packages/ti/csl/soc/k2*
    rm -rf pdk_$tag/packages/ti/csl/soc/omap*
    rm -rf pdk_$tag/packages/ti/csl/test
}

#Package both master and release tag
package $rel_tag
package j6_master

#print diff of both package
echo ""
echo "Difference between master tag and $rel_tag tag..."
echo ""
diff -r pdk_j6_master pdk_$rel_tag

exit
