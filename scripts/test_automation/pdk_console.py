
import re
import os
import sys
import serial
import time
import datetime

#import Error codes from the common utils Error Codes.py file
from common.utils import error_codes

#
# Exatra time for logging in case test case passed (in sec)
# This is required to log extra information after the pass msg
#
log_sec = 2

def reset_board(pw_com_port, pw_conn_port):
    """!
        This function resets the connected board by sending the command on the UART channel

        @param  pw_com_port: Com port for Remote power swith
        @param  pw_conn_port: Port on which Board is connected
		@return True : For successful
		@return Error Code : For any failure
    """
    ret_value = error_codes.RET_SUCCESS

    ser = serial.Serial()
    ser.port = pw_com_port
    ser.baudrate = 9600
    ser.bytesize = serial.EIGHTBITS #number of bits per bytes
    ser.parity = serial.PARITY_NONE #set parity check: no parity
    ser.stopbits = serial.STOPBITS_ONE #number of stop bits
    ser.timeout = 1            #non-block read

    try:
        ser.open()
    except:
        print("error open PW serial port\n")
        return error_codes.RET_FAIL

    # Flush the Input buffer
    ser.flushInput()
    # ser.flushOutput()

    temp = pw_conn_port + ".0\n"

    # Write command to power off the board
    ser.write(temp.encode())

    # Read Ack
    res = ser.readline()
    if "ACK" not in str(res.decode()):
        print("Error while resetting the board")
        ser.close()
        return error_codes.RET_FAIL

    # Flush the Input buffer
    ser.flushInput()

    time.sleep(1)

    temp = pw_conn_port + ".1\n"

    # Write command to power off the board
    ser.write(temp.encode())

    # Read Ack
    res = ser.readline()
    if "ACK" not in str(res.decode()):
        print("Error while resetting the board")
        ser.close()
        return error_codes.RET_FAIL

    # Close serial port
    ser.close()

    return ret_value


def get_logs(logfilehandle, com_port, pass_msg, timeout, input_opt):
    """!
        This function reads log data from the COM port and write into a file and wait for pass message

        @param  logfilehandle: log file handler
        @param  com_port: Port port to connect
        @param  pass_msg: Expected Pass message
        @param  timeout:
		@return True : For successful
		@return Error Code : For any failure
    """

    # Implement passing argument using cmd line
    #

    count = 0

    input_str = []
    input_value = []

    if input_opt != "":
        input_list = input_opt.split(",")
        for ipt_opt in input_list:
            input_str.append(ipt_opt.split('#')[0])
            input_value.append(ipt_opt.split('#')[1].replace("\\r", "\r"))

    ret_value = error_codes.RET_SUCCESS

    ser = serial.Serial()
    ser.port = com_port
    ser.baudrate = 115200
    ser.bytesize = serial.EIGHTBITS #number of bits per bytes
    ser.parity = serial.PARITY_NONE #set parity check: no parity
    ser.stopbits = serial.STOPBITS_ONE #number of stop bits
    ser.timeout = 1            #non-block read

    try:
        ser.open()
    except:
        logfilehandle.write("error open serial port\n")
        return error_codes.RET_FAIL

    cur_time = datetime.datetime.now()
    timeout_time = cur_time + datetime.timedelta(seconds=timeout)

    # Loop to get the logs Enter the input for test cases and check for pass str
    while True:
        res = ser.readline()
        resp_str = str(res.decode())
        cur_time = datetime.datetime.now()
        if resp_str != "":
            logfilehandle.write(resp_str)
            if count < len(input_str):
                if input_str[count] in resp_str:
                    ser.write(input_value[count].encode())
                    count = count + 1
            elif pass_msg != "":
                if pass_msg in resp_str:
                    ret_value = error_codes.RET_SUCCESS
                    break

                else:
                    if cur_time > timeout_time:
                        logfilehandle.write("\nApplication Timed out\n")
                        ret_value = error_codes.RET_FAIL
                        break

            else:
                if cur_time > timeout_time:
                    logfilehandle.write("\nApplication Timed out\n")
                    break

        else:
            if cur_time > timeout_time:
                #timed out
                logfilehandle.write("\nApplication Timed out\n")
                if pass_msg == "":
                    break
                else:
                    ret_value = error_codes.RET_FAIL
                    break

    cur_time = datetime.datetime.now()
    log_time = cur_time + datetime.timedelta(seconds=log_sec)

    if log_time > timeout_time:
        log_time = timeout_time

    #
    # In case testcase is not timed out get the logs for log_sec
    #
    if cur_time < timeout_time:
        while True:
            res = ser.readline()
            resp_str = str(res.decode())
            cur_time = datetime.datetime.now()
            if resp_str != "":
                logfilehandle.write(resp_str)

            if cur_time > log_time:
                break

    ser.flushInput()
    ser.flushOutput()
    ser.close()

    return ret_value

