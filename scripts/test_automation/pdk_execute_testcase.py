#! /usr/bin/python
# -*- coding: UTF-8 -*-

#  Copyright 2012-2017 Luiko Czub, TestLink-API-Python-client developers
#
#  Licensed under Apache 2.0
#

# import the required Python packages
import os
import re
import ast
import sys
import argparse
import glob
import serial
import time
import subprocess
import signal
import xml.etree.ElementTree as xmlET

# Append Test Framework scripts root directory (scr) path to sys.path
# for relative import.
sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__),\
                    "../../")))

# Import the required functions from common utils.
from common.utils.common_utils import cleanup_and_exit
from common.utils.common_utils import execute_subprocess_cmd
from common.utils.common_utils import create_log_file
from common.utils.common_utils import bench_setup

from pdk_console import reset_board
from pdk_console import get_logs

#
# Extra time needed for CCS to load and run
#
ccs_load_time = 25



#import Error codes from the common utils error_codes.py file
from common.utils import error_codes

# Initialize the global variables
TCLOG = None                    # Testcase Log file handler
LOCAL_TESTCASE_LOG_FILE = None  # Path of log file in testcase directory

#
# Config parameters - System dependent
#
com_port = None   #Com port used for logging
ccs_dss_path = None #CCS dss path used for running
ccxml_path = None  #ccxml File path
pw_com_port = None #Com port to reset the board
pw_conn_port = None # Port on which the TDA board is connected on the Remote power switch

# Global variable to store the arguments
ARGS = {}

def parse_arguments():
    """!
       This function defines and reads the arguments for this script.

       @param None
       @return None
    """
    global ARGS

    # Configure options for Command line arguments
    parser = argparse.ArgumentParser(description='Process input arguments.')

    parser.add_argument("-p", "--testcase_params", action="store", type=str,
                        dest='TestcaseParams', required=True,
                        help="Testcase Parameters")

    parser.add_argument("-b", "--bench_file", action="store", type=str,
                        dest='BenchFile', required=True,
                        help="Testcase log file")

    parser.add_argument("-l", "--log_filename", action="store", type=str,
                        dest='LogFile', required=True,
                        help="Testcase log file")

    parser.add_argument("--tc_first", action="store", type=str,
                        dest='tc_first', required=True,
                        help="True if first testcase in the test session else False")

    parser.add_argument("--tc_last", action="store", type=str,
                        dest='tc_last', required=True,
                        help="True if last testcase in the test session else False")

    parser.add_argument("--port_no", action="store", type=str,
                        dest='port_no', required=True,
                        help="Port No for Py4J Gateway Server")

    #Parse arguments and capture them in args
    parser.add_argument("--temp_path", action="store", type=str,
                        dest='temp_path', required=True,
                        help="Temp folder path to store temporary files")

    #Parse arguments and capture them in args
    if len(sys.argv) == 1:
        parser.print_help()
        sys.stdout.flush()
        parser.exit(1)
    else:
        ARGS = vars(parser.parse_args())

    # Construct a Python dict for the testcase parameters
    tc_params = {}
    tc_params_str = re.sub('"', '\'', ARGS['TestcaseParams'])
    # Escaping the special characters : '[' and ']'
    tc_params_str = re.sub('\\[\'', '[\"', tc_params_str)
    tc_params_str = re.sub('\']', '\"]', tc_params_str)

    # Population the dictionary with key,value pairs from the string
    tc_params = ast.literal_eval(tc_params_str)

    # Assign the ARGS with the values from the dictionary
    for tc_key, tc_value in tc_params.items():
        if tc_key == "var_TestRootPath":
            ARGS['TestRootPath'] = tc_value
        elif tc_key == "params_chan":
            ARGS['params_chan'] = tc_value
        elif tc_key == "var_CommonScriptsRootPath":
            ARGS['CommonScriptsRootPath'] = tc_value
        elif tc_key == "ScriptsRootPath":
            ARGS['ScriptsRootPath'] = tc_value
        elif tc_key == "platform":
            ARGS['platform'] = tc_value
        elif tc_key == "script_args":
            ARGS['script_args'] = tc_value
        elif tc_key == "timeout":
            ARGS['timeout'] = tc_value
        elif tc_key == "binary":
            ARGS['binary'] = tc_value
        elif tc_key == "pdk_commit_id":
            ARGS['pdk_commit_id'] = tc_value


def error_checks():
    """!
       This function checks the validity of the command line arguments

        @param None
        @return RET_SUCCESS : If arguments are valid
        @return Non-zero value : If arguments are invalid
    """

    global ARGS

    # Initialize the result to Success
    tc_result = error_codes.RET_SUCCESS

    # Error checks which are common for all test cases can be done only once
    # during execution of the first test case
    if ARGS['tc_first'] == "True":
        # Check if MCPI TF scripts root path is specified.
        if 'ScriptsRootPath' not in ARGS:
            print("\nError " + str(error_codes.ERR_SCRIPTROOTPATH_NOT_FOUND) + \
                  ": MCPI Test Framework Script root path not specified as argument - Please " + \
                  "check and try again\n")
            sys.stdout.flush()
            return error_codes.ERR_CRITICAL_ERROR
        # Check if the MCPI TF scripts root path specified is valid.
        elif not os.path.isdir(ARGS['ScriptsRootPath']):
            print("\nError " + str(error_codes.ERR_SCRIPTROOTPATH_NOT_FOUND) + \
                  ": MCPI Test Framework Script root path " + ARGS['ScriptsRootPath'] + \
                  " not found - Please check and try again\n")
            sys.stdout.flush()
            return error_codes.ERR_CRITICAL_ERROR

        # Check if Common scripts root path is specified.
        if 'CommonScriptsRootPath' not in ARGS:
            print("\nError " + str(error_codes.ERR_SCRIPTROOTPATH_NOT_FOUND) + \
                  ": Common Script root path not specified as argument - Please " + \
                  "check and try again\n")
            sys.stdout.flush()
            return error_codes.ERR_CRITICAL_ERROR
        # Check if Common scripts root path specified is valid.
        elif not os.path.isdir(ARGS['CommonScriptsRootPath']):
            print("\nError " + str(error_codes.ERR_SCRIPTROOTPATH_NOT_FOUND) + \
                  ": Common Script root path " + ARGS['CommonScriptsRootPath'] + \
                  " not found - Please check and try again\n")
            sys.stdout.flush()
            return error_codes.ERR_CRITICAL_ERROR
        # if Common scripts root path specified is valid, then it will execute below code

        # Check if Test root path is specified
        if 'TestRootPath' not in ARGS:
            print("\nError " + str(error_codes.ERR_CRITICAL_ERROR) + \
                  ": Test root path not specified as argument - Please " + \
                  "check and try again\n")
            sys.stdout.flush()
            return error_codes.ERR_CRITICAL_ERROR
        # Check if Test root path specified is valid
        elif not os.path.isdir(ARGS['TestRootPath']):
            print("\nError " + str(error_codes.ERR_TESTCASEDIR_NOT_FOUND) + \
                  ": Test root path " + ARGS['TestRootPath'] + \
                  " not found - Please check and try again\n")
            sys.stdout.flush()
            return error_codes.ERR_CRITICAL_ERROR

        # check if platform value is valid
        if 'platform' not in ARGS:
            print("\nError platform not found" + \
                  ": Test script not specified as argument - Please " + \
                  "check and try again\n")
            sys.stdout.flush()
            return error_codes.TC_FAIL

        # check if platform value is valid
        if 'LogFile' not in ARGS:
            print("\nError LogFile not found" + \
                  ": Test script not specified as argument - Please " + \
                  "check and try again\n")
            sys.stdout.flush()
            return error_codes.TC_FAIL


    # Following checks are done for each test case
    if 'script_args' not in ARGS:
        print("\nError platform not found" + \
              ": Test script not specified as argument - Please " + \
              "check and try again\n")
        sys.stdout.flush()
        return error_codes.TC_FAIL
    if 'timeout' not in ARGS:
        print("\nError platform not found" + \
              ": Test script not specified as argument - Please " + \
              "check and try again\n")
        sys.stdout.flush()
        return error_codes.TC_FAIL
    if 'binary' not in ARGS:
        print("\nError params_chan" + \
              ": Test script not specified as argument - Please " + \
              "check and try again\n")
        sys.stdout.flush()
        return error_codes.TC_FAIL

    if ARGS['script_args'] != None:
        script_params = ARGS['script_args'].split(";")
        for script_prm in script_params:
            if script_prm.split("=")[0] == "pass_string":
                ARGS['pass_string'] = script_prm.split("=")[1]
            elif script_prm.split("=")[0] == "cmd_args":
                ARGS['cmd_args'] = script_prm.split("=")[1]
            elif script_prm.split("=")[0] == "core":
                ARGS['core'] = script_prm.split("=")[1]

    if 'cmd_args' not in ARGS:
        ARGS['cmd_args'] = ""
    if 'pass_string' not in ARGS:
        ARGS['pass_string'] = ""
    if 'core' not in ARGS:
        if ARGS['platform'] == "tda3xx":
            ARGS['core'] = "ipu1_0"
        else:
            ARGS['core'] = "a15_0"
    elif ARGS['core'] == "":
        if ARGS['platform'] == "tda3xx":
            ARGS['core'] = "ipu1_0"
        else:
            ARGS['core'] = "a15_0"

    # if Test script is available in args then it will execute below code
    # Normalize all paths
    ARGS['CommonScriptsRootPath'] = os.path.normpath(ARGS['CommonScriptsRootPath'])
    ARGS['ScriptsRootPath'] = os.path.normpath(ARGS['ScriptsRootPath'])
    ARGS['LogFile'] = os.path.normpath(ARGS['LogFile'])
    ARGS['TestRootPath'] = os.path.normpath(ARGS['TestRootPath'])

    return tc_result


def get_config_param():
    """ Update the configuration parameters used
        : comp port
        : ccs dss path
        : ccxml path
    """
    global com_port
    global ccs_dss_path
    global ccxml_path
    global pw_com_port
    global pw_conn_port

    bench_hndlr = bench_setup(os.path.normpath(ARGS['BenchFile']))

    com_port = bench_hndlr['config_param']['port']
    ccs_dss_path = bench_hndlr['config_param']['ccs_dss_path']
    ccxml_path = bench_hndlr['config_param']['ccxml_path']
    pw_com_port = bench_hndlr['config_param']['pw_com_port']
    pw_conn_port = bench_hndlr['config_param']['pw_conn_port']

    ccs_dss_path = os.path.normpath(ccs_dss_path)
    ccxml_path = os.path.normpath(ccxml_path)

def check_setup():
    """!
       This function chekck if the setup is connected and ready

       @param None
       @return RET_SUCCESS : For success execution of Testcase
       @return ERR_CRITICAL_ERROR : For error
    """
    ret_value = error_codes.RET_SUCCESS

    ccs_status = error_codes.ERR_CRITICAL_ERROR

    # Reset the TDA board
    if ARGS['tc_first'] == "True":
        ret_value = reset_board(pw_com_port, pw_conn_port)

    # >>>> Check if COM configured port is avaiable
    ser = serial.Serial()
    ser.port = com_port
    ser.baudrate = 115200
    ser.bytesize = serial.EIGHTBITS #number of bits per bytes
    ser.parity = serial.PARITY_NONE #set parity check: no parity
    ser.stopbits = serial.STOPBITS_ONE #number of stop bits
    ser.timeout = 2            #non-block read

    try:
        ser.open()
    except:
        TCLOG.write("\nError open serial port\n")
        return error_codes.ERR_CRITICAL_ERROR

    ser.flushInput()
    ser.flushOutput()
    ser.close()

    # Check if COM configured port is avaiable EDN <<<<

    # >>>> Check if Jtag is connected

    ccsjs_path = ARGS['CommonScriptsRootPath'] + "/ccs/pdk_check_setup.js"

    ccsjs_path = os.path.normpath(ccsjs_path)

    cmd = "cmd /c " + ccs_dss_path + " " + ccsjs_path + " " + ccxml_path + " " + \
          ARGS['platform']

    # Execute the Binary Call js script with paramenters
    result_pipe = subprocess.Popen(cmd, shell=False,\
                    stdin=subprocess.PIPE, stdout=subprocess.PIPE,\
                    stderr=subprocess.STDOUT)

    for line in result_pipe.stdout:
        if "Target Connect Sequence DONE" in line.decode():
            ccs_status = error_codes.RET_SUCCESS

    if ccs_status == error_codes.ERR_CRITICAL_ERROR:
        TCLOG.write("\nError Connecting with JTAG\n")
        return error_codes.ERR_CRITICAL_ERROR

    # Check f Jtag is connected  END <<<<<<<<

    # Reset the TDA board
    if ARGS['tc_first'] == "True":
        ret_value = reset_board(pw_com_port, pw_conn_port)

    return ret_value

def main():
    """!
       This is the main function of the TIMAC wrapper script
       This will invoke the Legacy test script for TIMAC written in Python 2.7

       @param None
       @return None
    """
    global LOCAL_TESTCASE_LOG_FILE
    global TCLOG

    # Parse command line arguments
    parse_arguments()

    # Get local testcase log file path and
    # Create a log file to capture the test execution log
    LOCAL_TESTCASE_LOG_FILE = os.path.join(ARGS['CommonScriptsRootPath'], \
                                  "logs",ARGS['platform'])

    # check if log directory exists create otherwise
    if not os.path.exists(LOCAL_TESTCASE_LOG_FILE):
        os.makedirs(LOCAL_TESTCASE_LOG_FILE)

    LOCAL_TESTCASE_LOG_FILE = os.path.join(LOCAL_TESTCASE_LOG_FILE, os.path.basename(ARGS['LogFile']))
    (return_value, TCLOG) = create_log_file(LOCAL_TESTCASE_LOG_FILE)

    #  If log file is not created then exit and return error code.
    if return_value != error_codes.RET_SUCCESS:
        sys.stdout.flush()
        sys.exit(return_value)
    # If log file is created successfully then it will execute below code.

    # check arguments for error
    return_value = error_checks()

    # If error checks for arguments fails then exit with error code.
    if return_value != error_codes.RET_SUCCESS:
        sys.stdout.flush()
        cleanup_and_exit(return_value, LOCAL_TESTCASE_LOG_FILE, ARGS['LogFile'], TCLOG)
    # If argument error checks successful then it will execute below code.

    # get configuration parameters
    get_config_param()

    # Check if the setup is connected
    if ARGS['tc_first'] == "True":
        return_value = check_setup()
        if return_value == error_codes.ERR_CRITICAL_ERROR:
            TCLOG.write("\nSetup is not available exiting\n")
            sys.stdout.flush()
            cleanup_and_exit(return_value, LOCAL_TESTCASE_LOG_FILE, ARGS['LogFile'], TCLOG)

    # TODO add the argument for RC also from the testlink
    TCLOG.write("\nPDK Commit ID: " + ARGS['pdk_commit_id'])
    TCLOG.write("\nBinary: " + ARGS['binary'])
    TCLOG.write("\nBuild: " + "Release")
    TCLOG.write("\nPlatform: " + ARGS['platform'])

    return_value = execute_testcase_pdk(ARGS['core'])
    if return_value != error_codes.RET_SUCCESS:
        sys.stdout.flush()
        cleanup_and_exit(return_value, LOCAL_TESTCASE_LOG_FILE, ARGS['LogFile'], TCLOG)

    # Close testcase log file
    TCLOG.write("\n**************************************************")
    TCLOG.write("\nTest case execution completed")
    TCLOG.write("\n**************************************************")
    TCLOG.write("\n")
    sys.stdout.flush()

    # Close testcase log file and exit with testcase return value
    cleanup_and_exit(return_value, LOCAL_TESTCASE_LOG_FILE, \
                                 ARGS['LogFile'], TCLOG)

    return return_value


def execute_testcase_pdk(tda_core):
    """!
       This function invokes the legacy test script for executing the PDK test case
       and invoke the script to connect and get the logs

       @param
       @return RET_SUCCESS : For success execution of Testcase
       @return ERR_TC_ERROR : For error while execution of Testcase
    """

    # Initialize the result to Success
    ret_value = error_codes.RET_SUCCESS

    TCLOG.write("\n\nCore: " + tda_core + "\n")

    # If error checks for arguments fails then exit with error code.
    if ret_value != error_codes.RET_SUCCESS:
        TCLOG.write("\nError: reset Board Failed")
        # Try running the example even if the board reset is failed
        ret_value = error_codes.RET_SUCCESS

    # if argument error checks successful then it will execute below code.

    # Adding exta time to launch CCS connect to core and load binary
    script_timeout = int(ARGS['timeout']) + ccs_load_time

    if tda_core == "ipu1_0":
        binary_path = ARGS['TestRootPath'] + "/" + ARGS['binary'] + "/bin/" + ARGS['platform'] + "-evm/" + ARGS['binary'] \
                      + "_ipu1_0_release.xem4"
    elif tda_core == "a15_0":
        binary_path = ARGS['TestRootPath'] + "/" + ARGS['binary'] + "/bin/" + ARGS['platform'] + "-evm/" + ARGS['binary'] \
                      + "_a15_0_release.xa15fg"
    elif tda_core == "c66x":
        binary_path = ARGS['TestRootPath'] + "/" + ARGS['binary'] + "/bin/" + ARGS['platform'] + "-evm/" + ARGS['binary'] \
                      + "_c66x_release.xe66"
    elif tda_core == "arp32_1":
        binary_path = ARGS['TestRootPath'] + "/" + ARGS['binary'] + "/bin/" + ARGS['platform'] + "-evm/" + ARGS['binary'] \
                      + "_arp32_1_release.xearp32F"
    else:
        TCLOG.write("\nError: core automation not supported")
        return error_codes.RET_FAIL

    ccsjs_path = ARGS['CommonScriptsRootPath'] + "/ccs/launch_pdk_" + ARGS['platform'] + ".js"

    ccsjs_path = os.path.normpath(ccsjs_path)
    binary_path = os.path.normpath(binary_path)
    binary_path = binary_path.replace("\\", "/")
    binary_path = binary_path.replace("//", "\\\\")

    cmd = "cmd /c " + ccs_dss_path + " " + ccsjs_path + " " + ccxml_path + " " + \
          binary_path + " " + tda_core + " " +  ARGS['timeout']

    # Execute the Binary Call js script with paramenters
    result_pipe = subprocess.Popen(cmd, shell=True,\
                    stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL,\
                    stderr=subprocess.STDOUT)

    # Function call to connect to com port and read data
    ret_value = get_logs(TCLOG, com_port, ARGS['pass_string'], script_timeout, ARGS['cmd_args'])

    # If error checks for arguments fails then exit with error code.
    if ret_value != error_codes.RET_SUCCESS:
        TCLOG.write("\nError: Get log failed")
        return ret_value
    # if argument error checks successful then it will execute below code.

    # Sync with Execute subprocess
    result_pipe.wait()

    sys.stdout.flush()

    # Return the test case result to the MCPI TEE2 main script
    return ret_value


if __name__ == '__main__':
    main()


