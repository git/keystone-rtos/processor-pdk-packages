//
//File Name: launch_pdk_tda3xx.js
//Description:
//   Add entries in CCS menu to do common activities
//
// Scritp expects following arguments
//   arguments[0] ccxml file path
//   arguments[1] binary path
//   arguments[2] core to be run
//   arguments[3] timeout

// Import the DSS packages into our namespace to save on typing
importPackage(Packages.com.ti.debug.engine.scripting)
importPackage(Packages.com.ti.ccstudio.scripting.environment)
importPackage(Packages.java.lang)
importPackage(java.io);
importPackage(java.lang);


// command line arguments passed
configFilePath = arguments[0];
binaryPath = arguments[1];
tdaCore = arguments[2];
timeOut = arguments[3];

disableGels=0


var ds;
var debugServer;
var script;
var dsCore;
var dsIPU_0;

function dsOpenSession()
{
	if(tdaCore == "ipu1_0")
	{
        dsCore = debugServer.openSession( ".*Cortex_M4_IPU1_C0" );
	}
    else if(tdaCore == "ipu1_1")
	{
        dsCore = debugServer.openSession( ".*Cortex_M4_IPU1_C1" );
	}
    else if(tdaCore == "c66x")
	{
        dsCore = debugServer.openSession( ".*C66xx_DSP1" );
	}
    else if(tdaCore == "c66xdsp_1")
	{
        dsCore = debugServer.openSession( ".*C66xx_DSP1" );
	}
    else if(tdaCore == "c66xdsp_2")
	{
        dsCore = debugServer.openSession( ".*C66xx_DSP2" );
	}
    else if(tdaCore == "arp32_1")
	{
        dsCore = debugServer.openSession( ".*ARP32_EVE_1" );
	}

	if(tdaCore != "ipu1_0" && tdaCore != "ipu1_0")
	{
		dsIPU_0 = debugServer.openSession( ".*Cortex_M4_IPU1_C0" );
	}

}

function connectTargets()
{
    // Connect targets
    //print("Connecting to IPU1_0!");

	if(tdaCore != "ipu1_0" && tdaCore != "ipu1_0")
	{
		dsIPU_0.target.connect();
	}

    script.setScriptTimeout(15000);

	if(tdaCore == "ipu1_0" || tdaCore == "ipu1_0")
	{
        dsCore.target.connect();
	}
    else if(tdaCore == "c66x")
	{
		try
		{
			dsIPU_0.expression.evaluate("DSP1SSClkEnable_API()");
		}
		catch(e)
		{
			print("Some error in GEL execution for DSP_0");
		}
		dsCore.target.connect();
    }
    else if(tdaCore == "c66xdsp_1")
	{
		try
		{
			dsIPU_0.expression.evaluate("DSP1SSClkEnable_API()");
		}
		catch(e)
		{
			print("Some error in GEL execution for DSP_0");
		}
		dsCore.target.connect();
    }
    else if(tdaCore == "c66xdsp_2")
	{
		try
		{
			dsIPU_0.expression.evaluate("DSP2SSClkEnable_API()");
		}
		catch(e)
		{
			print("Some error in GEL execution for DSP_1");
		}
		dsCore.target.connect();
    }
    else if(tdaCore == "arp32_1")
	{
		try
		{
			dsIPU_0.expression.evaluate("EVESSClkEnable_API()");
		}
		catch(e)
		{
			print("Some error in GEL execution for EVE");
		}
		dsCore.target.connect();
    }
	script.setScriptTimeout(-1);
}

function disconnectTargets()
{
	if(tdaCore != "ipu1_0"  && tdaCore != "ipu1_0")
	{
		dsIPU_0.target.disconnect();
	}
    dsCore.target.disconnect();
}

function loadTargets()
{
	try
	{
        dsCore.memory.loadProgram(binaryPath);
    }
    catch (e)
    {
        print("Problems while loading!");
    }
}

function resetTargets()
{
	if(tdaCore != "ipu1_0" && tdaCore != "ipu1_0")
	{
		if(dsIPU_0.target.isConnected())
	    {
		    dsIPU_0.target.reset();
		}
	}

	if(dsCore.target.isConnected())
	{
		dsCore.target.reset();
	}
}

function restartTargets()
{
	if(tdaCore != "ipu1_0" && tdaCore != "ipu1_0")
	{
		dsIPU_0.target.restart();
	}
    dsCore.target.restart();
}

function runTargets()
{
	script.setScriptTimeout(timeOut * 1000);
	if(tdaCore != "ipu1_0" && tdaCore != "ipu1_0")
	{
		try
		{
			// Run the core
			dsIPU_0.target.runAsynch();
		}
		catch (e)
		{
			print("Time our exiting app");
		}
	}
	try
	{
        // Run the program
        dsCore.target.runAsynch();
    }
    catch (e)
    {
        print("Time our exiting app");
    }
	script.setScriptTimeout(-1);
}

function haltTargets()
{
    script.setScriptTimeout(3000);

	if(tdaCore != "ipu1_0" && tdaCore != "ipu1_0")
	{
		try
		{
			// halt the core program
			if(dsIPU_0.target.isConnected()) dsIPU_0.target.halt();
		}
		catch (e)
		{
			print("Problems while halting!");
		}
	}
	try
	{
        // halt the core program
        if(dsCore.target.isConnected()) dsCore.target.halt();
    }
    catch (e)
    {
        print("Problems while halting!");
    }
    script.setScriptTimeout(-1);
}

// Check to see if running from within CCSv4 Scripting Console
var withinCCS = (ds !== undefined);

// Create scripting environment and get debug server if running standalone
if (!withinCCS)
{
    // Import the DSS packages into our namespace to save on typing
    importPackage(Packages.com.ti.debug.engine.scripting);
    importPackage(Packages.com.ti.ccstudio.scripting.environment);
    importPackage(Packages.java.lang);


    // Create our scripting environment object - which is the main entry point into any script and
    // the factory for creating other Scriptable servers and Sessions
    script = ScriptingEnvironment.instance();

    // Get the Debug Server and start a Debug Session
	debugServer = script.getServer("DebugServer.1");

	debugServer.setConfig(configFilePath);

	dsOpenSession();
	haltTargets();
	connectTargets();
	resetTargets();
	loadTargets();
	runTargets();

	// If needed wait can be added here but as the example will continue executing even when session is ternimate
	// wait is not required

    dsCore.terminate();
    debugServer.stop();
}
