//
//File Name: pdk_check_setup.js
//Description:
//   Add entries in CCS menu to do common activities
//
// Scritp expects following arguments
//   arguments[0] ccxml file path
//   arguments[1] platform

// Import the DSS packages into our namespace to save on typing
importPackage(Packages.com.ti.debug.engine.scripting)
importPackage(Packages.com.ti.ccstudio.scripting.environment)
importPackage(Packages.java.lang)
importPackage(java.io);
importPackage(java.lang);


// command line arguments passed
configFilePath = arguments[0];
platform = arguments[1];

disableGels=0


var ds;
var debugServer;
var script;
var dsCore;

function dsOpenSession()
{
	if(platform == "tda3xx")
	{
        dsCore = debugServer.openSession( ".*Cortex_M4_IPU1_C0" );
	}
    else
	{
        dsCore = debugServer.openSession( ".*CortexA15_0" );
	}
}

function connectTargets()
{
    // Connect targets
    //print("Connecting to IPU1_0!");

    script.setScriptTimeout(180000);
    dsCore.target.connect();

	script.setScriptTimeout(-1);

    dsCore.target.reset();
}

function disconnectTargets()
{
    dsCore.target.disconnect();
}

// Check to see if running from within CCSv4 Scripting Console
var withinCCS = (ds !== undefined);

// Create scripting environment and get debug server if running standalone
if (!withinCCS)
{
    // Import the DSS packages into our namespace to save on typing
    importPackage(Packages.com.ti.debug.engine.scripting);
    importPackage(Packages.com.ti.ccstudio.scripting.environment);
    importPackage(Packages.java.lang);


    // Create our scripting environment object - which is the main entry point into any script and
    // the factory for creating other Scriptable servers and Sessions
    script = ScriptingEnvironment.instance();

    // Get the Debug Server and start a Debug Session
	debugServer = script.getServer("DebugServer.1");

	debugServer.setConfig(configFilePath);

	dsOpenSession();
	connectTargets();
    disconnectTargets();
	// If needed wait can be added here but as the example will continue executing even when session is ternimate
	// wait is not required

    dsCore.terminate();
    debugServer.stop();
}
