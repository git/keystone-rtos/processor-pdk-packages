#!/bin/bash
#   ============================================================================
#   @file   pdk_traceability.sh
#
#   @desc   Script to generate the requirement and design traceability matrix
#           for all mcal drivers
#
#   ============================================================================
#   Revision History
#   01-Apr-2015 Sivaraj         Initial draft.
#   01-Oct-2015 Sivaraj         Added ID count checker
#
#   ============================================================================

#   ============================================================================
#   CAUTION: DO NOT INVOKE THE SCRIPTS AS 'gitadmin'.
#            IT WILL ADD OBJECT FILES IN BASE REPOSITORY; WHICH SHOULD IDEALLY
#            BE CLEAN.
#   ============================================================

platform_family=$1
count_check_enable=$2
if [ "$platform_family" == "" ]; then
  platform_family=tdaxx
fi

#Script to grep each line in a text in source and config folder
code_mapper() {
    type=$1
    source_path=$2
    req_file_path=$3

    while read line
    do
        first_char=${line:0:1}
        if [ "$first_char" == "#" ] ||
           [ "$first_char" == "" ]; then
            #Skip comment and empty line
            continue
        fi

        # Extract requirement
        req_id=`echo $line | cut -d";" -f2`

        #print the ID in first column
        echo -e -n $req_id:

        #Search ID and format output - split into file name and line number
        if [ "$type" == "requirement" ]; then
            grep -r -n --include \*.h --include \*.c "DOX_REQ_TAG($req_id)" "$source_path" | head -1 | cut -d":" -f1,2 | uniq
        fi
        result1=${PIPESTATUS[0]}
        if [ "$type" == "design" ]; then
            grep -r -n --include \*.h --include \*.c "DOX_DES_TAG($req_id)" "$source_path" | head -1 | cut -d":" -f1,2 | uniq
        fi

        #Print dummy column (:) if nothing is found!!
        result2=${PIPESTATUS[0]}
        if [ "$result1" != "0" ] &&
           [ "$result2" != "0" ]; then
            echo :
        fi
    done <$req_file_path
}

#Script to check if the ID are occurring multiple times
count_checker() {
    source_path=$1
    req_file_path=$2
    while read line
    do
        # Extract requirement
        req_id=`echo $line | cut -d";" -f2`

        #Count ID
        num_count=`grep -r --include \*.h --include \*.c --include \*.xdm --exclude-dir=$platform_exclude "$req_id" "$source_path" | wc -l`
        if [ $num_count -gt 1 ]; then
            echo $req_id occurred $num_count times!! Each ID should be present only once!! Cross check!!
        fi
    done <$req_file_path
}

#Script to do post processing of generated CSV file
#   - Replace : with ,
#   - Remove relative path
proc_csv() {
    csv_path=$1

    #Replace : with ,
    sed -i "s/:/,/g" $csv_path

    #Remove relative path
    sed -i "s:../../packages/:packages/:g" $csv_path
}

run_tracer() {
    module=$1
    type=$2
    socType=$3

    #Exclude the other platform
    if [ "$socType" == "tda2xx" ]; then
      config_folder_suffix=TI_TDA2XX
    fi
    if [ "$socType" == "tda3xx" ]; then
      config_folder_suffix=TI_TDA3XX
    fi
    if [ "$socType" == "" ]; then
        config_folder_suffix=TI_TDAXX
    fi

    #create csv header
    echo $type ID,File,Line Number > output/PDK_"$platform_family"_"$module"_"$type"_Traceability.csv

    #generate traceability
    code_mapper $type ../../packages/ti $type/"$module"_"$type"_list.txt >> output/PDK_"$platform_family"_"$module"_"$type"_Traceability.csv
    proc_csv output/PDK_"$platform_family"_"$module"_"$type"_Traceability.csv

    #check repeated ID
    if [ "$count_check_enable" == "yes" ]; then
        count_checker ../../packages/ti $type/"$module"_"$type"_list.txt
    fi

    echo "$type" Traceability completed for $module!!
}

mkdir -p output

# PDK requirement traceability
run_tracer dss requirement
run_tracer ipclite requirement

# PDK design traceability
run_tracer dss design
run_tracer ipclite design
