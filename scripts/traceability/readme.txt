Usage Steps:
------------
1. Run the pdk_traceability.sh from git bash. Output CSV file for each module is generated in output/ folder
2. Adding New Modules
    - Add the requirement or design ID list file similar to <Requirement or Design>/<Module>_<Type>_List.txt
      where <Type> could be "Requirement" or "Design"
    - Each requirement or design ID should be in a separate line
    - Once done, add the module entry similar to <Dss> in pdk_traceability.sh file
    - The steps for adding requirement and design traceability are same;
      only the $type will be "Requirement" or "Design"
    - Use # for commenting, the script will skip those lines starting with # (first char, no space)