#Lines starting with # are ignored
#Module;Requirement ID;Description;
IPCLITE;PDK-1405;IPCLIB: The ipclib shall be able to send and receive 32bit value using mailbox interrupt to/from multiple remote processors. If there are N processors in the SOC, NxN communication with 32 bit payload should be possible.
IPCLITE;PDK-1406;IPCLIB: The ipclib ISR shall not have busy loop. It should either deliver a message or error status after specific / predicatable time out / wait count.
IPCLITE;PDK-1407;IPCLIB: The ipclib shall be able to support multiple interrupt per processor coming from different processors on single interrupt line depending upon nature of interrupt controller. ipclib based on configuration should figure out which mailboxes to read.
IPCLITE;PDK-1408;IPCLIB:The ipclib shall be able to register call back functions to receive payloads from remote preocessors. Callback functions shall be uniquely identified by remote processor id and event number. Upon registering same function more than once on same combination of event id and remote proc id error should be returned.
IPCLITE;PDK-1409;IPCLIB: The ipclib should not assume interrupt numbers/ maibox ids. It should be statically configurable by the application writer.
IPCLITE;PDK-1410;IPCLIB: The ipclib shall support API for getting unique self processor Id
IPCLITE;PDK-1411;IPCLIB: The ipclib shall support module intialization function where it receives configuration about the platform from application, it should store the configuration and use it during subsequent API execution, It should allocate all resources needed by ipclib in this.
IPCLITE;PDK-1412;IPCLIB: The ipclib shall not miss interrupts, irrespective of order of writes to mailboxes from remote processors.
IPCLITE;PDK-1413;IPCLIB: The ipclib shall have registerEvent, sendEvent and unregisterEvent functions and they should return appropriate error codes upon invalid parameters being passed or in case of error conditions like timeouts / registering multiple functions for same evenId and remote core etc.
IPCLITE;PDK-1414;IPCLIB: The ipclib shall support multiple EVENTS over single interrupt line between pair of processors, which means different callback functions can be registered from application expecting interrupts from one remote processor.
IPCLITE;PDK-1415;IPCLIB: The ipclib shall support mapping between processor ids and names, so that applications need not
IPCLITE;PDK-1416;IPCLIB : If the mailbox is FIFO full, sender should have option to wait / no wait in a busy loop. If means sender can choose to wait in busy loop till mailbox fifo has free space or can choose to check for legitimate error and try after sometime
IPCLITE;PDK-1417;IPCLIB: The ipclib shall be able to accept configuration about number of processors in the
IPCLITE;PDK-1418;IPCLIB: The ipclib shall support module de-intialization function where it should free all acquired resources.
IPCLITE;PDK-1419;IPCLIB: The ipclib shall be able to unregister callback
IPCLITE;PDK-1420;IPCLIB: The ipclib shall have single instance per platform and should not use any dynamic allocation
IPCLITE;PDK-1422;IPCLIB: The ipclib shall not enable / disable clks for mailbox IP
IPCLITE;PDK-1424;IPCLIB: The ipclib shall support single instance per platform
IPCLITE;PDK-1426;IPCLIB: Module instance creation: init parameters - shall receive and store configuration provided by application
IPCLITE;PDK-1429;IPCLIB: The ipclib should not assume interrupt numbers/ mailbox ids. It should be statically configurable by the application writer.
IPCLITE;PDK-1430;IPCLIB: The ipclib shall not miss interrupts, irrespective of order of writes to mailboxes from remote processors.
IPCLITE;PDK-1432;IPCLIB: The ipclib shall be able to support multiple interrupt per processor coming from different processors on single interrupt line depending upon nature of interrupt controller. ipclib based on configuration should figure out which mailboxes to read.
IPCLITE;PDK-1434;IPCLIB: API's registerEvent, sendEvent, unregisterEvent shall return appropriate error codes
IPCLITE;PDK-1435;IPCLIB: The ipclib shall be able to send 32bit value using mailbox interrupt to multiple remote processors. If there are N processors in the SOC, NxN communication with 32 bit payload should be possible on Tx path
IPCLITE;PDK-1438;IPCLIB:The ipclib shall be able to register call back functions to receive payloads from remote processors. Callback functions shall be uniquely identified by remote processor id and event number.
IPCLITE;PDK-1479;IPCLIB: shall allocate all required resources
IPCLITE;PDK-1480;IPCLIB: Shall use the configuration done in init across all other APIs
IPCLITE;PDK-1481;IPCLIB: IPCLib Shall not allocate memory dynamically
IPCLITE;PDK-1482;IPCLIB: The ipclib shall be able to receive 32bit value using mailbox interrupt from multiple remote processors. If there are N processors in the SOC, NxN communication with 32 bit payload should be possible on Rx path
IPCLITE;PDK-1586;IPCLIB: The ipclib shall not assume any order of booting, ipclib shall not support synchronization between cores within platform it will be handled by application
IPCLITE;PDK-1592;IPCLIB: Shall support mailbox disable using remote core Id
IPCLITE;PDK-1604;IPCLIB:Shall support mailbox enable using remote core Id
