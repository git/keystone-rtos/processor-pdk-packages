#!/bin/bash
#   ============================================================================
#   @file   stw_sync_lld_misc.sh
#
#   @desc   Script to sync-up the LLD/misc source code with starterware repo.
#           This will copy the source files from STW repo and move them as per
#           PDK dir structure. This also does the generic macro replacements.
#
#   ============================================================================
#   Revision History
#   08-Nov-2016 Sivaraj         Initial draft.
#
#   ============================================================================

#   ============================================================================
#   CAUTION: DO NOT INVOKE THE SCRIPTS AS 'gitadmin'.
#            IT WILL ADD OBJECT FILES IN BASE REPOSITORY; WHICH SHOULD IDEALLY
#            BE CLEAN.
#   ============================================================
# Command: stw_sync_lld_misc.sh <$stw_dir>
# Example (Linux): /datalocal/daily_build/scripts/stw_sync_lld_misc.sh /datalocal/daily_build/starterware/starterware_
# Example (Windows from git bash):                stw_sync_lld_misc.sh "/d/Vayu/Source/starterware/starterware_"

export PATH=$PATH:/bin
date=`date '+%b_%d_%Y'`
date_print=`date '+%d-%b-%Y %I:%M:%S %p'`
month=`date '+%b_%Y'`
year=`date '+%Y'`
if [ "$OS" == "" ]; then
  echo "No OS defined, defaulting to linux"
  OS=linux
fi

#Get user input
working_dir=`pwd`
stw_dir=$1
if [ "$stw_dir" == "" ]; then
  stw_dir=$working_dir/../../../starterware/starterware_
  if [ ! -d "$stw_dir" ]; then
    echo "Kindly provide Starterware path to copy files from!!"
    exit
  fi
fi
pdk_dir=$working_dir/../..
stw_lld_dir=$pdk_dir/packages/ti/drv/stw_lld
sync_temp_dir=$working_dir/temp_sync

echo "PDK STW_LLD-Starterware Sync-up"
echo "Date: $date_print"
echo ""

sedErrCheck() {
if [ $1 -ne 0 ]; then
    echo "Error occurred @ $2!!"
fi
}

############## Start clean ##############
echo [INFO] Start Clean!! Deleting tobe copied files/folders...
rm -rf $sync_temp_dir
#I2C LLD
rm -rf $stw_lld_dir/i2clld/*.h
rm -rf $stw_lld_dir/i2clld/src
#STW Platforms
rm -rf $stw_lld_dir/platform/*.h
rm -rf $stw_lld_dir/platform/src
#STW UART Console
rm -rf $stw_lld_dir/uartconsole/*.h
rm -rf $stw_lld_dir/uartconsole/src
#STW FATLIB
rm -rf $stw_lld_dir/fatlib/*.h
rm -rf $stw_lld_dir/fatlib/src
rm -rf $stw_lld_dir/fatlib/fatlib_edma
rm -rf $stw_lld_dir/fatlib/fatfs
#STW Board
rm -rf $stw_lld_dir/boards/src
rm -rf $stw_lld_dir/boards/*.h
#STW Devices
rm -rf $stw_lld_dir/devices/src
rm -rf $stw_lld_dir/devices/*.h
#Example utility
rm -rf $stw_lld_dir/examples/utility/*.h
rm -rf $stw_lld_dir/examples/src

############## Create required folders ##############
echo [INFO] Creating required folders...
mkdir -p $sync_temp_dir
mkdir -p $sync_temp_dir/i2clld/src
mkdir -p $stw_lld_dir/i2clld/src
mkdir -p $sync_temp_dir/platform/src/tda2xx
mkdir -p $sync_temp_dir/platform/src/tda3xx
mkdir -p $stw_lld_dir/platform/src/tda2xx
mkdir -p $stw_lld_dir/platform/src/tda3xx
mkdir -p $sync_temp_dir/uartconsole/src
mkdir -p $stw_lld_dir/uartconsole/src
mkdir -p $sync_temp_dir/fatlib/src
mkdir -p $stw_lld_dir/fatlib/src
mkdir -p $sync_temp_dir/fatlib/fatlib_edma
mkdir -p $stw_lld_dir/fatlib/fatlib_edma
mkdir -p $sync_temp_dir/fatlib/fatfs
mkdir -p $stw_lld_dir/fatlib/fatfs
mkdir -p $sync_temp_dir/boards/src
mkdir -p $stw_lld_dir/boards/src
mkdir -p $sync_temp_dir/devices/src
mkdir -p $stw_lld_dir/devices/src
mkdir -p $sync_temp_dir/examples/utility/src
mkdir -p $stw_lld_dir/examples/utility/src

############## Copy STW files to temp folder ##############
echo [INFO] Copying files from STW/BSP to temp folder...
#Copy source, includes and other files
cp -r $stw_dir/include/i2clib/*.h               $sync_temp_dir/i2clld
cp -r $stw_dir/i2clib/*                         $sync_temp_dir/i2clld/src
cp -r $stw_dir/include/platforms/*.h            $sync_temp_dir/platform
cp -r $stw_dir/platform/*                       $sync_temp_dir/platform/src
cp -r $stw_dir/system_config/tda2xx_common/*    $sync_temp_dir/platform/src/tda2xx
cp -r $stw_dir/system_config/tda3xx_common/*    $sync_temp_dir/platform/src/tda3xx
cp -r $stw_dir/utils/uart_console/*.h           $sync_temp_dir/uartconsole
cp -r $stw_dir/utils/uart_console/*.c           $sync_temp_dir/uartconsole/src
cp -r $stw_dir/fatlib/*.h                       $sync_temp_dir/fatlib
cp -r $stw_dir/fatlib/*.c                       $sync_temp_dir/fatlib/src
cp -r $stw_dir/fatlib/fatlib_edma/*.c           $sync_temp_dir/fatlib/fatlib_edma
cp -r $stw_dir/fatlib/fatfs/*                   $sync_temp_dir/fatlib/fatfs
cp -r $stw_dir/include/boards/*.h               $sync_temp_dir/boards
cp -r $stw_dir/boards/*                         $sync_temp_dir/boards/src
cp -r $stw_dir/include/devices/*.h              $sync_temp_dir/devices
cp -r $stw_dir/devices/*                        $sync_temp_dir/devices/src
cp -r $stw_dir/examples/utility/*.h             $sync_temp_dir/examples/utility/
cp -r $stw_dir/examples/utility/*.c             $sync_temp_dir/examples/utility/src

#Move SOC specific files inside SOC folder
mv -f $sync_temp_dir/platform/src/platform_tda2xx*      $sync_temp_dir/platform/src/tda2xx
mv -f $sync_temp_dir/platform/src/stw_platformTda2xx*   $sync_temp_dir/platform/src/tda2xx
mv -f $sync_temp_dir/platform/src/platform_tda3xx*      $sync_temp_dir/platform/src/tda3xx
mv -f $sync_temp_dir/platform/src/stw_platformTda3xx*   $sync_temp_dir/platform/src/tda3xx

#Remove unwanted files
rm $sync_temp_dir/i2clld/src/Makefile
rm $sync_temp_dir/platform/src/Makefile
rm $sync_temp_dir/platform/src/filelist.mk
rm $sync_temp_dir/platform/src/tda2xx/filelist.mk
rm $sync_temp_dir/platform/src/tda3xx/filelist.mk
rm $sync_temp_dir/platform/src/ti814x/filelist.mk
rm $sync_temp_dir/boards/src/Makefile
rm $sync_temp_dir/boards/src/filelist.mk
rm $sync_temp_dir/boards/src/stw_boardTI814x*
rm $sync_temp_dir/devices/src/Makefile
rm $sync_temp_dir/devices/src/filelist.mk
rm $sync_temp_dir/devices/src/ioexp/filelist.mk

############## Replace macros ##############
#echo [INFO] Finding and replacing STW/BSP macros to PDK macros \(You can ignore \"sed: no input files\" messages!!\)...
echo [INFO] Finding and replacing STW/BSP macros to PDK macros...
cd $sync_temp_dir
# _BUILD to SOC_
grep -r -l 'TDA2XX_BUILD'       * | xargs sed -i -e 's|TDA2XX_BUILD|SOC_TDA2XX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA2PX_BUILD'       * | xargs sed -i -e 's|TDA2PX_BUILD|SOC_TDA2PX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA2EX_BUILD'       * | xargs sed -i -e 's|TDA2EX_BUILD|SOC_TDA2EX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA3XX_BUILD'       * | xargs sed -i -e 's|TDA3XX_BUILD|SOC_TDA3XX|g';sedErrCheck $? $LINENO

############## Replace include paths ##############
echo [INFO] Replacing include path changes for PDK folder structure...
cd $sync_temp_dir
grep -r -l '#include "'                             * | xargs sed -i -e 's|#include "|#include <|g';sedErrCheck $? $LINENO
grep -r -l '\.h"'                                   * | xargs sed -i -e 's|\.h"|\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <hw/hw_'                       * | xargs sed -i -e 's|#include <hw/hw_|#include <hw_|g';sedErrCheck $? $LINENO
grep -r -l '#include <hw_types'                     * | xargs sed -i -e 's|#include <hw_types|#include <ti/csl/hw_types|g';sedErrCheck $? $LINENO
cd $sync_temp_dir/uartconsole/src
#Replace file location change specifically for soc_defines.h
sed -i '/<soc_defines\.h>/d'                            $sync_temp_dir/uartconsole/src/uartConsole.c
grep -r -l '#include <soc_defines\.h>'              * | xargs sed -i -e 's|#include <soc_defines\.h>|#include <ti/csl/soc\.h>|g';sedErrCheck $? $LINENO
cd $sync_temp_dir
sed -i -e 's|#include <soc_defines\.h>|#include <ti/csl/soc\.h>|g'  $sync_temp_dir/fatlib/hsmmcsd_API.h
sed -i '/<hw_ctrl_core_pad_io\.h>/d'                                $sync_temp_dir/platform/src/platform.c
sed -i '/<hw_ctrl_core_pad_io\.h>/d'                                $sync_temp_dir/platform/src/stw_platform.c
sed -i '/<hw_ctrl_core_pad_io\.h>/d'                                $sync_temp_dir/platform/src/tda2xx/platform_tda2xx.c
sed -i '/<hw_ctrl_core_pad_io\.h>/d'                                $sync_temp_dir/platform/src/tda2xx/platform_tda2xx_pad_config.c
sed -i '/<hw_ctrl_core_pad_io\.h>/d'                                $sync_temp_dir/platform/src/tda2xx/stw_platformTda2xx.c
sed -i '/<hw_ctrl_core_pad_io\.h>/d'                                $sync_temp_dir/platform/src/tda3xx/platform_tda3xx.c
sed -i '/<hw_ctrl_core_pad_io\.h>/d'                                $sync_temp_dir/platform/src/tda3xx/platform_tda3xx_pad_config.c
sed -i '/<hw_ctrl_core_pad_io\.h>/d'                                $sync_temp_dir/platform/src/tda3xx/stw_platformTda3xx.c
sed -i '/hw_ctrl_core_irq_dma\.h/d'                                 $sync_temp_dir/platform/src/tda3xx/stw_platformTda3xx.c
sed -i '/hw_ctrl_wkup\.h/d'                                         $sync_temp_dir/platform/src/tda3xx/stw_platformTda3xx.c
cd $sync_temp_dir
#Replace file location changes
grep -r -l '#include <soc\.h>'                      * | xargs sed -i -e 's|#include <soc\.h>|#include <ti/csl/soc\.h>|g';sedErrCheck $? $LINENO
grep -r -l 'stw_dataTypes\.h'                       * | xargs sed -i -e 's|#include <ti/drv/vps/include/common/stw_dataTypes\.h>|#include <ti/csl/tistdtypes\.h>|g';sedErrCheck $? $LINENO
grep -r -l 'common/stw_types\.h'                    * | xargs sed -i -e 's|common/stw_types\.h|ti/csl/csl_types\.h|g';sedErrCheck $? $LINENO
grep -r -l 'i2clib/lld_hsi2c\.h'                    * | xargs sed -i -e 's|i2clib/lld_hsi2c\.h|ti/drv/stw_lld/i2clld/lld_hsi2c\.h|g';sedErrCheck $? $LINENO
grep -r -l 'interrupt\.h'                           * | xargs sed -i -e 's|interrupt\.h|ti/csl/arch/csl_arch\.h|g';sedErrCheck $? $LINENO
grep -r -l '#include <edma\.h>'                     * | xargs sed -i -e 's|#include <edma\.h>|#include <ti/csl/csl_edma\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <uart\.h>'                     * | xargs sed -i -e 's|#include <uart\.h>|#include <ti/csl/csl_uart\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <platforms/stw_platform\.h>'   * | xargs sed -i -e 's|#include <platforms/stw_platform\.h>|#include <ti/drv/stw_lld/platform/stw_platform\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <platform\.h>'                 * | xargs sed -i -e 's|#include <platform\.h>|#include <ti/drv/stw_lld/platform/platform\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <hsmmcsd_edma\.h>'             * | xargs sed -i -e 's|#include <hsmmcsd_edma\.h>|#include <ti/drv/stw_lld/fatlib/hsmmcsd_edma\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <mmcsd_proto\.h>'              * | xargs sed -i -e 's|#include <mmcsd_proto\.h>|#include <ti/drv/stw_lld/fatlib/mmcsd_proto\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <hs_mmcsdlib\.h>'              * | xargs sed -i -e 's|#include <hs_mmcsdlib\.h>|#include <ti/drv/stw_lld/fatlib/hs_mmcsdlib\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <hsmmcsd_API\.h>'              * | xargs sed -i -e 's|#include <hsmmcsd_API\.h>|#include <ti/drv/stw_lld/fatlib/hsmmcsd_API\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <ff\.h>'                       * | xargs sed -i -e 's|#include <ff\.h>|#include <ti/drv/stw_lld/fatlib/fatfs/ff\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <fatfs\/ff\.h>'                * | xargs sed -i -e 's|#include <fatfs\/ff\.h>|#include <ti/drv/stw_lld/fatlib/fatfs/ff\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <integer\.h>'                  * | xargs sed -i -e 's|#include <integer\.h>|#include <ti/drv/stw_lld/fatlib/fatfs/integer\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <ffconf\.h>'                   * | xargs sed -i -e 's|#include <ffconf\.h>|#include <ti/drv/stw_lld/fatlib/fatfs/ffconf\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <osal/'                        * | xargs sed -i -e 's|#include <osal/|#include <ti/drv/vps/include/osal/|g';sedErrCheck $? $LINENO
grep -r -l '#include <dma_xbar\.h>'                 * | xargs sed -i -e 's|#include <dma_xbar\.h>|#include <ti/drv/stw_lld/platform/dma_xbar\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <irq_xbar\.h>'                 * | xargs sed -i -e 's|#include <irq_xbar\.h>|#include <ti/drv/stw_lld/platform/irq_xbar\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <chip_config\.h>'              * | xargs sed -i -e 's|#include <chip_config\.h>|#include <ti/drv/stw_lld/platform/chip_config\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <dma_xbar_event_ids\.h>'       * | xargs sed -i -e 's|#include <dma_xbar_event_ids\.h>|#include <ti/drv/stw_lld/platform/dma_xbar_event_ids\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <irq_xbar_interrupt_ids\.h>'   * | xargs sed -i -e 's|#include <irq_xbar_interrupt_ids\.h>|#include <ti/drv/stw_lld/platform/irq_xbar_interrupt_ids\.h>|g';sedErrCheck $? $LINENO
grep -r -l '<uartStdio\.h>'                         * | xargs sed -i -e 's|<uartStdio\.h>|<ti/drv/stw_lld/uartconsole/uartStdio\.h>|g';sedErrCheck $? $LINENO
grep -r -l '<uartConsole\.h>'                       * | xargs sed -i -e 's|<uartConsole\.h>|<ti/drv/stw_lld/uartconsole/uartConsole\.h>|g';sedErrCheck $? $LINENO
grep -r -l 'boards\/stw_board\.h'                   * | xargs sed -i -e 's|boards\/stw_board\.h|ti/drv/stw_lld/boards/stw_board\.h|g';sedErrCheck $? $LINENO
grep -r -l 'devices\/stw_device\.h'                 * | xargs sed -i -e 's|devices\/stw_device\.h|ti/drv/stw_lld/devices/stw_device\.h|g';sedErrCheck $? $LINENO
grep -r -l 'devices\/stw_deviceIoexp\.h'            * | xargs sed -i -e 's|devices\/stw_deviceIoexp\.h|ti/drv/stw_lld/devices/stw_deviceIoexp\.h|g';sedErrCheck $? $LINENO
grep -r -l '#include <gpio_v2\.h>'                  * | xargs sed -i -e 's|#include <gpio_v2\.h>|#include <ti/csl/csl_gpio\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <hw_ctrl_core_pad_io\.h>'      * | xargs sed -i -e 's|#include <hw_ctrl_core_pad_io\.h>|#include <ti/csl/soc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <stw_boardPriv\.h>'            * | xargs sed -i -e 's|#include <stw_boardPriv\.h>|#include <ti/drv/stw_lld/boards/src/stw_boardPriv\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <hw_counter_32k\.h>'           * | xargs sed -i -e 's|#include <hw_counter_32k\.h>|#include <ti/csl/cslr_synctimer\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <ammu\.h>'                     * | xargs sed -i -e 's|#include <ammu\.h>|#include <ti/csl/arch/csl_arch\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <stwutils_app\.h>'             * | xargs sed -i -e 's|#include <stwutils_app\.h>|#include <ti/drv/stw_lld/examples/utility/stwutils_app\.h>|g';sedErrCheck $? $LINENO
#Replace file/function/enums rename changes
grep -r -l '<hsi2c\.h'                              * | xargs sed -i -e 's|<hsi2c\.h|<ti/csl/csl_i2c\.h|g';sedErrCheck $? $LINENO
grep -r -l '<hw_ctrl_core_irq_dma\.h>'              * | xargs sed -i -e 's|<hw_ctrl_core_irq_dma\.h>|<ti/csl/soc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '<pmhal_prcm\.h>'                        * | xargs sed -i -e 's|<pmhal_prcm\.h>|<ti/drv/pm/pmhal\.h>|g';sedErrCheck $? $LINENO
grep -r -l '<hs_mmcsd\.h'                           * | xargs sed -i -e 's|<hs_mmcsd\.h|<ti/csl/csl_mmcsd\.h|g';sedErrCheck $? $LINENO
#Remove other unwanted header inclusion
grep -r -l 'common\/stw_dataTypes\.h'               * | xargs sed -i '/common\/stw_dataTypes\.h/d';sedErrCheck $? $LINENO
grep -r -l '#include <soc_defines\.h>'              * | xargs sed -i '/#include <soc_defines\.h>/d';sedErrCheck $? $LINENO
grep -r -l '#include <tda2xx\/soc_defines\.h>'      * | xargs sed -i '/#include <tda2xx\/soc_defines\.h>/d';sedErrCheck $? $LINENO
grep -r -l '#include <tda3xx\/soc_defines\.h>'      * | xargs sed -i '/#include <tda3xx\/soc_defines\.h>/d';sedErrCheck $? $LINENO
grep -r -l 'stw_config\.h'                          * | xargs sed -i '/stw_config\.h/d';sedErrCheck $? $LINENO
grep -r -l '<hw_ctrl_core\.h>'                      * | xargs sed -i '/<hw_ctrl_core\.h>/d';sedErrCheck $? $LINENO
grep -r -l '<hw_ctrl_wkup\.h>'                      * | xargs sed -i '/<hw_ctrl_wkup\.h>/d';sedErrCheck $? $LINENO
grep -r -l '<hw_ctrl_core_sec\.h>'                  * | xargs sed -i '/<hw_ctrl_core_sec\.h>/d';sedErrCheck $? $LINENO
grep -r -l '<hw_core_cm_core\.h>'                   * | xargs sed -i '/<hw_core_cm_core\.h>/d';sedErrCheck $? $LINENO
grep -r -l '<hw_l4per_cm_core\.h>'                  * | xargs sed -i '/<hw_l4per_cm_core\.h>/d';sedErrCheck $? $LINENO
grep -r -l '<hw_core_prm\.h>'                       * | xargs sed -i '/<hw_core_prm\.h>/d';sedErrCheck $? $LINENO
grep -r -l '<hw_ckgen_cm_core\.h>'                  * | xargs sed -i '/<hw_ckgen_cm_core\.h>/d';sedErrCheck $? $LINENO
grep -r -l '<hw_l3init_cm_core\.h>'                 * | xargs sed -i '/<hw_l3init_cm_core\.h>/d';sedErrCheck $? $LINENO
grep -r -l '<hw_l3init_prm\.h>'                     * | xargs sed -i '/<hw_l3init_prm\.h>/d';sedErrCheck $? $LINENO
grep -r -l '<hw_wkupaon_cm\.h>'                     * | xargs sed -i '/<hw_wkupaon_cm\.h>/d';sedErrCheck $? $LINENO
grep -r -l '<hw_ipu_cm_core_aon\.h>'                * | xargs sed -i '/<hw_ipu_cm_core_aon\.h>/d';sedErrCheck $? $LINENO
grep -r -l '<pmhal_cm\.h>'                          * | xargs sed -i '/<pmhal_cm\.h>/d';sedErrCheck $? $LINENO
grep -r -l '<pm_types\.h>'                          * | xargs sed -i '/<pm_types\.h>/d';sedErrCheck $? $LINENO
grep -r -l '<pmhal_mm\.h>'                          * | xargs sed -i '/<pmhal_mm\.h>/d';sedErrCheck $? $LINENO
sed -i '/#if defined (SOC_TDA2XX) || defined (SOC_TDA2PX) || defined (SOC_TDA2EX) || defined (SOC_TDA3XX)/I,+1 d' $sync_temp_dir/boards/src/stw_board.c
#Remove other unwanted code
sed -i '/On Cortex M4/,/^$/d' platform/src/platform.h
sed -i '/define GPMC_CONFIG/,/^$/d' platform/src/platformPriv.h
#Insert PDK specific code
sed -i '/#define PLATFORM_PRIV_H_/a \
\
\#include <ti/csl/cslr_gpmc.h>' platform/src/platformPriv.h

sed -i '/pmhal_mm\.h>/d' examples/utility/src/stwutils_app.c
sed -i '/hw_device_prm\.h>/d' examples/utility/src/stwutils_app.c
sed -i 's|BUILD_A15|__ARM_ARCH_7A__|g' platform/src/stw_platform.c
sed -i 's|BUILD_M4|__TI_ARM_V7M4__|g' platform/src/stw_platform.c

############## Copy files from temp folder to right PDK location ##############
echo [INFO] Copying files from temp folder to right PDK location...
cd $sync_temp_dir
#I2C LLD
cp -r $sync_temp_dir/i2clld/*                       $stw_lld_dir/i2clld/
#STW Platform
cp -r $sync_temp_dir/platform/*                     $stw_lld_dir/platform/
mv $stw_lld_dir/platform/src/platform.h             $stw_lld_dir/platform/
#STW UART Console
cp -r $sync_temp_dir/uartconsole/*                  $stw_lld_dir/uartconsole/
#STW FATLIB
cp -r $sync_temp_dir/fatlib/*                       $stw_lld_dir/fatlib/
#STW Boards
cp -r $sync_temp_dir/boards/*                       $stw_lld_dir/boards/
#STW Devices
cp -r $sync_temp_dir/devices/*                      $stw_lld_dir/devices/
#STW Example utility
cp -r $sync_temp_dir/examples/utility/*             $stw_lld_dir/examples/utility/


############## Checkout not to be copied files ##############
cd $stw_lld_dir
git checkout $stw_lld_dir/i2clld/src/makefile
git checkout $stw_lld_dir/platform/src/makefile
git checkout $stw_lld_dir/uartconsole/src/makefile
git checkout $stw_lld_dir/fatlib/src/makefile
git checkout $stw_lld_dir/fatlib/fatlib_edma/makefile
git checkout $stw_lld_dir/platform/chip_config.h
git checkout $stw_lld_dir/platform/dma_xbar.h
git checkout $stw_lld_dir/platform/dma_xbar_event_ids.h
git checkout $stw_lld_dir/platform/irq_xbar.h
git checkout $stw_lld_dir/platform/irq_xbar_interrupt_ids.h
git checkout $stw_lld_dir/boards/src/makefile
git checkout $stw_lld_dir/devices/src/makefile
git checkout $stw_lld_dir/examples/utility/src/makefile

############## clean-up temp files ##############
cd $working_dir
rm -rf $sync_temp_dir

#Check if any of the replaced macros are still present in the source
./stw_sync_check_macros.sh $stw_lld_dir

exit
