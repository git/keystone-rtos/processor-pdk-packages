#!/bin/bash
#   ============================================================================
#   @file   stw_sync_safety.sh
#
#   @desc   Script to sync-up the Safety Library source code with starterware repo.
#           This will copy the source files from STW repo and move them as per
#           PDK dir structure. This also does the generic macro replacements.
#
#   ============================================================================
#   Revision History
#   08-Nov-2016 Sivaraj         Initial draft.
#   30-May-2017 Sivaraj         Updated for separate LLD.
#
#   ============================================================================

#   ============================================================================
#   CAUTION: DO NOT INVOKE THE SCRIPTS AS 'gitadmin'.
#            IT WILL ADD OBJECT FILES IN BASE REPOSITORY; WHICH SHOULD IDEALLY
#            BE CLEAN.
#   ============================================================
# Command: stw_sync_safety.sh <$stw_dir>
# Example (Linux): /datalocal/daily_build/scripts/stw_sync_safety.sh /datalocal/daily_build/starterware/starterware_
# Example (Windows from git bash):                stw_sync_safety.sh "/d/Vayu/Source/starterware/starterware_"

export PATH=$PATH:/bin
date=`date '+%b_%d_%Y'`
date_print=`date '+%d-%b-%Y %I:%M:%S %p'`
month=`date '+%b_%Y'`
year=`date '+%Y'`
if [ "$OS" == "" ]; then
  echo "No OS defined, defaulting to linux"
  OS=linux
fi

#Get user input
working_dir=`pwd`
stw_dir=$1
if [ "$stw_dir" == "" ]; then
  stw_dir=$working_dir/../../../starterware/starterware_
  if [ ! -d "$stw_dir" ]; then
    echo "Kindly provide Starterware path to copy files from!!"
    exit
  fi
fi
pdk_dir=$working_dir/../..
ipc_dir=$pdk_dir/packages/ti/drv/ipc_lite
fw_dir=$pdk_dir/packages/ti/drv/fw_l3l4
diag_dir=$pdk_dir/packages/ti/diag
sync_temp_dir=$working_dir/temp_sync

echo "PDK Starterware Safety Library Sync-up"
echo "Date: $date_print"
echo ""

sedErrCheck() {
if [ $1 -ne 0 ]; then
    echo "Error occurred @ $2!!"
fi
}

############## Start clean ##############
echo [INFO] Start Clean!! Deleting tobe copied files/folders...
rm -rf $sync_temp_dir
rm -rf $diag_dir/src
rm -rf $diag_dir/*.h
rm -rf $ipc_dir/src
rm -rf $ipc_dir/*.h
rm -rf $fw_dir/src
rm -rf $fw_dir/*.h

############## Create required folders ##############
echo [INFO] Creating required folders...
mkdir -p $sync_temp_dir/diag/src
mkdir -p $sync_temp_dir/ipc/src
mkdir -p $sync_temp_dir/fw/src

############## Copy STW files to temp folder ##############
echo [INFO] Copying files from STW/BSP to temp folder...
#Copy source, includes and other files
cp $stw_dir/include/diagLib/*                                     $sync_temp_dir/diag
cp $stw_dir/diagLib/*                                             $sync_temp_dir/diag/src
cp $stw_dir/include/ipclib/*                                      $sync_temp_dir/ipc
cp -r $stw_dir/ipclib/*                                           $sync_temp_dir/ipc/src
cp $stw_dir/include/safetylib/*                                   $sync_temp_dir/fw
cp $stw_dir/safetylib/*                                           $sync_temp_dir/fw/src
#Remove unwanted files
rm -f $sync_temp_dir/diag/src/Makefile
rm -f $sync_temp_dir/ipc/src/Makefile
rm -f $sync_temp_dir/safety/src/Makefile
rm -f $sync_temp_dir/fw/src/Makefile

############## Replace macros ##############
#echo [INFO] Finding and replacing STW/BSP macros to PDK macros \(You can ignore \"sed: no input files\" messages!!\)...
echo [INFO] Finding and replacing STW/BSP macros to PDK macros...
cd $sync_temp_dir
# _BUILD to SOC_
grep -r -l 'TDA2XX_BUILD'       * | xargs sed -i -e 's|TDA2XX_BUILD|SOC_TDA2XX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA2PX_BUILD'       * | xargs sed -i -e 's|TDA2PX_BUILD|SOC_TDA2PX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA2EX_BUILD'       * | xargs sed -i -e 's|TDA2EX_BUILD|SOC_TDA2EX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA3XX_BUILD'       * | xargs sed -i -e 's|TDA3XX_BUILD|SOC_TDA3XX|g';sedErrCheck $? $LINENO
# Add SOC_AM support
grep -r -l 'defined (SOC_TDA2XX)' * | xargs sed -i -e 's:defined (SOC_TDA2XX):defined (SOC_TDA2XX) || defined (SOC_AM572x):g';sedErrCheck $? $LINENO
grep -r -l 'defined (SOC_TDA2EX)' * | xargs sed -i -e 's:defined (SOC_TDA2EX):defined (SOC_TDA2EX) || defined (SOC_AM571x):g';sedErrCheck $? $LINENO
# DSP/EVE core change
grep -r -l 'BUILD_DSP1'         * | xargs sed -i -e 's|BUILD_DSP1|BUILD_DSP_1|g';sedErrCheck $? $LINENO
grep -r -l 'BUILD_DSP2'         * | xargs sed -i -e 's|BUILD_DSP2|BUILD_DSP_2|g';sedErrCheck $? $LINENO
grep -r -l 'BUILD_EVE1'         * | xargs sed -i -e 's|BUILD_EVE1|BUILD_ARP32_1|g';sedErrCheck $? $LINENO
grep -r -l 'BUILD_EVE2'         * | xargs sed -i -e 's|BUILD_EVE2|BUILD_ARP32_2|g';sedErrCheck $? $LINENO
grep -r -l 'BUILD_EVE3'         * | xargs sed -i -e 's|BUILD_EVE3|BUILD_ARP32_3|g';sedErrCheck $? $LINENO
grep -r -l 'BUILD_EVE4'         * | xargs sed -i -e 's|BUILD_EVE4|BUILD_ARP32_4|g';sedErrCheck $? $LINENO
grep -r -l 'BUILD_EVE'          * | xargs sed -i -e 's|BUILD_EVE|BUILD_ARP32|g';sedErrCheck $? $LINENO

############## Replace include paths ##############
echo [INFO] Replacing include path changes for PDK folder structure...
cd $sync_temp_dir
grep -r -l '#include "'                             * | xargs sed -i -e 's|#include "|#include <|g';sedErrCheck $? $LINENO
grep -r -l '\.h"'                                   * | xargs sed -i -e 's|\.h"|\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <hw_types'                     * | xargs sed -i -e 's|#include <hw_types|#include <ti/csl/hw_types|g';sedErrCheck $? $LINENO
#Remove unwanted header inclusion
sed -i '/#include <soc_defines\.h>/d'                   diag/src/diagLib_can.c;sedErrCheck $? $LINENO
sed -i '/#include <soc_defines\.h>/d'                   diag/src/diagLib_crc.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_core_cm_core\.h>/d'               diag/src/diagLib_crc.c;sedErrCheck $? $LINENO
sed -i '/#include <ammu\.h>/d'                          diag/src/diagLib_eccIpu.c;sedErrCheck $? $LINENO
sed -i '/defined (SOC_TDA3XX)/d'                        diag/src/diagLib_eccIpu.c;sedErrCheck $? $LINENO
sed -i '/#include <unicache\.h>/d'                      diag/src/diagLib_eccIpu.c;sedErrCheck $? $LINENO
sed -i '/#endif/d'                                      diag/src/diagLib_eccIpu.c;sedErrCheck $? $LINENO
sed -i '/#include <soc_defines\.h>/d'                   fw/src/fwlibl3.c;sedErrCheck $? $LINENO
sed -i '/#include <soc_defines\.h>/d'                   fw/src/fwlibl4.c;sedErrCheck $? $LINENO
sed -i '/#include <soc_defines\.h>/d'                   ipc/src/tda2xx/ipclib_interruptTda2xx.c;sedErrCheck $? $LINENO
sed -i '/#include <soc_defines\.h>/d'                   ipc/src/tda2px/ipclib_interruptTda2px.c;sedErrCheck $? $LINENO
#Replace file location changes
grep -r -l '#include <soc\.h>'                      * | xargs sed -i -e 's|#include <soc\.h>|#include <ti/csl/soc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <soc_defines\.h>'              * | xargs sed -i -e 's|#include <soc_defines\.h>|#include <ti/csl/soc\.h>|g';sedErrCheck $? $LINENO
grep -r -l 'stw_dataTypes\.h'                       * | xargs sed -i -e 's|#include <ti/drv/vps/include/common/stw_dataTypes\.h>|#include <ti/csl/tistdtypes\.h>|g';sedErrCheck $? $LINENO
grep -r -l 'common/stw_types\.h'                    * | xargs sed -i -e 's|common/stw_types\.h|ti/csl/csl_types\.h|g';sedErrCheck $? $LINENO
grep -r -l '<interrupt\.h>'                         * | xargs sed -i -e 's|<interrupt\.h>|<ti/csl/arch/csl_arch\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <adc\.h>'                      * | xargs sed -i -e 's|#include <adc\.h>|#include <ti/csl/csl_adc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <crc\.h>'                      * | xargs sed -i -e 's|#include <crc\.h>|#include <ti/csl/csl_crc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <hw_crc\.h>'                   * | xargs sed -i -e 's|#include <hw_crc\.h>|#include <ti/csl/cslr_crc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <dcan\.h>'                     * | xargs sed -i -e 's|#include <dcan\.h>|#include <ti/csl/csl_dcan\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <edma\.h>'                     * | xargs sed -i -e 's|#include <edma\.h>|#include <ti/csl/csl_edma\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <emif\.h>'                     * | xargs sed -i -e 's|#include <emif\.h>|#include <ti/csl/csl_emif\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <l3fw\.h>'                     * | xargs sed -i -e 's|#include <l3fw\.h>|#include <ti/csl/csl_l3fw\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <l4fw\.h>'                     * | xargs sed -i -e 's|#include <l4fw\.h>|#include <ti/csl/csl_l4fw\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <mailbox\.h>'                  * | xargs sed -i -e 's|#include <mailbox\.h>|#include <ti/csl/csl_mailbox\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <mcspi\.h>'                    * | xargs sed -i -e 's|#include <mcspi\.h>|#include <ti/csl/csl_mcspi\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <ocmc_ecc_l1\.h>'              * | xargs sed -i -e 's|#include <ocmc_ecc_l1\.h>|#include <ti/csl/csl_ocmc_ecc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <ipu_ecc\.h>'                  * | xargs sed -i -e 's|#include <ipu_ecc\.h>|#include <ti/csl/arch/csl_arch\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <hw_counter_32k\.h>'           * | xargs sed -i -e 's|#include <hw_counter_32k\.h>|#include <ti/csl/cslr_synctimer\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <timer\.h>'                    * | xargs sed -i -e 's|#include <timer\.h>|#include <ti/csl/csl_timer\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <hw_ipu_unicache_cfg\.h>'      * | xargs sed -i -e 's|#include <hw_ipu_unicache_cfg\.h>|#include <ti/csl/arch/csl_arch\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <hw_ipu_unicache_mmu\.h>'      * | xargs sed -i -e 's|#include <hw_ipu_unicache_mmu\.h>|#include <ti/csl/cslr_cache\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <diagLib/'                     * | xargs sed -i -e 's|#include <diagLib/|#include <ti/diag/|g';sedErrCheck $? $LINENO
grep -r -l '#include <osal/'                        * | xargs sed -i -e 's|#include <osal/|#include <ti/drv/vps/include/osal/|g';sedErrCheck $? $LINENO
grep -r -l '#include <pm\/pm_types\.h>'             * | xargs sed -i -e 's|#include <pm\/pm_types\.h>|#include <ti/drv/pm/include/pm_types\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <ipclib_interruptTda2xx\.h>'   * | xargs sed -i -e 's|#include <ipclib_interruptTda2xx\.h>|#include <ti/drv/ipc_lite/ipclib_interruptTda2xx\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <ipclib_interruptTda2px\.h>'   * | xargs sed -i -e 's|#include <ipclib_interruptTda2px\.h>|#include <ti/drv/ipc_lite/ipclib_interruptTda2px\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <ipclib_interruptTda2ex\.h>'   * | xargs sed -i -e 's|#include <ipclib_interruptTda2ex\.h>|#include <ti/drv/ipc_lite/ipclib_interruptTda2ex\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <ipclib_interruptTda3xx\.h>'   * | xargs sed -i -e 's|#include <ipclib_interruptTda3xx\.h>|#include <ti/drv/ipc_lite/ipclib_interruptTda3xx\.h>|g';sedErrCheck $? $LINENO
grep -r -l 'in ipclib_interrupt.h>'                 * | xargs sed -i -e 's|in ipclib_interrupt.h>|in ipclib_interrupt.h\"|g';sedErrCheck $? $LINENO
#Remove other unwanted header inclusion
grep -r -l 'common/stw_dataTypes\.h'                * | xargs sed -i '/common\/stw_dataTypes\.h/d';sedErrCheck $? $LINENO
grep -r -l '#include <platform\.h>'                 * | xargs sed -i '/#include <platform\.h>/d';sedErrCheck $? $LINENO
grep -r -l '#include <hw_l3fw\.h>'                  * | xargs sed -i '/#include <hw_l3fw\.h>/d';sedErrCheck $? $LINENO
grep -r -l '#include <hw_ctrl_core\.h>'             * | xargs sed -i '/#include <hw_ctrl_core\.h>/d';sedErrCheck $? $LINENO

############## Copy files from temp folder to right PDK location ##############
############## Before to which create required folders in CSL directory #######
mkdir -p $diag_dir/src
mkdir -p $ipc_dir/src
mkdir -p $fw_dir/src
echo [INFO] Copying files from temp folder to right PDK location...
cd $sync_temp_dir
cp $sync_temp_dir/diag/*.h                           $diag_dir
cp $sync_temp_dir/diag/src/*                         $diag_dir/src
cp $sync_temp_dir/ipc/*.h                            $ipc_dir
cp -r $sync_temp_dir/ipc/src/*                       $ipc_dir/src
cp $sync_temp_dir/fw/*.h                             $fw_dir
cp $sync_temp_dir/fw/src/*                           $fw_dir/src

############## Checkout not to be copied files ##############
cd $diag_dir
git checkout src/makefile
cd $fw_dir
git checkout src/makefile
cd $ipc_dir
git checkout src/makefile

############## clean-up temp files ##############
cd $working_dir
rm -rf $sync_temp_dir

#Check if any of the replaced macros are still present in the source
./stw_sync_check_macros.sh $diag_dir
./stw_sync_check_macros.sh $fw_dir
./stw_sync_check_macros.sh $ipc_dir

exit
