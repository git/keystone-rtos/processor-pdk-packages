#!/bin/bash
#   ============================================================================
#   @file   stw_sync_security.sh
#
#   @desc   Script to sync-up the Security Library source code with starterware repo.
#           This will copy the source files from STW repo and move them as per
#           PDK dir structure. This also does the generic macro replacements.
#
#   ============================================================================
#   Revision History
#   08-Nov-2016 Sivaraj         Initial draft.
#
#   ============================================================================

#   ============================================================================
#   CAUTION: DO NOT INVOKE THE SCRIPTS AS 'gitadmin'.
#            IT WILL ADD OBJECT FILES IN BASE REPOSITORY; WHICH SHOULD IDEALLY
#            BE CLEAN.
#   ============================================================
# Command: stw_sync_security.sh <$stw_dir>
# Example (Linux): /datalocal/daily_build/scripts/stw_sync_security.sh /datalocal/daily_build/starterware/starterware_
# Example (Windows from git bash):                stw_sync_security.sh "/d/Vayu/Source/starterware/starterware_"

export PATH=$PATH:/bin
date=`date '+%b_%d_%Y'`
date_print=`date '+%d-%b-%Y %I:%M:%S %p'`
month=`date '+%b_%Y'`
year=`date '+%Y'`
if [ "$OS" == "" ]; then
  echo "No OS defined, defaulting to linux"
  OS=linux
fi

#Get user input
working_dir=`pwd`
stw_dir=$1
if [ "$stw_dir" == "" ]; then
  stw_dir=$working_dir/../../../starterware/starterware_
  if [ ! -d "$stw_dir" ]; then
    echo "Kindly provide Starterware path to copy files from!!"
    exit
  fi
fi
pdk_dir=$working_dir/../..
security_dir=$pdk_dir/packages/ti/boot/sbl_auto/security
sync_temp_dir=$working_dir/temp_sync

echo "PDK Starterware Security Library Sync-up"
echo "Date: $date_print"
echo ""

sedErrCheck() {
if [ $1 -ne 0 ]; then
    echo "Error occurred @ $2!!"
fi
}

############## Start clean ##############
echo [INFO] Start Clean!! Deleting tobe copied files/folders...
rm -rf $sync_temp_dir
rm -rf $security_dir/*.h
rm -rf $security_dir/include
rm -rf $security_dir/docs
rm -rf $security_dir/examples
rm -rf $security_dir/src
rm -rf $security_dir/tools

############## Create required folders ##############
echo [INFO] Creating required folders...
mkdir -p $sync_temp_dir/include

############## Copy STW files to temp folder ##############
echo [INFO] Copying files from STW/BSP to temp folder...
#Copy source, includes and other files
cp -r $stw_dir/include/security/*               $sync_temp_dir/include
cp -r $stw_dir/security/*                       $sync_temp_dir/
rm -rf $sync_temp_dir/tools/endian_swap/
mv $sync_temp_dir/docs/StarterWare_Security_Addon_UserGuide.pdf $sync_temp_dir/docs/Security_Addon_UserGuide.pdf
mv $sync_temp_dir/docs/ti_starterware_tdaxx_security_addon_manifest.html $sync_temp_dir/docs/ti_tdaxx_security_addon_manifest.html

#Remove unwanted files
rm $sync_temp_dir/Makefile

############## Replace macros ##############
#echo [INFO] Finding and replacing STW/BSP macros to PDK macros \(You can ignore \"sed: no input files\" messages!!\)...
echo [INFO] Finding and replacing STW/BSP macros to PDK macros...
cd $sync_temp_dir
# _BUILD to SOC_
#grep -r -l 'TDA2XX_BUILD'       * | xargs sed -i -e 's|TDA2XX_BUILD|SOC_TDA2XX|g';sedErrCheck $? $LINENO
#grep -r -l 'TDA2EX_BUILD'       * | xargs sed -i -e 's|TDA2EX_BUILD|SOC_TDA2EX|g';sedErrCheck $? $LINENO
#grep -r -l 'TDA3XX_BUILD'       * | xargs sed -i -e 's|TDA3XX_BUILD|SOC_TDA3XX|g';sedErrCheck $? $LINENO
#grep -r -l 'DRA7XX_BUILD'       * | xargs sed -i -e 's|DRA7XX_BUILD|SOC_DRA7XX|g';sedErrCheck $? $LINENO

############## Replace include paths ##############
echo [INFO] Replacing include path changes for PDK folder structure...
cd $sync_temp_dir
grep -r -l '#include "'                             * | xargs sed -i -e 's|#include "|#include <|g';sedErrCheck $? $LINENO
grep -r -l '\.h"'                                   * | xargs sed -i -e 's|\.h"|\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <hw/hw_'                       * | xargs sed -i -e 's|#include <hw/hw_|#include <hw_|g';sedErrCheck $? $LINENO
grep -r -l '#include <hw_types'                     * | xargs sed -i -e 's|#include <hw_types|#include <ti/csl/hw_types|g';sedErrCheck $? $LINENO
#Replace file location changes
grep -r -l '#include <soc\.h>'                      * | xargs sed -i -e 's|#include <soc\.h>|#include <ti/csl/soc\.h>|g';sedErrCheck $? $LINENO
grep -r -l 'stw_dataTypes\.h'                       * | xargs sed -i -e 's|#include <ti/drv/vps/include/common/stw_dataTypes\.h>|#include <ti/csl/tistdtypes\.h>|g';sedErrCheck $? $LINENO
grep -r -l 'common/stw_types\.h'                    * | xargs sed -i -e 's|common/stw_types\.h|ti/csl/csl_types\.h|g';sedErrCheck $? $LINENO
grep -r -l 'interrupt\.h'                           * | xargs sed -i -e 's|interrupt\.h|ti/csl/arch/csl_arch\.h|g';sedErrCheck $? $LINENO
grep -r -l '#include <platform\.h>'                 * | xargs sed -i -e 's|#include <platform\.h>|#include <ti/drv/stw_lld/platform/platform\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <uartStdio\.h>'                * | xargs sed -i -e 's|#include <uartStdio\.h>|#include <ti/drv/stw_lld/uartconsole/uartStdio\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <stwutils_app\.h>'             * | xargs sed -i -e 's|#include <stwutils_app\.h>|#include <ti/drv/stw_lld/examples/utility/stwutils_app\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <aes128\.h>'                   * | xargs sed -i -e 's|#include <aes128\.h>|#include <ti/boot/sbl_auto/security/include/aes128\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <aes128_cbc\.h>'               * | xargs sed -i -e 's|#include <aes128_cbc\.h>|#include <ti/boot/sbl_auto/security/include/aes128_cbc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <sha256\.h>'                   * | xargs sed -i -e 's|#include <sha256\.h>|#include <ti/boot/sbl_auto/security/include/sha256\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <tda3xx/hw_fusefarm\.h>'       * | xargs sed -i -e 's|#include <tda3xx/hw_fusefarm\.h>|#include <ti/boot/sbl_auto/security/include/tda3xx/hw_fusefarm\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <tda3xx/efuse\.h>'             * | xargs sed -i -e 's|#include <tda3xx/efuse\.h>|#include <ti/boot/sbl_auto/security/include/tda3xx/efuse\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <tda3xx/tda3xx_gp_prime\.h>'   * | xargs sed -i -e 's|#include <tda3xx/tda3xx_gp_prime\.h>|#include <ti/boot/sbl_auto/security/include/tda3xx/tda3xx_gp_prime\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <sbl_lib_config\.h>'           * | xargs sed -i -e 's|#include <sbl_lib_config\.h>|#include <ti/boot/sbl_auto/sbl_lib/sbl_lib_config\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <sbl_lib/sbl_lib\.h>'          * | xargs sed -i -e 's|#include <sbl_lib/sbl_lib\.h>|#include <ti/boot/sbl_auto/sbl_lib/sbl_lib\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <unicache\.h>'                 * | xargs sed -i -e 's|#include <unicache\.h>|#include <ti/csl/arch/csl_arch\.h>|g';sedErrCheck $? $LINENO
#Replace file/function/enums rename changes
grep -r -l '<pmhal_prcm\.h>'                        * | xargs sed -i -e 's|<pmhal_prcm\.h>|<ti/drv/pm/pmhal\.h>|g';sedErrCheck $? $LINENO
#Remove other unwanted header inclusion
grep -r -l 'hw_core_cm_core\.h'                     * | xargs sed -i '/hw_core_cm_core\.h/d';sedErrCheck $? $LINENO
grep -r -l 'common/stw_dataTypes\.h'                * | xargs sed -i '/common\/stw_dataTypes\.h/d';sedErrCheck $? $LINENO
grep -r -l '#include <soc_defines\.h>'              * | xargs sed -i '/#include <soc_defines\.h>/d';sedErrCheck $? $LINENO
grep -r -l '#include <hw_ipu_unicache_mmu\.h>'      * | xargs sed -i '/#include <hw_ipu_unicache_mmu\.h>/d';sedErrCheck $? $LINENO
grep -r -l '#include <hw_ctrl_core\.h>'             * | xargs sed -i '/#include <hw_ctrl_core\.h>/d';sedErrCheck $? $LINENO
grep -r -l '#include <hw_ctrl_wkup\.h>'             * | xargs sed -i '/#include <hw_ctrl_wkup\.h>/d';sedErrCheck $? $LINENO
grep -r -l '#include <pmhal_mm\.h>'                 * | xargs sed -i '/#include <pmhal_mm\.h>/d';sedErrCheck $? $LINENO
grep -r -l '#include <pm\/pmhal\/pmhal_cm\.h>'      * | xargs sed -i '/#include <pm\/pmhal\/pmhal_cm\.h>/d';sedErrCheck $? $LINENO

############## Copy files from temp folder to right PDK location ##############
echo [INFO] Copying files from temp folder to right PDK location...
cd $sync_temp_dir
cp -r $sync_temp_dir/*                          $security_dir

############## Checkout not to be copied files ##############
cd $security_dir
git checkout $security_dir/makefile
git checkout $security_dir/examples/efuse_app/makefile
git checkout $security_dir/docs

############## clean-up temp files ##############
cd $working_dir
rm -rf $sync_temp_dir

#Check if any of the replaced macros are still present in the source
./stw_sync_check_macros.sh $security_dir

exit
