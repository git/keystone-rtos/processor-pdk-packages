#!/bin/bash
#   ============================================================================
#   @file   stw_sync_examples_ut.sh
#
#   @desc   Script to sync-up the examples/UT from starterware repo.
#           This will copy the source files from STW repo and move them as per
#           PDK dir structure. This also does the generic macro replacements.
#
#   ============================================================================
#   Revision History
#   18-Mar-2017 Sivaraj         Initial draft.
#
#   ============================================================================
# Command: stw_sync_examples_ut.sh <$stw_dir>
# Example (Linux): /datalocal/daily_build/scripts/stw_sync_examples_ut.sh /datalocal/daily_build/starterware/starterware_
# Example (Windows from git bash):                stw_sync_examples_ut.sh "/d/Vayu/Source/starterware/starterware_"

export PATH=$PATH:/bin
date=`date '+%b_%d_%Y'`
date_print=`date '+%d-%b-%Y %I:%M:%S %p'`
month=`date '+%b_%Y'`
year=`date '+%Y'`
if [ "$OS" == "" ]; then
  echo "No OS defined, defaulting to linux"
  OS=linux
fi

#Get user input
working_dir=`pwd`
stw_dir=$1
if [ "$stw_dir" == "" ]; then
  stw_dir=$working_dir/../../../starterware/starterware_
  if [ ! -d "$stw_dir" ]; then
    echo "Kindly provide Starterware path to copy files from!!"
    exit
  fi
fi
pdk_dir=$working_dir/../..
stw_lld_dir=$pdk_dir/packages/ti/drv/stw_lld
sync_temp_dir=$working_dir/temp_sync
stw_test_dir=$stw_dir/../starterware_test

echo "PDK STW_LLD Example and UT Sync-up"
echo "Date: $date_print"
echo ""

sedErrCheck() {
if [ $1 -ne 0 ]; then
    echo "Error occurred @ $2!!"
fi
}

############## Start clean ##############
echo [INFO] Start Clean!! Deleting tobe copied files/folders...
rm -rf $sync_temp_dir
rm -rf $stw_lld_dir/examples/vps/
rm -rf $stw_lld_dir/unit_test

############## Create required folders ##############
echo [INFO] Creating required folders...
mkdir -p $sync_temp_dir
mkdir -p $sync_temp_dir/vps/vip_baremetal_app
mkdir -p $sync_temp_dir/vps/ovsensor_baremetal_app
mkdir -p $sync_temp_dir/unit_test/adcUt
mkdir -p $sync_temp_dir/unit_test/crcUt
mkdir -p $sync_temp_dir/unit_test/dcan
mkdir -p $sync_temp_dir/unit_test/dccUt
mkdir -p $sync_temp_dir/unit_test/eccUt
mkdir -p $sync_temp_dir/unit_test/esmUt
mkdir -p $sync_temp_dir/unit_test/mcspiUt
mkdir -p $sync_temp_dir/unit_test/qspiUt
mkdir -p $sync_temp_dir/unit_test/l3fwUt
mkdir -p $sync_temp_dir/unit_test/pmUt
mkdir -p $sync_temp_dir/unit_test/rtiUt
mkdir -p $sync_temp_dir/unit_test/mcanUt

############## Copy STW files to temp folder ##############
echo [INFO] Copying files from STW/BSP to temp folder...
cp -r $stw_dir/examples/vipCapt/*           $sync_temp_dir/vps/vip_baremetal_app
cp -r $stw_dir/examples/ov10630_sensor/*    $sync_temp_dir/vps/ovsensor_baremetal_app
cp -r $stw_test_dir/unit_test/adcUT/*       $sync_temp_dir/unit_test/adcUt
cp -r $stw_test_dir/unit_test/crcUT/*       $sync_temp_dir/unit_test/crcUt
cp -r $stw_test_dir/unit_test/dcan/*        $sync_temp_dir/unit_test/dcan
cp -r $stw_test_dir/unit_test/dccUT/*       $sync_temp_dir/unit_test/dccUt
cp -r $stw_test_dir/unit_test/eccUT/*       $sync_temp_dir/unit_test/eccUt
cp -r $stw_test_dir/unit_test/esmUT/*       $sync_temp_dir/unit_test/esmUt
cp -r $stw_test_dir/unit_test/mcspiUT/*     $sync_temp_dir/unit_test/mcspiUt
cp -r $stw_test_dir/unit_test/qspiUT/*      $sync_temp_dir/unit_test/qspiUt
cp -r $stw_test_dir/unit_test/L3FW/*        $sync_temp_dir/unit_test/l3fwUt
cp -r $stw_test_dir/unit_test/pm/*          $sync_temp_dir/unit_test/pmUt
cp -r $stw_test_dir/unit_test/rti/*         $sync_temp_dir/unit_test/rtiUt
cp -r $stw_test_dir/unit_test/mcanUt/*      $sync_temp_dir/unit_test/mcanUt
#Remove unwanted PM UT
rm -rf $sync_temp_dir/unit_test/pmUt/contisrv_pm_leakage_test
rm -rf $sync_temp_dir/unit_test/pmUt/sival_port_tests

############## Replace macros ##############
echo [INFO] Finding and replacing STW/BSP macros to PDK macros...
cd $sync_temp_dir
# _BUILD to SOC_
grep -r -l 'TDA2XX_BUILD'       * | xargs sed -i -e 's|TDA2XX_BUILD|SOC_TDA2XX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA2PX_BUILD'       * | xargs sed -i -e 's|TDA2PX_BUILD|SOC_TDA2PX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA2EX_BUILD'       * | xargs sed -i -e 's|TDA2EX_BUILD|SOC_TDA2EX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA3XX_BUILD'       * | xargs sed -i -e 's|TDA3XX_BUILD|SOC_TDA3XX|g';sedErrCheck $? $LINENO

############## Replace include paths ##############
echo [INFO] Replacing include path changes for PDK folder structure...
cd $sync_temp_dir
grep -r -l '#include "'                             * | xargs sed -i -e 's|#include "|#include <|g';sedErrCheck $? $LINENO
grep -r -l '\.h"'                                   * | xargs sed -i -e 's|\.h"|\.h>|g';sedErrCheck $? $LINENO
sed -i -e 's|MDIO_CTRL|CSL_MDIO_CONTROL_REG|g'          $sync_temp_dir/unit_test/dccUt/testLib/st_dccCommon.c
grep -r -l 'common/hw_pmhal_data\.h'                * | xargs sed -i -e 's|common/hw_pmhal_data\.h|ti/drv/pm/src/pmhal/prcm/common/hw_pmhal_data\.h|g';sedErrCheck $? $LINENO

############## Remove unused/unmapped requirements ##############
echo "[INFO] Remove unused/unmapped requirements (typically the head requirements)..."
grep -r -l ':SR2296:'                               * | xargs sed -i -e 's|:SR2296:|:|g';sedErrCheck $? $LINENO
grep -r -l ':SR2299:'                               * | xargs sed -i -e 's|:SR2299:|:|g';sedErrCheck $? $LINENO
grep -r -l ':SR2302:'                               * | xargs sed -i -e 's|:SR2302:|:|g';sedErrCheck $? $LINENO
grep -r -l ':SR2308:'                               * | xargs sed -i -e 's|:SR2308:|:|g';sedErrCheck $? $LINENO
grep -r -l ':SR2317:'                               * | xargs sed -i -e 's|:SR2317:|:|g';sedErrCheck $? $LINENO
grep -r -l ':SR2330:'                               * | xargs sed -i -e 's|:SR2330:|:|g';sedErrCheck $? $LINENO
grep -r -l ':SR2195:'                               * | xargs sed -i -e 's|:SR2195:|:|g';sedErrCheck $? $LINENO
grep -r -l ':SR2208:'                               * | xargs sed -i -e 's|:SR2208:|:|g';sedErrCheck $? $LINENO
grep -r -l ':SR2210:'                               * | xargs sed -i -e 's|:SR2210:|:|g';sedErrCheck $? $LINENO
grep -r -l ':SR2149:'                               * | xargs sed -i -e 's|:SR2149:|:|g';sedErrCheck $? $LINENO
grep -r -l ':SR2153:'                               * | xargs sed -i -e 's|:SR2153:|:|g';sedErrCheck $? $LINENO
grep -r -l ':SR2159:'                               * | xargs sed -i -e 's|:SR2159:|:|g';sedErrCheck $? $LINENO
grep -r -l ':SR2162:'                               * | xargs sed -i -e 's|:SR2162:|:|g';sedErrCheck $? $LINENO
grep -r -l ':SR2273:'                               * | xargs sed -i -e 's|:SR2273:|:|g';sedErrCheck $? $LINENO
grep -r -l ':SR2211:'                               * | xargs sed -i -e 's|:SR2211:|:|g';sedErrCheck $? $LINENO
grep -r -l ':SR2222:'                               * | xargs sed -i -e 's|:SR2222:|:|g';sedErrCheck $? $LINENO

############## Copy files from temp folder to right PDK location ##############
echo [INFO] Copying files from temp folder to right PDK location...
cd $sync_temp_dir
cp -r $sync_temp_dir/vps                    $stw_lld_dir/examples
cp -r $sync_temp_dir/unit_test              $stw_lld_dir

############## Replace include paths ##############
echo [INFO] Replacing include path changes for PDK folder structure...
cd $stw_lld_dir
#Remove other unwanted header inclusion
sed -i '/#include <soc_defines\.h>/d'               $stw_lld_dir/examples/vps/vip_baremetal_app/src/app_clk_prcm.c
sed -i '/#include <soc\.h>/d'                       $stw_lld_dir/examples/vps/vip_baremetal_app/src/main.c
sed -i '/#include <soc\.h>/d'                       $stw_lld_dir/examples/vps/ovsensor_baremetal_app/sensorconfig.c
sed -i '/#include <hw\/hw_ctrl_core_pad_io\.h>/d'   $stw_lld_dir/examples/vps/ovsensor_baremetal_app/sensorconfig.c
#Call generic script
$pdk_dir/docs/stw_bsp_include_path_migration.sh $stw_lld_dir/examples/   $pdk_dir no
$pdk_dir/docs/stw_bsp_include_path_migration.sh $stw_lld_dir/unit_test/  $pdk_dir no

############## Checkout not to be copied files ##############
cd $stw_lld_dir
git checkout $stw_lld_dir/examples/vps/vip_baremetal_app/makefile
git checkout $stw_lld_dir/examples/vps/ovsensor_baremetal_app/makefile
git checkout $stw_lld_dir/unit_test/stw_unit_test_component.mk
git checkout $stw_lld_dir/unit_test/adcUt/makefile
git checkout $stw_lld_dir/unit_test/crcUt/makefile
git checkout $stw_lld_dir/unit_test/dcan/dcanUt/makefile
git checkout $stw_lld_dir/unit_test/dcan/dcantestStub/makefile
git checkout $stw_lld_dir/unit_test/dccUt/makefile
git checkout $stw_lld_dir/unit_test/eccUt/makefile
git checkout $stw_lld_dir/unit_test/esmUt/makefile
git checkout $stw_lld_dir/unit_test/mcspiUt/makefile
git checkout $stw_lld_dir/unit_test/qspiUt/makefile
git checkout $stw_lld_dir/unit_test/l3fwUt/makefile
git checkout $stw_lld_dir/unit_test/l3fwUt/l3fw_permission_all/makefile
git checkout $stw_lld_dir/unit_test/l3fwUt/l3fw_writeOnly/makefile
git checkout $stw_lld_dir/unit_test/pmUt/makefile
git checkout $stw_lld_dir/unit_test/pmUt/pmlib_videopll/makefile
git checkout $stw_lld_dir/unit_test/pmUt/pmlib_clkrate/makefile
git checkout $stw_lld_dir/unit_test/pmUt/pmhal_vmapp/makefile
git checkout $stw_lld_dir/unit_test/pmUt/hwapps_char_tests/pmlib_idle/makefile
git checkout $stw_lld_dir/unit_test/pmUt/hwapps_char_tests/pmlib_pdoff/makefile
git checkout $stw_lld_dir/unit_test/pmUt/hwapps_char_tests/pmlib_autocg/makefile
git checkout $stw_lld_dir/unit_test/pmUt/pmhal_sw_warm_reset/makefile
git checkout $stw_lld_dir/unit_test/pmUt/pmlib_sysconfig/makefile
git checkout $stw_lld_dir/unit_test/pmUt/pmhal_dpll_core_sysclk2/makefile
git checkout $stw_lld_dir/unit_test/pmUt/pm_leakage_test/makefile
git checkout $stw_lld_dir/unit_test/pmUt/pmlib_videopll_calculator/makefile
git checkout $stw_lld_dir/unit_test/pmUt/pmhal_sw_cold_reset/makefile
git checkout $stw_lld_dir/unit_test/rtiUt/rti_dwwd_generate_reset/makefile
git checkout $stw_lld_dir/unit_test/rtiUt/rti_dwwd_nmi_generation/makefile
git checkout $stw_lld_dir/unit_test/rtiUt/rti_dwwd_reaction_change_in_closed_window/makefile
git checkout $stw_lld_dir/unit_test/rtiUt/rti_dwwd_reaction_change_in_open_window/makefile
git checkout $stw_lld_dir/unit_test/rtiUt/rti_dwwd_reset_status/makefile
git checkout $stw_lld_dir/unit_test/rtiUt/rti_dwwd_suspend_dsp_core0/makefile
git checkout $stw_lld_dir/unit_test/rtiUt/rti_dwwd_suspend_dsp_core1/makefile
git checkout $stw_lld_dir/unit_test/rtiUt/rti_dwwd_suspend_eve/makefile
git checkout $stw_lld_dir/unit_test/rtiUt/rti_dwwd_suspend_ipu_core0/makefile
git checkout $stw_lld_dir/unit_test/rtiUt/rti_dwwd_suspend_ipu_core1/makefile
git checkout $stw_lld_dir/unit_test/rtiUt/rti_dwwd_wz_100/makefile
git checkout $stw_lld_dir/unit_test/rtiUt/rti_dwwd_wz_12_5/makefile
git checkout $stw_lld_dir/unit_test/rtiUt/rti_dwwd_wz_25/makefile
git checkout $stw_lld_dir/unit_test/rtiUt/rti_dwwd_wz_3_125/makefile
git checkout $stw_lld_dir/unit_test/rtiUt/rti_dwwd_wz_50/makefile
git checkout $stw_lld_dir/unit_test/rtiUt/rti_dwwd_wz_6_25/makefile
git checkout $stw_lld_dir/unit_test/rtiUt/makefile
git checkout $stw_lld_dir/unit_test/lnk_arp32.cmd
git checkout $stw_lld_dir/unit_test/lnk_dsp.cmd
git checkout $stw_lld_dir/unit_test/lnk_m4.cmd
#Case sensitive filenames
if [ "$OS" == "linux" ]; then
  rm -f $stw_lld_dir/unit_test/adcUt/Makefile
  rm -f $stw_lld_dir/unit_test/crcUt/Makefile
  rm -f $stw_lld_dir/unit_test/dcan/dcanUt/Makefile
  rm -f $stw_lld_dir/unit_test/dcan/dcantestStub/Makefile
  rm -f $stw_lld_dir/unit_test/dccUt/Makefile
  rm -f $stw_lld_dir/unit_test/eccUt/Makefile
  rm -f $stw_lld_dir/unit_test/esmUt/Makefile
  rm -f $stw_lld_dir/unit_test/mcspiUt/Makefile
  rm -f $stw_lld_dir/unit_test/qspiUt/Makefile
  rm -f $stw_lld_dir/unit_test/pmUt/pm_leakage_test/Makefile
  rm -f $stw_lld_dir/unit_test/pmUt/pmhal_sw_cold_reset/Makefile
  rm -f $stw_lld_dir/unit_test/pmUt/pmhal_sw_warm_reset/Makefile
  rm -f $stw_lld_dir/unit_test/pmUt/pmlib_videopll/Makefile
  rm -f $stw_lld_dir/unit_test/pmUt/pmlib_videopll_calculator/Makefile
  rm -f $stw_lld_dir/unit_test/rtiUt/rti_dwwd_generate_reset/Makefile
  rm -f $stw_lld_dir/unit_test/rtiUt/rti_dwwd_nmi_generation/Makefile
  rm -f $stw_lld_dir/unit_test/rtiUt/rti_dwwd_reaction_change_in_closed_window/Makefile
  rm -f $stw_lld_dir/unit_test/rtiUt/rti_dwwd_reaction_change_in_open_window/Makefile
  rm -f $stw_lld_dir/unit_test/rtiUt/rti_dwwd_reset_status/Makefile
  rm -f $stw_lld_dir/unit_test/rtiUt/rti_dwwd_suspend_dsp_core0/Makefile
  rm -f $stw_lld_dir/unit_test/rtiUt/rti_dwwd_suspend_dsp_core1/Makefile
  rm -f $stw_lld_dir/unit_test/rtiUt/rti_dwwd_suspend_eve/Makefile
  rm -f $stw_lld_dir/unit_test/rtiUt/rti_dwwd_suspend_ipu_core0/Makefile
  rm -f $stw_lld_dir/unit_test/rtiUt/rti_dwwd_suspend_ipu_core1/Makefile
  rm -f $stw_lld_dir/unit_test/rtiUt/rti_dwwd_wz_100/Makefile
  rm -f $stw_lld_dir/unit_test/rtiUt/rti_dwwd_wz_12_5/Makefile
  rm -f $stw_lld_dir/unit_test/rtiUt/rti_dwwd_wz_25/Makefile
  rm -f $stw_lld_dir/unit_test/rtiUt/rti_dwwd_wz_3_125/Makefile
  rm -f $stw_lld_dir/unit_test/rtiUt/rti_dwwd_wz_50/Makefile
  rm -f $stw_lld_dir/unit_test/rtiUt/rti_dwwd_wz_6_25/Makefile
fi

############## clean-up temp files ##############
cd $working_dir
rm -rf $sync_temp_dir

#Requriement change for UT
find_replace() {
  input_file=$1
  target_file=$2

  #Replace one requirement at a time
  while read -r line
  do
    set -- $line
    #echo $1 to $2
    sed -i -e s/$1/$2/g $target_file
  done < $input_file

  # Check if we still have any old requriement ID
  num_count=`grep -r -e ':SR' "$target_file" | wc -l`
  if [ $num_count -gt 0 ]; then
    echo [WARN] ":SR" occurred $num_count times!! Old Requirement ID should not be there!! Cross check!!
  fi
  # Check if we still have any old requriement ID
  num_count=`grep -r -e 'PDP-' "$target_file" | wc -l`
  if [ $num_count -gt 0 ]; then
    echo [WARN] "PDP-" occurred $num_count times!! Old Requirement ID should not be there!! Cross check!!
  fi
}
find_replace requirement/misc_req.txt $stw_lld_dir/unit_test/adcUt/testInput/st_adcTestCases.h
find_replace requirement/misc_req.txt $stw_lld_dir/unit_test/crcUt/testInput/st_crcTestCases.h
find_replace requirement/misc_req.txt $stw_lld_dir/unit_test/dcan/dcanUt/testInput/st_dcanUtTestCases.h
find_replace requirement/misc_req.txt $stw_lld_dir/unit_test/dccUt/testInput/st_dccTestCases.h
find_replace requirement/misc_req.txt $stw_lld_dir/unit_test/eccUt/testInput/st_eccTestCases.h
find_replace requirement/misc_req.txt $stw_lld_dir/unit_test/esmUt/testInput/st_esmTestCases.h
find_replace requirement/misc_req.txt $stw_lld_dir/unit_test/mcspiUt/testInput/st_mcspiTestCases.h
find_replace requirement/misc_req.txt $stw_lld_dir/unit_test/qspiUt/testInput/st_qspiTestCases.h

#Check if any of the replaced macros are still present in the source
./stw_sync_check_macros.sh $stw_lld_dir/examples
./stw_sync_check_macros.sh $stw_lld_dir/unit_test

exit
