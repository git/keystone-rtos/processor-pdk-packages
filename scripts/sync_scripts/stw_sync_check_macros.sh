#!/bin/bash
#   ============================================================================
#   @file   stw_sync_check_macros.sh
#
#   @desc   Script to check if any of the replaced/not-to-be used macros are
#           still present in the PDK source
#
#   ============================================================================
#   Revision History
#   30-Nov-2015 Sivaraj         Initial draft.
#
#   ============================================================================

#   ============================================================================
#   CAUTION: DO NOT INVOKE THE SCRIPTS AS 'gitadmin'.
#            IT WILL ADD OBJECT FILES IN BASE REPOSITORY; WHICH SHOULD IDEALLY
#            BE CLEAN.
#   ============================================================
# Command: stw_sync_check_macros.sh <check_dir>

check_dir=$1

echo [INFO] Checking if any of the replaced/not-to-be used macros are still present in the PDK source location $check_dir...
num_count=`grep -r -e 'TDA2XX_BUILD' "$check_dir" | wc -l`
if [ $num_count -gt 0 ]; then
    echo [WARN] "TDA2XX_BUILD" occurred $num_count times in $check_dir!! This macro should not be there in source file!! Cross check!!
fi
num_count=`grep -r -e 'DRA7XX_BUILD' "$check_dir" | wc -l`
if [ $num_count -gt 0 ]; then
    echo [WARN] "DRA7XX_BUILD" occurred $num_count times in $check_dir!! This macro should not be there in source file!! Cross check!!
fi
num_count=`grep -r -e 'TDA2EX_BUILD' "$check_dir" | wc -l`
if [ $num_count -gt 0 ]; then
    echo [WARN] "TDA2EX_BUILD" occurred $num_count times in $check_dir!! This macro should not be there in source file!! Cross check!!
fi
num_count=`grep -r -e 'TDA3XX_BUILD' "$check_dir" | wc -l`
if [ $num_count -gt 0 ]; then
    echo [WARN] "TDA3XX_BUILD" occurred $num_count times in $check_dir!! This macro should not be there in source file!! Cross check!!
fi
num_count=`grep -r -e 'FAMILY_BUILD' "$check_dir" | wc -l`
if [ $num_count -gt 0 ]; then
    echo [WARN] "FAMILY_BUILD" occurred $num_count times in $check_dir!! This macro should not be there in source file!! Cross check!!
fi
num_count=`grep -rw -e 'BUILD_M4' "$check_dir" | wc -l`
if [ $num_count -gt 0 ]; then
    echo [WARN] "BUILD_M4" occurred $num_count times in $check_dir!! This macro should not be there in source file!! Cross check!!
fi
num_count=`grep -rw -e 'BUILD_A15' "$check_dir" | wc -l`
if [ $num_count -gt 0 ]; then
    echo [WARN] "BUILD_A15" occurred $num_count times in $check_dir!! This macro should not be there in source file!! Cross check!!
fi
num_count=`grep -rw -e 'BUILD_DSP' "$check_dir" | wc -l`
if [ $num_count -gt 0 ]; then
    echo [WARN] "BUILD_DSP" occurred $num_count times in $check_dir!! This macro should not be there in source file!! Cross check!!
fi
num_count=`grep -rw -e 'BUILD_DSP1' "$check_dir" | wc -l`
if [ $num_count -gt 0 ]; then
    echo [WARN] "BUILD_DSP1" occurred $num_count times in $check_dir!! This macro should not be there in source file!! Cross check!!
fi
num_count=`grep -rw -e 'BUILD_EVE' "$check_dir" | wc -l`
if [ $num_count -gt 0 ]; then
    echo [WARN] "BUILD_EVE" occurred $num_count times in $check_dir!! This macro should not be there in source file!! Cross check!!
fi
num_count=`grep -rw -e 'BUILD_EVE1' "$check_dir" | wc -l`
if [ $num_count -gt 0 ]; then
    echo [WARN] "BUILD_EVE1" occurred $num_count times in $check_dir!! This macro should not be there in source file!! Cross check!!
fi
num_count=`grep -rw -e 'BUILD_EVE2' "$check_dir" | wc -l`
if [ $num_count -gt 0 ]; then
    echo [WARN] "BUILD_EVE2" occurred $num_count times in $check_dir!! This macro should not be there in source file!! Cross check!!
fi
num_count=`grep -rw -e 'BUILD_EVE3' "$check_dir" | wc -l`
if [ $num_count -gt 0 ]; then
    echo [WARN] "BUILD_EVE3" occurred $num_count times in $check_dir!! This macro should not be there in source file!! Cross check!!
fi
num_count=`grep -rw -e 'BUILD_EVE4' "$check_dir" | wc -l`
if [ $num_count -gt 0 ]; then
    echo [WARN] "BUILD_EVE4" occurred $num_count times in $check_dir!! This macro should not be there in source file!! Cross check!!
fi
num_count=`grep -r -e 'PLATFORM_EVM_SI' "$check_dir" | wc -l`
if [ $num_count -gt 0 ]; then
    echo [WARN] "PLATFORM_EVM_SI" occurred $num_count times in $check_dir!! This macro should not be there in source file!! Cross check!!
fi
num_count=`grep -r -i -e 'All rights reserved' "$check_dir" | wc -l`
if [ $num_count -gt 0 ]; then
    echo [WARN] "All rights reserved" occurred $num_count times in $check_dir!! This macro should not be there in source file!! Cross check!!
fi
num_count=`grep -r -i -e 'PDP-' "$check_dir" | wc -l`
if [ $num_count -gt 0 ]; then
    echo [WARN] "PDP-" occurred $num_count times in $check_dir!! This macro should not be there in source file!! Cross check!!
fi
num_count=`grep -r -i -e ':SR' "$check_dir" | wc -l`
if [ $num_count -gt 0 ]; then
    echo [WARN] ":SR" occurred $num_count times in $check_dir!! This macro should not be there in source file!! Cross check!!
fi
