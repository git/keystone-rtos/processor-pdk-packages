#!/bin/bash
#   ============================================================================
#   @file   stw_sync_vps.sh
#
#   @desc   Script to sync-up the VPS source code with starterware repo.
#           This will copy the source files from STW repo and move them as per
#           PDK dir structure. This also does the generic macro replacements.
#
#   ============================================================================
#   Revision History
#   06-Nov-2015 Sivaraj         Initial draft.
#
#   ============================================================================

#   ============================================================================
#   CAUTION: DO NOT INVOKE THE SCRIPTS AS 'gitadmin'.
#            IT WILL ADD OBJECT FILES IN BASE REPOSITORY; WHICH SHOULD IDEALLY
#            BE CLEAN.
#   ============================================================
# Command: stw_sync_vps.sh <$stw_dir>
# Example (Linux): /datalocal/daily_build/scripts/stw_sync_vps.sh /datalocal/daily_build/starterware/starterware_ /datalocal/daily_build/vayu_drivers/bspdrivers_ /datalocal/daily_build/vayu_driver_test/bsptest
# Example (Windows from git bash):                stw_sync_vps.sh "/d/Vayu/Source/starterware/starterware_"       "/d/Vayu/Source/vayu_drivers/bspdrivers_"       "/d/Vayu/Source/vayu_driver_test/bsptest"

export PATH=$PATH:/bin
date=`date '+%b_%d_%Y'`
date_print=`date '+%d-%b-%Y %I:%M:%S %p'`
month=`date '+%b_%Y'`
year=`date '+%Y'`
if [ "$OS" == "" ]; then
  echo "No OS defined, defaulting to linux"
  OS=linux
fi

#Get user input
working_dir=`pwd`
stw_dir=$1
if [ "$stw_dir" == "" ]; then
  stw_dir=$working_dir/../../../starterware/starterware_
  if [ ! -d "$stw_dir" ]; then
    echo "Kindly provide Starterware path to copy files from!!"
    exit
  fi
fi
bsp_dir=$2
if [ "$bsp_dir" == "" ]; then
  bsp_dir=$stw_dir/../../vayu_drivers/bspdrivers_
fi
bsptest_dir=$3
if [ "$bsptest_dir" == "" ]; then
  bsptest_dir=$bsp_dir/../../vayu_driver_test/bsptest
fi
pdk_dir=$working_dir/../..
vps_dir=$pdk_dir/packages/ti/drv/vps
sync_temp_dir=$working_dir/temp_sync

echo "PDK VPS Starterware/BSP Sync-up"
echo "Date: $date_print"
echo ""

sedErrCheck() {
if [ $1 -ne 0 ]; then
    echo "Error occurred @ $2!!"
fi
}

############## Start clean ##############
echo [INFO] Start Clean!! Deleting tobe copied files/folders...
rm -rf $sync_temp_dir
#FVID2
rm $vps_dir/include/fvid2/*.h
rm $vps_dir/src/fvid2/src/*.*
#OSAL
rm $vps_dir/include/osal/*.h
rm $vps_dir/src/osal/tirtos/*.c
#COMMON
rm $vps_dir/include/common/*.h
rm $vps_dir/src/common/src/*.*
#VPS
rm $vps_dir/include/*.h
rm $vps_dir/include/dss/*.h
rm $vps_dir/include/iss/*.h
rm $vps_dir/include/vip/*.h
rm $vps_dir/include/vpe/*.h
rm $vps_dir/src/vpslib/captcore/*.h
rm $vps_dir/src/vpslib/captcore/src/*.*
rm $vps_dir/src/vpslib/common/*.h
rm $vps_dir/src/vpslib/common/src/*.*
rm $vps_dir/src/vpslib/dispcore/*.h
rm $vps_dir/src/vpslib/dispcore/src/*.*
rm -rf $vps_dir/src/vpslib/drv/*
rm $vps_dir/src/vpslib/hal/*.h
rm $vps_dir/src/vpslib/hal/src/*.*
rm $vps_dir/src/vpslib/isscore/*.h
rm $vps_dir/src/vpslib/isscore/src/*.*
rm $vps_dir/src/vpslib/calcore/*.h
rm $vps_dir/src/vpslib/calcore/src/*.*
rm $vps_dir/src/vpslib/vpecore/*.h
rm $vps_dir/src/vpslib/vpecore/src/*.*
rm $vps_dir/src/vpsdrv/*.h
rm -rf $vps_dir/src/vpsdrv/captdrv
rm -rf $vps_dir/src/vpsdrv/dispdrv
rm -rf $vps_dir/src/vpsdrv/m2mdrv
rm -rf $vps_dir/src/vpsdrv/m2mdrv_dss
rm -rf $vps_dir/src/vpsdrv/src
rm -rf $vps_dir/src/vpsdrv/vpedrv
#Platform
rm $vps_dir/include/platforms/*.h
rm -rf $vps_dir/src/platforms
#Devices
rm $vps_dir/include/devices/*.h
rm -rf $vps_dir/src/devices
#Boards
rm $vps_dir/include/boards/*.h
rm -rf $vps_dir/src/boards
#Examples
rm $vps_dir/examples/utility/*.*
rm -rf $vps_dir/examples/utility/src
rm -rf $vps_dir/examples/vpe
rm -rf $vps_dir/examples/loopback
rm -rf $vps_dir/examples/syncLoopback
rm -rf $vps_dir/examples/vip
rm -rf $vps_dir/examples/dss
rm -rf $vps_dir/examples/iss
#Docs
rm -rf $vps_dir/docs/tda2ex
rm -rf $vps_dir/docs/tda2xx
rm -rf $vps_dir/docs/tda3xx
rm -rf $vps_dir/docs/test_inputs
#Unit Test
rm -rf $vps_dir/unit_test

############## Create required folders ##############
echo [INFO] Creating required folders...
mkdir -p $sync_temp_dir
mkdir -p $sync_temp_dir/include/dss
mkdir -p $sync_temp_dir/include/iss
mkdir -p $sync_temp_dir/include/vip
mkdir -p $sync_temp_dir/include/vpe
mkdir -p $sync_temp_dir/include/fvid2
mkdir -p $sync_temp_dir/src
mkdir -p $sync_temp_dir/src/fvid2/src
mkdir -p $sync_temp_dir/include/osal
mkdir -p $sync_temp_dir/src/osal/tirtos
mkdir -p $sync_temp_dir/include/common
mkdir -p $sync_temp_dir/src/common/src
mkdir -p $sync_temp_dir/src/vpslib/captcore/src
mkdir -p $sync_temp_dir/src/vpslib/common/src
mkdir -p $sync_temp_dir/src/vpslib/dispcore/src
mkdir -p $sync_temp_dir/src/vpslib/drv/src
mkdir -p $sync_temp_dir/src/vpslib/hal/src
mkdir -p $sync_temp_dir/src/vpslib/isscore/src
mkdir -p $sync_temp_dir/src/vpslib/calcore/src
mkdir -p $sync_temp_dir/src/vpslib/vpecore/src
mkdir -p $sync_temp_dir/src/vpsdrv
mkdir -p $sync_temp_dir/include/platforms
mkdir -p $sync_temp_dir/src/platforms
mkdir -p $sync_temp_dir/include/devices
mkdir -p $sync_temp_dir/src/devices
mkdir -p $sync_temp_dir/include/boards
mkdir -p $sync_temp_dir/src/boards
mkdir -p $sync_temp_dir/examples/utility/src
mkdir -p $sync_temp_dir/examples/vpe
mkdir -p $sync_temp_dir/examples/loopback
mkdir -p $sync_temp_dir/examples/syncLoopback
mkdir -p $sync_temp_dir/examples/vip
mkdir -p $sync_temp_dir/examples/dss
mkdir -p $sync_temp_dir/examples/iss
mkdir -p $sync_temp_dir/unit_test/captureUt
mkdir -p $sync_temp_dir/unit_test/displayUt
mkdir -p $sync_temp_dir/unit_test/vpeUt
mkdir -p $vps_dir/include/dss
mkdir -p $vps_dir/include/iss
mkdir -p $vps_dir/include/vip
mkdir -p $vps_dir/include/vpe
mkdir -p $vps_dir/include/fvid2
mkdir -p $vps_dir/src/fvid2/src
mkdir -p $vps_dir/include/osal
mkdir -p $vps_dir/src/osal/tirtos
mkdir -p $vps_dir/include/common
mkdir -p $vps_dir/src/common/src
mkdir -p $vps_dir/src/vpslib/captcore/src
mkdir -p $vps_dir/src/vpslib/common/src
mkdir -p $vps_dir/src/vpslib/dispcore/src
mkdir -p $vps_dir/src/vpslib/drv/src
mkdir -p $vps_dir/src/vpslib/hal/src
mkdir -p $vps_dir/src/vpslib/isscore/src
mkdir -p $vps_dir/src/vpslib/calcore/src
mkdir -p $vps_dir/src/vpslib/vpecore/src
mkdir -p $vps_dir/src/vpsdrv
mkdir -p $vps_dir/include/platforms
mkdir -p $vps_dir/src/platforms
mkdir -p $vps_dir/include/devices
mkdir -p $vps_dir/src/devices
mkdir -p $vps_dir/include/boards
mkdir -p $vps_dir/src/boards
mkdir -p $vps_dir/examples/utility/src
mkdir -p $vps_dir/examples/vpe
mkdir -p $vps_dir/examples/loopback
mkdir -p $vps_dir/examples/syncLoopback
mkdir -p $vps_dir/examples/vip
mkdir -p $vps_dir/examples/dss
mkdir -p $vps_dir/examples/iss
mkdir -p $vps_dir/docs/tda2ex
mkdir -p $vps_dir/docs/tda2xx
mkdir -p $vps_dir/docs/tda3xx
mkdir -p $vps_dir/docs/test_inputs
mkdir -p $vps_dir/unit_test/captureUt
mkdir -p $vps_dir/unit_test/displayUt
mkdir -p $vps_dir/unit_test/vpeUt

############## Copy STW/BSP files to temp folder ##############
echo [INFO] Copying files from STW/BSP to temp folder...
#Copy source, includes and other files
cp -r $stw_dir/include/fvid2/*.h                $sync_temp_dir/include/fvid2
cp -r $bsp_dir/include/fvid2/*.h                $sync_temp_dir/include/fvid2
cp -r $bsp_dir/src/fvid2/*                      $sync_temp_dir/src/fvid2
cp -r $stw_dir/include/osal/*.h                 $sync_temp_dir/include/osal
cp -r $bsp_dir/src/osal/src/*                   $sync_temp_dir/src/osal/tirtos
cp -r $stw_dir/include/common/*.h               $sync_temp_dir/include/common
cp -r $bsp_dir/include/common/*.h               $sync_temp_dir/include/common
cp -r $stw_dir/utils/common/src/*               $sync_temp_dir/src/common/src
cp -r $bsp_dir/src/common/src/*                 $sync_temp_dir/src/common/src
cp -r $stw_dir/include/vps/*.h                  $sync_temp_dir/include
cp -r $bsp_dir/include/vps/*.h                  $sync_temp_dir/include
cp -r $stw_dir/include/vps/iss/*.h              $sync_temp_dir/include/iss
cp -r $bsp_dir/include/vps/iss/*.h              $sync_temp_dir/include/iss
cp -r $stw_dir/vpslib/captcore/*                $sync_temp_dir/src/vpslib/captcore
cp -r $stw_dir/vpslib/common/*                  $sync_temp_dir/src/vpslib/common
cp -r $stw_dir/vpslib/dispcore/*                $sync_temp_dir/src/vpslib/dispcore
cp -r $stw_dir/vpslib/drv/*                     $sync_temp_dir/src/vpslib/drv/src
cp -r $stw_dir/include/vps_dctrlDrv.h           $sync_temp_dir/include/dss
cp -r $stw_dir/include/vps_dssDrv.h             $sync_temp_dir/include/dss
cp -r $stw_dir/include/vps_vipDrv.h             $sync_temp_dir/include/vip
cp -r $stw_dir/vpslib/hal/*                     $sync_temp_dir/src/vpslib/hal
cp -r $stw_dir/vpslib/isscore/*                 $sync_temp_dir/src/vpslib/isscore
cp -r $stw_dir/vpslib/calcore/*                 $sync_temp_dir/src/vpslib/calcore
cp -r $stw_dir/vpslib/vpecore/*                 $sync_temp_dir/src/vpslib/vpecore
cp -r $bsp_dir/src/vps/*                        $sync_temp_dir/src/vpsdrv
cp -r $bsp_dir/include/platforms/*              $sync_temp_dir/include/platforms
cp -r $bsp_dir/src/platforms/*                  $sync_temp_dir/src/platforms
cp -r $bsp_dir/include/devices/*                $sync_temp_dir/include/devices
cp -r $bsp_dir/src/devices/*                    $sync_temp_dir/src/devices
cp -r $bsp_dir/include/boards/*                 $sync_temp_dir/include/boards
cp -r $bsp_dir/src/boards/*                     $sync_temp_dir/src/boards
cp -r $bsp_dir/examples/utility/*.*             $sync_temp_dir/examples/utility
cp -r $bsp_dir/examples/utility/src/*           $sync_temp_dir/examples/utility/src
cp -r $bsp_dir/examples/vps/vpe/*               $sync_temp_dir/examples/vpe
cp -r $bsp_dir/examples/vps/loopback/*          $sync_temp_dir/examples/loopback
cp -r $bsp_dir/examples/vps/syncLoopback/*      $sync_temp_dir/examples/syncLoopback
cp -r $bsp_dir/examples/vps/capture/*           $sync_temp_dir/examples/vip
cp -r $bsp_dir/examples/vps/display/*           $sync_temp_dir/examples/dss
cp -r $bsp_dir/examples/vps/iss/*               $sync_temp_dir/examples/iss
#Unit Test
cp -r $bsptest_dir/packages/ti/captureUt/*      $sync_temp_dir/unit_test/captureUt
cp -r $bsptest_dir/packages/ti/displayUt/*      $sync_temp_dir/unit_test/displayUt
cp -r $bsptest_dir/packages/ti/vpeUt/*          $sync_temp_dir/unit_test/vpeUt
#Remove unwanted files
rm $sync_temp_dir/include/common/stw_types.h
rm $sync_temp_dir/include/common/stw_dataTypes.h
find $sync_temp_dir/examples -iname '*M3.cfg' | xargs rm -f
rm $sync_temp_dir/include/devices/bsp_aic31.h
rm $sync_temp_dir/include/devices/bsp_audioICodec.h
rm -rf $sync_temp_dir/src/devices/aic31
rm -rf $sync_temp_dir/src/devices/tvp7002
rm -rf $sync_temp_dir/src/devices/mt9v022
rm $sync_temp_dir/src/boards/src/bsp_boardTI814x*
rm $sync_temp_dir/src/boards/src/bsp_boardOmap5430*
rm $sync_temp_dir/src/platforms/src/bsp_platformTI814x*
rm $sync_temp_dir/src/platforms/src/bsp_platformOmap*
rm $sync_temp_dir/unit_test/captureUt/st_captureM3.cfg
rm $sync_temp_dir/unit_test/captureUt/captureUtOldcode.zip
rm $sync_temp_dir/unit_test/displayUt/displayUtM3.cfg
rm $sync_temp_dir/unit_test/displayUt/displayUtoldcode.zip
rm $sync_temp_dir/unit_test/vpeUt/st_vpeM3.cfg

############## Replace macros ##############
#echo [INFO] Finding and replacing STW/BSP macros to PDK macros \(You can ignore \"sed: no input files\" messages!!\)...
echo [INFO] Finding and replacing STW/BSP macros to PDK macros...
cd $sync_temp_dir
# _BUILD to SOC_
grep -r -l 'TDA2XX_BUILD' * | xargs sed -i -e 's|TDA2XX_BUILD|SOC_TDA2XX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA2PX_BUILD' * | xargs sed -i -e 's|TDA2PX_BUILD|SOC_TDA2PX|g';sedErrCheck $? $LINENO
grep -r -l 'DRA7XX_BUILD' * | xargs sed -i -e 's|DRA7XX_BUILD|SOC_DRA75x|g';sedErrCheck $? $LINENO
grep -r -l 'TDA2EX_BUILD' * | xargs sed -i -e 's|TDA2EX_BUILD|SOC_TDA2EX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA3XX_BUILD' * | xargs sed -i -e 's|TDA3XX_BUILD|SOC_TDA3XX|g';sedErrCheck $? $LINENO
grep -r -l 'AM572x_BUILD' * | xargs sed -i -e 's|AM572x_BUILD|SOC_AM572x|g';sedErrCheck $? $LINENO
grep -r -l 'AM571x_BUILD' * | xargs sed -i -e 's|AM571x_BUILD|SOC_AM571x|g';sedErrCheck $? $LINENO
grep -r -l 'DRA7XX'       * | xargs sed -i -e 's|DRA7XX|DRA75x|g';sedErrCheck $? $LINENO
# Add SOC_AMxxx/DRA support
grep -r -l 'defined (SOC_TDA2XX)' * | xargs sed -i -e 's:defined (SOC_TDA2XX):defined (SOC_TDA2XX) || defined (SOC_AM572x):g';sedErrCheck $? $LINENO
grep -r -l 'defined (SOC_TDA2EX)' * | xargs sed -i -e 's:defined (SOC_TDA2EX):defined (SOC_TDA2EX) || defined (SOC_AM571x):g';sedErrCheck $? $LINENO
grep -r -l 'defined (SOC_TDA3XX)' * | xargs sed -i -e 's:defined (SOC_TDA3XX):defined (SOC_TDA3XX):g';sedErrCheck $? $LINENO
# undo SOC change for TDA specific files
sed -i -e 's: || defined (SOC_AM572x)::g' $sync_temp_dir/examples/utility/src/bsputils_lvds.c
sed -i -e 's: || defined (SOC_AM571x)::g' $sync_temp_dir/examples/utility/src/bsputils_lvds.c
sed -i -e 's: || defined (SOC_AM572x)::g' $sync_temp_dir/src/boards/src/bsp_board.c
sed -i -e 's: || defined (SOC_AM572x)::g' $sync_temp_dir/src/boards/src/bsp_boardTda2ex.c
sed -i -e 's: || defined (SOC_AM571x)::g' $sync_temp_dir/src/boards/src/bsp_boardTda2ex.c
sed -i -e 's: || defined (SOC_AM572x)::g' $sync_temp_dir/src/boards/src/bsp_boardTda2xx.c
sed -i -e 's: || defined (SOC_AM571x)::g' $sync_temp_dir/src/boards/src/bsp_boardTda2xx.c
sed -i -e 's: || defined (SOC_AM572x)::g' $sync_temp_dir/src/boards/src/bsp_boardTda3xx.c
sed -i -e 's: || defined (SOC_AM571x)::g' $sync_temp_dir/src/boards/src/bsp_boardTda3xx.c
sed -i -e 's: || defined (SOC_AM572x)::g' $sync_temp_dir/examples/utility/src/bsputils_fileio.c
sed -i -e 's: || defined (SOC_AM571x)::g' $sync_temp_dir/examples/utility/src/bsputils_fileio.c
# Remove PLATFORM_EVM_SI is not needed as this is not defined in PDK makerules
#grep -r -l 'PLATFORM_EVM_SI'                    * | xargs sed -i '/PLATFORM_EVM_SI/d';sedErrCheck $? $LINENO
#undo DRA75x change for platform enum
grep -r -l 'BSP_PLATFORM_SOC_ID_DRA75x'         * | xargs sed -i -e 's|BSP_PLATFORM_SOC_ID_DRA75x|BSP_PLATFORM_SOC_ID_DRA75X|g';sedErrCheck $? $LINENO

############## Replace include paths ##############
echo [INFO] Replacing include path changes for PDK folder structure...
cd $sync_temp_dir
grep -r -l '#include "'                         * | xargs sed -i -e 's|#include "|#include <|g';sedErrCheck $? $LINENO
grep -r -l '\.h"'                               * | xargs sed -i -e 's|\.h"|\.h>|g';sedErrCheck $? $LINENO
grep -r -l '\.txt"'                             * | xargs sed -i -e 's|\.txt"|\.txt>|g';sedErrCheck $? $LINENO
grep -r -l '#include <hw_types'                 * | xargs sed -i -e 's|#include <hw_types|#include <ti/csl/hw_types|g';sedErrCheck $? $LINENO
grep -r -l '#include <hw/hw_types'              * | xargs sed -i -e 's|#include <hw/hw_types|#include <ti/csl/hw_types|g';sedErrCheck $? $LINENO
grep -r -l '#include <hw_'                      * | xargs sed -i -e 's|#include <hw_|#include <ti/csl/cslr_|g';sedErrCheck $? $LINENO
#Replace file location changes
grep -r -l '#include <fvid2/'                   * | xargs sed -i -e 's|#include <fvid2/|#include <ti/drv/vps/include/fvid2/|g';sedErrCheck $? $LINENO
grep -r -l '#include <fvid2_drvMgr\.h>'         * | xargs sed -i -e 's|#include <fvid2_drvMgr\.h>|#include <ti/drv/vps/include/fvid2/fvid2_drvMgr\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <osal/'                    * | xargs sed -i -e 's|#include <osal/|#include <ti/drv/vps/include/osal/|g';sedErrCheck $? $LINENO
grep -r -l '#include <common/vps'               * | xargs sed -i -e 's|#include <common/vps|#include <ti/drv/vps/src/vpslib/common/vps|g';sedErrCheck $? $LINENO
grep -r -l '#include <common/iem'               * | xargs sed -i -e 's|#include <common/iem|#include <ti/drv/vps/src/vpslib/common/iem|g';sedErrCheck $? $LINENO
grep -r -l '#include <common/irm'               * | xargs sed -i -e 's|#include <common/irm|#include <ti/drv/vps/src/vpslib/common/irm|g';sedErrCheck $? $LINENO
grep -r -l '#include <common/cal_evtmgr'        * | xargs sed -i -e 's|#include <common/cal_evtmgr|#include <ti/drv/vps/src/vpslib/common/cal_evtmgr|g';sedErrCheck $? $LINENO
grep -r -l '#include <common/src/vps'           * | xargs sed -i -e 's|#include <common/src/vps|#include <ti/drv/vps/src/vpslib/common/src/vps|g';sedErrCheck $? $LINENO
grep -r -l '#include <common/'                  * | xargs sed -i -e 's|#include <common/|#include <ti/drv/vps/include/common/|g';sedErrCheck $? $LINENO
grep -r -l '#include <vps/'                     * | xargs sed -i -e 's|#include <vps/|#include <ti/drv/vps/include/|g';sedErrCheck $? $LINENO
sed -i -e 's|soc_defines\.h|soc\.h|g'                               $sync_temp_dir/src/vpslib/hal/src/vpshal_iss.c
sed -i -e 's|soc_defines\.h|soc\.h|g'                               $sync_temp_dir/src/vpslib/hal/vpshal_isscal.h
sed -i -e 's|soc_defines\.h|soc\.h|g'                               $sync_temp_dir/src/vpsdrv/src/vpsdrv_init.c
sed -i -e 's|soc_defines\.h|soc\.h|g'                               $sync_temp_dir/src/vpsdrv/captdrv/src/vpsdrv_capturePriv.h
sed -i -e 's|soc_defines\.h|soc\.h|g'                               $sync_temp_dir/examples/loopback/src/Loopback_priv.h
sed -i -e 's|soc_defines\.h|soc\.h|g'                               $sync_temp_dir/examples/vip/captureVip/src/CaptureVip_main.h
sed -i -e 's|soc_defines\.h|soc\.h|g'                               $sync_temp_dir/examples/vip/subFrmCaptureVip/src/SubFrmCaptureVip_main.h
sed -i -e 's|soc_defines\.h|soc\.h|g'                               $sync_temp_dir/examples/dss/displayDssWb/src/DisplayDssWb_main.h
sed -i -e 's|soc_defines\.h|soc\.h|g'                               $sync_temp_dir/unit_test/captureUt/testLib/st_capture.h
sed -i -e 's|soc_defines\.h|ti/drv/stw_lld/platform/irq_xbar\.h|g'  $sync_temp_dir/src/devices/radar_ar12xx/src/bspdrv_ar12xxPriv.h
grep -r -l '#include <soc\.h>'                  * | xargs sed -i -e 's|#include <soc\.h>|#include <ti/csl/soc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <irq_xbar_'                * | xargs sed -i -e 's|#include <irq_xbar_interrupt_ids\.h>|#include <ti/csl/arch/csl_arch\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <wd_timer\.h>'             * | xargs sed -i -e 's|#include <wd_timer\.h>|#include <ti/csl/csl_wd_timer\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <rti\.h>'                  * | xargs sed -i -e 's|#include <rti\.h>|#include <ti/csl/csl_rti\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <captcore/'                * | xargs sed -i -e 's|#include <captcore/|#include <ti/drv/vps/src/vpslib/captcore/|g';sedErrCheck $? $LINENO
grep -r -l '#include <dispcore/'                * | xargs sed -i -e 's|#include <dispcore/|#include <ti/drv/vps/src/vpslib/dispcore/|g';sedErrCheck $? $LINENO
grep -r -l '#include <hal/'                     * | xargs sed -i -e 's|#include <hal/|#include <ti/drv/vps/src/vpslib/hal/|g';sedErrCheck $? $LINENO
grep -r -l '#include <isscore/'                 * | xargs sed -i -e 's|#include <isscore/|#include <ti/drv/vps/src/vpslib/isscore/|g';sedErrCheck $? $LINENO
grep -r -l '#include <calcore/'                 * | xargs sed -i -e 's|#include <calcore/|#include <ti/drv/vps/src/vpslib/calcore/|g';sedErrCheck $? $LINENO
grep -r -l '#include <vpecore/'                 * | xargs sed -i -e 's|#include <vpecore/|#include <ti/drv/vps/src/vpslib/vpecore/|g';sedErrCheck $? $LINENO
grep -r -l 'include/vps_captureDssWb\.h'        * | xargs sed -i -e 's|include/vps_captureDssWb\.h|include/dss/vps_captureDssWb\.h|g';sedErrCheck $? $LINENO
grep -r -l 'include/vps_cfgDss\.h'              * | xargs sed -i -e 's|include/vps_cfgDss\.h|include/dss/vps_cfgDss\.h|g';sedErrCheck $? $LINENO
grep -r -l 'include/vps_displayDss\.h'          * | xargs sed -i -e 's|include/vps_displayDss\.h|include/dss/vps_displayDss.h|g';sedErrCheck $? $LINENO
grep -r -l 'include/vps_m2mDss\.h'              * | xargs sed -i -e 's|include/vps_m2mDss\.h|include/dss/vps_m2mDss.h|g';sedErrCheck $? $LINENO
grep -r -l 'include/vps_captureIss\.h'          * | xargs sed -i -e 's|include/vps_captureIss\.h|include/iss/vps_captureIss\.h|g';sedErrCheck $? $LINENO
grep -r -l 'include/vps_captureVip\.h'          * | xargs sed -i -e 's|include/vps_captureVip\.h|include/vip/vps_captureVip\.h|g';sedErrCheck $? $LINENO
grep -r -l 'include/vps_captureVipDataTypes\.h' * | xargs sed -i -e 's|include/vps_captureVipDataTypes\.h|include/vip/vps_captureVipDataTypes\.h|g';sedErrCheck $? $LINENO
grep -r -l 'include/vps_cfgVipParser\.h'        * | xargs sed -i -e 's|include/vps_cfgVipParser\.h|include/vip/vps_cfgVipParser\.h|g';sedErrCheck $? $LINENO
grep -r -l 'include/vps_advCfgDei\.h'           * | xargs sed -i -e 's|include/vps_advCfgDei\.h|include/vpe/vps_advCfgDei\.h|g';sedErrCheck $? $LINENO
grep -r -l 'include/vps_m2m\.h'                 * | xargs sed -i -e 's|include/vps_m2m\.h|include/vpe/vps_m2m\.h|g';sedErrCheck $? $LINENO
grep -r -l 'include/vps_cfgDei\.h'              * | xargs sed -i -e 's|include/vps_cfgDei\.h|include/vpe/vps_cfgDei\.h|g';sedErrCheck $? $LINENO
grep -r -l 'include/vps_m2mVpe\.h'              * | xargs sed -i -e 's|include/vps_m2mVpe\.h|include/vpe/vps_m2mVpe\.h|g';sedErrCheck $? $LINENO
grep -r -l '<platforms/'                        * | xargs sed -i -e 's|<platforms/|<ti/drv/vps/include/platforms/|g';sedErrCheck $? $LINENO
grep -r -l '<devices/'                          * | xargs sed -i -e 's|<devices/|<ti/drv/vps/include/devices/|g';sedErrCheck $? $LINENO
grep -r -l '<boards/'                           * | xargs sed -i -e 's|<boards/|<ti/drv/vps/include/boards/|g';sedErrCheck $? $LINENO
grep -r -l '<bsputils_'                         * | xargs sed -i -e 's|<bsputils_|<ti/drv/vps/examples/utility/bsputils_|g';sedErrCheck $? $LINENO
grep -r -l '<utility/bsputils_'                 * | xargs sed -i -e 's|<utility/bsputils_|<ti/drv/vps/examples/utility/bsputils_|g';sedErrCheck $? $LINENO
grep -r -l 'utility/bspCommonBIOS\.cfg'         * | xargs sed -i -e 's|utility/bspCommonBIOS\.cfg|\.\./\.\./utility/bspCommonBIOS\.cfg|g';sedErrCheck $? $LINENO
grep -r -l 'mcspi/bsp_mcspi\.h'                 * | xargs sed -i -e 's|mcspi/bsp_mcspi\.h|ti/drv/bsp_lld/mcspi/bsp_mcspi\.h|g';sedErrCheck $? $LINENO
grep -r -l 'vps_dctrlDrv\.h'                    * | xargs sed -i -e 's|vps_dctrlDrv\.h|ti/drv/vps/include/dss/vps_dctrlDrv\.h|g';sedErrCheck $? $LINENO
grep -r -l 'vps_dssDrv\.h'                      * | xargs sed -i -e 's|vps_dssDrv\.h|ti/drv/vps/include/dss/vps_dssDrv\.h|g';sedErrCheck $? $LINENO
grep -r -l 'vps_vipDrv\.h'                      * | xargs sed -i -e 's|vps_vipDrv\.h|ti/drv/vps/include/vip/vps_vipDrv\.h|g';sedErrCheck $? $LINENO
#Replace file/function/enums rename changes
grep -r -l 'stw_dataTypes\.h'                   * | xargs sed -i -e 's|#include <ti/drv/vps/include/common/stw_dataTypes\.h>|#include <ti/csl/tistdtypes\.h>|g';sedErrCheck $? $LINENO
grep -r -l 'hw/hw_iss_regs\.h'                  * | xargs sed -i -e 's|hw/hw_iss_regs\.h|ti/csl/cslr_iss\.h|g';sedErrCheck $? $LINENO
grep -r -l 'cslr_dispc\.h'                      * | xargs sed -i -e 's|cslr_dispc\.h|cslr_dss\.h|g';sedErrCheck $? $LINENO
grep -r -l 'hw/hw_dss_vid\.h'                   * | xargs sed -i -e 's|hw/hw_dss_vid\.h|ti/csl/cslr_dss\.h|g';sedErrCheck $? $LINENO
grep -r -l 'cslr_hdmi_phy\.h'                   * | xargs sed -i -e 's|cslr_hdmi_phy\.h|cslr_dss\.h|g';sedErrCheck $? $LINENO
grep -r -l 'hw/hw_dss_venc\.h'                  * | xargs sed -i -e 's|hw/hw_dss_venc\.h|ti/csl/cslr_dss\.h|g';sedErrCheck $? $LINENO
grep -r -l 'hw/hw_iss_h3a\.h'                   * | xargs sed -i -e 's|hw/hw_iss_h3a\.h|ti/csl/cslr_iss\.h|g';sedErrCheck $? $LINENO
grep -r -l 'hw/hw_iss_ipipe\.h'                 * | xargs sed -i -e 's|hw/hw_iss_ipipe\.h|ti/csl/cslr_iss\.h|g';sedErrCheck $? $LINENO
grep -r -l 'hw/hw_iss_isif\.h'                  * | xargs sed -i -e 's|hw/hw_iss_isif\.h|ti/csl/cslr_iss\.h|g';sedErrCheck $? $LINENO
grep -r -l 'hw/hw_iss_nsf3\.h'                  * | xargs sed -i -e 's|hw/hw_iss_nsf3\.h|ti/csl/cslr_iss\.h|g';sedErrCheck $? $LINENO
grep -r -l 'hw/hw_iss_rsz\.h'                   * | xargs sed -i -e 's|hw/hw_iss_rsz\.h|ti/csl/cslr_iss\.h|g';sedErrCheck $? $LINENO
grep -r -l 'hw/hw_iss_vtnf\.h'                  * | xargs sed -i -e 's|hw/hw_iss_vtnf\.h|ti/csl/cslr_iss\.h|g';sedErrCheck $? $LINENO
grep -r -l 'tda2ex/hw/hw_cal\.h'                * | xargs sed -i -e 's|tda2ex/hw/hw_cal\.h|ti/csl/cslr_cal\.h|g';sedErrCheck $? $LINENO
sed -i -e 's|hw/hw_iss_lvdsrx\.h|ti/csl/cslr_iss\.h|g' $sync_temp_dir/src/vpslib/hal/src/vpshal_isslvdsrx.c
sed -i -e 's|hw/hw_iss_simcop_regs\.h|ti/csl/cslr_iss\.h|g' $sync_temp_dir/src/vpslib/hal/src/vpshal_isssimcop.c
grep -r -l 'i2c/bsp_i2c\.h'                     * | xargs sed -i -e 's|i2c/bsp_i2c\.h|ti/drv/bsp_lld/i2c/bsp_i2c\.h|g';sedErrCheck $? $LINENO
grep -r -l 'uart/bsp_uart\.h'                   * | xargs sed -i -e 's|uart/bsp_uart\.h|ti/drv/bsp_lld/uart/bsp_uart\.h|g';sedErrCheck $? $LINENO
grep -r -l 'gpio_v2\.h'                         * | xargs sed -i -e 's|gpio_v2\.h|ti/csl/csl_gpio\.h|g';sedErrCheck $? $LINENO
grep -r -l 'pmlib_videopll\.h'                  * | xargs sed -i -e 's|pmlib_videopll\.h|ti/drv/pm/pmlib\.h|g';sedErrCheck $? $LINENO
grep -r -l 'pmhal_prcm\.h'                      * | xargs sed -i -e 's|pmhal_prcm\.h|ti/drv/pm/pmhal\.h|g';sedErrCheck $? $LINENO
grep -r -l 'hsmmcsd_API\.h'                     * | xargs sed -i -e 's|hsmmcsd_API\.h|ti/drv/stw_lld/fatlib/hsmmcsd_API\.h|g';sedErrCheck $? $LINENO
grep -r -l 'hsmmcsd_edma\.h'                    * | xargs sed -i -e 's|hsmmcsd_edma\.h|ti/drv/stw_lld/fatlib/hsmmcsd_edma\.h|g';sedErrCheck $? $LINENO
#Remove other unwanted header inclusion
grep -r -l '#include <soc_defines\.h>'          * | xargs sed -i '/#include <soc_defines\.h>/d';sedErrCheck $? $LINENO
grep -r -l '#include <hw/hw_iss_'               * | xargs sed -i '/#include <hw\/hw_iss_/d';sedErrCheck $? $LINENO
grep -r -l 'cslr_dss_family\.h'                 * | xargs sed -i '/cslr_dss_family\.h/d';sedErrCheck $? $LINENO
grep -r -l '#include <hw/hw_dss_'               * | xargs sed -i '/#include <hw\/hw_dss_/d';sedErrCheck $? $LINENO
grep -r -l 'cslr_hdmi_'                         * | xargs sed -i '/cslr_hdmi_/d';sedErrCheck $? $LINENO
grep -r -l 'hw_ctrl_core'                       * | xargs sed -i '/hw_ctrl_core/d';sedErrCheck $? $LINENO
grep -r -l 'hw_ctrl_wkup'                       * | xargs sed -i '/hw_ctrl_wkup/d';sedErrCheck $? $LINENO
grep -r -l 'hw_ipu_cm_core_aon'                 * | xargs sed -i '/hw_ipu_cm_core_aon/d';sedErrCheck $? $LINENO
grep -r -l 'hw_l3init_cm_core'                  * | xargs sed -i '/hw_l3init_cm_core/d';sedErrCheck $? $LINENO
grep -r -l 'hw_l4per_cm_core'                   * | xargs sed -i '/hw_l4per_cm_core/d';sedErrCheck $? $LINENO
grep -r -l 'ckgen_cm_core_aon'                  * | xargs sed -i '/ckgen_cm_core_aon/d';sedErrCheck $? $LINENO
sed -i -e 's|dma_xbar\.h|ti/drv/stw_lld/platform/dma_xbar\.h|g' $sync_temp_dir/examples/utility/src/bsputils_fileio.c
grep -r -l '<dma_xbar\.h>'                      * | xargs sed -i '/<dma_xbar\.h>/d';sedErrCheck $? $LINENO
grep -r -l '<dma_xbar_event_ids\.h>'            * | xargs sed -i '/<dma_xbar_event_ids\.h>/d';sedErrCheck $? $LINENO
grep -r -l 'hw_wkupaon_cm'                      * | xargs sed -i '/hw_wkupaon_cm/d';sedErrCheck $? $LINENO
grep -r -l 'cslr_ctrl_core\.h'                  * | xargs sed -i '/cslr_ctrl_core\.h/d';sedErrCheck $? $LINENO
grep -r -l 'cslr_ctrl_core_sec\.h'              * | xargs sed -i '/cslr_ctrl_core_sec\.h/d';sedErrCheck $? $LINENO
grep -r -l 'bspdrv_tvp7002\.h'                  * | xargs sed -i '/bspdrv_tvp7002\.h/d';sedErrCheck $? $LINENO
grep -r -l 'bspdrv_mt9v022\.h'                  * | xargs sed -i '/bspdrv_mt9v022\.h/d';sedErrCheck $? $LINENO
sed -i '/#include <tda2xx\/soc_defines\.h>/d' $sync_temp_dir/src/boards/src/bsp_boardTda2ex.c
sed -i '/#include <tda2xx\/soc_defines\.h>/d' $sync_temp_dir/src/boards/src/bsp_boardTda2xx.c
sed -i '/pm\/pmhal\/pmhal_mm\.h/d'            $sync_temp_dir/src/boards/src/bsp_boardTda2ex.c
sed -i '/pm\/pmhal\/pmhal_mm\.h/d'            $sync_temp_dir/src/boards/src/bsp_boardTda2xx.c
sed -i '/pmhal_mm\.h/d'                       $sync_temp_dir/src/devices/radar_ar12xx/src/bspdrv_ar12xxPriv.h

#Remove other unwanted function calls
grep -r -l 'Bsp_tvp7002'                        * | xargs sed -i '/Bsp_tvp7002/d';sedErrCheck $? $LINENO
grep -r -l 'Bsp_mt9v022'                        * | xargs sed -i '/Bsp_mt9v022/d';sedErrCheck $? $LINENO
#cfg file location change
sed -i -e 's|"\.\./\.\./utility/bspCommonBIOS\.cfg"|"\.\./utility/bspCommonBIOS\.cfg"|g' $sync_temp_dir/examples/loopback/loopback.cfg
sed -i -e 's|"\.\./\.\./utility/bspCommonBIOS\.cfg"|"\.\./utility/bspCommonBIOS\.cfg"|g' $sync_temp_dir/examples/syncLoopback/syncLoopback.cfg
sed -i -e 's|"\.\./\.\./utility/bspCommonBIOS\.cfg"|"\.\./\.\./examples/utility/bspCommonBIOS\.cfg"|g' $sync_temp_dir/unit_test/captureUt/st_capture.cfg
sed -i -e 's|"\.\./\.\./utility/bspCommonBIOS\.cfg"|"\.\./\.\./examples/utility/bspCommonBIOS\.cfg"|g' $sync_temp_dir/unit_test/displayUt/displayUt.cfg
sed -i -e 's|"\.\./\.\./utility/bspCommonBIOS\.cfg"|"\.\./\.\./examples/utility/bspCommonBIOS\.cfg"|g' $sync_temp_dir/unit_test/vpeUt/st_vpe.cfg
grep -r -l 'm4video'                            * | xargs sed -i -e 's|m4video|ipu1_1|g';sedErrCheck $? $LINENO
#PLATFORM to BOARD change
sed -i -e 's|PLATFORM|BOARD|g' $sync_temp_dir/examples/utility/BIOS_common.cfg
sed -i -e 's|PLATFORM|BOARD|g' $sync_temp_dir/examples/utility/bspCommonBIOS.cfg
#Memory section name change
grep -r -l 'VPS_DESC_MEM'                       * | xargs sed -i -e 's|VPS_DESC_MEM|APP_UNCACHED_DATA_BLK3_MEM|g';sedErrCheck $? $LINENO
grep -r -l 'FRAME_BUFFER_MEM'                   * | xargs sed -i -e 's|FRAME_BUFFER_MEM|APP_CACHED_DATA_BLK1_MEM|g';sedErrCheck $? $LINENO
grep -r -l 'TILER_BUFFER_MEM'                   * | xargs sed -i -e 's|TILER_BUFFER_MEM|APP_CACHED_DATA_BLK2_MEM|g';sedErrCheck $? $LINENO
grep -r -l 'VIDEO_M4_CODE_MEM'                  * | xargs sed -i -e 's|VIDEO_M4_CODE_MEM|APP_CODE2_MEM|g';sedErrCheck $? $LINENO
grep -r -l 'VPSS_M4_CODE_MEM'                   * | xargs sed -i -e 's|VPSS_M4_CODE_MEM|APP_CODE_MEM|g';sedErrCheck $? $LINENO
#XBAR instance change
grep -r -l 'XBAR_INST'                          * | xargs sed -i -e 's|XBAR_INST|CSL_XBAR_INST|g';sedErrCheck $? $LINENO
sed -i -e 's|,\ \(.*\)_IRQ)|,\ CSL_XBAR_\1_IRQ)|g' $sync_temp_dir/src/platforms/src/bsp_platformTda2xx.c
sed -i -e 's|,\ \(.*\)_IRQ)|,\ CSL_XBAR_\1_IRQ)|g' $sync_temp_dir/src/platforms/src/bsp_platformTda3xx.c
sed -i -e 's|ISS_IRQ_INT0|CSL_XBAR_ISS_IRQ_INT0|g' $sync_temp_dir/src/platforms/src/bsp_platformTda3xx.c
sed -i -e 's|EVE3_IRQ_OUT0|CSL_XBAR_EVE3_IRQ_OUT0|g' $sync_temp_dir/src/platforms/src/bsp_platformTda2xx.c
#misc changes
grep -r -l 'SRC_FILES_hal.MK'                   * | xargs sed -i -e 's|SRC_FILES_hal.MK|src_files_hal.mk|g';sedErrCheck $? $LINENO
sed -i '/Bsp_platformTda2xxMcSPI4DMAXbar/d'             $sync_temp_dir/src/platforms/src/bsp_platformTda2xx.c
sed -i -e 's|\bBSP\b|VPS|g'                             $sync_temp_dir/include/vps.h
sed -i -e '/BSP_DRV_I2C_API/,/the codecs/d'             $sync_temp_dir/include/vps.h
sed -i -e '/\#else/,/PALTFORM_EVM/d'                    $sync_temp_dir/examples/iss/loopback/src/Loopback_capt.c

############## Copy files from temp folder to right VPS location ##############
echo [INFO] Copying files from temp folder to right PDK location...
cd $sync_temp_dir
#FVID2
cp $sync_temp_dir/include/fvid2/*.h             $vps_dir/include/fvid2
cp $sync_temp_dir/src/fvid2/*.h                 $vps_dir/include/fvid2
cp $sync_temp_dir/src/fvid2/src/*.*             $vps_dir/src/fvid2/src
#OSAL
cp $sync_temp_dir/include/osal/*.h              $vps_dir/include/osal
cp $sync_temp_dir/src/osal/tirtos/*.c           $vps_dir/src/osal/tirtos
#COMMON
cp $sync_temp_dir/include/common/*.h            $vps_dir/include/common
cp $sync_temp_dir/src/common/src/*.*            $vps_dir/src/common/src
#VPS
cp    $sync_temp_dir/include/*.h                $vps_dir/include
cp    $sync_temp_dir/include/iss/*.h            $vps_dir/include/iss
cp -r $sync_temp_dir/src/vpslib/captcore/*      $vps_dir/src/vpslib/captcore
cp -r $sync_temp_dir/src/vpslib/common/*        $vps_dir/src/vpslib/common
cp -r $sync_temp_dir/src/vpslib/dispcore/*      $vps_dir/src/vpslib/dispcore
cp -r $sync_temp_dir/src/vpslib/drv/*           $vps_dir/src/vpslib/drv
cp -r $sync_temp_dir/src/vpslib/hal/*           $vps_dir/src/vpslib/hal
cp -r $sync_temp_dir/src/vpslib/isscore/*       $vps_dir/src/vpslib/isscore
cp -r $sync_temp_dir/src/vpslib/calcore/*       $vps_dir/src/vpslib/calcore
cp -r $sync_temp_dir/src/vpslib/vpecore/*       $vps_dir/src/vpslib/vpecore
cp -r $sync_temp_dir/src/vpsdrv/*.h             $vps_dir/src/vpsdrv
cp -r $sync_temp_dir/src/vpsdrv/captdrv         $vps_dir/src/vpsdrv/captdrv
cp -r $sync_temp_dir/src/vpsdrv/dispdrv         $vps_dir/src/vpsdrv/dispdrv
cp -r $sync_temp_dir/src/vpsdrv/m2mdrv          $vps_dir/src/vpsdrv/m2mdrv
cp -r $sync_temp_dir/src/vpsdrv/m2mdrv_dss      $vps_dir/src/vpsdrv/m2mdrv_dss
cp -r $sync_temp_dir/src/vpsdrv/src             $vps_dir/src/vpsdrv/src
cp -r $sync_temp_dir/src/vpsdrv/vpedrv          $vps_dir/src/vpsdrv/vpedrv
#Platforms
cp    $sync_temp_dir/include/platforms/*.h      $vps_dir/include/platforms
cp -r $sync_temp_dir/src/platforms/*            $vps_dir/src/platforms
#Devices
cp    $sync_temp_dir/include/devices/*.h        $vps_dir/include/devices
cp -r $sync_temp_dir/src/devices/*              $vps_dir/src/devices
#Boards
cp    $sync_temp_dir/include/boards/*.h         $vps_dir/include/boards
cp -r $sync_temp_dir/src/boards/*               $vps_dir/src/boards
#Examples
cp -r $sync_temp_dir/examples/utility/*.*       $vps_dir/examples/utility
cp -r $sync_temp_dir/examples/utility/src/*     $vps_dir/examples/utility/src
cp -r $sync_temp_dir/examples/vpe/*             $vps_dir/examples/vpe
cp -r $sync_temp_dir/examples/loopback/*        $vps_dir/examples/loopback
cp -r $sync_temp_dir/examples/syncLoopback/*    $vps_dir/examples/syncLoopback
cp -r $sync_temp_dir/examples/vip/*             $vps_dir/examples/vip
cp -r $sync_temp_dir/examples/dss/*             $vps_dir/examples/dss
cp -r $sync_temp_dir/examples/iss/*             $vps_dir/examples/iss
#Copy unit test
cp -r $sync_temp_dir/unit_test/captureUt/*      $vps_dir/unit_test/captureUt
cp -r $sync_temp_dir/unit_test/displayUt/*      $vps_dir/unit_test/displayUt
cp -r $sync_temp_dir/unit_test/vpeUt/*          $vps_dir/unit_test/vpeUt
#Baremetal headers
cp    $sync_temp_dir/include/dss/*.h            $vps_dir/include/dss
cp    $sync_temp_dir/include/vip/*.h            $vps_dir/include/vip

#VPS header movement to proper sub-folder
mv $vps_dir/include/vps_captureDssWb.h          $vps_dir/include/dss/vps_captureDssWb.h
mv $vps_dir/include/vps_cfgDss.h                $vps_dir/include/dss/vps_cfgDss.h
mv $vps_dir/include/vps_displayDss.h            $vps_dir/include/dss/vps_displayDss.h
mv $vps_dir/include/vps_m2mDss.h                $vps_dir/include/dss
mv $vps_dir/include/vps_captureIss.h            $vps_dir/include/iss/vps_captureIss.h
mv $vps_dir/include/vps_captureVip.h            $vps_dir/include/vip/vps_captureVip.h
mv $vps_dir/include/vps_captureVipDataTypes.h   $vps_dir/include/vip/vps_captureVipDataTypes.h
mv $vps_dir/include/vps_cfgVipParser.h          $vps_dir/include/vip/vps_cfgVipParser.h
mv $vps_dir/include/vps_advCfgDei.h             $vps_dir/include/vpe/vps_advCfgDei.h
mv $vps_dir/include/vps_m2m.h                   $vps_dir/include/vpe/vps_m2m.h
mv $vps_dir/include/vps_cfgDei.h                $vps_dir/include/vpe/vps_cfgDei.h
mv $vps_dir/include/vps_m2mVpe.h                $vps_dir/include/vpe/vps_m2mVpe.h

#Copy docs
cp -r $bsp_dir/docs/tda2ex/*                    $vps_dir/docs/tda2ex
cp -r $bsp_dir/docs/tda2xx/*                    $vps_dir/docs/tda2xx
cp -r $bsp_dir/docs/tda3xx/*                    $vps_dir/docs/tda3xx
cp -r $bsp_dir/docs/test_inputs/*               $vps_dir/docs/test_inputs

############## Checkout not to be copied files ##############
cd $vps_dir
git checkout src/platforms/makefile
git checkout src/devices/makefile
git checkout src/boards/makefile
git checkout src/devices/src/bsp_deviceI2cTiRtos.c
git checkout src/boards/src/bsp_boardAm572x.c
git checkout src/boards/src/bsp_boardAm572x.h
git checkout src/devices/ov2659
git checkout examples/vpe/m2mVpeScale/makefile
git checkout examples/loopback/makefile
git checkout examples/syncLoopback/makefile
git checkout examples/dss/displayDss/makefile
git checkout examples/dss/displayDssWb/makefile
git checkout examples/dss/displayDssWb/displayDss_c66x.cfg
git checkout examples/dss/m2mDssWb/makefile
git checkout examples/iss/captureIss/makefile
git checkout examples/iss/captureIssOtf/makefile
git checkout examples/iss/loopback/makefile
git checkout examples/iss/m2mIss/makefile
git checkout examples/iss/m2mSimcopLdcVtnf/makefile
git checkout examples/iss/m2mWdr/makefile
git checkout examples/vip/captureVip/makefile
git checkout examples/vip/captureVip/captureVip_a15.cfg
git checkout examples/vip/captureVip/captureVip_c66x.cfg
git checkout examples/vip/subFrmCaptureVip/makefile
git checkout examples/vip/subFrmCaptureVip/subFrmCaptureVip_a15.cfg
git checkout examples/vip/subFrmCaptureVip/subFrmCaptureVip_c66x.cfg
git checkout examples/dss/displayDss/displayDss_a15.cfg
git checkout examples/dss/displayDss/displayDss_c66x.cfg
git checkout examples/dss/displayDssWb/displayDss_a15.cfg
git checkout examples/loopback/loopback_a15.cfg
git checkout examples/loopback/loopback_c66x.cfg
git checkout examples/vpe/m2mVpeScale/m2mVpeScale_a15.cfg
git checkout examples/vpe/m2mVpeScale/m2mVpeScale_c66x.cfg
git checkout examples/utility/BIOS_common_a15.cfg
git checkout examples/utility/BIOS_common_c66.cfg
git checkout examples/utility/BIOS_common_m4.cfg
git checkout examples/utility/bspCommonBIOS.cfg
git checkout examples/utility/BIOS_common.cfg
git checkout examples/utility/bspCommonBIOS_a15_0.cfg
git checkout examples/utility/src/bsputils_uartTiRtos.c
git checkout examples/utility/src/bsputils_fileioTiRtos.c
git checkout unit_test/vps_ut_component.mk
git checkout unit_test/captureUt/makefile
git checkout unit_test/displayUt/makefile
git checkout unit_test/vpeUt/makefile
#Case sensitive filenames
if [ "$OS" == "linux" ]; then
  rm -f examples/dss/displayDss/Makefile
  rm -f examples/dss/displayDssWb/Makefile
  rm -f examples/iss/captureIss/Makefile
  rm -f examples/iss/captureIssOtf/Makefile
  rm -f examples/iss/loopback/Makefile
  rm -f examples/iss/m2mIss/Makefile
  rm -f examples/iss/m2mSimcopLdcVtnf/Makefile
  rm -f examples/iss/m2mWdr/Makefile
  rm -f examples/loopback/Makefile
  rm -f examples/vip/captureVip/Makefile
  rm -f examples/vip/subFrmCaptureVip/Makefile
  rm -f examples/vpe/m2mVpeScale/Makefile
  rm -f src/boards/Makefile
  rm -f src/devices/Makefile
  rm -f src/platforms/Makefile
  rm -f unit_test/captureUt/Makefile
  rm -f unit_test/displayUt/Makefile
  rm -f unit_test/vpeUt/Makefile
  mv src/devices/adv7611/SRC_FILES_adv7611.MK src/devices/adv7611/src_files_adv7611.mk
  mv src/devices/ar0132/SRC_FILES_ar0132.MK src/devices/ar0132/src_files_ar0132.mk
  mv src/devices/ar0132rccc/SRC_FILES_ar0132rccc.MK src/devices/ar0132rccc/src_files_ar0132rccc.mk
  mv src/devices/ar0140/SRC_FILES_ar0140.MK src/devices/ar0140/src_files_ar0140.mk
  mv src/devices/ds90uh925/SRC_FILES_ds90uh925.MK src/devices/ds90uh925/src_files_ds90uh925.mk
  mv src/devices/ds90uh926/SRC_FILES_ds90uh926.MK src/devices/ds90uh926/src_files_ds90uh926.mk
  mv src/devices/imx224/SRC_FILES_imx224.MK src/devices/imx224/src_files_imx224.mk
  mv src/devices/imx290/SRC_FILES_imx290.MK src/devices/imx290/src_files_imx290.mk
  mv src/devices/ioexp/SRC_FILES_ioexp.MK src/devices/ioexp/src_files_ioexp.mk
  mv src/devices/lcdCtrl/SRC_FILES_lcdCtrl.MK src/devices/lcdCtrl/src_files_lcdctrl.mk
  mv src/devices/ov1063x/SRC_FILES_ov1063x.MK src/devices/ov1063x/src_files_ov1063x.mk
  mv src/devices/ov10640/SRC_FILES_ov10640.MK src/devices/ov10640/src_files_ov10640.mk
  mv src/devices/pga450/SRC_FILES_pga450.MK src/devices/pga450/src_files_pga450.mk
  mv src/devices/radar_ar12xx/SRC_FILES_ar12xx.MK src/devices/radar_ar12xx/src_files_ar12xx.mk
  mv src/devices/sii9022a/SRC_FILES_sii9022a.MK src/devices/sii9022a/src_files_sii9022a.mk
  mv src/devices/sii9127/SRC_FILES_sii9127.MK src/devices/sii9127/src_files_sii9127.mk
  mv src/devices/tlc59108/SRC_FILES_tlc59108.MK src/devices/tlc59108/src_files_tlc59108.mk
  mv src/devices/tvp5158/SRC_FILES_tvp5158.MK src/devices/tvp5158/src_files_tvp5158.mk
  mv src/devices/ov2775/SRC_FILES_ov2775.MK src/devices/ov2775/src_files_ov2775.mk
fi
git checkout examples/dss/dss_baremetal_app
git checkout examples/utility/lnk_m4.cmd
git checkout src/vpslib/drv/makefile
git checkout examples/dss/displayDssBareMetal
git checkout examples/iss/isscapt_baremetal_app
git checkout examples/utility/lnk_a15.cmd
git checkout examples/utility/lnk_dsp.cmd
git checkout examples/utility/lnk_m4_am57x.cmd
git checkout examples/utility/makefile_baremetal.mk
git checkout examples/utility/src/bsputils_prf_baremetal.c
git checkout unit_test/captureUt/st_capture_c66x.cfg
git checkout unit_test/displayUt/displayUt_c66x.cfg
git checkout unit_test/cascadeRadarUt
git checkout unit_test/monitorRadarUt
git checkout unit_test/queueUt
git checkout unit_test/vpeUt/st_vpe_c66x.cfg
git checkout examples/dss/displayDssLowLatency/makefile

cd $working_dir

############## clean-up temp files ##############
cd $working_dir
rm -rf $sync_temp_dir

echo ""
echo "Attention:"
echo "Kindly merge include/platforms/* and src/platforms* manually as there are PDK specific changes done in it!!"
echo "Kindly merge src/boards/src/bsp_board.c manually as there are PDK specific changes done in it!!"
echo "Kindly merge examples/utility/src/bsputils_app.c manually as there are PDK specific changes done in it becasue of TI RTOS I2C driver!!"

echo "Checkout is disabled for above files so as to not revert any new changes!! Kindly commit or discard the changes with caution!!"
echo ""

#Requriement change for UT
find_replace() {
  input_file=$1
  target_file=$2

  #Replace one requirement at a time
  while read -r line
  do
    set -- $line
    #echo $1 to $2
    sed -i -e s/$1/$2/g $target_file
  done < $input_file

  # Check if we still have any old requriement ID
  num_count=`grep -r -e ':SR' "$target_file" | wc -l`
  if [ $num_count -gt 0 ]; then
    echo [WARN] ":SR" occurred $num_count times!! Old Requirement ID should not be there!! Cross check!!
  fi
}
find_replace requirement/vpe_req.txt $pdk_dir/packages/ti/drv/vps/unit_test/vpeUt/testInput/st_vpeTestCases.h
find_replace requirement/vip_req.txt $pdk_dir/packages/ti/drv/vps/unit_test/captureUt/testInput/st_captureTestCases.h
find_replace requirement/dss_req.txt $pdk_dir/packages/ti/drv/vps/unit_test/displayUt/testInput/displayUtTestInput_gblcfg.h

#Check if any of the replaced macros are still present in the source
./stw_sync_check_macros.sh $vps_dir

exit
