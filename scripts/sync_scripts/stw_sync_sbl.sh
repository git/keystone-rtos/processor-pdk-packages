#!/bin/bash
#   ============================================================================
#   @file   stw_sync_sbl.sh
#
#   @desc   Script to sync-up the SBL source code with starterware repo.
#           This will copy the source files from STW repo and move them as per
#           PDK dir structure. This also does the generic macro replacements.
#
#   ============================================================================
#   Revision History
#   08-Nov-2016 Sivaraj         Initial draft.
#
#   ============================================================================
# Command: stw_sync_sbl.sh <$stw_dir>
# Example (Linux): /datalocal/daily_build/scripts/stw_sync_sbl.sh /datalocal/daily_build/starterware/starterware_
# Example (Windows from git bash):                stw_sync_sbl.sh "/d/Vayu/Source/starterware/starterware_"

export PATH=$PATH:/bin
date=`date '+%b_%d_%Y'`
date_print=`date '+%d-%b-%Y %I:%M:%S %p'`
month=`date '+%b_%Y'`
year=`date '+%Y'`
if [ "$OS" == "" ]; then
  echo "No OS defined, defaulting to linux"
  OS=linux
fi

#Get user input
working_dir=`pwd`
stw_dir=$1
if [ "$stw_dir" == "" ]; then
  stw_dir=$working_dir/../../../starterware/starterware_
  if [ ! -d "$stw_dir" ]; then
    echo "Kindly provide Starterware path to copy files from!!"
    exit
  fi
fi
pdk_dir=$working_dir/../..
sbl_dir=$pdk_dir/packages/ti/boot/sbl_auto
sync_temp_dir=$working_dir/temp_sync

echo "PDK Starterware SBL Sync-up"
echo "Date: $date_print"
echo ""

sedErrCheck() {
if [ $1 -ne 0 ]; then
    echo "Error occurred @ $2!!"
fi
}

############## Start clean ##############
echo [INFO] Start Clean!! Deleting tobe copied files/folders...
rm -rf $sync_temp_dir
rm -rf $sbl_dir/sbl_app
rm -rf $sbl_dir/sbl_lib
rm -rf $sbl_dir/sbl_utils
rm -rf $sbl_dir/qspiflash
rm -rf $sbl_dir/norflash
rm -rf $sbl_dir/tools

############## Create required folders ##############
echo [INFO] Creating required folders...
mkdir -p $sync_temp_dir/sbl_app/src/test_auto
mkdir -p $sbl_dir/sbl_app/src
mkdir -p $sync_temp_dir/sbl_lib/src
mkdir -p $sbl_dir/sbl_lib/src
mkdir -p $sync_temp_dir/sbl_utils/src
mkdir -p $sbl_dir/sbl_utils/src
mkdir -p $sync_temp_dir/qspiflash/src
mkdir -p $sbl_dir/qspiflash/src
mkdir -p $sync_temp_dir/qspiflash/examples
mkdir -p $sbl_dir/qspiflash/examples
mkdir -p $sync_temp_dir/norflash/src
mkdir -p $sbl_dir/norflash/src
mkdir -p $sync_temp_dir/norflash/examples/nor_flash_writer
mkdir -p $sbl_dir/norflash/examples
mkdir -p $sync_temp_dir/tools
mkdir -p $sbl_dir/tools

############## Copy STW files to temp folder ##############
echo [INFO] Copying files from STW/BSP to temp folder...
cp -r $stw_dir/bootloader/sbl/src                       $sync_temp_dir/sbl_app
cp -r $stw_dir/bootloader/sbl/include/*                 $sync_temp_dir/sbl_app/src/test_auto
cp -r $stw_dir/include/sbl_lib/*.h                      $sync_temp_dir/sbl_lib
cp -r $stw_dir/bootloader/sbl_lib/*.h                   $sync_temp_dir/sbl_lib
cp -r $stw_dir/bootloader/sbl_lib/src                   $sync_temp_dir/sbl_lib
cp -r $stw_dir/include/sbl_utils/*.h                    $sync_temp_dir/sbl_utils
cp -r $stw_dir/bootloader/sbl_utils/src                 $sync_temp_dir/sbl_utils
cp -r $stw_dir/qspilib/qspi_flash/*.h                   $sync_temp_dir/qspiflash
cp -r $stw_dir/qspilib/qspi_flash/*.c                   $sync_temp_dir/qspiflash/src
cp -r $stw_dir/tools/flashtools/qspi_flash_writer       $sync_temp_dir/qspiflash/examples
cp -r $stw_dir/norflashlib/inc/*                        $sync_temp_dir/norflash
cp -r $stw_dir/norflashlib/src/*                        $sync_temp_dir/norflash/src
mv $sync_temp_dir/norflash/nor.h                        $sync_temp_dir/norflash/nor_flash.h
mv $sync_temp_dir/norflash/src/nor.c                    $sync_temp_dir/norflash/src/nor_flash.c
cp  $stw_dir/tools/flashtools/nor_flash_writer/*.*      $sync_temp_dir/norflash/examples/nor_flash_writer
cp -r $stw_dir/tools/flashtools/nor_flash_writer/inc/*  $sync_temp_dir/norflash/examples/nor_flash_writer
cp -r $stw_dir/tools/flashtools/nor_flash_writer/src/*  $sync_temp_dir/norflash/examples/nor_flash_writer
cp -r $stw_dir/bootloader/Tools/*                       $sync_temp_dir/tools
cp -r $stw_dir/security/tools/endian_swap               $sync_temp_dir/tools

############## Replace macros ##############
echo [INFO] Finding and replacing STW/BSP macros to PDK macros...
cd $sync_temp_dir
# _BUILD to SOC_
grep -r -l 'TDA2XX_BUILD'       * | xargs sed -i -e 's|TDA2XX_BUILD|SOC_TDA2XX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA2PX_BUILD'       * | xargs sed -i -e 's|TDA2PX_BUILD|SOC_TDA2PX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA2EX_BUILD'       * | xargs sed -i -e 's|TDA2EX_BUILD|SOC_TDA2EX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA3XX_BUILD'       * | xargs sed -i -e 's|TDA3XX_BUILD|SOC_TDA3XX|g';sedErrCheck $? $LINENO
#grep -r -l 'DRA7XX_BUILD'       * | xargs sed -i -e 's|DRA7XX_BUILD|SOC_DRA7XX|g';sedErrCheck $? $LINENO
grep -r -l '#include "'         * | xargs sed -i -e 's|#include "|#include <|g';sedErrCheck $? $LINENO
grep -r -l '\.h"'               * | xargs sed -i -e 's|\.h"|\.h>|g';sedErrCheck $? $LINENO

############## Remove unwanted include paths ##############
echo [INFO] Removing unwanted include path changes for PDK folder structure...
cd $sync_temp_dir
grep -r -l '#include <stwutils_app\.h>'         * | xargs sed -i -e 's|#include <stwutils_app\.h>|#include <ti/drv/stw_lld/examples/utility/stwutils_app\.h>|g';sedErrCheck $? $LINENO
sed -i '/#include <soc_defines\.h>/d'               $sync_temp_dir/sbl_app/src/tda2xx/sbl_tda2xx_main.c;sedErrCheck $? $LINENO
sed -i '/#include <cp15\.h>/d'                      $sync_temp_dir/sbl_app/src/tda2xx/sbl_tda2xx_main.c;sedErrCheck $? $LINENO
sed -i '/#include <pmhal_cm\.h>/d'                  $sync_temp_dir/sbl_app/src/tda2xx/sbl_tda2xx_main.c;sedErrCheck $? $LINENO
sed -i '/#include <mpu_wugen\.h>/d'                 $sync_temp_dir/sbl_app/src/tda2xx/sbl_tda2xx_main.c;sedErrCheck $? $LINENO
sed -i '/#include <pmhal_pdm\.h>/d'                 $sync_temp_dir/sbl_app/src/tda2xx/sbl_tda2xx_main.c;sedErrCheck $? $LINENO
sed -i '/#include <soc_defines\.h>/d'               $sync_temp_dir/sbl_app/src/tda2ex/sbl_tda2ex_main.c;sedErrCheck $? $LINENO
sed -i '/#include <cp15\.h>/d'                      $sync_temp_dir/sbl_app/src/tda2ex/sbl_tda2ex_main.c;sedErrCheck $? $LINENO
sed -i '/#include <pmhal_cm\.h>/d'                  $sync_temp_dir/sbl_app/src/tda2ex/sbl_tda2ex_main.c;sedErrCheck $? $LINENO
sed -i '/#include <mpu_wugen\.h>/d'                 $sync_temp_dir/sbl_app/src/tda2ex/sbl_tda2ex_main.c;sedErrCheck $? $LINENO
sed -i '/#include <pmhal_pdm\.h>/d'                 $sync_temp_dir/sbl_app/src/tda2ex/sbl_tda2ex_main.c;sedErrCheck $? $LINENO
sed -i '/#include <soc_defines\.h>/d'               $sync_temp_dir/sbl_app/src/mflash/sbl_mflash_utils.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_core\.h>/d'              $sync_temp_dir/sbl_app/src/mflash/sbl_mflash_utils.c;sedErrCheck $? $LINENO
sed -i '/#include <pm\/pmhal\/pmhal_rm\.h>/d'       $sync_temp_dir/sbl_app/src/mflash/sbl_mflash_utils.c;sedErrCheck $? $LINENO
sed -i '/#include <pm\/pmhal\/pmhal_mm\.h>/d'       $sync_temp_dir/sbl_app/src/mflash/sbl_mflash_utils.c;sedErrCheck $? $LINENO
sed -i '/#include <pm\/pmhal\/pmhal_pdm\.h>/d'      $sync_temp_dir/sbl_app/src/mflash/sbl_mflash_utils.c;sedErrCheck $? $LINENO
sed -i '/#include <soc_defines\.h>/d'               $sync_temp_dir/sbl_app/src/mflash/sbl_tda3xx_mflash_main.c;sedErrCheck $? $LINENO
sed -i '/#include <pm\/pmhal\/pmhal_cm\.h>/d'       $sync_temp_dir/sbl_app/src/mflash/sbl_tda3xx_mflash_main.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ipu_unicache_mmu\.h>/d'       $sync_temp_dir/sbl_app/src/tda3xx/sbl_tda3xx_main.c;sedErrCheck $? $LINENO
sed -i '/#include <ipu_wugen\.h>/d'                 $sync_temp_dir/sbl_app/src/tda3xx/sbl_tda3xx_main.c;sedErrCheck $? $LINENO
sed -i '/#include <ipu_ecc\.h>/d'                   $sync_temp_dir/sbl_app/src/tda3xx/sbl_tda3xx_main.c;sedErrCheck $? $LINENO
sed -i '/#include <unicache\.h>/d'                  $sync_temp_dir/sbl_app/src/tda3xx/sbl_tda3xx_main.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_core_sec\.h>/d'          $sync_temp_dir/sbl_app/src/tda3xx/sbl_tda3xx_main.c;sedErrCheck $? $LINENO
sed -i '/#include <soc_defines\.h>/d'               $sync_temp_dir/sbl_app/src/tda3xx/sbl_tda3xx_main.c;sedErrCheck $? $LINENO
sed -i '/#include <pm\/pmhal\/pmhal_cm\.h>/d'       $sync_temp_dir/sbl_app/src/tda3xx/sbl_tda3xx_main.c;sedErrCheck $? $LINENO
sed -i '/#include <pm\/pmhal\/pmhal_mm\.h>/d'       $sync_temp_dir/sbl_app/src/tda3xx/sbl_tda3xx_main.c;sedErrCheck $? $LINENO
sed -i '/#include <pm\/pmhal\/pmhal_rm\.h>/d'       $sync_temp_dir/sbl_app/src/tda3xx/sbl_tda3xx_main.c;sedErrCheck $? $LINENO
sed -i '/#include <pm\/pmhal\/pmhal_pdm\.h>/d'      $sync_temp_dir/sbl_app/src/tda3xx/sbl_tda3xx_main.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_device_prm\.h>/d'             $sync_temp_dir/sbl_app/src/sbl_test_auto.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_custefuse_cm_core\.h>/d'      $sync_temp_dir/sbl_app/src/sbl_test_auto.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_core_sec\.h>/d'          $sync_temp_dir/sbl_app/src/sbl_test_auto.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_wkup\.h>/d'              $sync_temp_dir/sbl_lib/src/sbl_lib_board.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_core\.h>/d'              $sync_temp_dir/sbl_lib/src/sbl_lib_board.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_core\.h>/d'              $sync_temp_dir/sbl_lib/src/sbl_lib_common.c;sedErrCheck $? $LINENO
sed -i '/#include <soc_defines\.h>/d'               $sync_temp_dir/sbl_lib/src/sbl_lib_common.c;sedErrCheck $? $LINENO
sed -i '/#include <pm\/pmhal\/pmhal_rm\.h>/d'       $sync_temp_dir/sbl_lib/src/sbl_lib_common.c;sedErrCheck $? $LINENO
sed -i '/#include <pm\/pmhal\/pmhal_mm\.h>/d'       $sync_temp_dir/sbl_lib/src/sbl_lib_common.c;sedErrCheck $? $LINENO
sed -i '/#include <pm\/pmhal\/pmhal_pdm\.h>/d'      $sync_temp_dir/sbl_lib/src/sbl_lib_common.c;sedErrCheck $? $LINENO
sed -i '/#include <soc_defines\.h>/d'               $sync_temp_dir/sbl_lib/src/tda2xx/sbl_lib_tda2xx_hs.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_core\.h>/d'              $sync_temp_dir/sbl_lib/src/tda2xx/sbl_lib_tda2xx_hs.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_core\.h>/d'              $sync_temp_dir/sbl_lib/src/tda2xx/sbl_lib_tda2xx_platform.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_core_sec\.h>/d'          $sync_temp_dir/sbl_lib/src/tda2xx/sbl_lib_tda2xx_platform.c;sedErrCheck $? $LINENO
sed -i '/#include <soc_defines\.h>/d'               $sync_temp_dir/sbl_lib/src/tda2xx/sbl_lib_tda2xx_platform.c;sedErrCheck $? $LINENO
sed -i '/#include <pmhal_mpu_lprm\.h>/d'            $sync_temp_dir/sbl_lib/src/tda2xx/sbl_lib_tda2xx_platform.c;sedErrCheck $? $LINENO
sed -i '/#include <pm\/pmhal\/pmhal_cm\.h>/d'       $sync_temp_dir/sbl_lib/src/tda2xx/sbl_lib_tda2xx_platform.c;sedErrCheck $? $LINENO
sed -i '/#include <pm\/pmhal\/pmhal_rm\.h>/d'       $sync_temp_dir/sbl_lib/src/tda2xx/sbl_lib_tda2xx_platform.c;sedErrCheck $? $LINENO
sed -i '/#include <pm\/pmhal\/pmhal_mm\.h>/d'       $sync_temp_dir/sbl_lib/src/tda2xx/sbl_lib_tda2xx_platform.c;sedErrCheck $? $LINENO
sed -i '/#include <pm\/pmhal\/pmhal_sd\.h>/d'       $sync_temp_dir/sbl_lib/src/tda2xx/sbl_lib_tda2xx_platform.c;sedErrCheck $? $LINENO
sed -i '/#include <pm\/pmhal\/pmhal_pdm\.h>/d'      $sync_temp_dir/sbl_lib/src/tda2xx/sbl_lib_tda2xx_platform.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_core\.h>/d'              $sync_temp_dir/sbl_lib/src/tda3xx/sbl_lib_tda3xx_platform.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_core_sec\.h>/d'          $sync_temp_dir/sbl_lib/src/tda3xx/sbl_lib_tda3xx_platform.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ipu_m4_nvic\.h>/d'            $sync_temp_dir/sbl_lib/src/tda3xx/sbl_lib_tda3xx_platform.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ipu_wugen_local_prcm\.h>/d'   $sync_temp_dir/sbl_lib/src/tda3xx/sbl_lib_tda3xx_platform.c;sedErrCheck $? $LINENO
sed -i '/#include <soc_defines\.h>/d'               $sync_temp_dir/sbl_lib/src/tda3xx/sbl_lib_tda3xx_platform.c;sedErrCheck $? $LINENO
sed -i '/#include <pm\/pmhal\/pmhal_rm\.h>/d'       $sync_temp_dir/sbl_lib/src/tda3xx/sbl_lib_tda3xx_platform.c;sedErrCheck $? $LINENO
sed -i '/#include <pm\/pmhal\/pmhal_mm\.h>/d'       $sync_temp_dir/sbl_lib/src/tda3xx/sbl_lib_tda3xx_platform.c;sedErrCheck $? $LINENO
sed -i '/#include <pm\/pmhal\/pmhal_sd\.h>/d'       $sync_temp_dir/sbl_lib/src/tda3xx/sbl_lib_tda3xx_platform.c;sedErrCheck $? $LINENO
sed -i '/#include <pm\/pmhal\/pmhal_pdm\.h>/d'      $sync_temp_dir/sbl_lib/src/tda3xx/sbl_lib_tda3xx_platform.c;sedErrCheck $? $LINENO
sed -i '/#include <soc\.h>/d'                       $sync_temp_dir/sbl_utils/src/sbl_utils_common.c;sedErrCheck $? $LINENO
sed -i '/#include <soc_defines\.h>/d'               $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2ex_ddr_config.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_core_pad\.h>/d'          $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2ex_ddr_config.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_wkup\.h>/d'              $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2ex_ddr_config.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_device_prm\.h>/d'             $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2ex_ddr_config.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ckgen_cm_core_aon\.h>/d'      $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2ex_ddr_config.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_core_pad\.h>/d'          $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2xx.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_core_pad_io\.h>/d'       $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2xx.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_core\.h>/d'              $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2xx.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_device_prm\.h>/d'             $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2xx.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_wkup\.h>/d'              $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2xx.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ckgen_prm\.h>/d'              $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2xx.c;sedErrCheck $? $LINENO
sed -i '/#include <soc_defines\.h>/d'               $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2xx.c;sedErrCheck $? $LINENO
sed -i '/#include <pm\/pm_utils\.h>/d'              $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2xx.c;sedErrCheck $? $LINENO
sed -i '/#include <pmhal_vm\.h>/d'                  $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2xx.c;sedErrCheck $? $LINENO
sed -i '/#include <pmhal_cm\.h>/d'                  $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2xx.c;sedErrCheck $? $LINENO
sed -i '/#include <pmhal_mm\.h>/d'                  $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2xx.c;sedErrCheck $? $LINENO
sed -i '/#include <pmhal_pdm\.h>/d'                 $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2xx.c;sedErrCheck $? $LINENO
sed -i '/#include <pmhal_pmic\.h>/d'                $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2xx.c;sedErrCheck $? $LINENO
sed -i '/#include <pmhal_pmicComm\.h>/d'            $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2xx.c;sedErrCheck $? $LINENO
sed -i '/#include <soc_defines\.h>/d'               $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2xx_ddr_config.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_core_pad\.h>/d'          $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2xx_ddr_config.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_wkup\.h>/d'              $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2xx_ddr_config.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_device_prm\.h>/d'             $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2xx_ddr_config.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ckgen_cm_core_aon\.h>/d'      $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2xx_ddr_config.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_core_pad_io\.h>/d'       $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2xx_iodelay.c;sedErrCheck $? $LINENO
sed -i '/#include <soc_defines\.h>/d'               $sync_temp_dir/sbl_utils/src/tda2xx/sbl_utils_tda2xx_iodelay.c;sedErrCheck $? $LINENO
sed -i '/#include <soc_defines\.h>/d'               $sync_temp_dir/sbl_utils/src/tda3xx/sbl_utils_tda3xx.c;sedErrCheck $? $LINENO
sed -i '/#include <unicache\.h>/d'                  $sync_temp_dir/sbl_utils/src/tda3xx/sbl_utils_tda3xx.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_core_sec\.h>/d'          $sync_temp_dir/sbl_utils/src/tda3xx/sbl_utils_tda3xx.c;sedErrCheck $? $LINENO
sed -i '/#include <pm\/pm_utils\.h>/d'              $sync_temp_dir/sbl_utils/src/tda3xx/sbl_utils_tda3xx.c;sedErrCheck $? $LINENO
sed -i '/#include <pmhal_cm\.h>/d'                  $sync_temp_dir/sbl_utils/src/tda3xx/sbl_utils_tda3xx.c;sedErrCheck $? $LINENO
sed -i '/#include <pmhal_mm\.h>/d'                  $sync_temp_dir/sbl_utils/src/tda3xx/sbl_utils_tda3xx.c;sedErrCheck $? $LINENO
sed -i '/#include <pmhal_vm\.h>/d'                  $sync_temp_dir/sbl_utils/src/tda3xx/sbl_utils_tda3xx.c;sedErrCheck $? $LINENO
sed -i '/#include <pmhal_pdm\.h>/d'                 $sync_temp_dir/sbl_utils/src/tda3xx/sbl_utils_tda3xx.c;sedErrCheck $? $LINENO
sed -i '/#include <pmhal_pmic\.h>/d'                $sync_temp_dir/sbl_utils/src/tda3xx/sbl_utils_tda3xx.c;sedErrCheck $? $LINENO
sed -i '/#include <pmhal_pmicComm\.h>/d'            $sync_temp_dir/sbl_utils/src/tda3xx/sbl_utils_tda3xx.c;sedErrCheck $? $LINENO
sed -i '/#include <soc_defines\.h>/d'               $sync_temp_dir/sbl_utils/src/tda3xx/sbl_utils_tda3xx_ddr_config.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_core_pad\.h>/d'          $sync_temp_dir/sbl_utils/src/tda3xx/sbl_utils_tda3xx_ddr_config.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_wkup\.h>/d'              $sync_temp_dir/sbl_utils/src/tda3xx/sbl_utils_tda3xx_ddr_config.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_device_prm\.h>/d'             $sync_temp_dir/sbl_utils/src/tda3xx/sbl_utils_tda3xx_ddr_config.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ckgen_cm_core_aon\.h>/d'      $sync_temp_dir/sbl_utils/src/tda3xx/sbl_utils_tda3xx_ddr_config.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_l4per_cm_core\.h>/d'          $sync_temp_dir/qspiflash/src/qspi_flash.c;sedErrCheck $? $LINENO
sed -i '/#include <hw_ctrl_core\.h>/d'              $sync_temp_dir/qspiflash/src/qspi_flash.c;sedErrCheck $? $LINENO
sed -i '/#include <soc_defines\.h>/d'               $sync_temp_dir/qspiflash/examples/qspi_flash_writer/main.c;sedErrCheck $? $LINENO
grep -r -l 'debug\.h>'                          * | xargs sed -i -e 's|debug\.h>|ti/boot/sbl_auto/norflash/src/debug\.h>|g';sedErrCheck $? $LINENO
sed -i -e 's|#include <src/tda|#include <ti/boot/sbl_auto/sbl_lib/src/tda|g' $sync_temp_dir/sbl_lib/sbl_lib_config.h;sedErrCheck $? $LINENO
grep -r -l '<sbl_test_auto\.h>'                 * | xargs sed -i -e 's|<sbl_test_auto\.h>|<ti/boot/sbl_auto/sbl_app/src/test_auto/sbl_test_auto\.h>|g';sedErrCheck $? $LINENO
sed -i -e 's|0x43300000U|SOC_EDMA_TPCC_BASE|g' $sync_temp_dir/sbl_lib/sbl_lib_board.h;sedErrCheck $? $LINENO

############## Copy files from temp folder to right PDK location ##############
echo [INFO] Copying files from temp folder to right PDK location...
cd $sync_temp_dir
cp -r $sync_temp_dir/sbl_app                    $sbl_dir
mv $sbl_dir/sbl_app/src/sbl_test_auto.c         $sbl_dir/sbl_app/src/test_auto
cp -r $sync_temp_dir/sbl_lib                    $sbl_dir
cp -r $sync_temp_dir/sbl_utils                  $sbl_dir
cp -r $sync_temp_dir/qspiflash/*.h              $sbl_dir/qspiflash
cp -r $sync_temp_dir/qspiflash/src              $sbl_dir/qspiflash
cp -r $sync_temp_dir/qspiflash/examples         $sbl_dir/qspiflash
cp -r $sync_temp_dir/norflash/*.h               $sbl_dir/norflash
cp -r $sync_temp_dir/norflash/src               $sbl_dir/norflash
cp -r $sync_temp_dir/norflash/examples          $sbl_dir/norflash
mv $sbl_dir/norflash/debug.h                    $sbl_dir/norflash/src
cp -r $sync_temp_dir/tools/*                    $sbl_dir/tools

############## Replace include paths ##############
echo [INFO] Replacing include path changes for PDK folder structure...
cd $sbl_dir
$pdk_dir/docs/stw_bsp_include_path_migration.sh $sbl_dir $pdk_dir no

############## Checkout not to be copied files ##############
cd $sbl_dir
git checkout $sbl_dir/sbl_app/makefile_all.mk
git checkout $sbl_dir/sbl_app/makefile_mshield.mk
git checkout $sbl_dir/sbl_app/makefile_opps.mk
git checkout $sbl_dir/sbl_app/src/tda2xx/makefile
git checkout $sbl_dir/sbl_app/src/tda2ex/makefile
git checkout $sbl_dir/sbl_app/src/tda3xx/makefile
rm -f $sbl_dir/sbl_app/src/tda2xx/makefile_tda2xx.mk
rm -f $sbl_dir/sbl_app/src/tda2ex/makefile_tda2ex.mk
rm -f $sbl_dir/sbl_app/src/tda3xx/makefile_tda3xx.mk
rm -f $sbl_dir/sbl_app/src/Makefile
git checkout $sbl_dir/sbl_lib/src/makefile
git checkout $sbl_dir/sbl_utils/src/makefile
git checkout $sbl_dir/qspiflash/makefile
git checkout $sbl_dir/qspiflash/src/makefile
git checkout $sbl_dir/qspiflash/qspiflash_component.mk
git checkout $sbl_dir/qspiflash/examples/qspi_flash_writer/makefile
git checkout $sbl_dir/norflash/makefile
git checkout $sbl_dir/norflash/src/makefile
git checkout $sbl_dir/norflash/norflash_component.mk
git checkout $sbl_dir/norflash/examples/nor_flash_writer/makefile
git checkout $sbl_dir/tools/MulticoreImageGen.bat
git checkout $sbl_dir/tools/MulticoreImageGen_tda2ex.bat
git checkout $sbl_dir/tools/MulticoreImageGen_tda2px.bat
git checkout $sbl_dir/tools/MulticoreImageGen_tda2xx.bat
git checkout $sbl_dir/tools/MulticoreImageGen_tda3xx.bat
git checkout $sbl_dir/tools/mflash/sbl_mflash_create_tda3xx.bat
git checkout $sbl_dir/tools/mflash/sbl_mflash_create_tda3xx.sh
#Case sensitive filenames
if [ "$OS" == "linux" ]; then
  rm -f $sbl_dir/qspiflash/examples/qspi_flash_writer/Makefile
  rm -f $sbl_dir/norflash/examples/nor_flash_writer/Makefile
fi

############## clean-up temp files ##############
cd $working_dir
rm -rf $sync_temp_dir

#Check if any of the replaced macros are still present in the source
./stw_sync_check_macros.sh $sbl_dir

exit
