#!/bin/bash
#   ============================================================================
#   @file   stw_sync_csl.sh
#
#   @desc   Script to sync-up the CSL source code with starterware repo.
#           This will copy the source files from STW repo and move them as per
#           PDK dir structure. This also does the generic macro replacements.
#
#   ============================================================================
#   Revision History
#   06-Mov-2015 Sivaraj         Initial draft.
#
#   ============================================================================

#   ============================================================================
#   CAUTION: DO NOT INVOKE THE SCRIPTS AS 'gitadmin'.
#            IT WILL ADD OBJECT FILES IN BASE REPOSITORY; WHICH SHOULD IDEALLY
#            BE CLEAN.
#   ============================================================
# Command: stw_sync_csl.sh <$stw_dir>
# Example (Linux): /datalocal/daily_build/scripts/stw_sync_csl.sh /datalocal/daily_build/starterware/starterware_
# Example (Windows from git bash):                stw_sync_csl.sh "/d/Vayu/Source/starterware/starterware_"

export PATH=$PATH:/bin
date=`date '+%b_%d_%Y'`
date_print=`date '+%d-%b-%Y %I:%M:%S %p'`
month=`date '+%b_%Y'`
year=`date '+%Y'`

#Get user input
#Get user input
working_dir=`pwd`
stw_dir=$1
if [ "$stw_dir" == "" ]; then
  stw_dir=$working_dir/../../../starterware/starterware_
  if [ ! -d "$stw_dir" ]; then
    echo "Kindly provide Starterware path to copy files from!!"
    exit
  fi
fi
pdk_dir=$working_dir/../..
csl_dir=$pdk_dir/packages/ti/csl
pdk_drv_dir=$pdk_dir/packages/ti/drv
sync_temp_dir=$working_dir/temp_sync

echo "PDK CSL Starterware Sync-up"
echo "Date: $date_print"
echo ""

sedErrCheck() {
if [ $1 -ne 0 ]; then
    echo "Error occurred @ $2!!"
fi
}

############## Start clean ##############
rm -rf $sync_temp_dir
#ADC
rm -f $csl_dir/src/ip/adc/V0/adc.h
rm -f $csl_dir/src/ip/adc/V0/hw_*.h
rm -f $csl_dir/src/ip/adc/V0/priv/adc.c
#CRC
rm -f $csl_dir/src/ip/crc/V0/crc.h
rm -f $csl_dir/src/ip/crc/V0/hw_*.h
rm -f $csl_dir/src/ip/crc/V0/priv/crc.c
#CAL
rm -f $csl_dir/src/ip/cal/V0/hw_*.h
#DCAN
rm -f $csl_dir/src/ip/dcan/V0/dcan.h
rm -f $csl_dir/src/ip/dcan/V0/V0_0/hw_*.h
rm -f $csl_dir/src/ip/dcan/V0/V0_1/hw_*.h
rm -f $csl_dir/src/ip/dcan/V0/priv/dcan.c
#DCC
rm -f $csl_dir/src/ip/dcc/V0/dcc.h
rm -f $csl_dir/src/ip/dcc/V0/hw_*.h
rm -f $csl_dir/src/ip/dcc/V0/priv/dcc.c
#DSS
rm -f $csl_dir/src/ip/dss/V1/hw_*.h
rm -f $csl_dir/src/ip/dss/V2/hw_*.h
#EDMA
rm -f $csl_dir/src/ip/edma/V1/edma.h
rm -f $csl_dir/src/ip/edma/V1/hw_*.h
rm -f $csl_dir/src/ip/edma/V1/priv/edma.c
#EMIF
rm -rf $csl_dir/src/ip/emif/V0
#ESM
rm -f $csl_dir/src/ip/esm/V0/esm.h
rm -f $csl_dir/src/ip/esm/V0/hw_*.h
rm -f $csl_dir/src/ip/esm/V0/priv/esm.c
#EVE
rm -f $csl_dir/src/ip/eve/V0/hw_*.h
#GPIO
rm -f $csl_dir/src/ip/gpio/V1/gpio_v2.h
rm -f $csl_dir/src/ip/gpio/V1/hw_*.h
rm -f $csl_dir/src/ip/gpio/V1/priv/gpio_v2.c
#GPMC
rm -f $csl_dir/src/ip/gpmc/V1/gpmc.h
rm -f $csl_dir/src/ip/gpmc/V1/hw_*.h
rm -f $csl_dir/src/ip/gpmc/V1/priv/gpmc.c
#I2C
rm -f $csl_dir/src/ip/i2c/V2/i2c.h
rm -f $csl_dir/src/ip/i2c/V2/hw_*.h
rm -f $csl_dir/src/ip/i2c/V2/priv/i2c.c
#ISS
rm -f $csl_dir/src/ip/iss/V0/hw_*.h
#L3FW
rm -f $csl_dir/src/ip/l3fw/V0/l3fw.h
rm -f $csl_dir/src/ip/l3fw/V0/hw_*.h
rm -f $csl_dir/src/ip/l3fw/V0/priv/l3fw.c
#L4FW
rm -f $csl_dir/src/ip/l4fw/V0/l4fw.h
rm -f $csl_dir/src/ip/l4fw/V0/hw_*.h
rm -f $csl_dir/src/ip/l4fw/V0/priv/l4fw.c
#MAILBOX
rm -f $csl_dir/src/ip/mailbox/V0/mailbox.h
rm -f $csl_dir/src/ip/mailbox/V0/V0_0/hw_*.h
rm -f $csl_dir/src/ip/mailbox/V0/priv/mailbox.c
#MCAN
rm -f $csl_dir/src/ip/mcan/V0/mcan.h
rm -f $csl_dir/src/ip/mcan/V0/hw_*.h
rm -f $csl_dir/src/ip/mcan/V0/priv/mcan.c
#MCASP
rm -f $csl_dir/src/ip/mcasp/V0/mcasp.h
rm -f $csl_dir/src/ip/mcasp/V0/hw_*.h
rm -f $csl_dir/src/ip/mcasp/V0/priv/mcasp.c
#MCSPI
rm -f $csl_dir/src/ip/mcspi/V0/mcspi.h
rm -f $csl_dir/src/ip/mcspi/V0/hw_*.h
rm -f $csl_dir/src/ip/mcspi/V0/priv/mcspi.c
#MMC
rm -f $csl_dir/src/ip/mmc/V0/mmcsd.h
rm -f $csl_dir/src/ip/mmc/V0/hw_*.h
rm -f $csl_dir/src/ip/mmc/V0/priv/mmcsd.c
#MMU
rm -f $csl_dir/src/ip/mmu/dsp/V0/mmu.h
rm -f $csl_dir/src/ip/mmu/dsp/V0/hw_*.h
rm -f $csl_dir/src/ip/mmu/dsp/V0/priv/mmu.c
rm -f $csl_dir/src/ip/mmu/ipu/V0/hw_*.h
#OCMC
rm -f $csl_dir/src/ip/ocmc/V0/hw_*.h
rm -rf $csl_dir/src/ip/ocmc/V0/priv
#PCIE
rm -f $csl_dir/src/ip/pcie/V1/pcie.h
rm -f $csl_dir/src/ip/pcie/V1/hw_*.h
rm -f $csl_dir/src/ip/pcie/V1/priv/pcie.c
#QSPI
rm -f $csl_dir/src/ip/qspi/V1/qspi.h
rm -f $csl_dir/src/ip/qspi/V1/hw_*.h
rm -f $csl_dir/src/ip/qspi/V1/priv/qspi.c
#RTI
rm -f $csl_dir/src/ip/rti/V0/rti.h
rm -f $csl_dir/src/ip/rti/V0/hw_*.h
rm -f $csl_dir/src/ip/rti/V0/priv/rti.c
#SPINLOCK
rm -f $csl_dir/src/ip/spinlock/V0/spinlock.h
rm -f $csl_dir/src/ip/spinlock/V0/hw_*.h
rm -f $csl_dir/src/ip/spinlock/V0/priv/spinlock.c
#SYNCTIMER
rm -f $csl_dir/src/ip/synctimer/V0/hw_*.h
#TESOC
rm -f $csl_dir/src/ip/tesoc/V0/tesoc.h
rm -f $csl_dir/src/ip/tesoc/V0/hw_*.h
rm -f $csl_dir/src/ip/tesoc/V0/priv/tesoc.c
#TIMER
rm -f $csl_dir/src/ip/timer/V1/timer.h
rm -f $csl_dir/src/ip/timer/V1/hw_*.h
rm -f $csl_dir/src/ip/timer/V1/priv/timer.c
#UART
rm -f $csl_dir/src/ip/uart/V1/uart.h
rm -f $csl_dir/src/ip/uart/V1/hw_*.h
rm -f $csl_dir/src/ip/uart/V1/priv/uart.c
#VIP/VPE
rm -f $csl_dir/src/ip/csc/V0/hw_*.h
rm -f $csl_dir/src/ip/chr_us/V0/hw_*.h
rm -f $csl_dir/src/ip/dei/V0/hw_*.h
rm -f $csl_dir/src/ip/sc/V0/hw_*.h
rm -f $csl_dir/src/ip/vip/V0/hw_*.h
rm -f $csl_dir/src/ip/vpe/V0/hw_*.h
rm -f $csl_dir/src/ip/vpdma/V0/hw_*.h
#WD_TIMER
rm -f $csl_dir/src/ip/wd_timer/V0/wd_timer.h
rm -f $csl_dir/src/ip/wd_timer/V0/hw_*.h
rm -f $csl_dir/src/ip/wd_timer/V0/priv/wd_timer.c
#CACHE
rm -rf $csl_dir/src/ip/cache/ipu/V0/
rm -rf $csl_dir/src/ip/cache/ipu/V1/
#SOC_FILES
rm -f $csl_dir/soc/tda2xx/hw_*.h
rm -f $csl_dir/soc/tda2px/hw_*.h
rm -f $csl_dir/soc/tda2ex/hw_*.h
rm -f $csl_dir/soc/tda3xx/hw_*.h
rm -f $csl_dir/soc/tda2xx/armv7a/*
rm -f $csl_dir/soc/tda2px/armv7a/*
rm -f $csl_dir/soc/tda2ex/armv7a/*
rm -f $csl_dir/soc/tda2xx/armv7m/*
rm -f $csl_dir/soc/tda2px/armv7m/*
rm -f $csl_dir/soc/tda2ex/armv7m/*
rm -f $csl_dir/soc/tda3xx/armv7m/*
rm -f $csl_dir/soc/tda2xx/c66x/*
rm -f $csl_dir/soc/tda2px/c66x/*
rm -f $csl_dir/soc/tda2ex/c66x/*
rm -f $csl_dir/soc/tda3xx/c66x/*
rm -f $csl_dir/soc/tda2xx/arp32/*
rm -f $csl_dir/soc/tda2px/arp32/*
rm -f $csl_dir/soc/tda3xx/arp32/*
#ARCH_FILES
rm -f $csl_dir/arch/a15/*.h
rm -f $csl_dir/arch/a15/src/*.*
rm -rf $csl_dir/arch/a15/V1/
rm -f $csl_dir/arch/c66x/*.h
rm -f $csl_dir/arch/c66x/src/*.*
rm -f $csl_dir/arch/m4/*.h
rm -f $csl_dir/arch/m4/src/*.c
rm -f $csl_dir/arch/arp32/*.h
rm -f $csl_dir/arch/arp32/src/*.*

############## Create required folders ##############
mkdir -p $sync_temp_dir
mkdir -p $sync_temp_dir/tda2xx
mkdir -p $sync_temp_dir/tda2px
mkdir -p $sync_temp_dir/tda2ex
mkdir -p $sync_temp_dir/tda3xx
mkdir -p $sync_temp_dir/tda2xx/hw
mkdir -p $sync_temp_dir/tda2px/hw
mkdir -p $sync_temp_dir/tda2ex/hw
mkdir -p $sync_temp_dir/tda3xx/hw
mkdir -p $sync_temp_dir/tda2xx/armv7a
mkdir -p $sync_temp_dir/tda2xx/armv7m
mkdir -p $sync_temp_dir/tda2xx/c66x
mkdir -p $sync_temp_dir/tda2xx/arp32
mkdir -p $sync_temp_dir/tda2px/armv7a
mkdir -p $sync_temp_dir/tda2px/armv7m
mkdir -p $sync_temp_dir/tda2px/c66x
mkdir -p $sync_temp_dir/tda2px/arp32
mkdir -p $sync_temp_dir/tda2ex/armv7a
mkdir -p $sync_temp_dir/tda2ex/armv7m
mkdir -p $sync_temp_dir/tda2ex/c66x
mkdir -p $sync_temp_dir/tda3xx/armv7m
mkdir -p $sync_temp_dir/tda3xx/c66x
mkdir -p $sync_temp_dir/tda3xx/arp32
mkdir -p $sync_temp_dir/c66x
mkdir -p $sync_temp_dir/c66x/src
mkdir -p $sync_temp_dir/m4
mkdir -p $sync_temp_dir/arp32
mkdir -p $sync_temp_dir/arp32/src
mkdir -p $sync_temp_dir/tda2xx/armv7a/tda2xx
mkdir -p $sync_temp_dir/tda2px/armv7a/tda2px

############## Copy STW files to temp folder ##############
#Copy DAL source, includes and hw files
cp $stw_dir/drivers/*.c                                           $sync_temp_dir
cp $stw_dir/include/*.h                                           $sync_temp_dir
cp $stw_dir/include/hw/*.h                                        $sync_temp_dir
cp $stw_dir/include/tda2xx/hw/*.h                                 $sync_temp_dir/tda2xx/
cp $stw_dir/include/tda2xx/tda2px/hw/*.h                          $sync_temp_dir/tda2px/
cp $stw_dir/include/tda2xx/tda2ex/hw/*.h                          $sync_temp_dir/tda2ex/
cp $stw_dir/include/tda3xx/hw/*.h                                 $sync_temp_dir/tda3xx/
cp $stw_dir/include/tda2xx/hw/*.h                                 $sync_temp_dir/tda2xx/hw
cp $stw_dir/include/tda2xx/tda2px/hw/*.h                          $sync_temp_dir/tda2px/hw
cp $stw_dir/include/tda2xx/hw/*.h                                 $sync_temp_dir/tda2px/hw
cp $stw_dir/include/tda2xx/tda2ex/hw/*.h                          $sync_temp_dir/tda2ex/hw
cp $stw_dir/include/tda2xx/hw/*.h                                 $sync_temp_dir/tda2ex/hw
cp $stw_dir/include/tda3xx/hw/*.h                                 $sync_temp_dir/tda3xx/hw
cp $stw_dir/include/armv7a/tda2xx/soc.h                           $sync_temp_dir/tda2xx/armv7a
cp $stw_dir/include/armv7a/tda2xx/soc.h                           $sync_temp_dir/tda2px/armv7a
cp $stw_dir/include/armv7a/tda2xx/soc.h                           $sync_temp_dir/tda2ex/armv7a
cp $stw_dir/include/armv7m/tda2xx/soc.h                           $sync_temp_dir/tda2xx/armv7m
cp $stw_dir/include/armv7m/tda2xx/soc.h                           $sync_temp_dir/tda2px/armv7m
cp $stw_dir/include/armv7m/tda2xx/soc.h                           $sync_temp_dir/tda2ex/armv7m
cp $stw_dir/include/armv7m/tda3xx/soc.h                           $sync_temp_dir/tda3xx/armv7m
cp $stw_dir/include/c66x/tda2xx/soc.h                             $sync_temp_dir/tda2xx/c66x
cp $stw_dir/include/c66x/tda2xx/soc.h                             $sync_temp_dir/tda2px/c66x
cp $stw_dir/include/c66x/tda2xx/soc.h                             $sync_temp_dir/tda2ex/c66x
cp $stw_dir/include/c66x/tda3xx/soc.h                             $sync_temp_dir/tda3xx/c66x
cp $stw_dir/include/arp32/tda2xx/soc.h                            $sync_temp_dir/tda2xx/arp32
cp $stw_dir/include/arp32/tda2xx/soc.h                            $sync_temp_dir/tda2px/arp32
cp $stw_dir/include/arp32/tda3xx/soc.h                            $sync_temp_dir/tda3xx/arp32
cp $stw_dir/include/armv7a/*.h                                    $sync_temp_dir/tda2xx/armv7a
cp $stw_dir/include/armv7a/tda2xx/*.h                             $sync_temp_dir/tda2xx/armv7a
cp $stw_dir/system_config/armv7a/tda2xx/*                         $sync_temp_dir/tda2xx/armv7a
cp $stw_dir/include/tda2xx/hw/hw_mpu_intc_phys.h                  $sync_temp_dir/tda2xx/armv7a
cp $stw_dir/system_config/c66x/*.c                                $sync_temp_dir/c66x/src
cp $stw_dir/system_config/c66x/*.asm                              $sync_temp_dir/c66x/src
cp $stw_dir/include/c66x/*.h                                      $sync_temp_dir/c66x
cp $stw_dir/include/hw/hw_dsp_icfg.h                              $sync_temp_dir/c66x
cp $stw_dir/include/hw/hw_dsp_system.h                            $sync_temp_dir/c66x
cp $stw_dir/include/hw/hw_xmc.h                                   $sync_temp_dir/c66x
cp $stw_dir/include/armv7m/interrupt.h                            $sync_temp_dir/m4
cp $stw_dir/include/armv7m/ipu_wugen.h                            $sync_temp_dir/m4
cp $stw_dir/include/armv7m/ammu.h                                 $sync_temp_dir/m4
cp $stw_dir/include/armv7m/unicache.h                             $sync_temp_dir/m4
cp $stw_dir/include/armv7m/tda3xx/ipu_ecc.h                       $sync_temp_dir/m4
cp $stw_dir/system_config/armv7m/ipu_wugen.c                      $sync_temp_dir/m4
cp $stw_dir/system_config/armv7m/interrupt.c                      $sync_temp_dir/m4
cp $stw_dir/system_config/armv7m/ammu.c                           $sync_temp_dir/m4
cp $stw_dir/system_config/armv7m/unicache.c                       $sync_temp_dir/m4
cp $stw_dir/system_config/armv7m/tda3xx/ipu_ecc.c                 $sync_temp_dir/m4
cp $stw_dir/include/hw/hw_ipu_m4_nvic.h                           $sync_temp_dir
cp $stw_dir/include/hw/hw_ipu_wugen_local_prcm.h                  $sync_temp_dir
cp $stw_dir/include/hw/hw_counter_32k.h                           $sync_temp_dir
cp $stw_dir/system_config/arp32/*.c                               $sync_temp_dir/arp32/src
cp $stw_dir/system_config/arp32/*.asm                             $sync_temp_dir/arp32/src
cp $stw_dir/include/arp32/*.h                                     $sync_temp_dir/arp32

############## Replace macros ##############
cd $sync_temp_dir
# _BUILD to SOC_
grep -r -l 'TDA2XX_BUILD' * | xargs sed -i -e 's|TDA2XX_BUILD|SOC_TDA2XX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA2PX_BUILD' * | xargs sed -i -e 's|TDA2PX_BUILD|SOC_TDA2PX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA2EX_BUILD' * | xargs sed -i -e 's|TDA2EX_BUILD|SOC_TDA2EX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA3XX_BUILD' * | xargs sed -i -e 's|TDA3XX_BUILD|SOC_TDA3XX|g';sedErrCheck $? $LINENO
# Add SOC_AMxxx/DRA support
grep -r -l 'defined (SOC_TDA2XX)' * | xargs sed -i -e 's:defined (SOC_TDA2XX):defined (SOC_TDA2XX) || defined (SOC_DRA75x) || defined (SOC_AM572x):g';sedErrCheck $? $LINENO
grep -r -l 'defined (SOC_TDA2EX)' * | xargs sed -i -e 's:defined (SOC_TDA2EX):defined (SOC_TDA2EX) || defined (SOC_DRA72x) || defined (SOC_AM571x):g';sedErrCheck $? $LINENO
grep -r -l 'defined (SOC_TDA3XX)' * | xargs sed -i -e 's:defined (SOC_TDA3XX):defined (SOC_TDA3XX) || defined (SOC_DRA78x):g';sedErrCheck $? $LINENO

############## Replace Functions ##############
cd $sync_temp_dir
#grep -r -l 'CP15FPUEnable'    * | xargs sed -i -e 's|CP15FPUEnable|CSL_a15FPUEnable|g';sedErrCheck $? $LINENO
#grep -r -l 'CP15VectorBaseAddrSet'    * | xargs sed -i -e 's|CP15VectorBaseAddrSet|CSL_a15VectorBaseAddrSet|g';sedErrCheck $? $LINENO
sed -i -e 's|CPUirqe|CSL_a15EnableIrq|g' $sync_temp_dir/tda2xx/armv7a/interrupt.c;sedErrCheck $? $LINENO
sed -i -e 's|CPUirqd|CSL_a15DisableIrq|g' $sync_temp_dir/tda2xx/armv7a/interrupt.c;sedErrCheck $? $LINENO
sed -i -e 's|CPUfiqd|CSL_a15DisableFiq|g' $sync_temp_dir/tda2xx/armv7a/interrupt.c;sedErrCheck $? $LINENO
sed -i -e 's|CPUfiqe|CSL_a15EnableFiq|g' $sync_temp_dir/tda2xx/armv7a/interrupt.c;sedErrCheck $? $LINENO
sed -i -e 's|CPUIntStatus|CSL_a15IntrStatus|g' $sync_temp_dir/tda2xx/armv7a/interrupt.c;sedErrCheck $? $LINENO
sed -i -e 's|CPUAbortHandler|Intc_AbortHandler|g' $sync_temp_dir/tda2xx/armv7a/exceptionhandler.asm;sedErrCheck $? $LINENO
sed -i -e 's|SVCHandler|SVC_Handler|g' $sync_temp_dir/tda2xx/armv7a/exceptionhandler.asm;sedErrCheck $? $LINENO
grep -r -l 'CopyVectorTable'    * | xargs sed -i -e 's|CopyVectorTable|CSL_A15_INIT_copyVectorTable|g';sedErrCheck $? $LINENO

############## Replace include paths ##############
cd $sync_temp_dir
#Change arch file includes in ARCH & remove other hw files inclusion as it is taken care in csl_arch.h
grep -r -l '#include "mpu_wugen\.h"'                *                                   | xargs sed -i -e 's|#include "mpu_wugen\.h"|#include <ti/csl/arch/csl_arch\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include "hw_ipu_unicache_cfg\.h"'      m4/*                                | xargs sed -i -e 's|#include "hw_ipu_unicache_cfg\.h"|#include <cslr_cache\.h>|g';sedErrCheck $? $LINENO
sed -i -e 's|#include "hw_intc\.h"|#include <ti/csl/arch/csl_arch\.h>|g'        tda2xx/armv7a/intvec.c
sed -i -e 's|#include "hw_intc\.h"|#include <ti/csl/arch/csl_arch\.h>|g'        tda2xx/armv7a/interrupt.h
sed -i -e 's|#include "hw_intc\.h"|#include <ti/csl/arch/csl_arch\.h>|g'        tda2xx/armv7a/interrupt.c
sed -i -e 's|#include "cpu\.h"|#include <ti/csl/csl_a15\.h>|g'                  tda2xx/armv7a/interrupt.c
sed -i -e 's|#include "cp15\.h"|#include <ti/csl/csl_a15\.h>|g'                 tda2xx/armv7a/intvec.c
sed -i '/#include "cpu\.h"/d'                                                   tda2xx/armv7a/intvec.c
sed -i '/#include "hw_mpu_wugen\.h"/d'                                          tda2xx/armv7a/mpu_wugen.c
sed -i '/#include "hw_mpu_intc_dist\.h"/d'                                      tda2xx/armv7a/mpu_wugen.c
sed -i '/#include "hw_mpu_intc_dist\.h"/d'                                      tda2xx/armv7a/interrupt.c
sed -i '/#include "hw_mpu_intc_phys\.h"/d'                                      tda2xx/armv7a/interrupt.c
sed -i '/#include "soc_defines\.h"/d'                                           c66x/src/dsp_wugen.c
sed -i '/#include "hw_dsp_system\.h"/d'                                         c66x/src/dsp_wugen.c
sed -i '/#include "hw_dsp_icfg\.h"/d'                                           c66x/src/dsp_wugen.c
sed -i '/#include "hw_dsp_icfg\.h"/d'                                           c66x/src/dsp_icfg.c
sed -i -e 's|#include "hw_dsp_system\.h"|#include <ti/csl/arch/csl_arch\.h>|g'  c66x/src/dsp_wugen.c
sed -i -e 's|XBAR_INST_DMA_DSP1_DREQ_19|CSL_XBAR_INST_DMA_DSP1_DREQ_19|g'       c66x/src/dsp_wugen.c
sed -i '/#include "hw_dsp_system\.h"/d'                                         c66x/src/dsp_config.c
sed -i '/#include "hw_dsp_icfg\.h"/d'                                           c66x/src/dsp_ecc.c
sed -i '/#include "hw_dsp_icfg\.h"/d'                                           c66x/src/dsp_xmc.c
sed -i '/#include "hw_xmc\.h"/d'                                                c66x/src/dsp_xmc.c
sed -i '/#include "dsp_icfg\.h"/d'                                              c66x/src/dsp_xmc.c
sed -i '/#include "hw_dsp_icfg\.h"/d'                                           c66x/src/interrupt.c
sed -i '/#include "hw_ipu_wugen_local_prcm\.h"/d'                               m4/ipu_wugen.c
sed -i '/#include "hw_ipu_m4_nvic\.h"/d'                                        m4/ipu_wugen.c
sed -i -e 's|#include "hw_ipu_unicache_mmu\.h"|#include <ti/csl/cslr_mmu\.h>|g' m4/ammu.h
sed -i '/#include "hw_ipu_unicache_mmu\.h"/d'                                   m4/ammu.c
#Change hw_ files
grep -r -l '#include "hw_types'                     *   | xargs sed -i -e 's|#include "hw_types|#include <hw_types|g';sedErrCheck $? $LINENO
grep -r -l '#include "hw_'                          *.h | xargs sed -i -e 's|#include "hw_|#include <ti/csl/cslr_|g';sedErrCheck $? $LINENO
grep -r -l '#include "hw_'                          *.c | xargs sed -i -e 's|#include "hw_|#include <ti/csl/cslr_|g';sedErrCheck $? $LINENO
grep -r -l '#include "'                             *   | xargs sed -i -e 's|#include "|#include <|g';sedErrCheck $? $LINENO
grep -r -l '\.h"'                                   *   | xargs sed -i -e 's|\.h"|\.h>|g';sedErrCheck $? $LINENO
#ARCH changes
grep -r -l '#include <hw_dsp_icfg\.h>'              *   | xargs sed -i -e 's|#include <hw_dsp_icfg\.h>|#include <ti/csl/arch/csl_arch\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <hw_xmc\.h>'                   *   | xargs sed -i -e 's|#include <hw_xmc\.h>|#include <ti/csl/arch/csl_arch\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <dsp_config\.h>'               *   | xargs sed -i -e 's|#include <dsp_config\.h>|#include <ti/csl/arch/csl_arch\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <dsp_ecc\.h>'                  *   | xargs sed -i -e 's|#include <dsp_ecc\.h>|#include <ti/csl/arch/csl_arch\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <dsp_icfg\.h>'                 *   | xargs sed -i -e 's|#include <dsp_icfg\.h>|#include <ti/csl/arch/csl_arch\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <dsp_usrSpvSupport\.h>'        *   | xargs sed -i -e 's|#include <dsp_usrSpvSupport\.h>|#include <ti/csl/arch/csl_arch\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <dsp_wugen\.h>'                *   | xargs sed -i -e 's|#include <dsp_wugen\.h>|#include <ti/csl/arch/csl_arch\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <dsp_xmc\.h>'                  *   | xargs sed -i -e 's|#include <dsp_xmc\.h>|#include <ti/csl/arch/csl_arch\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <hw_eve_control\.h>'           *   | xargs sed -i -e 's|#include <hw_eve_control\.h>|#include <ti/csl/cslr_eve\.h>|g';sedErrCheck $? $LINENO
#Replace file location changes
grep -r -l '#include <hw_types\.h>'                 *   | xargs sed -i -e 's|#include <hw_types\.h>|#include <ti/csl/hw_types\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <soc\.h>'                      *   | xargs sed -i -e 's|#include <soc\.h>|#include <ti/csl/soc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <soc_defines\.h>'              *   | xargs sed -i -e 's|#include <soc_defines\.h>|#include <ti/csl/soc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <hw_intc\.h>'                  *   | xargs sed -i -e 's|#include <hw_intc\.h>|#include <ti/csl/arch/a15/hw_intc\.h>|g';sedErrCheck $? $LINENO
#Replace file rename changes
grep -r -l 'common/stw_types\.h'                    *   | xargs sed -i -e 's|common/stw_types\.h|ti/csl/csl_types\.h|g';sedErrCheck $? $LINENO
grep -r -l 'cslr_edma_tc\.h'                        *   | xargs sed -i -e 's|cslr_edma_tc\.h|cslr_edma\.h|g';sedErrCheck $? $LINENO
grep -r -l 'hsi2c\.'                                *   | xargs sed -i -e 's|hsi2c\.|i2c\.|g';sedErrCheck $? $LINENO
grep -r -l 'hs_mmcsd\.'                             *   | xargs sed -i -e 's|hs_mmcsd\.|mmcsd\.|g';sedErrCheck $? $LINENO
grep -r -l 'cslr_pciectrl_ep_cfg_pcie\.h'           *   | xargs sed -i -e 's|cslr_pciectrl_ep_cfg_pcie\.h|cslr_pcie\.h|g';sedErrCheck $? $LINENO
grep -r -l 'cslr_ocmc_ram\.h'                       *   | xargs sed -i -e 's|cslr_ocmc_ram\.h|cslr_ocmc_ecc\.h|g';sedErrCheck $? $LINENO
grep -r -l 'cslr_l4_ap\.h'                          *   | xargs sed -i -e 's|cslr_l4_ap\.h|cslr_l4fw\.h|g';sedErrCheck $? $LINENO
grep -r -l 'cslr_mcanss\.h'                         *   | xargs sed -i -e 's|cslr_mcanss\.h|cslr_mcan\.h|g';sedErrCheck $? $LINENO
#Remove other unwanted header inclusion
grep -r -l 'common/stw_dataTypes\.h'                *   | xargs sed -i '/common\/stw_dataTypes\.h/d';sedErrCheck $? $LINENO
grep -r -l 'ti/csl/cslr_edma_tpcc\.h'               *   | xargs sed -i '/ti\/csl\/cslr_edma_tpcc\.h/d';sedErrCheck $? $LINENO
grep -r -l 'ti/csl/cslr_pciectrl_'                  *   | xargs sed -i '/ti\/csl\/cslr_pciectrl_/d';sedErrCheck $? $LINENO
grep -r -l 'ti/csl/cslr_l4_ia'                      *   | xargs sed -i '/ti\/csl\/cslr_l4_ia/d';sedErrCheck $? $LINENO
#Replace dal include file names with CSL include file names
grep -r -l '#include <adc\.h>'                      *   | xargs sed -i -e 's|#include <adc\.h>|#include <ti/csl/csl_adc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <crc\.h>'                      *   | xargs sed -i -e 's|#include <crc\.h>|#include <ti/csl/csl_crc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <dcan\.h>'                     *   | xargs sed -i -e 's|#include <dcan\.h>|#include <ti/csl/csl_dcan\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <dcc\.h>'                      *   | xargs sed -i -e 's|#include <dcc\.h>|#include <ti/csl/csl_dcc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <edma\.h>'                     *   | xargs sed -i -e 's|#include <edma\.h>|#include <ti/csl/csl_edma\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <emif\.h>'                     *   | xargs sed -i -e 's|#include <emif\.h>|#include <ti/csl/csl_emif\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <esm\.h>'                      *   | xargs sed -i -e 's|#include <esm\.h>|#include <ti/csl/csl_esm\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <gpio_v2\.h>'                  *   | xargs sed -i -e 's|#include <gpio_v2\.h>|#include <ti/csl/csl_gpio\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <gpmc\.h>'                     *   | xargs sed -i -e 's|#include <gpmc\.h>|#include <ti/csl/csl_gpmc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <i2c\.h>'                      *   | xargs sed -i -e 's|#include <i2c\.h>|#include <ti/csl/csl_i2c\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <l3fw\.h>'                     *   | xargs sed -i -e 's|#include <l3fw\.h>|#include <ti/csl/csl_l3fw\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <l4fw\.h>'                     *   | xargs sed -i -e 's|#include <l4fw\.h>|#include <ti/csl/csl_l4fw\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <mailbox\.h>'                  *   | xargs sed -i -e 's|#include <mailbox\.h>|#include <ti/csl/csl_mailbox\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <mcan\.h>'                     *   | xargs sed -i -e 's|#include <mcan\.h>|#include <ti/csl/csl_mcan\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <mcasp\.h>'                    *   | xargs sed -i -e 's|#include <mcasp\.h>|#include <ti/csl/csl_mcasp\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <mcspi\.h>'                    *   | xargs sed -i -e 's|#include <mcspi\.h>|#include <ti/csl/csl_mcspi\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <mmcsd\.h>'                    *   | xargs sed -i -e 's|#include <mmcsd\.h>|#include <ti/csl/csl_mmcsd\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <mmu\.h>'                      *   | xargs sed -i -e 's|#include <mmu\.h>|#include <ti/csl/csl_mmu\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <ocmc_ecc_l1\.h>'              *   | xargs sed -i -e 's|#include <ocmc_ecc_l1\.h>|#include <ti/csl/csl_ocmc_ecc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <ocmc_ecc_l2\.h>'              *   | xargs sed -i -e 's|#include <ocmc_ecc_l2\.h>|#include <ti/csl/csl_ocmc_ecc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <pcie\.h>'                     *   | xargs sed -i -e 's|#include <pcie\.h>|#include <ti/csl/csl_pcie\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <qspi\.h>'                     *   | xargs sed -i -e 's|#include <qspi\.h>|#include <ti/csl/csl_qspi\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <rti\.h>'                      *   | xargs sed -i -e 's|#include <rti\.h>|#include <ti/csl/csl_rti\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <spinlock\.h>'                 *   | xargs sed -i -e 's|#include <spinlock\.h>|#include <ti/csl/csl_spinlock\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <tesoc\.h>'                    *   | xargs sed -i -e 's|#include <tesoc\.h>|#include <ti/csl/csl_tesoc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <timer\.h>'                    *   | xargs sed -i -e 's|#include <timer\.h>|#include <ti/csl/csl_timer\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <uart\.h>'                     *   | xargs sed -i -e 's|#include <uart\.h>|#include <ti/csl/csl_uart\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <wd_timer\.h>'                 *   | xargs sed -i -e 's|#include <wd_timer\.h>|#include <ti/csl/csl_wd_timer\.h>|g';sedErrCheck $? $LINENO

############## Copy files from temp folder to right CSL location ##############
############## Before to which create required folders in CSL directory #######
mkdir -p $csl_dir/src/ip/adc
mkdir -p $csl_dir/src/ip/adc/V0
mkdir -p $csl_dir/src/ip/adc/V0/priv
mkdir -p $csl_dir/src/ip/crc
mkdir -p $csl_dir/src/ip/crc/V0
mkdir -p $csl_dir/src/ip/crc/V0/priv
mkdir -p $csl_dir/src/ip/cal
mkdir -p $csl_dir/src/ip/cal/V0
mkdir -p $csl_dir/src/ip/dcan
mkdir -p $csl_dir/src/ip/dcan/V0
mkdir -p $csl_dir/src/ip/dcan/V0/priv
mkdir -p $csl_dir/src/ip/dcan/V0/V0_0
mkdir -p $csl_dir/src/ip/dcan/V0/V0_1
mkdir -p $csl_dir/src/ip/dcc
mkdir -p $csl_dir/src/ip/dcc/V0
mkdir -p $csl_dir/src/ip/dcc/V0/priv
mkdir -p $csl_dir/src/ip/dss/V1
mkdir -p $csl_dir/src/ip/dss/V2
mkdir -p $csl_dir/src/ip/edma
mkdir -p $csl_dir/src/ip/edma/V1
mkdir -p $csl_dir/src/ip/edma/V1/priv
mkdir -p $csl_dir/src/ip/emif/V0/priv
mkdir -p $csl_dir/src/ip/emif/V0/V0_0
mkdir -p $csl_dir/src/ip/emif/V0/V0_1
mkdir -p $csl_dir/src/ip/emif/V0/V0_2
mkdir -p $csl_dir/src/ip/esm
mkdir -p $csl_dir/src/ip/esm/V0
mkdir -p $csl_dir/src/ip/esm/V0/priv
mkdir -p $csl_dir/src/ip/eve
mkdir -p $csl_dir/src/ip/eve/V0
mkdir -p $csl_dir/src/ip/gpio
mkdir -p $csl_dir/src/ip/gpio/V1
mkdir -p $csl_dir/src/ip/gpio/V1/priv
mkdir -p $csl_dir/src/ip/gpmc/V1/priv
mkdir -p $csl_dir/src/ip/i2c
mkdir -p $csl_dir/src/ip/i2c/V2
mkdir -p $csl_dir/src/ip/i2c/V2/priv
mkdir -p $csl_dir/src/ip/iss
mkdir -p $csl_dir/src/ip/iss/V0
mkdir -p $csl_dir/src/ip/l3fw
mkdir -p $csl_dir/src/ip/l3fw/V0
mkdir -p $csl_dir/src/ip/l3fw/V0/priv
mkdir -p $csl_dir/src/ip/l4fw/V0/priv
mkdir -p $csl_dir/src/ip/mailbox
mkdir -p $csl_dir/src/ip/mailbox/V0
mkdir -p $csl_dir/src/ip/mailbox/V0/V0_0
mkdir -p $csl_dir/src/ip/mailbox/V0/priv
mkdir -p $csl_dir/src/ip/mcan
mkdir -p $csl_dir/src/ip/mcan/V0
mkdir -p $csl_dir/src/ip/mcan/V0/priv
mkdir -p $csl_dir/src/ip/mcasp
mkdir -p $csl_dir/src/ip/mcasp/V0
mkdir -p $csl_dir/src/ip/mcasp/V0/priv
mkdir -p $csl_dir/src/ip/mcspi
mkdir -p $csl_dir/src/ip/mcspi/V0
mkdir -p $csl_dir/src/ip/mcspi/V0/priv
mkdir -p $csl_dir/src/ip/mmc
mkdir -p $csl_dir/src/ip/mmc/V0
mkdir -p $csl_dir/src/ip/mmc/V0/priv
mkdir -p $csl_dir/src/ip/mmu
mkdir -p $csl_dir/src/ip/mmu/dsp
mkdir -p $csl_dir/src/ip/mmu/dsp/V0
mkdir -p $csl_dir/src/ip/mmu/dsp/V0/priv
mkdir -p $csl_dir/src/ip/mmu
mkdir -p $csl_dir/src/ip/mmu/ipu
mkdir -p $csl_dir/src/ip/mmu/ipu/V0
mkdir -p $csl_dir/src/ip/ocmc
mkdir -p $csl_dir/src/ip/ocmc/V0
mkdir -p $csl_dir/src/ip/ocmc/V0/priv
mkdir -p $csl_dir/src/ip/pcie
mkdir -p $csl_dir/src/ip/pcie/V1
mkdir -p $csl_dir/src/ip/pcie/V1/priv
mkdir -p $csl_dir/src/ip/qspi
mkdir -p $csl_dir/src/ip/qspi/V1
mkdir -p $csl_dir/src/ip/qspi/V1/priv
mkdir -p $csl_dir/src/ip/rti
mkdir -p $csl_dir/src/ip/rti/V0
mkdir -p $csl_dir/src/ip/rti/V0/priv
mkdir -p $csl_dir/src/ip/spinlock
mkdir -p $csl_dir/src/ip/spinlock/V0
mkdir -p $csl_dir/src/ip/spinlock/V0/priv
mkdir -p $csl_dir/src/ip/synctimer
mkdir -p $csl_dir/src/ip/synctimer/V0
mkdir -p $csl_dir/src/ip/tesoc
mkdir -p $csl_dir/src/ip/tesoc/V0
mkdir -p $csl_dir/src/ip/tesoc/V0/priv
mkdir -p $csl_dir/src/ip/timer
mkdir -p $csl_dir/src/ip/timer/V1
mkdir -p $csl_dir/src/ip/timer/V1/priv
mkdir -p $csl_dir/src/ip/uart
mkdir -p $csl_dir/src/ip/uart/V1
mkdir -p $csl_dir/src/ip/uart/V1/priv
mkdir -p $csl_dir/src/ip/csc
mkdir -p $csl_dir/src/ip/csc/V0
mkdir -p $csl_dir/src/ip/chr_us
mkdir -p $csl_dir/src/ip/chr_us/V0
mkdir -p $csl_dir/src/ip/dei
mkdir -p $csl_dir/src/ip/dei/V0
mkdir -p $csl_dir/src/ip/sc
mkdir -p $csl_dir/src/ip/sc/V0
mkdir -p $csl_dir/src/ip/vip
mkdir -p $csl_dir/src/ip/vip/V0
mkdir -p $csl_dir/src/ip/vpe
mkdir -p $csl_dir/src/ip/vpe/V0
mkdir -p $csl_dir/src/ip/vpdma
mkdir -p $csl_dir/src/ip/vpdma/V0
mkdir -p $csl_dir/src/ip/wd_timer
mkdir -p $csl_dir/src/ip/wd_timer/V0
mkdir -p $csl_dir/src/ip/wd_timer/V0/priv
mkdir -p $csl_dir/src/ip/cache/
mkdir -p $csl_dir/src/ip/cache/ipu/V0
mkdir -p $csl_dir/src/ip/cache/ipu/V1
mkdir -p $csl_dir/arch
mkdir -p $csl_dir/arch/a15
mkdir -p $csl_dir/arch/a15/src
mkdir -p $csl_dir/arch/a15/V1
mkdir -p $csl_dir/arch/m4
mkdir -p $csl_dir/arch/m4/src
mkdir -p $csl_dir/arch/c66x
mkdir -p $csl_dir/arch/c66x/src
mkdir -p $csl_dir/arch/arp32
mkdir -p $csl_dir/arch/arp32/src
mkdir -p $csl_dir/soc/tda2xx/armv7a
mkdir -p $csl_dir/soc/tda2px/armv7a
mkdir -p $csl_dir/soc/tda2ex/armv7a
mkdir -p $csl_dir/soc/tda2xx/armv7m
mkdir -p $csl_dir/soc/tda2px/armv7m
mkdir -p $csl_dir/soc/tda2ex/armv7m
mkdir -p $csl_dir/soc/tda3xx/armv7m
mkdir -p $csl_dir/soc/tda2xx/c66x
mkdir -p $csl_dir/soc/tda2px/c66x
mkdir -p $csl_dir/soc/tda2ex/c66x
mkdir -p $csl_dir/soc/tda3xx/c66x
mkdir -p $csl_dir/soc/tda2xx/arp32
mkdir -p $csl_dir/soc/tda2px/arp32
mkdir -p $csl_dir/soc/tda3xx/arp32

cd $sync_temp_dir
#ADC
cp $sync_temp_dir/adc.h                     $csl_dir/src/ip/adc/V0
cp $sync_temp_dir/tda3xx/hw_adc*.h          $csl_dir/src/ip/adc/V0
cp $sync_temp_dir/adc.c                     $csl_dir/src/ip/adc/V0/priv
#CRC
cp $sync_temp_dir/crc.h                     $csl_dir/src/ip/crc/V0
cp $sync_temp_dir/tda3xx/hw_crc*.h          $csl_dir/src/ip/crc/V0
cp $sync_temp_dir/crc.c                     $csl_dir/src/ip/crc/V0/priv
#CAL
cp $sync_temp_dir/tda2ex/hw_cal.h           $csl_dir/src/ip/cal/V0
#DCAN
cp $sync_temp_dir/dcan.h                    $csl_dir/src/ip/dcan/V0/dcan.h
cp $sync_temp_dir/tda2xx/hw_dcan.h          $csl_dir/src/ip/dcan/V0/V0_0/hw_dcan.h
cp $sync_temp_dir/tda3xx/hw_dcan.h          $csl_dir/src/ip/dcan/V0/V0_1/hw_dcan.h
cp $sync_temp_dir/dcan.c                    $csl_dir/src/ip/dcan/V0/priv/dcan.c
#DCC
cp $sync_temp_dir/dcc.h                     $csl_dir/src/ip/dcc/V0
cp $sync_temp_dir/tda3xx/hw_dcc*.h          $csl_dir/src/ip/dcc/V0
cp $sync_temp_dir/dcc.c                     $csl_dir/src/ip/dcc/V0/priv
#DSS
cp $sync_temp_dir/tda2xx/hw_dss*.h          $csl_dir/src/ip/dss/V1
cp $sync_temp_dir/tda2xx/hw_hdmi*.h         $csl_dir/src/ip/dss/V1
cp $sync_temp_dir/tda2xx/hw_dispc*.h        $csl_dir/src/ip/dss/V1
cp $sync_temp_dir/tda3xx/hw_dss*.h          $csl_dir/src/ip/dss/V2
rm -f $csl_dir/src/ip/dss/V1/hw_dss_cm_core.h
rm -f $csl_dir/src/ip/dss/V1/hw_dss_prm.h
rm -f $csl_dir/src/ip/dss/V2/hw_dss_cm_core.h
rm -f $csl_dir/src/ip/dss/V2/hw_dss_glbce_rgb.h
rm -f $csl_dir/src/ip/dss/V2/hw_dss_prm.h
#EDMA
cp $sync_temp_dir/edma.h                    $csl_dir/src/ip/edma/V1
cp $sync_temp_dir/hw_edma*.h                $csl_dir/src/ip/edma/V1
cp $sync_temp_dir/edma.c                    $csl_dir/src/ip/edma/V1/priv
#EMIF
cp $sync_temp_dir/emif.h                    $csl_dir/src/ip/emif/V0/emif.h
cp $sync_temp_dir/tda2xx/hw_emif.h          $csl_dir/src/ip/emif/V0/V0_0/hw_emif.h
cp $sync_temp_dir/tda3xx/hw_emif.h          $csl_dir/src/ip/emif/V0/V0_1/hw_emif.h
cp $sync_temp_dir/tda2px/hw_emif.h          $csl_dir/src/ip/emif/V0/V0_2/hw_emif.h
cp $sync_temp_dir/emif.c                    $csl_dir/src/ip/emif/V0/priv/emif.c
#ESM
cp $sync_temp_dir/esm.h                     $csl_dir/src/ip/esm/V0
cp $sync_temp_dir/tda3xx/hw_esm*.h          $csl_dir/src/ip/esm/V0
cp $sync_temp_dir/esm.c                     $csl_dir/src/ip/esm/V0/priv
#EVE
cp $sync_temp_dir/tda2xx/hw_eve_control.h   $csl_dir/src/ip/eve/V0
#GPIO
cp $sync_temp_dir/gpio_v2.h                 $csl_dir/src/ip/gpio/V1
cp $sync_temp_dir/hw_gpio*.h                $csl_dir/src/ip/gpio/V1
cp $sync_temp_dir/gpio_v2.c                 $csl_dir/src/ip/gpio/V1/priv
#GPMC
cp $sync_temp_dir/gpmc.h                    $csl_dir/src/ip/gpmc/V1
cp $sync_temp_dir/hw_gpmc*.h                $csl_dir/src/ip/gpmc/V1
cp $sync_temp_dir/gpmc.c                    $csl_dir/src/ip/gpmc/V1/priv
#I2C
cp $sync_temp_dir/hsi2c.h                   $csl_dir/src/ip/i2c/V2/i2c.h
cp $sync_temp_dir/hw_i2c*.h                 $csl_dir/src/ip/i2c/V2
cp $sync_temp_dir/hsi2c.c                   $csl_dir/src/ip/i2c/V2/priv/i2c.c
#ISS
cp $sync_temp_dir/tda3xx/hw_iss_regs.h          $csl_dir/src/ip/iss/V0
cp $sync_temp_dir/tda3xx/hw_iss_h3a.h           $csl_dir/src/ip/iss/V0
cp $sync_temp_dir/tda3xx/hw_iss_ipipe.h         $csl_dir/src/ip/iss/V0
cp $sync_temp_dir/tda3xx/hw_iss_ipipeif.h       $csl_dir/src/ip/iss/V0
cp $sync_temp_dir/tda3xx/hw_iss_isif.h          $csl_dir/src/ip/iss/V0
cp $sync_temp_dir/tda3xx/hw_iss_lvdsrx.h        $csl_dir/src/ip/iss/V0
cp $sync_temp_dir/tda3xx/hw_iss_nsf3.h          $csl_dir/src/ip/iss/V0
cp $sync_temp_dir/tda3xx/hw_iss_regs.h          $csl_dir/src/ip/iss/V0
cp $sync_temp_dir/tda3xx/hw_iss_rsz.h           $csl_dir/src/ip/iss/V0
cp $sync_temp_dir/tda3xx/hw_iss_simcop_dma.h    $csl_dir/src/ip/iss/V0
cp $sync_temp_dir/tda3xx/hw_iss_simcop_regs.h   $csl_dir/src/ip/iss/V0
cp $sync_temp_dir/tda3xx/hw_iss_sys1.h          $csl_dir/src/ip/iss/V0
cp $sync_temp_dir/tda3xx/hw_iss_sys2.h          $csl_dir/src/ip/iss/V0
cp $sync_temp_dir/tda3xx/hw_iss_sys3.h          $csl_dir/src/ip/iss/V0
cp $sync_temp_dir/tda3xx/hw_iss_vtnf.h          $csl_dir/src/ip/iss/V0
#L3FW
cp $sync_temp_dir/l3fw.h                    $csl_dir/src/ip/l3fw/V0
cp $sync_temp_dir/tda3xx/hw_l3fw*.h         $csl_dir/src/ip/l3fw/V0
cp $sync_temp_dir/l3fw.c                    $csl_dir/src/ip/l3fw/V0/priv
#L4FW
cp $sync_temp_dir/l4fw.h                    $csl_dir/src/ip/l4fw/V0
cp $sync_temp_dir/hw_l4*.h                  $csl_dir/src/ip/l4fw/V0
cp $sync_temp_dir/l4fw.c                    $csl_dir/src/ip/l4fw/V0/priv
#Mailbox
cp $sync_temp_dir/mailbox.h                 $csl_dir/src/ip/mailbox/V0
cp $sync_temp_dir/hw_mailbox*.h             $csl_dir/src/ip/mailbox/V0/V0_0
cp $sync_temp_dir/mailbox.c                 $csl_dir/src/ip/mailbox/V0/priv
#MCAN
cp $sync_temp_dir/mcan.h                    $csl_dir/src/ip/mcan/V0
cp $sync_temp_dir/tda3xx/hw_mcan*.h         $csl_dir/src/ip/mcan/V0
cp $sync_temp_dir/mcan.c                    $csl_dir/src/ip/mcan/V0/priv
#MCASP
cp $sync_temp_dir/mcasp.h                   $csl_dir/src/ip/mcasp/V0
cp $sync_temp_dir/hw_mcasp*.h               $csl_dir/src/ip/mcasp/V0
cp $sync_temp_dir/mcasp.c                   $csl_dir/src/ip/mcasp/V0/priv
#MCSPI
cp $sync_temp_dir/mcspi.h                   $csl_dir/src/ip/mcspi/V0
cp $sync_temp_dir/hw_mcspi*.h               $csl_dir/src/ip/mcspi/V0
cp $sync_temp_dir/mcspi.c                   $csl_dir/src/ip/mcspi/V0/priv
#MMC
cp $sync_temp_dir/hs_mmcsd.h                $csl_dir/src/ip/mmc/V0/mmcsd.h
cp $sync_temp_dir/hw_mmc*.h                 $csl_dir/src/ip/mmc/V0
cp $sync_temp_dir/hs_mmcsd.c                $csl_dir/src/ip/mmc/V0/priv/mmcsd.c
#MMU
cp $sync_temp_dir/mmu.h                     $csl_dir/src/ip/mmu/dsp/V0
cp $sync_temp_dir/hw_mmu*.h                 $csl_dir/src/ip/mmu/dsp/V0
cp $sync_temp_dir/mmu.c                     $csl_dir/src/ip/mmu/dsp/V0/priv
cp $sync_temp_dir/hw_ipu_mmu.h              $csl_dir/src/ip/mmu/ipu/V0/hw_ipu_mmu.h
#OCMC
cp $sync_temp_dir/ocmc_ecc_l1.h             $csl_dir/src/ip/ocmc/V0
cp $sync_temp_dir/ocmc_ecc_l2.h             $csl_dir/src/ip/ocmc/V0
cp $sync_temp_dir/hw_ocmc*.h                $csl_dir/src/ip/ocmc/V0
cp $sync_temp_dir/ocmc_ecc_l1.c             $csl_dir/src/ip/ocmc/V0/priv
cp $sync_temp_dir/ocmc_ecc_l2.c             $csl_dir/src/ip/ocmc/V0/priv
#PCIE
cp $sync_temp_dir/pcie.h                    $csl_dir/src/ip/pcie/V1
cp $sync_temp_dir/tda2xx/hw_pcie*.h         $csl_dir/src/ip/pcie/V1
cp $sync_temp_dir/pcie.c                    $csl_dir/src/ip/pcie/V1/priv
#QSPI
cp $sync_temp_dir/qspi.h                    $csl_dir/src/ip/qspi/V1
cp $sync_temp_dir/hw_qspi*.h                $csl_dir/src/ip/qspi/V1
cp $sync_temp_dir/qspi.c                    $csl_dir/src/ip/qspi/V1/priv
#RTI
cp $sync_temp_dir/rti.h                     $csl_dir/src/ip/rti/V0
cp $sync_temp_dir/tda3xx/hw_rti*.h          $csl_dir/src/ip/rti/V0
cp $sync_temp_dir/rti.c                     $csl_dir/src/ip/rti/V0/priv
#SPINLOCK
cp $sync_temp_dir/spinlock.h                $csl_dir/src/ip/spinlock/V0
cp $sync_temp_dir/hw_spinlock*.h            $csl_dir/src/ip/spinlock/V0
cp $sync_temp_dir/spinlock.c                $csl_dir/src/ip/spinlock/V0/priv
#COUNTER_32K
cp $sync_temp_dir/hw_counter_32k.h          $csl_dir/src/ip/synctimer/V0
#TESOC
cp $sync_temp_dir/tesoc.h                   $csl_dir/src/ip/tesoc/V0
cp $sync_temp_dir/tda3xx/hw_tesoc*.h        $csl_dir/src/ip/tesoc/V0
cp $sync_temp_dir/tesoc.c                   $csl_dir/src/ip/tesoc/V0/priv
#TIMER
cp $sync_temp_dir/timer.h                   $csl_dir/src/ip/timer/V1
cp $sync_temp_dir/hw_timer*.h               $csl_dir/src/ip/timer/V1
cp $sync_temp_dir/timer.c                   $csl_dir/src/ip/timer/V1/priv
#UART
cp $sync_temp_dir/uart.h                    $csl_dir/src/ip/uart/V1
cp $sync_temp_dir/hw_uart*.h                $csl_dir/src/ip/uart/V1
cp $sync_temp_dir/uart.c                    $csl_dir/src/ip/uart/V1/priv
#VIP/VPE
cp $sync_temp_dir/hw_csc*.h                 $csl_dir/src/ip/csc/V0
cp $sync_temp_dir/hw_chr_us*.h              $csl_dir/src/ip/chr_us/V0
cp $sync_temp_dir/hw_dei*.h                 $csl_dir/src/ip/dei/V0
cp $sync_temp_dir/hw_sc*.h                  $csl_dir/src/ip/sc/V0
cp $sync_temp_dir/hw_vip*.h                 $csl_dir/src/ip/vip/V0
cp $sync_temp_dir/hw_vpe*.h                 $csl_dir/src/ip/vpe/V0
cp $sync_temp_dir/hw_vpdma*.h               $csl_dir/src/ip/vpdma/V0
#WD_TIMER
cp $sync_temp_dir/wd_timer.h                $csl_dir/src/ip/wd_timer/V0
cp $sync_temp_dir/hw_wd_timer*.h            $csl_dir/src/ip/wd_timer/V0
cp $sync_temp_dir/wd_timer.c                $csl_dir/src/ip/wd_timer/V0/priv
#CACHE
cp $sync_temp_dir/tda2xx/hw_ipu_unicache_cfg.h            $csl_dir/src/ip/cache/ipu/V0
cp $sync_temp_dir/tda3xx/hw_ipu_unicache_cfg.h            $csl_dir/src/ip/cache/ipu/V1
#MMU
cp $sync_temp_dir/hw_ipu_unicache_mmu.h                   $csl_dir/src/ip/mmu/ipu/V0
#ARCH_FILES
cp $sync_temp_dir/tda2xx/armv7a/exceptionhandler.asm     $csl_dir/arch/a15/src
cp $sync_temp_dir/tda2xx/armv7a/interrupt.c              $csl_dir/arch/a15/src
cp $sync_temp_dir/tda2xx/armv7a/mpu_wugen.c              $csl_dir/arch/a15/src
cp $sync_temp_dir/tda2xx/armv7a/*.h                      $csl_dir/arch/a15
cp $sync_temp_dir/tda2xx/hw/hw_mpu_intc_dist.h           $csl_dir/arch/a15
cp $sync_temp_dir/tda2xx/hw/hw_mpu_wugen.h               $csl_dir/arch/a15
cp $sync_temp_dir/tda2xx/armv7a/*.c                      $csl_dir/arch/a15/V1
cp $sync_temp_dir/tda2xx/armv7a/*.asm                    $csl_dir/arch/a15/V1
rm -f $csl_dir/arch/a15/V1/bl_init.asm
rm -f $csl_dir/arch/a15/V1/exceptionhandler.asm
rm -f $csl_dir/arch/a15/V1/initmmu.asm
rm -f $csl_dir/arch/a15/V1/interrupt.c
rm -f $csl_dir/arch/a15/V1/intvec.c
rm -f $csl_dir/arch/a15/V1/mpu_wugen.c
rm -f $csl_dir/arch/a15/soc.h
cp $sync_temp_dir/c66x/src/*                             $csl_dir/arch/c66x/src
cp $sync_temp_dir/c66x/*.h                               $csl_dir/arch/c66x/
cp $sync_temp_dir/m4/*.c                                 $csl_dir/arch/m4/src
cp $sync_temp_dir/m4/*.h                                 $csl_dir/arch/m4
cp $sync_temp_dir/arp32/src/*                            $csl_dir/arch/arp32/src
cp $sync_temp_dir/arp32/*.h                              $csl_dir/arch/arp32/

#Keep only necessary files and Remove unnecessary files from armv7a
rm -f $sync_temp_dir/tda2px/hw/hw_dcan.h
rm -f $sync_temp_dir/tda2px/hw/hw_dispc.h
rm -f $sync_temp_dir/tda2px/hw/hw_dss_family.h
rm -f $sync_temp_dir/tda2px/hw/hw_emif.h
rm -f $sync_temp_dir/tda2px/hw/hw_eve_control.h
rm -f $sync_temp_dir/tda2px/hw/hw_hdmi_dwc_tx.h
rm -f $sync_temp_dir/tda2px/hw/hw_hdmi_phy.h
rm -f $sync_temp_dir/tda2px/hw/hw_hdmi_pll.h
rm -f $sync_temp_dir/tda2px/hw/hw_hdmi_wp.h
rm -f $sync_temp_dir/tda2px/hw/hw_ipu_unicache_cfg.h
rm -f $sync_temp_dir/tda2px/hw/hw_l3fw.h
rm -f $sync_temp_dir/tda2px/hw/hw_mpu_axi2ocp.h
rm -f $sync_temp_dir/tda2px/hw/hw_mpu_csstm.h
rm -f $sync_temp_dir/tda2px/hw/hw_mpu_intc_dist.h
rm -f $sync_temp_dir/tda2px/hw/hw_mpu_intc_phys.h
rm -f $sync_temp_dir/tda2px/hw/hw_mpu_intc_virthyp.h
rm -f $sync_temp_dir/tda2px/hw/hw_mpu_intc_virtvm.h
rm -f $sync_temp_dir/tda2px/hw/hw_mpu_wdtimer.h
rm -f $sync_temp_dir/tda2px/hw/hw_mpu_wugen.h
rm -f $sync_temp_dir/tda2px/hw/hw_pciectrl_ep_cfg_dbics.h
rm -f $sync_temp_dir/tda2px/hw/hw_pciectrl_ep_cfg_dbics2.h
rm -f $sync_temp_dir/tda2px/hw/hw_pciectrl_ep_cfg_pcie.h
rm -f $sync_temp_dir/tda2px/hw/hw_pciectrl_pl_conf.h
rm -f $sync_temp_dir/tda2px/hw/hw_pciectrl_rc_cfg_dbics.h
rm -f $sync_temp_dir/tda2px/hw/hw_pciectrl_rc_cfg_dbics2.h
rm -f $sync_temp_dir/tda2px/hw/hw_pciectrl_ti_conf.h
rm -f $sync_temp_dir/tda3xx/hw/hw_esm.h
rm -f $sync_temp_dir/tda2xx/armv7a/mpu*
rm -rf $sync_temp_dir/tda2xx/armv7a/tda2xx
rm -f $sync_temp_dir/tda2xx/armv7a/bl_init*
rm -f $sync_temp_dir/tda2ex/hw/hw_cal.h
rm -f $sync_temp_dir/tda2ex/hw/hw_camerarx_core.h
rm -f $sync_temp_dir/tda2ex/hw/hw_dcan.h
rm -f $sync_temp_dir/tda2ex/hw/hw_dispc.h
rm -f $sync_temp_dir/tda2ex/hw/hw_dss_family.h
rm -f $sync_temp_dir/tda2ex/hw/hw_emif.h
rm -f $sync_temp_dir/tda2ex/hw/hw_eve_control.h
rm -f $sync_temp_dir/tda2ex/hw/hw_hdmi_*.h
rm -f $sync_temp_dir/tda2ex/hw/hw_ipu_unicache_cfg.h
rm -f $sync_temp_dir/tda2ex/hw/hw_l3fw.h
rm -f $sync_temp_dir/tda2ex/hw/hw_pciectrl_*.h
cp $sync_temp_dir/tda2ex/hw/hw_mpu_cm_core_aon.h      $csl_dir/soc/tda2ex/
cp $sync_temp_dir/tda2ex/hw/hw_mpu_prcm*.h            $csl_dir/soc/tda2ex/
cp $sync_temp_dir/tda2ex/hw/hw_mpu_prm.h              $csl_dir/soc/tda2ex/
rm -f $sync_temp_dir/tda2ex/hw/hw_mpu*
rm -f $sync_temp_dir/tda2xx/hw/hw_dcan.h
rm -f $sync_temp_dir/tda2xx/hw/hw_dispc.h
rm -f $sync_temp_dir/tda2xx/hw/hw_dss_family.h
rm -f $sync_temp_dir/tda2xx/hw/hw_emif.h
rm -f $sync_temp_dir/tda2xx/hw/hw_eve_control.h
rm -f $sync_temp_dir/tda2xx/hw/hw_hdmi_*.h
rm -f $sync_temp_dir/tda2xx/hw/hw_ipu_unicache_cfg.h
rm -f $sync_temp_dir/tda2xx/hw/hw_l3fw.h
rm -f $sync_temp_dir/tda2xx/hw/hw_pciectrl_*.h
cp $sync_temp_dir/tda2xx/hw/hw_mpu_cm_core_aon.h      $csl_dir/soc/tda2xx/
cp $sync_temp_dir/tda2xx/hw/hw_mpu_prcm*.h            $csl_dir/soc/tda2xx/
cp $sync_temp_dir/tda2xx/hw/hw_mpu_prm.h              $csl_dir/soc/tda2xx/
rm -f $sync_temp_dir/tda2xx/hw/hw_mpu*
rm -f $sync_temp_dir/tda3xx/hw/hw_l3fw.h
rm -f $sync_temp_dir/tda3xx/hw/hw_emif.h
rm -f $sync_temp_dir/tda3xx/hw/hw_eve_control.h
rm -f $sync_temp_dir/tda3xx/hw/hw_ipu_unicache_cfg.h
rm -f $sync_temp_dir/tda3xx/hw/hw_adc.h
rm -f $sync_temp_dir/tda3xx/hw/hw_crc.h
rm -f $sync_temp_dir/tda3xx/hw/hw_dcan.h
rm -f $sync_temp_dir/tda3xx/hw/hw_dcc.h
rm -f $sync_temp_dir/tda3xx/hw/hw_rti.h
rm -f $sync_temp_dir/tda3xx/hw/hw_tesoc.h
cp $sync_temp_dir/tda3xx/hw/hw_dss_cm_core.h           $csl_dir/soc/tda3xx/
cp $sync_temp_dir/tda3xx/hw/hw_dss_prm.h               $csl_dir/soc/tda3xx/
rm -f $sync_temp_dir/tda3xx/hw/hw_dss*
rm -f $sync_temp_dir/tda3xx/hw/hw_ipu_cm3_nvic.h
rm -f $sync_temp_dir/tda3xx/hw/hw_ipu_icecrusher.h
cp $sync_temp_dir/tda3xx/hw/hw_iss_cm_core_aon.h       $csl_dir/soc/tda3xx/
cp $sync_temp_dir/tda3xx/hw/hw_iss_ctset_core.h        $csl_dir/soc/tda3xx/
cp $sync_temp_dir/tda3xx/hw/hw_iss_prm.h               $csl_dir/soc/tda3xx/
rm -f $sync_temp_dir/tda3xx/hw/hw_iss*
rm -f $sync_temp_dir/tda3xx/hw/hw_mcan*
#SOC_FILES
cp $sync_temp_dir/tda2xx/hw/*                     $csl_dir/soc/tda2xx
cp $sync_temp_dir/tda2px/hw/*                     $csl_dir/soc/tda2px
cp $sync_temp_dir/tda2ex/hw/*                     $csl_dir/soc/tda2ex
cp $sync_temp_dir/tda3xx/hw/*                     $csl_dir/soc/tda3xx
cp $sync_temp_dir/tda2xx/armv7a/soc.h             $csl_dir/soc/tda2xx/armv7a
cp $sync_temp_dir/tda2px/armv7a/*                 $csl_dir/soc/tda2px/armv7a
cp $sync_temp_dir/tda2ex/armv7a/*                 $csl_dir/soc/tda2ex/armv7a
cp $sync_temp_dir/tda2xx/armv7m/*                 $csl_dir/soc/tda2xx/armv7m
cp $sync_temp_dir/tda2px/armv7m/*                 $csl_dir/soc/tda2px/armv7m
cp $sync_temp_dir/tda2ex/armv7m/*                 $csl_dir/soc/tda2ex/armv7m
cp $sync_temp_dir/tda3xx/armv7m/*                 $csl_dir/soc/tda3xx/armv7m
cp $sync_temp_dir/tda2xx/c66x/*                   $csl_dir/soc/tda2xx/c66x
cp $sync_temp_dir/tda2px/c66x/*                   $csl_dir/soc/tda2px/c66x
cp $sync_temp_dir/tda2ex/c66x/*                   $csl_dir/soc/tda2ex/c66x
cp $sync_temp_dir/tda3xx/c66x/*                   $csl_dir/soc/tda3xx/c66x
cp $sync_temp_dir/tda2xx/arp32/*                  $csl_dir/soc/tda2xx/arp32
cp $sync_temp_dir/tda2px/arp32/*                  $csl_dir/soc/tda2px/arp32
cp $sync_temp_dir/tda3xx/arp32/*                  $csl_dir/soc/tda3xx/arp32
cp $sync_temp_dir/hw_ipu_m4_nvic.h                $csl_dir/soc/tda2xx
cp $sync_temp_dir/hw_ipu_wugen_local_prcm.h       $csl_dir/soc/tda2xx
cp $sync_temp_dir/hw_ipu_m4_nvic.h                $csl_dir/soc/tda2px
cp $sync_temp_dir/hw_ipu_wugen_local_prcm.h       $csl_dir/soc/tda2px
cp $sync_temp_dir/hw_ipu_m4_nvic.h                $csl_dir/soc/tda2ex
cp $sync_temp_dir/hw_ipu_wugen_local_prcm.h       $csl_dir/soc/tda2ex
cp $sync_temp_dir/hw_ipu_m4_nvic.h                $csl_dir/soc/tda3xx
cp $sync_temp_dir/hw_ipu_wugen_local_prcm.h       $csl_dir/soc/tda3xx
############## Retain some files in original state which need manual merge ##############
cd $csl_dir/
echo "Kindly merge ti/csl/soc/tda2xx/cslr_soc_defines.h manually!!"
git checkout soc/tda2xx/cslr_soc_defines.h
echo "Kindly merge ti/csl/soc/tda2ex/cslr_soc_defines.h manually!!"
git checkout soc/tda2ex/cslr_soc_defines.h
echo "Kindly merge ti/csl/soc/tda3xx/cslr_soc_defines.h manually!!"
git checkout soc/tda3xx/cslr_soc_defines.h
echo "Kindly compare starterware_\system_config\armv7a\tda2xx\bl_init.asm with arch/a15/src/csl_a15_init.asm manually!!"
echo "Kindly compare starterware_\system_config\armv7a\tda2xx\intvec.c with arch/a15/src/csl_a15_startup.c manually!!"

############## Retain PDK specific files ##############
git checkout arch/a15/csl_a15_startup.h
git checkout arch/a15/src/csl_a15_init.asm
git checkout arch/a15/src/csl_a15_startup.c
git checkout arch/a15/src/exceptionhandler_keystone.asm
git checkout arch/a15/src/src_files_a15init.mk
git checkout src/ip/timer/V1/hw_dmtimer1ms.h

############## Final misc changes ##############
sed -i -e 's| \|\| defined (SOC_AM572x)||g' arch/a15/V1/cp15.c
sed -i -e 's| \|\| defined (SOC_AM571x)||g' arch/a15/V1/cp15.c
sed -i -e 's| \|\| defined (SOC_AM572x)||g' arch/a15/cp15.h
sed -i -e 's| \|\| defined (SOC_AM571x)||g' arch/a15/cp15.h
sed -i -e 's|<interrupt\.h>|<ti\/csl\/arch\/arp32\/interrupt\.h>|g'     arch/arp32/inth.h
sed -i -e 's|#include <ti\/csl\/arch\/csl_arch\.h>|\/\* None \*\/|g'    arch/c66x/dsp_icfg.h
sed -i -e 's|#include <ti\/csl\/arch\/csl_arch\.h>|\/\* None \*\/|g'    arch/c66x/dsp_xmc.h
sed -i -e 's|#include <|#include "|g'       arch/m4/src/ipu_ecc.c
sed -i -e 's|\.h>|\.h"|g'                   arch/m4/src/ipu_ecc.c
sed -i -e 's|#include <|#include "|g'       src/ip/crc/V0/crc.h
sed -i -e 's|\.h>|\.h"|g'                   src/ip/crc/V0/crc.h
sed -i -e 's|#include <|#include "|g'       src/ip/crc/V0/priv/crc.c
sed -i -e 's|\.h>|\.h"|g'                   src/ip/crc/V0/priv/crc.c
sed -i -e 's|#include <|#include "|g'       src/ip/i2c/V2/i2c.h
sed -i -e 's|\.h>|\.h"|g'                   src/ip/i2c/V2/i2c.h
sed -i -e 's|#include <|#include "|g'       src/ip/i2c/V2/priv/i2c.c
sed -i -e 's|\.h>|\.h"|g'                   src/ip/i2c/V2/priv/i2c.c
sed -i -e 's|#include <|#include "|g'       src/ip/mailbox/V0/mailbox.h
sed -i -e 's|\.h>|\.h"|g'                   src/ip/mailbox/V0/mailbox.h
sed -i -e 's|#include <|#include "|g'       src/ip/mailbox/V0/priv/mailbox.c
sed -i -e 's|\.h>|\.h"|g'                   src/ip/mailbox/V0/priv/mailbox.c
sed -i -e 's|#include <|#include "|g'       src/ip/ocmc/V0/ocmc_ecc_l1.h
sed -i -e 's|\.h>|\.h"|g'                   src/ip/ocmc/V0/ocmc_ecc_l1.h
sed -i -e 's|#include <|#include "|g'       src/ip/ocmc/V0/priv/ocmc_ecc_l1.c
sed -i -e 's|\.h>|\.h"|g'                   src/ip/ocmc/V0/priv/ocmc_ecc_l1.c
sed -i -e 's|#include <|#include "|g'       src/ip/ocmc/V0/priv/ocmc_ecc_l2.c
sed -i -e 's|\.h>|\.h"|g'                   src/ip/ocmc/V0/priv/ocmc_ecc_l2.c
#Remove AM macros from TDA soc files
grep -r -l ' || defined (SOC_AM572x)' soc/tda2xx | xargs sed -i -e 's: || defined (SOC_AM572x)::g';sedErrCheck $? $LINENO
grep -r -l ' || defined (SOC_AM572x)' soc/tda2ex | xargs sed -i -e 's: || defined (SOC_AM572x)::g';sedErrCheck $? $LINENO

############## clean-up temp files ##############
cd $working_dir
rm -rf $sync_temp_dir

#Check if any of the replaced macros are still present in the source
./stw_sync_check_macros.sh $csl_dir

exit
