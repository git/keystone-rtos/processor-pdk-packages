#!/bin/bash
#   ============================================================================
#   @file   stw_sync_pm.sh
#
#   @desc   Script to sync-up the source code with starterware repo. This will
#           copy the source files from STW repo and move them as per PDK dir
#           structure. This also does the generic macro replacements.
#
#   ============================================================================
#   Revision History
#   02-Sep-2015 Sivaraj         Initial draft.
#
#   ============================================================================

#   ============================================================================
#   CAUTION: DO NOT INVOKE THE SCRIPTS AS 'gitadmin'.
#            IT WILL ADD OBJECT FILES IN BASE REPOSITORY; WHICH SHOULD IDEALLY
#            BE CLEAN.
#   ============================================================
# Command: stw_sync_pm.sh <$stw_dir>
# Example (Linux): /datalocal/daily_build/scripts/stw_sync_pm.sh /datalocal/daily_build/starterware/starterware_
# Example (Windows from git bash):                stw_sync_pm.sh "/d/Vayu/Source/starterware/starterware_"

if [ "$OS" == "" ]; then
  echo [INFO] "No OS defined, defaulting to linux"
  OS=linux
fi

export PATH=$PATH:/bin
date=`date '+%b_%d_%Y'`
date_print=`date '+%d-%b-%Y %I:%M:%S %p'`
month=`date '+%b_%Y'`
year=`date '+%Y'`

#Get user input
working_dir=`pwd`
pdk_dir=$working_dir/../..
pm_dir=$pdk_dir/packages/ti/drv/pm
stw_dir=$1
if [ "$stw_dir" == "" ]; then
  stw_dir=$working_dir/../../../starterware/starterware_
  if [ ! -d "$stw_dir" ]; then
    echo "Kindly provide Starterware path to copy files from!!"
    exit
  fi
fi

echo "PDK PM Starterware Sync-up"
echo "Date: $date_print"
echo ""

sedErrCheck() {
if [ $1 -ne 0 ]; then
    echo "Error occurred @ $2!!"
fi
}

############## Start clean ##############
echo [INFO] Start Clean!! Deleting tobe copied files/folders...
rm -rf $pm_dir/include/*.h
rm -rf $pm_dir/include/prcm/V0
rm -rf $pm_dir/include/prcm/V1
rm -rf $pm_dir/include/prcm/V2
rm -rf $pm_dir/include/prcm/V5
rm -rf $pm_dir/include/prcm/*.h
rm -rf $pm_dir/src/prcm/pmhal
rm -rf $pm_dir/src/prcm/pmlib
rm -rf $pm_dir/include/pmic

############## Create required folders ##############
echo [INFO] Creating required folders...
mkdir -p $pm_dir/include
mkdir -p $pm_dir/include/prcm/V0
mkdir -p $pm_dir/include/prcm/V1
mkdir -p $pm_dir/include/prcm/V2
mkdir -p $pm_dir/include/prcm/V5
mkdir -p $pm_dir/include/psc
mkdir -p $pm_dir/src/pmhal/prcm/common
mkdir -p $pm_dir/src/pmhal/prcm/V0
mkdir -p $pm_dir/src/pmhal/prcm/V1
mkdir -p $pm_dir/src/pmhal/prcm/V2
mkdir -p $pm_dir/src/pmhal/prcm/V5
mkdir -p $pm_dir/src/pmhal/psc
mkdir -p $pm_dir/src/pmlib/prcm
mkdir -p $pm_dir/src/pmlib/psc
mkdir -p $pm_dir/src/pmrtos/prcm
mkdir -p $pm_dir/src/pmrtos/psc
mkdir -p $pm_dir/include/pmic

############## Copy STW files ##############
echo [INFO] Copying files from STW to PM folder...
#Copy includes
cp -r -f $stw_dir/include/pm/pm_*               $pm_dir/include
cp -r -f $stw_dir/include/pm/pmlib/*            $pm_dir/include
cp -r -f $stw_dir/include/pm/pmhal/*            $pm_dir/include/prcm
cp -r -f $stw_dir/include/tda2xx/pmhal*         $pm_dir/include/prcm/V0
# For tda2ex, first copy tda2xx and then overwrite with tda2ex specific files
cp -r -f $stw_dir/include/tda2xx/pmhal*         $pm_dir/include/prcm/V1
cp -r -f $stw_dir/include/tda2xx/tda2ex/pmhal*  $pm_dir/include/prcm/V1
cp -r -f $stw_dir/include/tda3xx/pmhal*         $pm_dir/include/prcm/V2
# For tda2px, first copy tda2xx and then overwrite with tda2px specific files
cp -r -f $stw_dir/include/tda2xx/pmhal*         $pm_dir/include/prcm/V5
cp -r -f $stw_dir/include/tda2xx/tda2px/pmhal*  $pm_dir/include/prcm/V5
#Copy source files except makefile, cd is needed to copy recursively with relative path/dir
cd $stw_dir/pm/pmhal
find ./ -iname '*.*' -print0 | xargs -0 cp -f --parents --target-directory=$pm_dir/src/pmhal/prcm
cd $stw_dir/pm/pmlib
find ./ -iname '*.*' -print0 | xargs -0 cp -f --parents --target-directory=$pm_dir/src/pmlib/prcm
rm -rf $pm_dir/src/pmhal/prcm/V0
rm -rf $pm_dir/src/pmhal/prcm/V2
rm -rf $pm_dir/src/pmhal/prcm/V5
rm -rf $pm_dir/src/pmlib/prcm/V0
rm -rf $pm_dir/src/pmlib/prcm/V2
rm -rf $pm_dir/src/pmlib/prcm/V5
mv $pm_dir/src/pmhal/prcm/tda2xx $pm_dir/src/pmhal/prcm/V0
mv $pm_dir/src/pmhal/prcm/tda3xx $pm_dir/src/pmhal/prcm/V2
mv $pm_dir/src/pmhal/prcm/tda2px $pm_dir/src/pmhal/prcm/V5
mv $pm_dir/src/pmlib/prcm/tda2xx $pm_dir/src/pmlib/prcm/V0
mv $pm_dir/src/pmlib/prcm/tda3xx $pm_dir/src/pmlib/prcm/V2
mv $pm_dir/src/pmlib/prcm/tda2px $pm_dir/src/pmlib/prcm/V5

############## Replace macros ##############
echo [INFO] Finding and replacing STW macros to PDK macros \(You can ignore \"sed: no input files\" messages!!\)...
cd $pm_dir
# _BUILD to SOC_
grep -r -l 'TDA2XX_BUILD'           * --exclude-dir=examples | xargs sed -i -e 's|TDA2XX_BUILD|SOC_TDA2XX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA2PX_BUILD'           * --exclude-dir=examples | xargs sed -i -e 's|TDA2PX_BUILD|SOC_TDA2PX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA2EX_BUILD'           * --exclude-dir=examples | xargs sed -i -e 's|TDA2EX_BUILD|SOC_TDA2EX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA3XX_BUILD'           * --exclude-dir=examples | xargs sed -i -e 's|TDA3XX_BUILD|SOC_TDA3XX|g';sedErrCheck $? $LINENO
# Add SOC_AMxxx/DRA support
grep -r -l 'defined (SOC_TDA2XX)'   * --exclude-dir=examples | xargs sed -i -e 's:defined (SOC_TDA2XX):defined (SOC_TDA2XX) || defined (SOC_AM572x) || defined (SOC_DRA75x):g';sedErrCheck $? $LINENO
grep -r -l 'defined (SOC_TDA2EX)'   * --exclude-dir=examples | xargs sed -i -e 's:defined (SOC_TDA2EX):defined (SOC_TDA2EX) || defined (SOC_DRA72x) || defined (SOC_AM571x):g';sedErrCheck $? $LINENO
grep -r -l 'defined (SOC_TDA3XX)'   * --exclude-dir=examples | xargs sed -i -e 's:defined (SOC_TDA3XX):defined (SOC_TDA3XX) || defined (SOC_DRA78x):g';sedErrCheck $? $LINENO
# BUILD_<SOC> to in build macro
grep -r -l 'BUILD_M4'               * --exclude-dir=examples | xargs sed -i -e 's|BUILD_M4|__TI_ARM_V7M4__|g';sedErrCheck $? $LINENO
grep -r -l 'BUILD_A15'              * --exclude-dir=examples | xargs sed -i -e 's|BUILD_A15|__ARM_ARCH_7A__|g';sedErrCheck $? $LINENO
grep -r -l 'BUILD_DSP'              * --exclude-dir=examples | xargs sed -i -e 's|BUILD_DSP|_TMS320C6X|g';sedErrCheck $? $LINENO

############## Replace Functions ##############
cd $pm_dir
grep -r -l 'CP15ReadCoreId'         * | xargs sed -i -e 's|CP15ReadCoreId|CSL_a15ReadCoreId|g';sedErrCheck $? $LINENO

############# Replace include path for interface ##############
echo [INFO] Replacing include path changes for PDK folder structure...
cd $pm_dir/include
grep -l '#include "pm_'    *.h | xargs sed -i -e 's|#include "pm_|#include <ti/drv/pm/include/pm_|g';sedErrCheck $? $LINENO
grep -l '#include "pmhal_' *.h | xargs sed -i -e 's|#include "pmhal_|#include <ti/drv/pm/include/prcm/pmhal_|g';sedErrCheck $? $LINENO
grep -l '#include "pmlib_' *.h | xargs sed -i -e 's|#include "pmlib_|#include <ti/drv/pm/include/pmlib_|g';sedErrCheck $? $LINENO
grep -l '#include "'       *.h | xargs sed -i -e 's|#include "|#include <|g';sedErrCheck $? $LINENO
grep -l '\.h"'             *.h | xargs sed -i -e 's|\.h"|\.h>|g';sedErrCheck $? $LINENO

cd $pm_dir/include/prcm
grep -l '#include "pm_'    *.h | xargs sed -i -e 's|#include "pm_|#include <ti/drv/pm/include/pm_|g';sedErrCheck $? $LINENO
grep -l '#include "pmhal_' *.h | xargs sed -i -e 's|#include "pmhal_|#include <ti/drv/pm/include/prcm/pmhal_|g';sedErrCheck $? $LINENO
grep -l '#include "pmlib_' *.h | xargs sed -i -e 's|#include "pmlib_|#include <ti/drv/pm/include/pmlib_|g';sedErrCheck $? $LINENO
grep -l '#include "'       *.h | xargs sed -i -e 's|#include "|#include <|g';sedErrCheck $? $LINENO
grep -l '.h"'              *.h | xargs sed -i -e 's|.h"|.h>|g';sedErrCheck $? $LINENO

cd $pm_dir/include/prcm/V0
grep -l '#include "pm_'    *.h | xargs sed -i -e 's|#include "pm_|#include <ti/drv/pm/include/pm_|g';sedErrCheck $? $LINENO
grep -l '#include "pmhal_' *.h | xargs sed -i -e 's|#include "pmhal_|#include <ti/drv/pm/include/prcm/pmhal_|g';sedErrCheck $? $LINENO
grep -l '#include "'       *.h | xargs sed -i -e 's|#include "|#include <|g';sedErrCheck $? $LINENO
grep -l '\.h"'             *.h | xargs sed -i -e 's|\.h"|\.h>|g';sedErrCheck $? $LINENO

cd $pm_dir/include/prcm/V1
grep -l '#include "pm_'    *.h | xargs sed -i -e 's|#include "pm_|#include <ti/drv/pm/include/pm_|g';sedErrCheck $? $LINENO
grep -l '#include "pmhal_' *.h | xargs sed -i -e 's|#include "pmhal_|#include <ti/drv/pm/include/prcm/pmhal_|g';sedErrCheck $? $LINENO
grep -l '#include "'       *.h | xargs sed -i -e 's|#include "|#include <|g';sedErrCheck $? $LINENO
grep -l '\.h"'             *.h | xargs sed -i -e 's|\.h"|\.h>|g';sedErrCheck $? $LINENO

cd $pm_dir/include/prcm/V2
grep -l '#include "pm_'    *.h | xargs sed -i -e 's|#include "pm_|#include <ti/drv/pm/include/pm_|g';sedErrCheck $? $LINENO
grep -l '#include "pmhal_' *.h | xargs sed -i -e 's|#include "pmhal_|#include <ti/drv/pm/include/prcm/pmhal_|g';sedErrCheck $? $LINENO
grep -l '#include "'       *.h | xargs sed -i -e 's|#include "|#include <|g';sedErrCheck $? $LINENO
grep -l '\.h"'             *.h | xargs sed -i -e 's|\.h"|\.h>|g';sedErrCheck $? $LINENO

cd $pm_dir/include/prcm/V5
grep -l '#include "pm_'    *.h | xargs sed -i -e 's|#include "pm_|#include <ti/drv/pm/include/pm_|g';sedErrCheck $? $LINENO
grep -l '#include "pmhal_' *.h | xargs sed -i -e 's|#include "pmhal_|#include <ti/drv/pm/include/prcm/pmhal_|g';sedErrCheck $? $LINENO
grep -l '#include "'       *.h | xargs sed -i -e 's|#include "|#include <|g';sedErrCheck $? $LINENO
grep -l '\.h"'             *.h | xargs sed -i -e 's|\.h"|\.h>|g';sedErrCheck $? $LINENO

############## Replace SOC include path ##############
cd $pm_dir/include
grep -r -l '#include "soc\.h"'          * | xargs sed -i -e 's|#include "soc\.h"|#include <ti/csl/soc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <soc\.h>'          * | xargs sed -i -e 's|#include <soc\.h>|#include <ti/csl/soc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include "hw_types\.h"'     * | xargs sed -i -e 's|#include "hw_types\.h"|#include <ti/csl/hw_types\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <hw_types\.h>'     * | xargs sed -i -e 's|#include <hw_types\.h>|#include <ti/csl/hw_types\.h>|g';sedErrCheck $? $LINENO
grep -r -l '"hw_hdmi_pll\.h"'           * | xargs sed -i -e 's|"hw_hdmi_pll\.h"|<ti/csl/cslr_dss\.h>|g';sedErrCheck $? $LINENO
grep -r -l 'hw_hdmi_pll\.h'             * | xargs sed -i -e 's|hw_hdmi_pll\.h|ti/csl/cslr_dss\.h|g';sedErrCheck $? $LINENO
grep -r -l '"hw_dss_family\.h"'         * | xargs sed -i -e 's|"hw_dss_family\.h"|<ti/csl/cslr_dss\.h>|g';sedErrCheck $? $LINENO
#Remove other SOC hw files as soc.h in PDK includes all the SOC HW files
grep -r -l 'hw_ctrl_core\.h'            * | xargs sed -i '/hw_ctrl_core\.h/d';sedErrCheck $? $LINENO
grep -r -l 'hw_ocp_socket_prm\.h'       * | xargs sed -i '/hw_ocp_socket_prm\.h/d';sedErrCheck $? $LINENO
grep -r -l 'hw_ctrl_core_sec'           * | xargs sed -i '/hw_ctrl_core_sec/d';sedErrCheck $? $LINENO
grep -r -l 'hw_device_prm\.h'           * | xargs sed -i '/hw_device_prm\.h/d';sedErrCheck $? $LINENO
grep -r -l 'hw_ctrl_wkup\.h'            * | xargs sed -i '/hw_ctrl_wkup\.h/d';sedErrCheck $? $LINENO
grep -r -l 'hw_ckgen_prm\.h'            * | xargs sed -i '/hw_ckgen_prm\.h/d';sedErrCheck $? $LINENO
grep -r -l '#include "hw_mpu_'          * | xargs sed -i '/#include "hw_mpu_/d';sedErrCheck $? $LINENO

cd $pm_dir/src/pmhal
grep -r -l '#include "soc\.h"'          * | xargs sed -i -e 's|#include "soc\.h"|#include <ti/csl/soc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <soc\.h>'          * | xargs sed -i -e 's|#include <soc\.h>|#include <ti/csl/soc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include "hw_types\.h"'     * | xargs sed -i -e 's|#include "hw_types\.h"|#include <ti/csl/hw_types\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <hw_types\.h>'     * | xargs sed -i -e 's|#include <hw_types\.h>|#include <ti/csl/hw_types\.h>|g';sedErrCheck $? $LINENO
grep -r -l '"hw_hdmi_pll\.h"'           * | xargs sed -i -e 's|"hw_hdmi_pll\.h"|<ti/csl/cslr_dss\.h>|g';sedErrCheck $? $LINENO
grep -r -l 'hw_hdmi_pll\.h'             * | xargs sed -i -e 's|hw_hdmi_pll\.h|ti/csl/cslr_dss\.h|g';sedErrCheck $? $LINENO
grep -r -l '"hw_dss_family\.h"'         * | xargs sed -i -e 's|"hw_dss_family\.h"|<ti/csl/cslr_dss\.h>|g';sedErrCheck $? $LINENO
sed -i -e 's|"pmhal_prcm_includes.h"|<ti/drv/pm/include/prcm/pmhal_prcm_includes\.h>|g' $pm_dir/src/pmhal/prcm/common/hw_pmhal_data.h
sed -i -e 's|"pmhal_prcm.h"|<ti/drv/pm/include/prcm/pmhal_prcm\.h>|g'                   $pm_dir/src/pmhal/prcm/common/hw_pmhal_data.h
#Remove other SOC hw files as soc.h in PDK includes all the SOC HW files
grep -r -l 'hw_ctrl_core\.h'            * | xargs sed -i '/hw_ctrl_core\.h/d';sedErrCheck $? $LINENO
grep -r -l 'hw_ocp_socket_prm\.h'       * | xargs sed -i '/hw_ocp_socket_prm\.h/d';sedErrCheck $? $LINENO
grep -r -l 'hw_ctrl_core_sec'           * | xargs sed -i '/hw_ctrl_core_sec/d';sedErrCheck $? $LINENO
grep -r -l 'hw_device_prm\.h'           * | xargs sed -i '/hw_device_prm\.h/d';sedErrCheck $? $LINENO
grep -r -l 'hw_ctrl_wkup\.h'            * | xargs sed -i '/hw_ctrl_wkup\.h/d';sedErrCheck $? $LINENO
grep -r -l 'hw_ckgen_prm\.h'            * | xargs sed -i '/hw_ckgen_prm\.h/d';sedErrCheck $? $LINENO
grep -r -l '#include "hw_mpu_'          * | xargs sed -i '/#include "hw_mpu_/d';sedErrCheck $? $LINENO

cd $pm_dir/src/pmlib
grep -r -l '#include "soc\.h"'         * | xargs sed -i -e 's|#include "soc\.h"|#include <ti/csl/soc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <soc\.h>'         * | xargs sed -i -e 's|#include <soc\.h>|#include <ti/csl/soc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include "hw_types\.h"'    * | xargs sed -i -e 's|#include "hw_types\.h"|#include <ti/csl/hw_types\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <hw_types\.h>'    * | xargs sed -i -e 's|#include <hw_types\.h>|#include <ti/csl/hw_types\.h>|g';sedErrCheck $? $LINENO
grep -r -l '"hw_hdmi_pll\.h"'          * | xargs sed -i -e 's|"hw_hdmi_pll\.h"|<ti/csl/cslr_dss\.h>|g';sedErrCheck $? $LINENO
grep -r -l 'hw_hdmi_pll\.h'            * | xargs sed -i -e 's|hw_hdmi_pll\.h|ti/csl/cslr_dss\.h|g';sedErrCheck $? $LINENO
grep -r -l '"hw_dss_family\.h"'        * | xargs sed -i -e 's|"hw_dss_family\.h"|<ti/csl/cslr_dss\.h>|g';sedErrCheck $? $LINENO
grep -r -l '"hw_eve_control\.h"'       * | xargs sed -i -e 's|"hw_eve_control\.h"|<ti/csl/cslr_eve\.h>|g';sedErrCheck $? $LINENO
sed -i -e 's|SOC_TDA2XX and SOC_TDA2EX|SOC_TDA2XX, SOC_DRA75x and SOC_TDA2EX|g' $pm_dir/src/pmlib/prcm/pmlib_videopll.c
#Remove other SOC hw files as soc.h in PDK includes all the SOC HW files
grep -r -l 'hw_ctrl_core\.h'            * | xargs sed -i '/hw_ctrl_core\.h/d';sedErrCheck $? $LINENO
grep -r -l 'hw_ocp_socket_prm\.h'       * | xargs sed -i '/hw_ocp_socket_prm\.h/d';sedErrCheck $? $LINENO
grep -r -l 'hw_ctrl_core_sec'           * | xargs sed -i '/hw_ctrl_core_sec/d';sedErrCheck $? $LINENO
grep -r -l 'hw_device_prm\.h'           * | xargs sed -i '/hw_device_prm\.h/d';sedErrCheck $? $LINENO
grep -r -l 'hw_ctrl_wkup\.h'            * | xargs sed -i '/hw_ctrl_wkup\.h/d';sedErrCheck $? $LINENO
grep -r -l 'hw_ckgen_prm\.h'            * | xargs sed -i '/hw_ckgen_prm\.h/d';sedErrCheck $? $LINENO
grep -r -l '#include "hw_mpu_'          * | xargs sed -i '/#include "hw_mpu_/d';sedErrCheck $? $LINENO
grep -r -l '#include "hw_ipu_m4_nvic'   * | xargs sed -i '/#include "hw_ipu_m4_nvic/d';sedErrCheck $? $LINENO
grep -r -l 'hw_ipu_wugen_local_prcm'    * | xargs sed -i '/hw_ipu_wugen_local_prcm/d';sedErrCheck $? $LINENO

#Change arch file includes in PM lib
grep -r -l '#include "cp15\.h"'          * | xargs sed -i -e 's|#include "cp15\.h"|#include <ti/csl/arch/csl_arch\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include "mpu_wugen\.h"'     * | xargs sed -i -e '/#include "mpu_wugen\.h"/d';sedErrCheck $? $LINENO
grep -r -l '#include "hw_dsp_icfg\.h"'   * | xargs sed -i -e 's|#include "hw_dsp_icfg\.h"|#include <ti/csl/arch/csl_arch\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include "hw_dsp_system\.h"' * | xargs sed -i '/#include "hw_dsp_system\.h"/d';sedErrCheck $? $LINENO
grep -r -l '#include "hw_ipu_mmu\.h"'    * | xargs sed -i -e 's|#include "hw_ipu_mmu\.h"|#include <ti/csl/cslr_mmu\.h>|g';sedErrCheck $? $LINENO

#Move PMIC files to PMIC folder
cd $pm_dir
grep -r -l '"pmhal_lp8731.h"'           * | xargs sed -i -e 's|"pmhal_lp8731.h"|<ti/drv/pm/include/pmic/pmhal_lp8731\.h>|g';sedErrCheck $? $LINENO
grep -r -l '"pmhal_lp8731_lp3907.h"'    * | xargs sed -i -e 's|"pmhal_lp8731_lp3907.h"|<ti/drv/pm/include/pmic/pmhal_lp8731_lp3907\.h>|g';sedErrCheck $? $LINENO
grep -r -l '"pmhal_lp8733.h"'           * | xargs sed -i -e 's|"pmhal_lp8733.h"|<ti/drv/pm/include/pmic/pmhal_lp8733\.h>|g';sedErrCheck $? $LINENO
grep -r -l '"pmhal_tps659037.h"'        * | xargs sed -i -e 's|"pmhal_tps659037.h"|<ti/drv/pm/include/pmic/pmhal_tps659037\.h>|g';sedErrCheck $? $LINENO
grep -r -l '"pmhal_tps659039.h"'        * | xargs sed -i -e 's|"pmhal_tps659039.h"|<ti/drv/pm/include/pmic/pmhal_tps659039\.h>|g';sedErrCheck $? $LINENO
grep -r -l '"pmhal_tps65917.h"'         * | xargs sed -i -e 's|"pmhal_tps65917.h"|<ti/drv/pm/include/pmic/pmhal_tps65917\.h>|g';sedErrCheck $? $LINENO
mv $pm_dir/src/pmhal/prcm/pmhal_lp8731.h        $pm_dir/include/pmic
mv $pm_dir/src/pmhal/prcm/pmhal_lp8731_lp3907.h $pm_dir/include/pmic
mv $pm_dir/src/pmhal/prcm/pmhal_lp8733.h        $pm_dir/include/pmic
mv $pm_dir/src/pmhal/prcm/pmhal_tps659037.h     $pm_dir/include/pmic
mv $pm_dir/src/pmhal/prcm/pmhal_tps659039.h     $pm_dir/include/pmic
mv $pm_dir/src/pmhal/prcm/pmhal_tps65917.h      $pm_dir/include/pmic
mv $pm_dir/src/pmhal/prcm/pmhal_lp87565.h       $pm_dir/include/pmic

#MISC changes
sed -i -e 's|tda2xx/pmhal_prcm.h|V0/pmhal_prcm.h|g'                include/prcm/V0/pmhal_prcm.h
sed -i -e 's|tda2xx/pmhal_prcm_modinc.h|V0/pmhal_prcm_modinc.h|g'  include/prcm/V0/pmhal_prcm_modinc.h
sed -i -e 's|tda2ex/pmhal_prcm.h|V1/pmhal_prcm.h|g'                include/prcm/V1/pmhal_prcm.h
sed -i -e 's|tda2xx/pmhal_prcm_modinc.h|V1/pmhal_prcm_modinc.h|g'  include/prcm/V1/pmhal_prcm_modinc.h
sed -i -e 's|tda3xx/pmhal_prcm.h|V2/pmhal_prcm.h|g'                include/prcm/V2/pmhal_prcm.h
sed -i -e 's|tda3xx/pmhal_prcm_modinc.h|V2/pmhal_prcm_modinc.h|g'  include/prcm/V2/pmhal_prcm_modinc.h
sed -i -e 's|tda2px/pmhal_prcm.h|V5/pmhal_prcm.h|g'                include/prcm/V5/pmhal_prcm.h
sed -i -e 's|tda2px/pmhal_prcm_modinc.h|V5/pmhal_prcm_modinc.h|g'  include/prcm/V5/pmhal_prcm_modinc.h
sed -i -e 's|tda2px/hw_pmhal_bgap_data.h|V5/hw_pmhal_bgap_data.h|g'             src/pmhal/prcm/V5/hw_pmhal_bgap_data.h
sed -i -e 's|tda2px/hw_pmhal_clocktree_data.h|V5/hw_pmhal_clocktree_data.h|g'   src/pmhal/prcm/V5/hw_pmhal_clocktree_data.h

# Checkout PDK specific files
git checkout $pm_dir/PowerDevice.h
git checkout $pm_dir/include/prcm/V0/PowerDevice.h
git checkout $pm_dir/include/prcm/V1/PowerDevice.h
git checkout $pm_dir/include/prcm/V2/PowerDevice.h
git checkout $pm_dir/include/prcm/V5/PowerDevice.h
git checkout $pm_dir/include/prcm/pmhal_prcm.h
git checkout $pm_dir/include/prcm/pmhal_prcm_includes.h
git checkout $pm_dir/include/prcm/pmhal_prcm_modinc.h
git checkout $pm_dir/src/pmrtos

echo ""
echo "Attention:"
echo "Retaining some files in original state which need manual merge!!"
cd $pm_dir/
echo "Kindly merge include/prcm/V0/pmhal_prcm_includes.h manually!!"
git checkout include/prcm/V0/pmhal_prcm_includes.h
echo "Kindly merge include/prcm/V1/pmhal_prcm_includes.h manually!!"
git checkout include/prcm/V1/pmhal_prcm_includes.h
echo "Kindly merge include/prcm/V2/pmhal_prcm_includes.h manually!!"
git checkout include/prcm/V2/pmhal_prcm_includes.h
echo "Kindly merge include/prcm/V5/pmhal_prcm_includes.h manually!!"
git checkout include/prcm/V5/pmhal_prcm_includes.h
echo "Kindly add newly included header files to pmhal.h/pmlib.h files manually"
git checkout pmhal.h
git checkout pmlib.h
echo "No changes in Examples needed. Kindly take care manually!!"
echo ""
git checkout examples

cd $working_dir

#Check if any of the replaced macros are still present in the source
./stw_sync_check_macros.sh $pm_dir

exit
