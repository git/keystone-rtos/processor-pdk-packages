#!/bin/bash
#   ============================================================================
#   @file   stw_sync_bsp_lld.sh
#
#   @desc   Script to sync-up the BSP LLDsource code with BSP repo.
#           This will copy the source files from BSP repo and move them as per
#           PDK dir structure. This also does the generic macro replacements.
#
#   ============================================================================
#   Revision History
#   08-Nov-2016 Sivaraj         Initial draft.
#
#   ============================================================================

#   ============================================================================
#   CAUTION: DO NOT INVOKE THE SCRIPTS AS 'gitadmin'.
#            IT WILL ADD OBJECT FILES IN BASE REPOSITORY; WHICH SHOULD IDEALLY
#            BE CLEAN.
#   ============================================================
# Command: stw_sync_bsp_lld.sh <$bsp_dir> <$bsptest_dir>
# Example (Linux):                 ./stw_sync_bsp_lld.sh /data/datalocal1_videoapps01/user/sivaraj/vayu_drivers/bspdrivers_
# Example (Windows from git bash): stw_sync_bsp_lld.sh   "/d/Vayu/Source/vayu_drivers/bspdrivers_"

export PATH=$PATH:/bin
date=`date '+%b_%d_%Y'`
date_print=`date '+%d-%b-%Y %I:%M:%S %p'`
month=`date '+%b_%Y'`
year=`date '+%Y'`
if [ "$OS" == "" ]; then
  echo "No OS defined, defaulting to linux"
  OS=linux
fi

#Get user input
working_dir=`pwd`
bsp_dir=$1
if [ "$bsp_dir" == "" ]; then
  bsp_dir=$working_dir/../../../vayu_drivers/bspdrivers_
  if [ ! -d "$bsp_dir" ]; then
    echo "Kindly provide BSP path to copy files from!!"
    exit
  fi
fi
bsptest_dir=$3
if [ "$bsptest_dir" == "" ]; then
  bsptest_dir=$bsp_dir/../../vayu_driver_test/bsptest
fi
pdk_dir=$working_dir/../..
bsp_lld_dir=$pdk_dir/packages/ti/drv/bsp_lld
sync_temp_dir=$working_dir/temp_sync

echo "PDK BSP_LLD-BSP Sync-up"
echo "Date: $date_print"
echo ""

sedErrCheck() {
if [ $1 -ne 0 ]; then
    echo "Error occurred @ $2!!"
fi
}

############## Start clean ##############
echo [INFO] Start Clean!! Deleting tobe copied files/folders...
rm -rf $sync_temp_dir
#I2C
rm -rf $bsp_lld_dir/i2c/*.h
rm -rf $bsp_lld_dir/i2c/src
rm -rf $bsp_lld_dir/i2c/examples
#UART
rm -rf $bsp_lld_dir/uart/*.h
rm -rf $bsp_lld_dir/uart/src
rm -rf $bsp_lld_dir/uart/examples
#McSPI
rm -rf $bsp_lld_dir/mcspi/*.h
rm -rf $bsp_lld_dir/mcspi/src
rm -rf $bsp_lld_dir/mcspi/examples
#Unit Test
rm -rf $bsp_lld_dir/uart/unit_test

############## Create required folders ##############
echo [INFO] Creating required folders...
mkdir -p $sync_temp_dir
mkdir -p $sync_temp_dir/i2c/src
mkdir -p $bsp_lld_dir/i2c/src
mkdir -p $sync_temp_dir/i2c/examples/i2c_led_blink
mkdir -p $bsp_lld_dir/i2c/examples/i2c_led_blink
mkdir -p $sync_temp_dir/i2c/examples/i2c_utility
mkdir -p $bsp_lld_dir/i2c/examples/i2c_utility
mkdir -p $sync_temp_dir/uart/src
mkdir -p $bsp_lld_dir/uart/src
mkdir -p $sync_temp_dir/uart/examples/uart_echo
mkdir -p $bsp_lld_dir/uart/examples/uart_echo
mkdir -p $sync_temp_dir/mcspi/src
mkdir -p $bsp_lld_dir/mcspi/src
mkdir -p $sync_temp_dir/mcspi/examples/mcspi_loopback
mkdir -p $bsp_lld_dir/mcspi/examples/mcspi_loopback
mkdir -p $sync_temp_dir/mcspi/examples/mcspi_spi1tospi2
mkdir -p $bsp_lld_dir/mcspi/examples/mcspi_spi1tospi2
mkdir -p $sync_temp_dir/uart/unit_test/uartUt
mkdir -p $bsp_lld_dir/uart/unit_test

############## Copy BSP files to temp folder ##############
echo [INFO] Copying files from STW/BSP to temp folder...
#Copy source, includes and other files
cp -r $bsp_dir/include/i2c/*.h                          $sync_temp_dir/i2c
cp -r $bsp_dir/src/i2c/src/*                            $sync_temp_dir/i2c/src
cp -r $bsp_dir/examples/i2c/*                           $sync_temp_dir/i2c/examples/i2c_led_blink
cp -r $bsp_dir/examples/misc/i2cUtility/*               $sync_temp_dir/i2c/examples/i2c_utility
cp -r $bsp_dir/include/uart/*.h                         $sync_temp_dir/uart
cp -r $bsp_dir/src/uart/src/*                           $sync_temp_dir/uart/src
cp -r $bsp_dir/examples/uart/*                          $sync_temp_dir/uart/examples/uart_echo
cp -r $bsp_dir/include/mcspi/*.h                        $sync_temp_dir/mcspi
cp -r $bsp_dir/src/mcspi/src/*                          $sync_temp_dir/mcspi/src
cp -r $bsp_dir/examples/mcspiMasterSlave/loopback/*     $sync_temp_dir/mcspi/examples/mcspi_loopback
cp -r $bsp_dir/examples/mcspiMasterSlave/spi1tospi2/*   $sync_temp_dir/mcspi/examples/mcspi_spi1tospi2
#Unit Test
cp -r $bsptest_dir/packages/ti/uartUt/*                 $sync_temp_dir/uart/unit_test/uartUt

#Remove unwanted files
rm $sync_temp_dir/i2c/examples/i2c_led_blink/i2cSampleM3vpss.cfg
rm $sync_temp_dir/i2c/examples/i2c_utility/i2cUtilityM3.cfg
rm $sync_temp_dir/uart/examples/uart_echo/AmmuCfg.cfg
rm $sync_temp_dir/uart/examples/uart_echo/uartSampleA15.cfg
rm $sync_temp_dir/uart/examples/uart_echo/uartSampleM3.cfg
rm $sync_temp_dir/mcspi/examples/mcspi_loopback/mcspiSampleM3.cfg
rm $sync_temp_dir/uart/unit_test/uartUt/Makefile
rm $sync_temp_dir/uart/unit_test/uartUt/st_uartM3.cfg

############## Replace macros ##############
echo [INFO] Finding and replacing STW/BSP macros to PDK macros...
cd $sync_temp_dir
# _BUILD to SOC_
grep -r -l 'TDA2XX_BUILD' * | xargs sed -i -e 's|TDA2XX_BUILD|SOC_TDA2XX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA2PX_BUILD' * | xargs sed -i -e 's|TDA2PX_BUILD|SOC_TDA2PX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA2EX_BUILD' * | xargs sed -i -e 's|TDA2EX_BUILD|SOC_TDA2EX|g';sedErrCheck $? $LINENO
grep -r -l 'TDA3XX_BUILD' * | xargs sed -i -e 's|TDA3XX_BUILD|SOC_TDA3XX|g';sedErrCheck $? $LINENO
grep -r -l 'DRA7XX'       * | xargs sed -i -e 's|DRA7XX|DRA75X|g';sedErrCheck $? $LINENO

############## Replace include paths ##############
echo [INFO] Replacing include path changes for PDK folder structure...
cd $sync_temp_dir
grep -r -l '#include "'                         * | xargs sed -i -e 's|#include "|#include <|g';sedErrCheck $? $LINENO
grep -r -l '\.h"'                               * | xargs sed -i -e 's|\.h"|\.h>|g';sedErrCheck $? $LINENO
#Replace file location changes
sed -i -e 's|soc_defines\.h|soc\.h|g' $sync_temp_dir/uart/unit_test/uartUt/testLib/st_uart.h
grep -r -l '#include <hw_types'                 * | xargs sed -i -e 's|#include <hw_types|#include <ti/csl/hw_types|g';sedErrCheck $? $LINENO
grep -r -l '#include <osal/'                    * | xargs sed -i -e 's|#include <osal/|#include <ti/drv/vps/include/osal/|g';sedErrCheck $? $LINENO
grep -r -l '#include <common/'                  * | xargs sed -i -e 's|#include <common/|#include <ti/drv/vps/include/common/|g';sedErrCheck $? $LINENO
grep -r -l 'i2c/bsp_i2c\.h'                     * | xargs sed -i -e 's|i2c/bsp_i2c\.h|ti/drv/bsp_lld/i2c/bsp_i2c\.h|g';sedErrCheck $? $LINENO
grep -r -l 'i2clib/lld_hsi2c\.h'                * | xargs sed -i -e 's|i2clib/lld_hsi2c\.h|ti/drv/stw_lld/i2clld/lld_hsi2c\.h|g';sedErrCheck $? $LINENO
grep -r -l 'uart/bsp_uart\.h'                   * | xargs sed -i -e 's|uart/bsp_uart\.h|ti/drv/bsp_lld/uart/bsp_uart\.h|g';sedErrCheck $? $LINENO
grep -r -l '<soc\.h>'                           * | xargs sed -i -e 's|<soc\.h>|<ti/csl/soc\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <uart\.h>'                 * | xargs sed -i -e 's|#include <uart\.h>|#include <ti/csl/csl_uart\.h>|g';sedErrCheck $? $LINENO
grep -r -l 'mcspi/bsp_mcspi\.h'                 * | xargs sed -i -e 's|mcspi/bsp_mcspi\.h|ti/drv/bsp_lld/mcspi/bsp_mcspi\.h|g';sedErrCheck $? $LINENO
grep -r -l '#include <mcspi\.h>'                * | xargs sed -i -e 's|#include <mcspi\.h>|#include <ti/csl/csl_mcspi\.h>|g';sedErrCheck $? $LINENO
grep -r -l 'pmhal_prcm\.h'                      * | xargs sed -i -e 's|pmhal_prcm\.h|ti/drv/pm/pmhal\.h|g';sedErrCheck $? $LINENO
grep -r -l '<platforms/'                        * | xargs sed -i -e 's|<platforms/|<ti/drv/vps/include/platforms/|g';sedErrCheck $? $LINENO
grep -r -l '<devices/'                          * | xargs sed -i -e 's|<devices/|<ti/drv/vps/include/devices/|g';sedErrCheck $? $LINENO
grep -r -l '<boards/'                           * | xargs sed -i -e 's|<boards/|<ti/drv/vps/include/boards/|g';sedErrCheck $? $LINENO
grep -r -l '<utility/bsputils_'                 * | xargs sed -i -e 's|<utility/bsputils_|<ti/drv/vps/examples/utility/bsputils_|g';sedErrCheck $? $LINENO
grep -r -l '#include <dma_xbar\.h>'             * | xargs sed -i -e 's|#include <dma_xbar\.h>|#include <ti/drv/stw_lld/platform/dma_xbar\.h>|g';sedErrCheck $? $LINENO
grep -r -l '#include <irq_xbar\.h>'             * | xargs sed -i -e 's|#include <irq_xbar\.h>|#include <ti/drv/stw_lld/platform/irq_xbar\.h>|g';sedErrCheck $? $LINENO
#Replace file/function/enums rename changes
#Remove other unwanted header inclusion
grep -r -l '#include <soc_defines\.h>'          $sync_temp_dir/uart/src     | xargs sed -i '/#include <soc_defines\.h>/d';sedErrCheck $? $LINENO
grep -r -l '#include <soc_defines\.h>'          $sync_temp_dir/mcspi/src    | xargs sed -i '/#include <soc_defines\.h>/d';sedErrCheck $? $LINENO
grep -r -l '#include <pmhal_mm\.h>'             * | xargs sed -i '/#include <pmhal_mm\.h>/d';sedErrCheck $? $LINENO
grep -r -l 'hw_ctrl_core'                       * | xargs sed -i '/hw_ctrl_core/d';sedErrCheck $? $LINENO
grep -r -l 'hw_l4per_cm_core'                   * | xargs sed -i '/hw_l4per_cm_core/d';sedErrCheck $? $LINENO
grep -r -l 'hw_ipu_cm_core_aon'                 * | xargs sed -i '/hw_ipu_cm_core_aon/d';sedErrCheck $? $LINENO
grep -r -l 'hw_wkupaon_cm'                      * | xargs sed -i '/hw_wkupaon_cm/d';sedErrCheck $? $LINENO
grep -r -l 'utility/bspCommonBIOS\.cfg'         * | xargs sed -i -e 's|utility/bspCommonBIOS\.cfg|\.\./\.\./\.\.\/\.\.\/vps/examples/utility/bspCommonBIOS\.cfg|g';sedErrCheck $? $LINENO

############## Copy files from temp folder to right PDK location ##############
echo [INFO] Copying files from temp folder to right PDK location...
cd $sync_temp_dir
#I2C
cp -r $sync_temp_dir/i2c/*                          $bsp_lld_dir/i2c/
cp -r $sync_temp_dir/uart/*                         $bsp_lld_dir/uart/
cp -r $sync_temp_dir/mcspi/*                        $bsp_lld_dir/mcspi/
#Copy unit test
cp -r $sync_temp_dir/uart/unit_test/uartUt/*        $bsp_lld_dir/uart/unit_test/uartUt

#Call generic script
$pdk_dir/docs/stw_bsp_include_path_migration.sh $bsp_lld_dir/i2c/examples/       $pdk_dir no
$pdk_dir/docs/stw_bsp_include_path_migration.sh $bsp_lld_dir/uart/examples/      $pdk_dir no
$pdk_dir/docs/stw_bsp_include_path_migration.sh $bsp_lld_dir/mcspi/examples/     $pdk_dir no

############## Checkout not to be copied files ##############
cd $bsp_lld_dir
git checkout $bsp_lld_dir/i2c/src/makefile
git checkout $bsp_lld_dir/i2c/examples/i2c_led_blink/makefile
git checkout $bsp_lld_dir/i2c/examples/i2c_led_blink/i2cSample.cfg
git checkout $bsp_lld_dir/i2c/examples/i2c_utility/makefile
git checkout $bsp_lld_dir/uart/src/makefile
git checkout $bsp_lld_dir/uart/examples/uart_echo/makefile
git checkout $bsp_lld_dir/mcspi/src/makefile
git checkout $bsp_lld_dir/mcspi/examples/mcspi_loopback/makefile
git checkout $bsp_lld_dir/mcspi/examples/mcspi_spi1tospi2/makefile
git checkout $bsp_lld_dir/uart/unit_test/bsp_uart_ut_component.mk
git checkout $bsp_lld_dir/uart/unit_test/uartUt/makefile
git checkout $bsp_lld_dir/uart/unit_test/uartUt/st_uart.cfg
#Case sensitive filenames
if [ "$OS" == "linux" ]; then
  rm -f $bsp_lld_dir/i2c/examples/i2c_utility/Makefile
  rm -f $bsp_lld_dir/uart/examples/uart_echo/Makefile
  rm -f $bsp_lld_dir/mcspi/examples/mcspi_loopback/Makefile
  rm -f $bsp_lld_dir/mcspi/examples/mcspi_spi1tospi2/Makefile
fi

############## clean-up temp files ##############
cd $working_dir
rm -rf $sync_temp_dir

#Requriement change for UT
find_replace() {
  input_file=$1
  target_file=$2

  #Replace one requirement at a time
  while read -r line
  do
    set -- $line
    #echo $1 to $2
    sed -i -e s/$1/$2/g $target_file
  done < $input_file

  # Check if we still have any old requriement ID
  num_count=`grep -r -e ':SR' "$target_file" | wc -l`
  if [ $num_count -gt 0 ]; then
    echo [WARN] ":SR" occurred $num_count times!! Old Requirement ID should not be there!! Cross check!!
  fi
}
find_replace requirement/uart_req.txt $pdk_dir/packages/ti/drv/bsp_lld/uart/unit_test/uartUt/testInput/st_uartTestCases.h

#Check if any of the replaced macros are still present in the source
./stw_sync_check_macros.sh $bsp_lld_dir

exit
