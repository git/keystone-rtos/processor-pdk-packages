find_replace() {
  input_file=$1
  target_file=$2

  #Replace one requirement at a time
  while read -r line
  do
    set -- $line
    #echo $1 to $2
    sed -i -e s/$1/$2/g $target_file
  done < $input_file

  # Check if we still have any old requriement ID
  num_count=`grep -r -e ':SR' "$target_file" | wc -l`
  if [ $num_count -gt 0 ]; then
    echo [WARN] ":SR" occurred $num_count times!! Old Requirement ID should not be there!! Cross check!!
  fi
}

find_replace vpe_req.txt    ../../../packages/ti/drv/vps/unit_test/vpeUt/testInput/st_vpeTestCases.h
find_replace vip_req.txt    ../../../packages/ti/drv/vps/unit_test/captureUt/testInput/st_captureTestCases.h
find_replace dss_req.txt    ../../../packages/ti/drv/vps/unit_test/displayUt/testInput/displayUtTestInput_gblcfg.h
find_replace uart_req.txt   ../../../packages/ti/drv/bsp_lld/uart/unit_test/uartUt/testInput/st_uartTestCases.h
