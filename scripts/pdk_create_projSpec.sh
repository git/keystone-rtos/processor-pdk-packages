#!/bin/bash
# ******************************************************************************
# * FILE PURPOSE: PDK Unit Test and Example ProjectSpec Creator
# ******************************************************************************
# * FILE NAME: pdk_create_projspec.sh
# *
# * DESCRIPTION: 
# *  The script file is used to create the test and example projects of all
# *  components under PDK. These projects are available in the specified 
# *  workspace. It expects to be run in the folder pdk/packages.
# *
# * USAGE:
# *  pdk_create_projspec.sh [soc]
# * 
# *  Description:      (first option is default)
# *  soc           -   AM335x / AM437x / AM571x / AM572x / AM574x / K2E / K2G / K2K / K2H / K2L /
# *                    C6678 / C6657 / DRA72x / DRA75x / DRA78x / OMAPL137 / OMAPL138 / am65xx
# * 
# *  Example:
# *      pdk_create_projspec.sh AM572x
# * 
# * Copyright (C) 2012-2020, Texas Instruments, Inc.
# *****************************************************************************

# Lists of valid input parameters - MUST be updated as new input parameters are added
# *****************************************************************************
soc_list=("AM335x" "AM437x" "AM571x" "AM572x" "AM574x" "K2E" "K2K" "K2H" "K2L" "K2G" "C6678" "C6657" "DRA72x" "DRA75x" "DRA78x" "OMAPL137" "OMAPL138" "am65xx")

# Parameter Validation: Check if the argument was passed to the batch file.
# *****************************************************************************
# Argument [soc] is used to set SOC variable.
if [ -n "$1" ]; then
    export SOC="$1"
fi

error_exit () {
    echo "Exiting..."
    echo "========================================================================="
    exit 1
}

contains () {
    local name=$1[@]
    local list=("${!name}")
    local in_list=0

    for item in "${list[@]}"
    do
        if [ "$2" = "$item" ]; then
            in_list=1
            break
        fi
    done

    if [ $in_list -eq 0 ]; then
        echo "ERROR: $3 ($2) is invalid"
        error_exit
    fi
}

contains soc_list          "$SOC"           "SOC"

# *****************************************************************************
# Component Versions
# *****************************************************************************

# This is the format of the executable being created - valid values are 'ELF' and 'COFF'
export OUTPUT_FORMAT=ELF

# Version of CG-Tools
export CGT_VERSION=8.3.2

# Version of CG-ARM-Tools for M4
export CGT_M4_VERSION=18.12.1.LTS

# Version of XDC
export XDC_VERSION=3.55.02.22

# Version of BIOS
export BIOS_VERSION=6.76.02.02

# Version of CG-Tools for ARM
export CGT_VERSION_ARM=GNU_7.2.1:Linaro

# Version of CG-Tools for A53
export CGT_VERSION_ARM_A53="GNU_7.2.1:Linaro aarch64"

# Version of the IPC
export IPC_VERSION=3_50_04_07

# EDMA3 Version 
export EDMA_VERSION=2.12.05.30E

# Version of the PDK
export PDK_VERSION=1.0.10

# Version of the NDK 
export NDK_VERSION=3.61.01.01

# Version of UIA
export UIA_VERSION=2.30.01.02

# Version of CG-Tools for ARM
export CGT_VERSION_ARM9=18.1.5.LTS

export ENDIAN="little"

export PDK_SHORT_NAME=$PWD


if [[ "$SOC" != "am65xx" ]]; then
    export RTSC_EDMA=";com.ti.sdo.edma3:$EDMA_VERSION"
fi

export RTSC_NDK=";com.ti.rtsc.NDK:${NDK_VERSION}-eng"
export RTSC_UIA=";com.ti.uia:$UIA_VERSION"

if [[ "$SOC" != "AM335x" && "$SOC" != "AM437x" && "$SOC" != "OMAPL137" ]]; then
    export RTSC_IPC=";com.ti.rtsc.IPC:$IPC_VERSION"
fi

echo "*****************************************************************************"
echo "Detecting all projects in PDK and importing them in the examples folder"

ccs_create_pspec() {

    textfile=${1#*/}

    ccs_proj_name=${textfile%.*}

    echo Creating Project "$ccs_proj_name"

    # Extract the module from the proj name and make it lowercase
    drv="$(echo $ccs_proj_name | cut -d'_' -f 1 | awk '{print tolower($0)}')"

    # Create folder for example project if it does not exist
    mkdir -p -- ${PDK_SHORT_NAME}/../examples/${drv}
    
    # Set projectspec file name and location
    pspec=${PDK_SHORT_NAME}/../examples/${drv}/${ccs_proj_name}.projectspec
	
    # Create projectspec file
    > "$pspec"

    # Get any compiler options from project.txt file, remove first part of string, and remove double quotes
    compiler_options=$(grep ccs.setCompilerOptions $textfile | cut -b 24- | sed -e  's/"//g')" -DUSE_BIOS"

    # Remove "-rtsc.enableRtsc" from compiler options (causes error when importing project and is not required)
    compiler_options=${compiler_options//"-rtsc.enableRtsc"/}
    
    # Remove USE_BIOS flag if nonOS project
    if [[ "$textfile" =~ "nonOS" ]]; then
        compiler_options=${compiler_options//"-DUSE_BIOS"/}
    fi

    # Get any linker options from project.txt file, remove first part of string, and remove double quotes
    linker_options=$(grep ccs.setLinkerOptions "$textfile" | cut -b 22- | sed -e  's/"//g') 

    # Get any XDCtools options from project.txt file, remove first part of string, replace backslashes with single quotes (CCS doesn't allow \" with pspecs currently), and remove all double quotes
    xdc_options=$(grep rtsc.setConfiguroOptions "$textfile" | cut -b 28- | sed 's/\\/'"'"'/g' | sed "s/\"//g")
    echo $COMPILER
    # Write projectspec file
    echo "<projectSpec>"                                                         >> $pspec
    echo "    <project"                                                          >> $pspec
    echo "        name=\"${ccs_proj_name}\""                                     >> $pspec
    echo "        device=\"${CCS_DEVICE}\""                                      >> $pspec
    echo "        endianness=\"${ENDIAN}\""                                      >> $pspec
    echo "        outputFormat=\"${OUTPUT_FORMAT}\""                             >> $pspec
    echo "        outputType=\"executable\""                                     >> $pspec
    echo "        cgtVersion=\"${COMPILER}\""                                 >> $pspec
    echo "        runtimeSupportLibrary=\"libc.a\""                              >> $pspec
    echo "        launchWizard=\"False\""                                        >> $pspec
    echo "        linkerCommandFile=\"\""                                        >> $pspec
    echo "        ignoreDefaultDeviceSettings=\"False\""                         >> $pspec
    echo "        compilerBuildOptions=\"${compiler_options} ${COMPILER_OPT}\""  >> $pspec  
    echo "        linkerBuildOptions=\"${linker_options} ${LINKER_OPT}\""        >> $pspec
    echo "    >"                                                                 >> $pspec
   
    #new line
    echo >> $pspec
   
    # Write XDC settings to projectspec file
    echo "        <!--Project Properties-->"                                       >> $pspec
    echo "        <property name=\"type\" value=\"rtsc\"/>"                        >> $pspec
    echo "        <property name=\"xdcToolsVersion\" value=\"${XDC_VERSION}\"/>"   >> $pspec
    echo "        <property name=\"buildProfile\" value=\"debug\"/>"               >> $pspec
    echo "        <property name=\"products\" value=\"${RTSC_PRODUCTS}\"/>"        >> $pspec
    echo "        <property name=\"platform\" value=\"${RTSC_PLATFORM_NAME}\"/>"   >> $pspec
    echo "        <property name=\"target\" value=\"${RTSC_TARGET}\"/>"            >> $pspec
    echo "        <property name=\"configuroOptions\" value=\"${xdc_options}\"/>"  >> $pspec

    #new line
    echo >> $pspec

    # Get project files from project .txt file and print to projectspec file
    echo "        <!--Project Files-->" >> $pspec
    while read -r line ; do
	echo "        <file action=\"link\" path=$line targetDirectory=\".\" />" >> $pspec
    done < <(grep ccs.linkFile $textfile | cut -b 15-) 
    
    #new line	
    echo >> $pspec
    
    # Write macros.ini variables to projectspec
    echo "        <!--Path Variables-->" >> $pspec
    echo "        <pathVariable name=\"PDK_INSTALL_PATH\"        path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"CSL_INSTALL_PATH\"        path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"BOARD_INSTALL_PATH\"      path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"SBL_BOOT_INSTALL_PATH\"   path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"FATFS_INSTALL_PATH\"      path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"OSAL_INSTALL_PATH\"       path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"NIMU_INSTAL_PATH\"        path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"NIMU_ICSS_INSTALL_PATH\"  path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"GPIO_INSTALL_PATH\"       path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"I2C_INSTALL_PATH\"        path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"ICSS_EMAC_INSTALL_PATH\"  path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"MMCSD_INSTALL_PATH\"      path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"PCIE_INSTALL_PATH\"       path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"PRUSS_INSTALL_PATH\"      path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"SPI_INSTALL_PATH\"        path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"UART_INSTALL_PATH\"       path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"USB_INSTALL_PATH\"        path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"SRIO_INSTALL_PATH\"       path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"CPPI_INSTALL_PATH\"       path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"QMSS_INSTALL_PATH\"       path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"FFTC_INSTALL_PATH\"       path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"TCP3D_INSTALL_PATH\"      path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"PASS_INSTALL_PATH\"       path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"BCP_INSTALL_PATH\"        path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"AIF2_INSTALL_PATH\"       path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"PKTLIB_INSTALL_PATH\"     path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"NWAL_INSTALL_PATH\"       path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"SA_INSTALL_PATH\"         path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"DFE_INSTALL_PATH\"        path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"IQN_INSTALL_PATH\"        path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"IQN2_INSTALL_PATH\"       path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
    echo "        <pathVariable name=\"PKTLIB_INSTALL_PATH\"     path=\"\${COM_TI_PDK_INSTALL_DIR}/packages\"  scope=\"project\"> </pathVariable>"  >> $pspec
	
    #new line	
    echo >> $pspec

    echo "    </project>"  >> $pspec
    echo "</projectSpec>"  >> $pspec
    
    echo "Project creation complete"

}

export -f ccs_create_pspec

# define platform.mk file for retrieving core and board list (starting with 6.3 platform.mk will contain core and board list for SOCs)
platform_file=${PDK_SHORT_NAME}/ti/build/makerules/platform.mk

# get core list for given SOC from platform.mk file
core_list=$(grep -i CORE_LIST_${SOC} $platform_file)
core_list=${core_list#*=}

if [[ -z "$core_list" ]]; then
    echo "No cores detected in platform.mk file for SOC $SOC: $platform_file"
    error_exit
fi

# get board list for given SOC from platform.mk file
board_list=$(grep -i BOARD_LIST_${SOC} $platform_file)
board_list=${board_list#*=}

if [[ -z "$board_list" ]]; then
    echo "No boards detected in platform.mk file for SOC $SOC: $platform_file"
    error_exit
fi

# Loop through and create projectSpec for every board of every core of given SOC
for core in $core_list; do
echo $core
    if [[ "$core" == "c"* ]]; then
        core="dsp"
    fi
    
    if [[ "$core" == "a"* ]]; then
        core="arm"
    fi
    
    if [[ "$core" == "mpu"* ]]; then
        core="mpu"
    fi
    
    if [[ "$core" == "ipu"* ]]; then
        core="m4"
    fi
    
    if [[ "$core" == "mcu"* ]]; then
        core="mcu"
    fi

    # set default dsp to c66, will be changed further down for specific SOCs
    if [[ "$core" == "dsp" ]]; then
        export RTSC_TARGET=ti.targets.elf.C66
        export CCS_DEVICE="com.ti.ccstudio.deviceModel.C6000.GenericC64xPlusDevice"
        export DSP_TYPE=c66
    fi
    
    # set default arm options, will be changed further down for specific SOCs
    if [[ "$core" == "arm" ]]; then
        export LINKER_OPT=\"-L\${BIOS_CG_ROOT}/packages/gnu/targets/arm/libs/install-native/arm-none-eabi/lib/hard\ --specs=nano.specs\"
    	export COMPILER_OPT=\"-I\${CG_TOOL_ROOT}/arm-none-eabi/include/newlib-nano\"
    	export COMPILER=$CGT_VERSION_ARM
    else 
        export LINKER_OPT=""
    	export COMPILER_OPT=""
    	export COMPILER=$CGT_VERSION
    fi

    # PDK Part Number & Platform name
    if [ "$SOC" = "K2K" ]; then
        export PDK_ECLIPSE_ID=com.ti.pdk.k2hk
        export RTSC_PLATFORM_NAME=ti.platforms.evmTCI6638K2K
        if [ "$core" == "arm" ]; then
            export RTSC_TARGET=gnu.targets.arm.A15F
            export CCS_DEVICE="Cortex A.TCI6638K2K"
        fi
    
    elif [ "$SOC" = "K2H" ]; then
        export PDK_ECLIPSE_ID=com.ti.pdk.k2hk
        export RTSC_PLATFORM_NAME=ti.platforms.evmTCI6636K2H
        if [ "$core" == "arm" ]; then
            export RTSC_TARGET=gnu.targets.arm.A15F
            export CCS_DEVICE="Cortex A.66AK2H12"
        fi
    
    elif [ "$SOC" = "K2L" ]; then
        export PDK_ECLIPSE_ID=com.ti.pdk.k2l
        export RTSC_PLATFORM_NAME=ti.platforms.evmTCI6630K2L
        if [ "$core" == "arm" ]; then
            export RTSC_TARGET=gnu.targets.arm.A15F
            export CCS_DEVICE="Cortex A.TCI6630K2L"
        fi
    
    elif [ "$SOC" = "K2E" ]; then
        export PDK_ECLIPSE_ID=com.ti.pdk.k2e
        export RTSC_PLATFORM_NAME=ti.platforms.evmC66AK2E
        if [ "$core" == "arm" ]; then
            export RTSC_TARGET=gnu.targets.arm.A15F
            export CCS_DEVICE="Cortex A.66AK2E05"
        fi
    
    elif [ "$SOC" = "OMAPL137" ]; then
        export PDK_ECLIPSE_ID=com.ti.pdk.omapl137
        export RTSC_PLATFORM_NAME=ti.platforms.evmOMAPL137
        if [ "$core" == "arm" ]; then
            export RTSC_TARGET=ti.targets.arm.elf.Arm9
            export CCS_DEVICE="ARM9.OMAPL137"
    	    export LINKER_OPT=""
    	    export COMPILER_OPT=""
    	    export COMPILER=$CGT_VERSION_ARM9
        else
            export RTSC_TARGET=ti.targets.elf.C674
            export CCS_DEVICE="com.ti.ccstudio.deviceModel.C6000.GenericC674xDevice"
            export DSP_TYPE=c674x
        fi
    
    elif [ "$SOC" = "OMAPL138" ]; then
        export PDK_ECLIPSE_ID=com.ti.pdk.omapl138
        export RTSC_PLATFORM_NAME=ti.platforms.evmOMAPL138
        if [ "$core" == "arm" ]; then
            export RTSC_TARGET=ti.targets.arm.elf.Arm9
            export CCS_DEVICE="ARM9.OMAPL138"
    	    export LINKER_OPT=""
    	    export COMPILER_OPT=""
    	    export COMPILER=$CGT_VERSION_ARM9
        else 
            export RTSC_TARGET=ti.targets.elf.C674
            export CCS_DEVICE="com.ti.ccstudio.deviceModel.C6000.GenericC674xDevice"
            export DSP_TYPE=c674x
        fi

    elif [ "$SOC" = "K2G" ]; then
        export PDK_ECLIPSE_ID=com.ti.pdk.k2g
        export RTSC_PLATFORM_NAME=ti.platforms.evmTCI66AK2G02
        if [ "$core" == "arm" ]; then
            export RTSC_TARGET=gnu.targets.arm.A15F
            export CCS_DEVICE="Cortex A.66AK2G02"
        fi
    
    elif [ "$SOC" = "AM571x" ]; then
        export PDK_ECLIPSE_ID=com.ti.pdk.am57xx
        export RTSC_PLATFORM_NAME=ti.platforms.idkAM571X
        if [ "$core" = "arm" ]; then
            export RTSC_TARGET=gnu.targets.arm.A15F
            export CCS_DEVICE="Cortex A.AM5728_RevA"
        elif [ "$core" == "m4" ]; then
            export RTSC_TARGET=ti.targets.arm.elf.M4
            export CCS_DEVICE="Cortex M.AM5728_RevA"
        fi
    
    elif [ "$SOC" == "AM572x" ]; then
        export PDK_ECLIPSE_ID=com.ti.pdk.am57xx
        if [ "$core" == "arm" ]; then
            export RTSC_TARGET=gnu.targets.arm.A15F
            export CCS_DEVICE="Cortex A.AM5728_RevA"
        elif [ "$core" == "m4" ]; then
            export RTSC_TARGET=ti.targets.arm.elf.M4
            export CCS_DEVICE="Cortex M.AM5728_RevA"
        fi
    
    elif [ "$SOC" = "AM574x" ]; then
        export PDK_ECLIPSE_ID=com.ti.pdk.am57xx
        export RTSC_PLATFORM_NAME=ti.platforms.idkAM572X
        if [ "$core" = "arm" ]; then
            export RTSC_TARGET=gnu.targets.arm.A15F
            export CCS_DEVICE="Cortex A.AM5728_RevA"
        elif [ "$core" == "m4" ]; then
            export RTSC_TARGET=ti.targets.arm.elf.M4
            export CCS_DEVICE="Cortex M.AM5728_RevA"
        fi
    
    elif [ "$SOC" == "DRA72x" ]; then
        export PDK_ECLIPSE_ID=com.ti.pdk.dra7xx
        export RTSC_PLATFORM_NAME=ti.platforms.evmDRA7XX
        if [ "$core" == "arm" ]; then
            export RTSC_TARGET=gnu.targets.arm.A15F
            export CCS_DEVICE="Cortex A.DRA72x"
        elif [ "$core" == "m4" ]; then
            export RTSC_TARGET=ti.targets.arm.elf.M4
            export CCS_DEVICE="Cortex M.DRA72x"
        fi
    
    elif [ "$SOC" == "DRA75x" ]; then
        export PDK_ECLIPSE_ID=com.ti.pdk.dra7xx
        export RTSC_PLATFORM_NAME=ti.platforms.evmDRA7XX
        if [ "$core" == "arm" ]; then
            export RTSC_TARGET=gnu.targets.arm.A15F
            export CCS_DEVICE="Cortex A.DRA75x_DRA74x"
        elif [ "$core" == "m4" ]; then
            export RTSC_TARGET=ti.targets.arm.elf.M4
            export CCS_DEVICE="Cortex M.DRA75x_DRA74x"
        fi
    
    elif [ "$SOC" == "DRA78x" ]; then
        export PDK_ECLIPSE_ID=com.ti.pdk.dra7xx
        export RTSC_PLATFORM_NAME=ti.platforms.evmTDA3XX
        if [ "$core" == "m4" ]; then
            export RTSC_TARGET=ti.targets.arm.elf.M4
            export CCS_DEVICE="Cortex M.TDA3x"
        fi
    
    elif [ "$SOC" == "AM335x" ]; then
        export PDK_ECLIPSE_ID=com.ti.pdk.am335x
        export RTSC_PLATFORM_NAME=ti.platforms.evmAM3359
        export RTSC_TARGET=gnu.targets.arm.A8F
        export CCS_DEVICE="Cortex A.AM3359.ICE_AM3359"
    
    elif [ "$SOC" = "AM437x" ]; then
        export PDK_ECLIPSE_ID=com.ti.pdk.am437x
        export RTSC_PLATFORM_NAME=ti.platforms.evmAM437X
        export RTSC_TARGET=gnu.targets.arm.A9F
        export CCS_DEVICE="Cortex A.AM4379.IDK_AM437X"
    
    elif [ "$SOC" = "C6678" ]; then
        export PDK_ECLIPSE_ID=com.ti.pdk.c667x
        export RTSC_PLATFORM_NAME=ti.platforms.evm6678
    
    elif [ "$SOC" = "C6657" ]; then
        export PDK_ECLIPSE_ID=com.ti.pdk.c665x
        export RTSC_PLATFORM_NAME=ti.platforms.evm6657
    
    elif [ "$SOC" = "am65xx" ]; then
        export PDK_ECLIPSE_ID=com.ti.pdk.am65xx
        if [ "$core" = "mpu" ]; then
            export RTSC_PLATFORM_NAME=ti.platforms.cortexA:AM65X
            export RTSC_TARGET=gnu.targets.arm.A53F
            export CCS_DEVICE="Cortex A.AM6548"
    	export LINKER_OPT=""
    	export COMPILER_OPT=""
    	export COMPILER="$CGT_VERSION_ARM_A53"
        elif [ "$core" == "mcu" ]; then
            export RTSC_PLATFORM_NAME=ti.platforms.cortexR:AM65X
            export RTSC_TARGET=ti.targets.arm.elf.R5F
            export CCS_DEVICE="Cortex R.AM6548"
        fi
    fi  
    export RTSC_PRODUCTS="com.ti.rtsc.SYSBIOS:$BIOS_VERSION;$PDK_ECLIPSE_ID:$PDK_VERSION"
    export RTSC_PRODUCTS="${RTSC_PRODUCTS}${RTSC_EDMA}${RTSC_IPC}${RTSC_NDK}${RTSC_UIA}"

    # Remove outer double quotes from extra compiler and linker options
    COMPILER_OPT=$(sed -e 's/^"//' -e 's/"$//' <<<"$COMPILER_OPT")
    LINKER_OPT=$(sed -e 's/^"//' -e 's/"$//' <<<"$LINKER_OPT")

    # create projectspecs for all boards of a given core
    for board in $board_list; do
    
       # am572x has rtsc platform for each board
        if [ "$board" == "idkAM572x" ]; then
            export RTSC_PLATFORM_NAME=ti.platforms.idkAM572X
        elif [ "$board" == "evmAM572x" ]; then
            export RTSC_PLATFORM_NAME=ti.platforms.evmAM572X
        fi
    
        if [[ "$core" == "dsp" ]]; then
    	    find -iname "*$board*$DSP_TYPE***roject.txt" -execdir bash -c 'ccs_create_pspec {}' \;
            if [ $(find -iname "*$board*$DSP_TYPE*roject.txt" | wc -l) -eq 0 ]; then
               echo "No projects detected"
            fi
        fi
        if [[ "$core" == "arm" ]] || [[ "$core" == "mpu" ]]; then
            find -iname "*$board*$core*roject.txt" -execdir bash -c 'ccs_create_pspec {}' \;
            if [ $(find -iname "*$board*$core*roject.txt" | wc -l) -eq 0 ]; then
               echo "No projects detected"
            fi
        fi
        if [[ "$core" == "m4" ]]|| [[ "$core" == "mcu" ]]; then
            find -iname "*$board*$core*roject.txt" -execdir bash -c 'ccs_create_pspec {}' \;
            if [ $(find -iname "*$board*$core*roject.txt" | wc -l) -eq 0 ]; then
               echo "No projects detected"
            fi
        fi
    done

done

echo "Project generation complete"
echo "*****************************************************************************"