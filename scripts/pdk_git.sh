#!/bin/bash
#   ============================================================================
#   @file   pdk_git.sh
#
#   @desc   Script to do GIT clone, rebase, status, log, prune on all the repo
#           with server and generate API guide
#
#   ============================================================================
#   Revision History
#   11-Jan-2016 Kedar/Sivaraj   Initial draft.
#   16-Nov-2016 Sivaraj         Added named argument and clone tag inputs.
#   02-Jan-2017 Sivaraj         Changes for git repo in "packages/ti" folder
#   06-Jan-2017 Sivaraj         Added oneliner command - can be used to put the
#                               commit log of all repo's in test log files
#   17-Feb-2017 Sivaraj         Added clone config to clone repos based on team need
#   01-Mar-2017 Sivaraj         Added diff, reset option
#   26-May-2017 Ankur           Added option to generate API guide
#   20-Jul-2017 Sivaraj         Changes for gerrit repo migration to bitbucket
#   08-Nov-2017 Sivaraj         Changes for UDMA LLD repo
#   04-Jan-2018 Sivaraj         Changes for Sciclient and VHWA repos
#   03-Apr-2018 Sivaraj         Changes for IPC repos
#   04-Jul-2018 Sivaraj         Updated for j6_master
#
#   ============================================================================

#Get user input
while [ $# -gt 0 ]; do
  case "$1" in
    #Commonly used options for GIT operation
    --clone)
      command="clone"
      ;;
    #clone_config supported values: "default", "prsdk_vision", "prsdk_k3"
    --clone_config=*)
      clone_config="${1#*=}"
      ;;
    --mirror_site=*)
      mirror_site="${1#*=}"
      ;;
    --rebase)
      command="rebase"
      ;;
    --reset)
      command="reset"
      ;;
    --diff)
      command="diff"
      ;;
    --branch)
      command="branch"
      ;;
    --run_cmd=*)
      command="run_cmd"
      cmd_to_run="${1#*=}"
      ;;
    --allclean)
      command="allclean"
      ;;
    --log)
      command="log"
      ;;
    --status)
      command="status"
      ;;
    --prune)
      command="prune"
      ;;
    --cleandxf)
      command="cleandxf"
      ;;
    --onelinelog)
      command="onelinelog"
      ;;
    --doxygen)
      command="doxygen"
      ;;
    --doxygen_install_path=*)
      doxygen_install_path="${1#*=}"
      ;;
    --doxygen_silent_mode)
      doxygen_silent_mode="true"
      ;;
    --cm_status_accounting)
      command="cm_status_accounting"
      ;;
    --old_tag=*)
      old_tag="${1#*=}"
      ;;
    --new_tag=*)
      new_tag="${1#*=}"
      ;;
    #Below options are typically used by daily/release builds to take different branch/tags
    --pdk_top_tag=*)
      pdk_top_tag="${1#*=}"
      ;;
    --docs_tag=*)
      docs_tag="${1#*=}"
      ;;
    --internal_docs_tag=*)
      internal_docs_tag="${1#*=}"
      ;;
    --csl_tag=*)
      ti_csl_tag="${1#*=}"
      ;;
    --build_tag=*)
      build_tag="${1#*=}"
      ;;
    --board_tag=*)
      board_tag="${1#*=}"
      ;;
    --osal_tag=*)
      osal_tag="${1#*=}"
      ;;
    --transport_tag=*)
      transport_tag="${1#*=}"
      ;;
    --sbl_tag=*)
      sbl_tag="${1#*=}"
      ;;
    --i2c_tag=*)
      i2c_tag="${1#*=}"
      ;;
    --uart_tag=*)
      uart_tag="${1#*=}"
      ;;
    --spi_tag=*)
      spi_tag="${1#*=}"
      ;;
    --mcasp_tag=*)
      mcasp_tag="${1#*=}"
      ;;
    --mmcsd_tag=*)
      mmcsd_tag="${1#*=}"
      ;;
    --pcie_tag=*)
      pcie_tag="${1#*=}"
      ;;
    --sa_tag=*)
      sa_tag="${1#*=}"
      ;;
    --pm_tag=*)
      pm_tag="${1#*=}"
      ;;
    --vps_tag=*)
      vps_tag="${1#*=}"
      ;;
    --fvid2_tag=*)
      fvid2_tag="${1#*=}"
      ;;
    --dss_tag=*)
      dss_tag="${1#*=}"
      ;;
    --cal_tag=*)
      cal_tag="${1#*=}"
      ;;
    --csirx_tag=*)
      csirx_tag="${1#*=}"
      ;;
    --cpsw_tag=*)
      cpsw_tag="${1#*=}"
      ;;
    --ipc_lite_tag=*)
      ipc_lite_tag="${1#*=}"
      ;;
    --fw_l3l4_tag=*)
      fw_l3l4_tag="${1#*=}"
      ;;
    --diag_tag=*)
      diag_tag="${1#*=}"
      ;;
    --security_tag=*)
      security_tag="${1#*=}"
      ;;
    --stw_lld_tag=*)
      stw_lld_tag="${1#*=}"
      ;;
    --bsp_lld_tag=*)
      bsp_lld_tag="${1#*=}"
      ;;
    --udma_tag=*)
      udma_tag="${1#*=}"
      ;;
    --ipc_tag=*)
      ipc_tag="${1#*=}"
      ;;
    --sciclient_tag=*)
      sciclient_tag="${1#*=}"
      ;;
    --vhwa_tag=*)
      vhwa_tag="${1#*=}"
      ;;
    --emac_tag=*)
      emac_tag="${1#*=}"
      ;;
    --pruss_tag=*)
      pruss_tag="${1#*=}"
      ;;
    --gpio_tag=*)
      gpio_tag="${1#*=}"
      ;;
    --usb_tag=*)
      usb_tag="${1#*=}"
      ;;
    --fatfs_tag=*)
      fatfs_tag="${1#*=}"
      ;;
    --lld_list_bb=*)
      lld_list_bb="${1#*=}"
      ;;
    --misc_list_bb=*)
      misc_list_bb="${1#*=}"
      ;;
    --working_dir=*)
      working_dir="${1#*=}"
      ;;
    *)
      printf "Error: Invalid argument $1!!\n"
  esac
  shift
done

if [ "$working_dir" == "" ]; then
  working_dir=`pwd`
  if [ "$command" == "clone" ]; then
    #clone in same directory
    pdk_dir=$working_dir/pdk
  else
    #Script called from within PDK git for rebase/status
    cd ..
    pdk_dir=`pwd`
  fi
else
  pdk_dir=$working_dir/pdk
  cd $pdk_dir
fi

#Default value if not provided
: ${clone_config:="default"}
#Select repos's based on config
if [ "$clone_config" == "prsdk_vision" ]; then
  lld_list_bb="pm vps ipc_lite fw_l3l4"
  misc_list_bb="build diag"
  lld_list_bb_vision="stw_lld bsp_lld"
  misc_list_bb_vision="sbl security docs"
  : ${pdk_top_tag:="j6_master"}
  : ${docs_tag:="j6_master"}
  : ${build_tag:="j6_master"}
  : ${ti_csl_tag:="j6_master"}
  : ${diag_tag:="j6_master"}
  : ${sbl_tag:="j6_master"}
  : ${security_tag:="j6_master"}
  : ${bsp_lld_tag:="j6_master"}
  : ${ipc_lite_tag:="j6_master"}
  : ${fw_l3l4_tag:="j6_master"}
  : ${pm_tag:="j6_master"}
  : ${stw_lld_tag:="j6_master"}
  : ${vps_tag:="j6_master"}
fi
if [ "$clone_config" == "prsdk_k3" ]; then
  lld_list_bb="udma fvid2 dss cal sciclient uart pm gpio i2c spi emac pruss mmcsd"
  misc_list_bb="build osal transport board sbl fatfs"
  lld_list_bb_vision="vhwa ipc csirx cpsw"
  misc_list_bb_vision="j7_docs internal_docs"
  : ${ti_csl_tag:="j7_master"}
  : ${uart_tag:="master"}
  : ${pm_tag:="master"}
  : ${gpio_tag:="master"}
  : ${i2c_tag:="master"}
  : ${spi_tag:="master"}
  : ${emac_tag:="master"}
  : ${pruss_tag:="master"}
  : ${build_tag:="master"}
  : ${osal_tag:="master"}
  : ${transport_tag:="master"}
  : ${board_tag:="master"}
  : ${sbl_tag:="master"}
  : ${mmcsd_tag:="master"}
  : ${fatfs_tag:="master"}
fi
if [ "$clone_config" == "prsdk_m4" ]; then
  lld_list_bb="udma fvid2 dss cal sciclient uart pm gpio i2c spi emac pruss mmcsd"
  misc_list_bb="build osal transport board sbl fatfs"
  lld_list_bb_vision="vhwa ipc csirx"
  misc_list_bb_vision="j7_docs internal_docs"
  : ${ti_csl_tag:="master"}
  : ${uart_tag:="master"}
  : ${pm_tag:="master"}
  : ${gpio_tag:="master"}
  : ${i2c_tag:="master"}
  : ${spi_tag:="master"}
  : ${emac_tag:="master"}
  : ${pruss_tag:="master"}
  : ${build_tag:="master"}
  : ${osal_tag:="master"}
  : ${transport_tag:="master"}
  : ${board_tag:="master"}
  : ${sbl_tag:="master"}
fi
if [ "$clone_config" == "prsdk_j7" ]; then
  lld_list_bb="udma fvid2 dss sciclient uart pm gpio i2c spi emac pruss"
  misc_list_bb="build osal transport board sbl"
  lld_list_bb_vision="vhwa ipc csirx cpsw"
  misc_list_bb_vision="j7_docs internal_docs"
  : ${ti_csl_tag:="j7_master"}
  : ${uart_tag:="master"}
  : ${pm_tag:="master"}
  : ${i2c_tag:="master"}
  : ${gpio_tag:="master"}
  : ${spi_tag:="master"}
  : ${emac_tag:="master"}
  : ${pruss_tag:="master"}
  : ${build_tag:="master"}
  : ${osal_tag:="master"}
  : ${transport_tag:="master"}
  : ${board_tag:="master"}
  : ${sbl_tag:="master"}
fi
: ${mirror_site:=""}
: ${doxygen_install_path:=""}
: ${doxygen_silent_mode:="false"}
: ${old_tag:=""}
: ${new_tag:="."}
: ${pdk_top_tag:="master"}
: ${docs_tag:="master"}
: ${internal_docs_tag:="master"}
: ${board_tag:="master"}
: ${sbl_tag:="master"}
: ${build_tag:="master"}
: ${ti_csl_tag:="master"}
: ${osal_tag:="master"}
: ${transport_tag:="master"}
: ${bsp_lld_tag:="master"}
: ${i2c_tag:="master"}
: ${mcasp_tag:="master"}
: ${mmcsd_tag:="master"}
: ${pcie_tag:="master"}
: ${sa_tag:="master"}
: ${pm_tag:="master"}
: ${ipc_lite_tag:="master"}
: ${fw_l3l4_tag:="master"}
: ${diag_tag:="master"}
: ${security_tag:="master"}
: ${spi_tag:="master"}
: ${stw_lld_tag:="master"}
: ${uart_tag:="master"}
: ${vps_tag:="master"}
: ${fvid2_tag:="master"}
: ${dss_tag:="master"}
: ${cal_tag:="master"}
: ${csirx_tag:="master"}
: ${cpsw_tag:="master"}
: ${udma_tag:="master"}
: ${ipc_tag:="master"}
: ${sciclient_tag:="master"}
: ${vhwa_tag:="master"}
: ${emac_tag:="master"}
: ${pruss_tag:="master"}
: ${gpio_tag:="master"}
: ${usb_tag:="master"}
: ${fatfs_tag:="master"}
: ${lld_list_bb:="pm vps gpio i2c uart spi mcasp mmcsd pcie sa ipc_lite fw_l3l4 udma fvid2 dss cal sciclient"}
: ${misc_list_bb:="build osal transport board diag"}
: ${lld_list_bb_vision:="stw_lld bsp_lld vhwa ipc csirx cpsw"}
: ${misc_list_bb_vision:="sbl security docs internal_docs"}

dir_list=(
	""
	"docs"
	"internal_docs"
	"packages/ti/board"
	"packages/ti/build"
	"packages/ti/csl"
	"packages/ti/diag"
	"packages/ti/osal"
	"packages/ti/transport"
	"packages/ti/fs/fatfs"
	"packages/ti/boot/sbl"
	"packages/ti/boot/sbl_auto"
	"packages/ti/boot/sbl_auto/security"
	"packages/ti/drv/bsp_lld"
	"packages/ti/drv/cal"
	"packages/ti/drv/csirx"
	"packages/ti/drv/cpsw"
	"packages/ti/drv/dss"
	"packages/ti/drv/emac"
	"packages/ti/drv/gpio"
	"packages/ti/drv/i2c"
	"packages/ti/drv/ipc"
	"packages/ti/drv/ipc_lite"
	"packages/ti/drv/fvid2"
	"packages/ti/drv/fw_l3l4"
	"packages/ti/drv/mcasp"
	"packages/ti/drv/mmcsd"
	"packages/ti/drv/pcie"
	"packages/ti/drv/pm"
	"packages/ti/drv/pruss"
	"packages/ti/drv/sa"
	"packages/ti/drv/sciclient"
	"packages/ti/drv/spi"
	"packages/ti/drv/stw_lld"
	"packages/ti/drv/uart"
	"packages/ti/drv/udma"
	"packages/ti/drv/usb"
	"packages/ti/drv/vhwa"
	"packages/ti/drv/vps"
)

# BSP LLD components list
bsp_lld_com="i2c mcspi uart"

git_rebase() {
  git_dir=$pdk_dir/$1
  if [ ! -d "$git_dir" ]; then
    return
  fi

  cd $git_dir
  echo === Rebase for $git_dir ...

  # stash changes if not clean
  #if git diff-index --quiet HEAD --; then
  if [ -n "$(git status --untracked-files=no --porcelain)" ]; then
    is_clean="no"
    git stash
  else
    is_clean="yes"
  fi

  # fetch and rebase
  git fetch; git rebase

  # apply if stashed
  if [ "$is_clean" == "no" ]; then
    git stash pop
  fi

  echo ""
}

run_cmd() {
  git_dir=$pdk_dir/$1
  if [ ! -d "$git_dir" ]; then
    return
  fi

  cd $git_dir
  echo === $3 for $git_dir ...
  $2
  echo ""
}

git_diff() {
  run_cmd $1 "git diff" "Diff"
}

git_reset() {
  run_cmd $1 "git checkout ." "Reset"
}

git_branch() {
  git_dir=$pdk_dir/$1
  if [ ! -d "$git_dir" ]; then
    return
  fi

  cd $git_dir
  printf "%-40s: " "pdk/"$1
  git rev-parse --abbrev-ref HEAD
}

git_allclean() {
  run_cmd $1 "rm -rf lib" "Allclean"
}

git_status() {
  run_cmd $1 "git status" "Status"
}

git_prune() {
  run_cmd $1 "git fetch origin --prune" "Prune"
}

git_cleandxf() {
  run_cmd $1 "git clean -dxf" "git clean dxf"
}

git_onelinelog() {
  git_dir=$pdk_dir/$1
  if [ ! -d "$git_dir" ]; then
    return
  fi

  cd $git_dir
  temp=`git log -n 1 --pretty=oneline`
  printf "%-40s: $temp\n" "pdk/"$1
}

git_log() {
  run_cmd $1 "git log -n 1 --stat" "git log"
}

generate_apidoc() {
  git_dir=$pdk_dir/$1

  if [ ! -d "$git_dir" ]; then
    return
  fi

  if [ "$1" == "packages/ti/build" ]  || [ "$1" == "packages/ti/board" ] || [ "$1" == "packages/ti/osal" ] || [ "$1" == "packages/ti/transport" ]; then
    return
  fi
  if [ "$1" == "packages/ti/drv/i2c" ]  || [ "$1" == "packages/ti/drv/mcasp" ] || [ "$1" == "packages/ti/drv/mmcsd" ]; then
    return
  fi
  if [ "$1" == "packages/ti/drv/pcie" ] || [ "$1" == "packages/ti/drv/uart" ] || [ "$1" == "packages/ti/drv/sa" ]; then
    return
  fi
  if [ "$1" == "packages/ti/drv/spi" ]; then
    return
  fi

  if [ "$doxygen_install_path" == "" ]; then
    doxygen_cmd=doxygen
  else
    doxygen_cmd=$doxygen_install_path/bin/doxygen
  fi

  if [ "$1" == "packages/ti/csl" ]; then
    if [ ! -f "$git_dir/docs/doxyfile_tdax" ]; then
      return
    fi
    cd $git_dir
    # Generate API guide
    $doxygen_cmd ./docs/doxyfile_tdax 1>$pdk_dir/scripts/doxygen_log/${PWD##*/}.txt 2>$pdk_dir/scripts/doxygen_log/warning/${PWD##*/}_warning.txt
  elif [ "$1" == "packages/ti/drv/bsp_lld" ]; then
    for comp in $bsp_lld_com
    do
      comp_dir=$git_dir/$comp
      if [ ! -f "$comp_dir/docs/Doxyfile" ]; then
        return
      fi
      cd $comp_dir
      # Generate API guide
      $doxygen_cmd ./docs/Doxyfile 1>$pdk_dir/scripts/doxygen_log/${PWD##*/}.txt 2>$pdk_dir/scripts/doxygen_log/warning/${PWD##*/}_warning.txt
    done
  else
    if [ ! -f "$git_dir/docs/Doxyfile" ]; then
      return
    fi
    cd $git_dir
    # Generate API guide
    $doxygen_cmd ./docs/Doxyfile 1>$pdk_dir/scripts/doxygen_log/${PWD##*/}.txt 2>$pdk_dir/scripts/doxygen_log/warning/${PWD##*/}_warning.txt
  fi
  cd $pdk_dir
}

doxy_warning_print()
{
  doxy_dir=$1
  for entry in "$doxy_dir"/*
  do
    if [ -s "$entry" ]; then
      echo ""
      echo "$entry"
      cat $entry
      echo ""
    fi
  done
}

git_clone() {
  git_server_bb_org="ssh://git@bitbucket.itg.ti.com/processor-sdk"
  git_server_bb_vision_org="ssh://git@bitbucket.itg.ti.com/processor-sdk-vision"
  if [ "x$mirror_site" == "x" ]; then
    git_server_bb=$git_server_bb_org
    git_server_bb_vision=$git_server_bb_vision_org
  else
    git_server_bb="ssh://git@bitbucket-mirror-"$mirror_site".itg.ti.com:7999/bitbucket/processor-sdk"
    git_server_bb_vision="ssh://git@bitbucket-mirror-"$mirror_site".itg.ti.com:7999/bitbucket/processor-sdk-vision"
  fi

  #Remove any old directory
  rm -rf pdk/

  echo "=== Cloning PDK Repositories ..."
  echo Cloning $git_server_bb/processor-pdk-packages.git ${pdk_top_tag} ...
  git clone -q $git_server_bb/processor-pdk-packages.git pdk
  cd $working_dir/pdk
  if [ x${pdk_top_tag} != xmaster ]; then
    git checkout -q ${pdk_top_tag}
  fi
  git remote set-url --push origin $git_server_bb_org/processor-pdk-packages.git

  # #######################################################################
  # List of Git present in bit-bucket
  # #######################################################################
  # install all LLDs in bit-bucket
  mkdir -p $working_dir/pdk/packages/ti/drv
  cd $working_dir/pdk/packages/ti/drv
  for lld in $lld_list_bb
  do
      if [ "${lld}" == "mmcsd" ]; then
        clone_dir="sd-mmc"
      else
        if [ "${lld}" == "fvid2" ] || [ "${lld}" == "vps" ] || [ "${lld}" == "pm" ] || [ "${lld}" == "ipc_lite" ] || [ "${lld}" == "fw_l3l4" ] || [ "${lld}" == "sciclient" ] || [ "${lld}" == "usb" ]; then
          clone_dir=${lld}
        else
          clone_dir=${lld}-lld
        fi
      fi
      lld_tag=${lld}_tag
      echo Cloning ${git_server_bb}/${clone_dir}.git ${!lld_tag} ...
      git clone -q ${git_server_bb}/${clone_dir}.git ${lld}
      cd $lld
      if [ x${!lld_tag} != xmaster ]; then
          git checkout -q ${!lld_tag}
      fi
      git remote set-url --push origin $git_server_bb_org/${clone_dir}.git
      cd ..
  done
  # install all misc git in bit-bucket
  cd $working_dir/pdk/packages/ti
  for misc in $misc_list_bb
  do
      clone_dir=${misc}
      if [ "${misc}" == "build" ]; then
        clone_dir="processor-pdk-build"
      fi
      misc_tag=${misc}_tag
      echo Cloning ${git_server_bb}/${clone_dir}.git ${!misc_tag} ...
      git clone -q ${git_server_bb}/${clone_dir}.git ${misc}
      cd $misc
      if [ x${!misc_tag} != xmaster ]; then
          git checkout -q ${!misc_tag}
      fi
      git remote set-url --push origin $git_server_bb_org/${clone_dir}.git
      cd ..

      # move SBL to boot folder
      if [ "${misc}" == "sbl" ]; then
        mkdir -p boot
        mv $misc boot/
      fi
      # move FATFS to fs folder
      if [ "${misc}" == "fatfs" ]; then
        mkdir -p fs
        mv $misc fs/
      fi
  done
  cd $working_dir/pdk

  # #######################################################################
  # Setup LLD in VISION bit-bucket
  # #######################################################################
  # install all vision bit-bucket LLDs
  mkdir -p $working_dir/pdk/packages/ti/drv
  cd $working_dir/pdk/packages/ti/drv
  for lld in $lld_list_bb_vision
  do
      clone_dir=${lld}
      if [ "${lld}" == "csirx" ]; then
        clone_dir=${lld}-lld
      fi
      if [ "${lld}" == "cpsw" ]; then
        clone_dir=${lld}-lld
      fi
      lld_tag=${lld}_tag
      echo Cloning ${git_server_bb_vision}/${clone_dir}.git ${!lld_tag} ...
      git clone -q ${git_server_bb_vision}/${clone_dir}.git ${lld}
      cd $lld
      if [ x${!lld_tag} != xmaster ]; then
          git checkout -q ${!lld_tag}
      fi
      git remote set-url --push origin $git_server_bb_vision_org/${clone_dir}.git
      cd ..
  done
  # install all misc git in vision bit-bucket
  cd $working_dir/pdk/packages/ti
  for misc in $misc_list_bb_vision
  do
      clone_dir=${misc}
      if [ "${misc}" == "docs" ]; then
        clone_dir="pdk_docs"
      fi
      if [ "${misc}" == "internal_docs" ]; then
        clone_dir="pdk_internal_docs"
      fi
      if [ "${misc}" == "j7_docs" ]; then
        clone_dir="pdk_j7_docs"
        misc="docs"
      fi
      misc_tag=${misc}_tag
      echo Cloning ${git_server_bb_vision}/${clone_dir}.git ${!misc_tag} ...
      git clone -q ${git_server_bb_vision}/${clone_dir}.git ${misc}
      cd $misc
      if [ x${!misc_tag} != xmaster ]; then
          git checkout -q ${!misc_tag}
      fi
      git remote set-url --push origin $git_server_bb_vision_org/${clone_dir}.git
      cd ..

      # move SBL to boot folder
      if [ "${misc}" == "sbl" ]; then
        mkdir -p boot
        mv $misc boot/
        mv boot/sbl boot/sbl_auto
      fi
      if [ "${misc}" == "security" ]; then
        mkdir -p boot/sbl_auto
        mv $misc boot/sbl_auto
      fi
      # move docs to top folder
      if [ "${misc}" == "docs" ]; then
        mv $misc $working_dir/pdk
      fi
      if [ "${misc}" == "internal_docs" ]; then
        mv $misc $working_dir/pdk
      fi
  done
  cd $working_dir/pdk

  # #######################################################################
  # Setup CSL for IP Blocks
  # #######################################################################
  cd $working_dir/pdk/packages/ti
  echo Cloning ${git_server_bb}/common-csl-ip.git ${ti_csl_tag} ...
  git clone -q ${git_server_bb}/common-csl-ip.git
  mv common-csl-ip csl
  cd csl
  if [ x${ti_csl_tag} != xmaster ]; then
    git checkout -q ${ti_csl_tag}
  fi
  git remote set-url --push origin $git_server_bb_org/common-csl-ip.git
  cd $working_dir/pdk/

  echo "=== Cloning PDK Repository Complete !!!"
}

git_cm_status() {
  git_dir=$pdk_dir/$1
  if [ ! -d "$git_dir" ]; then
    return
  fi
  if [ "packages/ti/board" == "$1" ] || [ "packages/ti/osal" == "$1" ] || [ "packages/ti/transport" == "$1" ]; then
    return
  fi
  if [ "packages/ti/drv/i2c" == "$1" ] || [ "packages/ti/drv/mcasp" == "$1" ]; then
    return
  fi
  if [ "packages/ti/drv/mmcsd" == "$1" ] || [ "packages/ti/drv/pcie" == "$1" ]; then
    return
  fi
  if [ "packages/ti/drv/uart" == "$1" ] || [ "packages/ti/drv/sa" == "$1" ]; then
    return
  fi
  if [ "packages/ti/drv/spi" == "$1" ]; then
    return
  fi

  cd $git_dir
  printf "=== CM Status Accounting for pdk/$1 from release/tag $old_tag to $new_tag ...\n"
  printf "\n"

  printf "Number of Files Added   : "
  git log --pretty --oneline --name-status "$old_tag".."$new_tag" | sort -u | grep $'A\t' | cut -f 2 | wc -l
  printf "Number of Files Modified: "
  git log --pretty --oneline --name-status "$old_tag".."$new_tag" | sort -u | grep $'M\t' | cut -f 2 | wc -l
  printf "Number of Files Deleted : "
  git log --pretty --oneline --name-status "$old_tag".."$new_tag" | sort -u | grep $'D\t' | cut -f 2 | wc -l

  printf "\n"
  printf "List of Files Added:\n"
  git log --pretty --oneline --name-status "$old_tag".."$new_tag" | sort -u | grep $'A\t' | cut -f 2

  printf "\n"
  printf "List of Files Modified:\n"
  git log --pretty --oneline --name-status "$old_tag".."$new_tag" | sort -u | grep $'M\t' | cut -f 2

  printf "\n"
  printf "List of Files Deleted:\n"
  git log --pretty --oneline --name-status "$old_tag".."$new_tag" | sort -u | grep $'D\t' | cut -f 2

  echo ""
}

if [ "$command" == "cm_status_accounting" ]; then
	if [ x"" == x$old_tag ]; then
		printf "Need to provide old tag for CM status accounting!!\n"
		printf "Usage: \n    ./pdk_git.sh --cm_status_accounting --old_tag=<\"REL.PDK.TDA.01.07.00.04\"> --new_tag=\"<. if not provided>\" \n"
		exit
	fi
	for dir in "${dir_list[@]}"
	do
		git_cm_status "$dir"
	done
fi
if [ "$command" == "doxygen" ]; then
    rm -rf $pdk_dir/scripts/doxygen_log
    mkdir -p $pdk_dir/scripts/doxygen_log
    mkdir -p $pdk_dir/scripts/doxygen_log/warning
	for dir in "${dir_list[@]}"
	do
	    generate_apidoc "$dir"
	done
	if [ "$doxygen_silent_mode" == "false" ]; then
	  doxy_warning_print "$pdk_dir/scripts/doxygen_log/warning"
	fi
fi
if [ "$command" == "status" ]; then
	for dir in "${dir_list[@]}"
	do
		git_status "$dir"
	done
fi
if [ "$command" == "prune" ]; then
	for dir in "${dir_list[@]}"
	do
		git_prune "$dir"
	done
fi
if [ "$command" == "cleandxf" ]; then
	for dir in "${dir_list[@]}"
	do
		git_cleandxf "$dir"
	done
fi
if [ "$command" == "onelinelog" ]; then
	for dir in "${dir_list[@]}"
	do
		git_onelinelog "$dir"
	done
fi
if [ "$command" == "log" ]; then
	for dir in "${dir_list[@]}"
	do
		git_log "$dir"
	done
fi
if [ "$command" == "rebase" ]; then
	for dir in "${dir_list[@]}"
	do
		git_rebase "$dir"
	done
fi
if [ "$command" == "run_cmd" ]; then
	for dir in "${dir_list[@]}"
	do
		run_cmd "$dir" "$cmd_to_run" "$cmd_to_run"
	done
fi
if [ "$command" == "reset" ]; then
	for dir in "${dir_list[@]}"
	do
		git_reset "$dir"
	done
fi
if [ "$command" == "diff" ]; then
	for dir in "${dir_list[@]}"
	do
		git_diff "$dir"
	done
fi
if [ "$command" == "branch" ]; then
	for dir in "${dir_list[@]}"
	do
		git_branch "$dir"
	done
fi
if [ "$command" == "allclean" ]; then
	for dir in "${dir_list[@]}"
	do
		git_allclean "$dir"
	done
fi
if [ "$command" == "clone" ]; then
	git_clone
fi

cd $working_dir
exit
