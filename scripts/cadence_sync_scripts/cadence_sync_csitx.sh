#!/bin/bash
#   ============================================================================
#   @desc   Script to sync-up the cadence csi-tx driver to PDK CSL folder
#
#   ============================================================================
#   Revision History
#   27-Oct-2018 Sivaraj         Initial draft.
#
#   ============================================================================
# Command: cadence_sync_csitx.sh <$cadence_dir>
# Example (Linux): ./cadence_sync_csitx.sh /adasuser/sivaraj/k3/ip_releases_3p/cadence

export PATH=$PATH:/bin
date=`date '+%b_%d_%Y'`
date_print=`date '+%d-%b-%Y %I:%M:%S %p'`
month=`date '+%b_%Y'`
year=`date '+%Y'`
if [ "$OS" == "" ]; then
  echo "No OS defined, defaulting to linux"
  OS=linux
fi

#Get user input
working_dir=`pwd`
cadence_dir=$1
if [ "$cadence_dir" == "" ]; then
  cadence_dir=$working_dir/../../../ip_releases_3p/cadence
fi
if [ ! -d "$cadence_dir" ]; then
  echo "Kindly provide Cadence repo path to copy files from!!"
  exit
fi
csl_csitx_dir=$working_dir/../../packages/ti/csl/src/ip/csitx
cadence_csitx_dir=$cadence_dir/csi_tx/csi_tx_drv/software/core_driver

echo "PDK CSL CSI-TX Sync-up with Cadence Deliverable..."
echo "Date: $date_print"
echo ""

sedErrCheck() {
if [ $1 -ne 0 ]; then
    echo "Error occurred @ $2!!"
fi
}

############## Start clean ##############
echo [INFO] Start Clean!! Deleting tobe copied files/folders...
rm -rf $csl_csitx_dir/V0

############## Create required folders ##############
echo [INFO] Creating required folders...
mkdir -p $csl_csitx_dir/V0
mkdir -p $csl_csitx_dir/V0/priv

############## Copy STW/BSP files to temp folder ##############
echo [INFO] Copying files from cadence to folder...
cp -r $cadence_csitx_dir/include/csitx_if.h             $csl_csitx_dir/V0/
cp -r $cadence_csitx_dir/include/csitx_structs_if.h     $csl_csitx_dir/V0/
cp -r $cadence_csitx_dir/common/cdn_errno.h             $csl_csitx_dir/V0/
cp -r $cadence_csitx_dir/common/cps_drv.h               $csl_csitx_dir/V0/priv
cp -r $cadence_csitx_dir/src/csitx.c                    $csl_csitx_dir/V0/priv
cp -r $cadence_csitx_dir/src/csitx_regs.h               $csl_csitx_dir/V0/
cp -r $cadence_csitx_dir/src/csitx_regs_macros.h        $csl_csitx_dir/V0/
cp -r $cadence_csitx_dir/src/csitx_priv.h               $csl_csitx_dir/V0/priv
cp -r $cadence_csitx_dir/src/csitx_sanity.h             $csl_csitx_dir/V0/priv

############## Replace/Remove unwanted include paths ##############
sed -i '/\#include \"cdn_inttypes\.h\"/d'               $csl_csitx_dir/V0/csitx_if.h
sed -i '/\#include \"cdn_stdtypes\.h\"/d'               $csl_csitx_dir/V0/csitx_if.h
sed -i '/\#include \"csitx_regs\.h\"/d'                 $csl_csitx_dir/V0/csitx_if.h
sed -i '/cps_drv\.h/d'                                  $csl_csitx_dir/V0/csitx_regs.h
sed -i '/\#include \"csitx_regs_macros\.h\"/d'          $csl_csitx_dir/V0/csitx_regs.h
sed -i '/\#include \"cdn_stdtypes\.h\"/d'               $csl_csitx_dir/V0/csitx_structs_if.h
sed -i '/\#include \"csitx_if\.h\"/d'                   $csl_csitx_dir/V0/csitx_structs_if.h
sed -i '/cps\.h/d'                                      $csl_csitx_dir/V0/priv/cps_drv.h
sed -i '/\cdn_inttypes\.h/d'                            $csl_csitx_dir/V0/priv/csitx.c
sed -i -e 's|cdn_inttypes\.h|ti\/csl\/csl_csitx\.h|g'   $csl_csitx_dir/V0/priv/csitx.c
sed -i '/cdn_stdtypes\.h/d'                             $csl_csitx_dir/V0/priv/csitx.c
sed -i '/cdn_log\.h/d'                                  $csl_csitx_dir/V0/priv/csitx.c
sed -i '/cdn_errno\.h/d'                                $csl_csitx_dir/V0/priv/csitx.c
sed -i '/cps\.h/d'                                      $csl_csitx_dir/V0/priv/csitx.c
sed -i '/csitx_regs\.h/d'                               $csl_csitx_dir/V0/priv/csitx.c
sed -i '/csitx_regs_macros\.h/d'                        $csl_csitx_dir/V0/priv/csitx.c
sed -i '/csitx_if\.h/d'                                 $csl_csitx_dir/V0/priv/csitx.c
sed -i '/csitx_structs_if\.h/d'                         $csl_csitx_dir/V0/priv/csitx.c
sed -i '/csitx_priv\.h/d'                               $csl_csitx_dir/V0/priv/csitx.c
sed -i '/\#include \"cdn_errno\.h\"/d'                  $csl_csitx_dir/V0/priv/csitx_sanity.h
sed -i '/\#include \"cdn_stdtypes\.h\"/d'               $csl_csitx_dir/V0/priv/csitx_sanity.h
sed -i '/\#include \"csitx_if\.h\"/d'                   $csl_csitx_dir/V0/priv/csitx_sanity.h

############## Replace macro name change ##############
sed -i '/vDbgMsg/d'                                     $csl_csitx_dir/V0/priv/csitx.c
sed -i -e 's|CPS_UncachedRead32|CPS_REG_READ|g'         $csl_csitx_dir/V0/priv/csitx.c
sed -i -e 's|CPS_ReadReg32|CSL_REG32_RD_RAW|g'          $csl_csitx_dir/V0/priv/cps_drv.h
sed -i -e 's|CPS_WriteReg32|CSL_REG32_WR_RAW|g'         $csl_csitx_dir/V0/priv/cps_drv.h

############## Checkout not to be copied files ##############
cd $csl_csitx_dir
git checkout V0/cslr_csi_tx_if.h
git checkout V0/cslr_csitx.h

cd $working_dir

exit
