#!/bin/bash
#   ============================================================================
#   @desc   Script to sync-up the cadence dsi-tx driver to PDK CSL folder
#
#   ============================================================================
#   Revision History
#   27-Oct-2018 Sivaraj         Initial draft.
#   18-Mar-2019 Rishabh         Updated script
#   ============================================================================
# Command: cadence_sync_dsitx.sh <$cadence_dir>
# Example (Linux): ./cadence_sync_dsitx.sh /adasuser/rishabh/git/ip_releases_3p/cadence

export PATH=$PATH:/bin
date=`date '+%b_%d_%Y'`
date_print=`date '+%d-%b-%Y %I:%M:%S %p'`
month=`date '+%b_%Y'`
year=`date '+%Y'`
if [ "$OS" == "" ]; then
  echo "No OS defined, defaulting to linux"
  OS=linux
fi

#Get user input
working_dir=`pwd`
cadence_dir=$1
if [ "$cadence_dir" == "" ]; then
  cadence_dir=$working_dir/../../../ip_releases_3p/cadence
fi
if [ ! -d "$cadence_dir" ]; then
  echo "Kindly provide Cadence repo path to copy files from!!"
  exit
fi

###############################################################
##################### Copy D-PHY driver #######################
###############################################################

csl_phy_dir=$working_dir/../../packages/ti/drv/dss/src/csl/dphy
cadence_phy_dir=$cadence_dir/dsi_tx/dsi_tx_drv/software/dphy_driver
csl_dsitx_dir=$working_dir/../../packages/ti/drv/dss/src/csl/dsi
cadence_dsitx_dir=$cadence_dir/dsi_tx/dsi_tx_drv/software/core_driver

echo "PDK CSL DSI-TX Sync-up with Cadence Deliverable..."
echo "Date: $date_print"
echo ""

sedErrCheck() {
if [ $1 -ne 0 ]; then
    echo "Error occurred @ $2!!"
fi
}

############## Start clean ##############
echo [INFO] Start Clean!! Deleting to be copied files/folders...
cp $csl_dsitx_dir/cslr_dsi.h            $cadence_dsitx_dir/
cp $csl_dsitx_dir/csl_dsi.h             $cadence_dsitx_dir/
cp $csl_dsitx_dir/src/src_files_dsi.mk  $cadence_dsitx_dir/
cp $csl_phy_dir/cslr_dphy.h             $cadence_phy_dir/
cp $csl_phy_dir/csl_dphy.h              $cadence_phy_dir/
cp $csl_phy_dir/src/src_files_dphy.mk   $cadence_phy_dir/
rm -rf $csl_dsitx_dir
rm -rf $csl_phy_dir

############## Create required folders ##############
echo [INFO] Creating required folders...
echo ""
mkdir -p $csl_dsitx_dir/
mkdir -p $csl_dsitx_dir/src
mkdir -p $csl_dsitx_dir/include
mkdir -p $csl_phy_dir
mkdir -p $csl_phy_dir/src
mkdir -p $csl_phy_dir/include

############## Copy files from Cadence driver ##############
echo [INFO] Copying DPHY files from cadence to folder...
cp -r $cadence_phy_dir/common/cdn_errno.h                $csl_phy_dir/include
cp -r $cadence_phy_dir/common/cps_drv.h                  $csl_phy_dir/src
cp -r $cadence_phy_dir/include/dphy_if.h                 $csl_phy_dir/include
cp -r $cadence_phy_dir/include/dphy_structs_if.h         $csl_phy_dir/include
cp -r $cadence_phy_dir/src/dphy_regs.h                   $csl_phy_dir/include
cp -r $cadence_phy_dir/src/dphy_regs_macros.h            $csl_phy_dir/include
cp -r $cadence_phy_dir/src/dphy_if_sanity.h              $csl_phy_dir/src
cp -r $cadence_phy_dir/src/dphy_if_sanity.c              $csl_phy_dir/src
cp -r $cadence_phy_dir/src/dphy_if.c                     $csl_phy_dir/src
mv    $cadence_phy_dir/src_files_dphy.mk                 $csl_phy_dir/src
mv    $cadence_phy_dir/cslr_dphy.h                       $csl_phy_dir/
mv    $cadence_phy_dir/csl_dphy.h                        $csl_phy_dir/

############## Replace/Remove unwanted include paths ##############
sed -i '/cdn_stdtypes\.h/d'                              $csl_phy_dir/include/dphy_if.h
sed -i '/cdn_stdtypes\.h/d'                              $csl_phy_dir/include/dphy_structs_if.h
sed -i '/dphy_if\.h/d'                                   $csl_phy_dir/include/dphy_structs_if.h
sed -i '/dphy_regs_macros\.h/d'                          $csl_phy_dir/include/dphy_regs.h
sed -i '/cdn_stdtypes\.h/d'                              $csl_phy_dir/src/dphy_if_sanity.h
sed -i '/cdn_errno\.h/d'                                 $csl_phy_dir/src/dphy_if_sanity.h
sed -i '/dphy_if\.h/d'                                   $csl_phy_dir/src/dphy_if_sanity.h
sed -i 's|cdn_stdtypes\.h|src\/csl\/dphy\/csl_dphy.h|g'  $csl_phy_dir/src/dphy_if_sanity.c
sed -i '/cdn_errno\.h/d'                                 $csl_phy_dir/src/dphy_if_sanity.c
sed -i '/dphy_structs_if\.h/d'                           $csl_phy_dir/src/dphy_if_sanity.c
sed -i 's|cdn_stdtypes\.h|src\/csl\/dphy\/csl_dphy.h|g'  $csl_phy_dir/src/dphy_if.c
sed -i '/cdn_errno\.h/d'                                 $csl_phy_dir/src/dphy_if.c
sed -i '/cdn_log\.h/d'                                   $csl_phy_dir/src/dphy_if.c
sed -i '/dphy_if\.h/d'                                   $csl_phy_dir/src/dphy_if.c
sed -i '/dphy_structs_if\.h/d'                           $csl_phy_dir/src/dphy_if.c
sed -i '/dphy_regs\.h/d'                                 $csl_phy_dir/src/dphy_if.c
sed -i -e 's|CPS_ReadReg32|CSL_REG32_RD_RAW|g'           $csl_phy_dir/src/cps_drv.h
sed -i -e 's|CPS_WriteReg32|CSL_REG32_WR_RAW|g'          $csl_phy_dir/src/cps_drv.h

############## Copy files from Cadence driver ##############
echo [INFO] Copying DSITX files from cadence to folder...
cp -r $cadence_dsitx_dir/include/dsitx_if.h             $csl_dsitx_dir/include
cp -r $cadence_dsitx_dir/include/dsitx_structs_if.h     $csl_dsitx_dir/include
cp -r $cadence_dsitx_dir/include/dsitx_utils.h          $csl_dsitx_dir/src
cp -r $cadence_dsitx_dir/common/cdn_errno.h             $csl_dsitx_dir/include
cp -r $cadence_dsitx_dir/common/cps_drv.h               $csl_dsitx_dir/src
cp -r $cadence_dsitx_dir/src/dsitx.c                    $csl_dsitx_dir/src
cp -r $cadence_dsitx_dir/src/dsitx_cfg.h                $csl_dsitx_dir/src
cp -r $cadence_dsitx_dir/src/dsitx_priv.h               $csl_dsitx_dir/src
cp -r $cadence_dsitx_dir/src/dsitx_regs.h               $csl_dsitx_dir/include
cp -r $cadence_dsitx_dir/src/dsitx_regs_macros.h        $csl_dsitx_dir/include
cp -r $cadence_dsitx_dir/src/dsitx_sanity.h             $csl_dsitx_dir/src
cp -r $cadence_dsitx_dir/src/dsitx_utils.c              $csl_dsitx_dir/src
cp -r $cadence_dsitx_dir/src/dsitx_sanity.c             $csl_dsitx_dir/src

############## Replace/Remove unwanted include paths ##############
sed -i '/cdn_stdtypes\.h/d'                               $csl_dsitx_dir/include/dsitx_if.h
sed -i '/dsitx_regs_macros\.h/d'                          $csl_dsitx_dir/include/dsitx_if.h
sed -i '/cdn_stdtypes\.h/d'                               $csl_dsitx_dir/include/dsitx_structs_if.h
sed -i '/dsitf_if\.h/d'                                   $csl_dsitx_dir/include/dsitx_structs_if.h
sed -i '/cdn_stdtypes\.h/d'                               $csl_dsitx_dir/src/dsitx_sanity.h
sed -i '/cdn_errno\.h/d'                                  $csl_dsitx_dir/src/dsitx_sanity.h
sed -i '/dsitx_if\.h/d'                                   $csl_dsitx_dir/src/dsitx_sanity.h
sed -i '/cdn_stdint\.h/d'                                 $csl_dsitx_dir/src/dsitx_utils.h
sed -i '/cps\.h/d'                                        $csl_dsitx_dir/src/dsitx_utils.h
sed -i '/dsitx_if\.h/d'                                   $csl_dsitx_dir/src/dsitx_utils.h
sed -i 's|cdn_stdtypes\.h|src\/csl\/dsi\/csl_dsi.h|g'     $csl_dsitx_dir/src/dsitx.c
sed -i '/cdn_errno\.h/d'                                  $csl_dsitx_dir/src/dsitx.c
sed -i '/cdn_log\.h/d'                                    $csl_dsitx_dir/src/dsitx.c
sed -i '/dsitx_if\.h/d'                                   $csl_dsitx_dir/src/dsitx.c
sed -i '/dsitx_structs_if\.h/d'                           $csl_dsitx_dir/src/dsitx.c
sed -i '/dsitx_regs\.h/d'                                 $csl_dsitx_dir/src/dsitx.c
sed -i -e 's|cdn_errno\.h|src\/csl\/dsi\/csl_dsi.h|g'     $csl_dsitx_dir/src/dsitx_utils.c
sed -i '/dsitx_if\.h/d'                                   $csl_dsitx_dir/src/dsitx_utils.c
sed -i -e 's|cdn_stdtypes\.h|src\/csl\/dsi\/csl_dsi.h|g'  $csl_dsitx_dir/src/dsitx_sanity.c
sed -i '/cdn_errno\.h/d'                                  $csl_dsitx_dir/src/dsitx_sanity.c
sed -i '/dsitx_structs_if\.h/d'                           $csl_dsitx_dir/src/dsitx_sanity.c

############## Replace macro name change ##############
sed -i '/vDbgMsg/d'                                       $csl_dsitx_dir/src/dsitx.c
sed -i -e 's|CPS_UncachedRead32|CPS_REG_READ|g'           $csl_dsitx_dir/src/dsitx.c
sed -i -e 's|CPS_ReadReg32|CSL_REG32_RD_RAW|g'            $csl_dsitx_dir/src/cps_drv.h
sed -i -e 's|CPS_WriteReg32|CSL_REG32_WR_RAW|g'           $csl_dsitx_dir/src/cps_drv.h

############## Copy DP CSLR files ##############
mv    $cadence_dsitx_dir/src_files_dsi.mk                 $csl_dsitx_dir/src
mv    $cadence_dsitx_dir/csl_dsi.h                        $csl_dsitx_dir/
mv    $cadence_dsitx_dir/cslr_dsi.h                       $csl_dsitx_dir/

############## Checkout not to be copied files ##############
cd $csl_dsitx_dir

cd $working_dir

exit
