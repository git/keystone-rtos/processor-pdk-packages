#!/bin/bash
#   ============================================================================
#   @desc   Script to sync-up the cadence DDR driver to PDK CSL folder
#
#   ============================================================================
#   Revision History
#   02-Jul-2018 Sivaraj         Initial draft.
#
#   ============================================================================

#   ============================================================================
#   CAUTION: DO NOT INVOKE THE SCRIPTS AS 'gitadmin'.
#            IT WILL ADD OBJECT FILES IN BASE REPOSITORY; WHICH SHOULD IDEALLY
#            BE CLEAN.
#   ============================================================
# Command: cadence_sync_ddr.sh <$cadence_dir>
# Example (Linux): ./cadence_sync_ddr.sh /adasuser/sivaraj/k3/ip_releases_3p/cadence

export PATH=$PATH:/bin
date=`date '+%b_%d_%Y'`
date_print=`date '+%d-%b-%Y %I:%M:%S %p'`
month=`date '+%b_%Y'`
year=`date '+%Y'`
if [ "$OS" == "" ]; then
  echo "No OS defined, defaulting to linux"
  OS=linux
fi

#Get user input
working_dir=`pwd`
cadence_dir=$1
if [ "$cadence_dir" == "" ]; then
  cadence_dir=$working_dir/../../../ip_releases_3p/cadence
fi
if [ ! -d "$cadence_dir" ]; then
  echo "Kindly provide Cadence repo path to copy files from!!"
  exit
fi
csl_ddr_dir=$working_dir/../../packages/ti/csl/src/ip/lpddr
cadence_ddr_dir=$cadence_dir/lpddr/lpddr_drv/software/core_driver

echo "PDK CSL DDR Sync-up with Cadence Deliverable..."
echo "Date: $date_print"
echo ""

sedErrCheck() {
if [ $1 -ne 0 ]; then
    echo "Error occurred @ $2!!"
fi
}

############## Start clean ##############
echo [INFO] Start Clean!! Deleting tobe copied files/folders...
rm -rf $csl_ddr_dir/V0

############## Create required folders ##############
echo [INFO] Creating required folders...
mkdir -p $csl_ddr_dir/V0
mkdir -p $csl_ddr_dir/V0/priv

############## Copy STW/BSP files to temp folder ##############
echo [INFO] Copying files from cadence to folder...
cp -r $cadence_ddr_dir/include/lpddr4_if.h          $csl_ddr_dir/V0/
cp -r $cadence_ddr_dir/include/lpddr4_structs_if.h  $csl_ddr_dir/V0/
cp -r $cadence_ddr_dir/include/cdn_errno.h          $csl_ddr_dir/V0/
cp -r $cadence_ddr_dir/include/cps_drv_lpddr4.h     $csl_ddr_dir/V0/priv
cp -r $cadence_ddr_dir/src/*.*                      $csl_ddr_dir/V0/priv
#remove unwanted files
rm -rf $csl_ddr_dir/V0/priv/lpddr4_obj_if.c

############## Replace/Remove unwanted include paths ##############
sed -i '/cdn_stdtypes\.h/d'                                             $csl_ddr_dir/V0/lpddr4_if.h
sed -i '/cdn_stdtypes\.h/d'                                             $csl_ddr_dir/V0/lpddr4_structs_if.h
sed -i '/lpddr4_if\.h/d'                                                $csl_ddr_dir/V0/lpddr4_structs_if.h
sed -i '/cps\.h/d'                                                      $csl_ddr_dir/V0/priv/cps_drv_lpddr4.h
sed -i -e 's|\<cdn_errno\.h\>|ti\/csl\/csl_lpddr\.h|g'                  $csl_ddr_dir/V0/priv/lpddr4.c
sed -i '/lpddr4_if\.h/d'                                                $csl_ddr_dir/V0/priv/lpddr4.c
sed -i '/lpddr4_structs_if\.h/d'                                        $csl_ddr_dir/V0/priv/lpddr4.c
sed -i -e 's|static volatile uint32_t LPDDR4|static uint32_t LPDDR4|g'  $csl_ddr_dir/V0/priv/lpddr4.c
sed -i -e 's|\"cdn_stdint\.h\"|\<ti\/csl\/csl_lpddr\.h\>|g'             $csl_ddr_dir/V0/priv/lpddr4_ctl_regs_rw_masks.c
sed -i '/cdn_stdint\.h/d'                                               $csl_ddr_dir/V0/priv/lpddr4_ctl_regs_rw_masks.h
sed -i '/cdn_errno\.h/d'                                                $csl_ddr_dir/V0/priv/lpddr4_sanity.h
sed -i '/cdn_stdtypes\.h/d'                                             $csl_ddr_dir/V0/priv/lpddr4_sanity.h
sed -i '/lpddr4_if\.h/d'                                                $csl_ddr_dir/V0/priv/lpddr4_sanity.h

############## Replace macro and other name change ##############
sed -i -e 's|CPS_UncachedRead32|CSL_REG32_RD_RAW|g'     $csl_ddr_dir/V0/priv/cps_drv_lpddr4.h
sed -i -e 's|CPS_UncachedWrite32|CSL_REG32_WR_RAW|g'    $csl_ddr_dir/V0/priv/cps_drv_lpddr4.h

sed -i -e 's|LPDDR4_PhyIndepInterrupt interrupt|LPDDR4_PhyIndepInterrupt intr|g'    $csl_ddr_dir/V0/lpddr4_if.h
sed -i -e 's|LPDDR4_CtlInterrupt interrupt|LPDDR4_CtlInterrupt intr|g'              $csl_ddr_dir/V0/lpddr4_if.h
sed -i -e 's|LPDDR4_PhyIndepInterrupt interrupt|LPDDR4_PhyIndepInterrupt intr|g'    $csl_ddr_dir/V0/priv/lpddr4.c
sed -i -e 's|LPDDR4_CtlInterrupt interrupt|LPDDR4_CtlInterrupt intr|g'              $csl_ddr_dir/V0/priv/lpddr4.c
sed -i -e 's|LPDDR4_PhyIndepInterrupt interrupt|LPDDR4_PhyIndepInterrupt intr|g'    $csl_ddr_dir/V0/priv/lpddr4_sanity.h
sed -i -e 's|LPDDR4_CtlInterrupt interrupt|LPDDR4_CtlInterrupt intr|g'              $csl_ddr_dir/V0/priv/lpddr4_sanity.h

sed -i -e 's|, interrupt|, intr|g'                      $csl_ddr_dir/V0/priv/lpddr4.c
sed -i -e 's|(uint32_t)interrupt|(uint32_t)intr|g'      $csl_ddr_dir/V0/priv/lpddr4.c
sed -i -e 's|interrupt \!|intr \!|g'                    $csl_ddr_dir/V0/priv/lpddr4_sanity.h

cd $working_dir

exit
