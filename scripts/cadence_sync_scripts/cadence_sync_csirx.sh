#!/bin/bash
#   ============================================================================
#   @desc   Script to sync-up the cadence csi-rx driver to PDK CSL folder
#
#   ============================================================================
#   Revision History
#   02-Jul-2018 Sivaraj         Initial draft.
#
#   ============================================================================

#   ============================================================================
#   CAUTION: DO NOT INVOKE THE SCRIPTS AS 'gitadmin'.
#            IT WILL ADD OBJECT FILES IN BASE REPOSITORY; WHICH SHOULD IDEALLY
#            BE CLEAN.
#   ============================================================
# Command: cadence_sync_csirx.sh <$cadence_dir>
# Example (Linux): ./cadence_sync_csirx.sh /adasuser/sivaraj/k3/ip_releases_3p/cadence

export PATH=$PATH:/bin
date=`date '+%b_%d_%Y'`
date_print=`date '+%d-%b-%Y %I:%M:%S %p'`
month=`date '+%b_%Y'`
year=`date '+%Y'`
if [ "$OS" == "" ]; then
  echo "No OS defined, defaulting to linux"
  OS=linux
fi

#Get user input
working_dir=`pwd`
cadence_dir=$1
if [ "$cadence_dir" == "" ]; then
  cadence_dir=$working_dir/../../../ip_releases_3p/cadence
fi
if [ ! -d "$cadence_dir" ]; then
  echo "Kindly provide Cadence repo path to copy files from!!"
  exit
fi
csl_csirx_dir=$working_dir/../../packages/ti/csl/src/ip/csirx
cadence_csirx_dir=$cadence_dir/csi_rx/csi_rx_drv/software/core_driver

echo "PDK CSL CSI-RX Sync-up with Cadence Deliverable..."
echo "Date: $date_print"
echo ""

sedErrCheck() {
if [ $1 -ne 0 ]; then
    echo "Error occurred @ $2!!"
fi
}

############## Start clean ##############
echo [INFO] Start Clean!! Deleting tobe copied files/folders...
rm -rf $csl_csirx_dir/V0

############## Create required folders ##############
echo [INFO] Creating required folders...
mkdir -p $csl_csirx_dir/V0
mkdir -p $csl_csirx_dir/V0/priv

############## Copy STW/BSP files to temp folder ##############
echo [INFO] Copying files from cadence to folder...
cp -r $cadence_csirx_dir/include/csirx_if.h             $csl_csirx_dir/V0/
cp -r $cadence_csirx_dir/include/csirx_regs.h           $csl_csirx_dir/V0/
cp -r $cadence_csirx_dir/include/csirx_regs_macros.h    $csl_csirx_dir/V0/
cp -r $cadence_csirx_dir/include/csirx_structs_if.h     $csl_csirx_dir/V0/
cp -r $cadence_csirx_dir/common/cdn_errno.h             $csl_csirx_dir/V0/
cp -r $cadence_csirx_dir/src/csirx.c                    $csl_csirx_dir/V0/priv
#cp -r $cadence_csirx_dir/src/csirx_priv.h               $csl_csirx_dir/V0/priv
cp -r $cadence_csirx_dir/src/csirx_sanity.h             $csl_csirx_dir/V0/priv
cp -r $cadence_csirx_dir/common/cps_drv.h               $csl_csirx_dir/V0/priv

############## Replace/Remove unwanted include paths ##############
sed -i '/\#include \"cdn_inttypes\.h\"/d'               $csl_csirx_dir/V0/csirx_if.h
sed -i '/\#include \"cdn_stdtypes\.h\"/d'               $csl_csirx_dir/V0/csirx_if.h
sed -i '/\#include \"csirx_regs\.h\"/d'                 $csl_csirx_dir/V0/csirx_if.h
sed -i '/cps_drv\.h/d'                                  $csl_csirx_dir/V0/csirx_regs.h
sed -i '/\#include \"csirx_regs_macros\.h\"/d'          $csl_csirx_dir/V0/csirx_regs.h
sed -i '/\#include \"cdn_stdtypes\.h\"/d'               $csl_csirx_dir/V0/csirx_structs_if.h
sed -i '/\#include \"csirx_if\.h\"/d'                   $csl_csirx_dir/V0/csirx_structs_if.h
sed -i '/cps\.h/d'                                      $csl_csirx_dir/V0/priv/cps_drv.h
sed -i '/\cdn_inttypes\.h/d'                            $csl_csirx_dir/V0/priv/csirx.c
sed -i -e 's|cdn_inttypes\.h|ti\/csl\/csl_csirx\.h|g'   $csl_csirx_dir/V0/priv/csirx.c
sed -i '/cdn_stdtypes\.h/d'                             $csl_csirx_dir/V0/priv/csirx.c
sed -i '/cdn_log\.h/d'                                  $csl_csirx_dir/V0/priv/csirx.c
sed -i '/cdn_errno\.h/d'                                $csl_csirx_dir/V0/priv/csirx.c
sed -i '/cps\.h/d'                                      $csl_csirx_dir/V0/priv/csirx.c
sed -i '/csirx_regs\.h/d'                               $csl_csirx_dir/V0/priv/csirx.c
sed -i '/csirx_regs_macros\.h/d'                        $csl_csirx_dir/V0/priv/csirx.c
sed -i '/csirx_if\.h/d'                                 $csl_csirx_dir/V0/priv/csirx.c
sed -i '/csirx_structs_if\.h/d'                         $csl_csirx_dir/V0/priv/csirx.c
sed -i '/\#include \"cdn_errno\.h\"/d'                  $csl_csirx_dir/V0/priv/csirx_sanity.h
sed -i '/\#include \"cdn_stdtypes\.h\"/d'               $csl_csirx_dir/V0/priv/csirx_sanity.h
sed -i '/\#include \"csirx_if\.h\"/d'                   $csl_csirx_dir/V0/priv/csirx_sanity.h

############## Replace macro name change ##############
sed -i '/vDbgMsg/d'                                     $csl_csirx_dir/V0/priv/csirx.c
#sed -i -e 's|CPS_UncachedRead32|CPS_REG_READ|g'         $csl_csirx_dir/V0/priv/csirx.c
sed -i -e 's|CPS_ReadReg32|CSL_REG32_RD_RAW|g'          $csl_csirx_dir/V0/priv/cps_drv.h
sed -i -e 's|CPS_WriteReg32|CSL_REG32_WR_RAW|g'         $csl_csirx_dir/V0/priv/cps_drv.h

############## Checkout not to be copied files ##############
cd $csl_csirx_dir
git checkout V0/cslr_csi_rx_if.h
git checkout V0/cslr_csirx.h

cd $working_dir

exit
