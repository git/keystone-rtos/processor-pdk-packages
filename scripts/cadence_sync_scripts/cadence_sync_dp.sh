#!/bin/bash
#   ============================================================================
#   @desc   Script to sync-up the cadence DP driver to PDK CSL folder
#
#   ============================================================================
#   Revision History
#   18-Mar-2019 Rishabh         First version.
#
#   ============================================================================
# Command: cadence_sync_dp.sh <$cadence_dir>
# Example (Linux): ./cadence_sync_dp.sh /adasuser/rishabh/git/ip_releases_3p/cadence

export PATH=$PATH:/bin
date=`date '+%b_%d_%Y'`
date_print=`date '+%d-%b-%Y %I:%M:%S %p'`
month=`date '+%b_%Y'`
year=`date '+%Y'`
if [ "$OS" == "" ]; then
  echo "No OS defined, defaulting to linux"
  OS=linux
fi

#Get user input
working_dir=`pwd`
cadence_dir=$1
if [ "$cadence_dir" == "" ]; then
  cadence_dir=$working_dir/../../../ip_releases_3p/cadence
fi
if [ ! -d "$cadence_dir" ]; then
  echo "Kindly provide Cadence repo path to copy files from!!"
  exit
fi

###############################################################
################## Copy Torrent PHY driver ####################
###############################################################

csl_phy_dir=$working_dir/../../packages/ti/drv/dss/src/csl/dp_sd0801
cadence_phy_dir=$cadence_dir/dp/software/dp_sd0801_driver
csl_dp_dir=$working_dir/../../packages/ti/drv/dss/src/csl/dp
cadence_dp_dir=$cadence_dir/dp/software/core_driver

echo "PDK CSL eDP Sync-up with Cadence Deliverable..."
echo "Date: $date_print"
echo ""

sedErrCheck() {
if [ $1 -ne 0 ]; then
    echo "Error occurred @ $2!!"
fi
}

############## Start clean ##############
echo [INFO] Start Clean!! Deleting to be copied files/folders...
cp $csl_dp_dir/csl_dp.h $cadence_dp_dir/
cp $csl_dp_dir/cslr_dp.h $cadence_dp_dir/
cp $csl_dp_dir/src/src_files_dp.mk $cadence_dp_dir/
cp $csl_phy_dir/csl_dp_sd0801.h $cadence_phy_dir/
cp $csl_phy_dir/cslr_dp_sd0801.h $cadence_phy_dir/
cp $csl_phy_dir/src/src_files_sd0801.mk $cadence_phy_dir/
rm -rf $csl_dp_dir
rm -rf $csl_phy_dir

############## Create required folders ##############
echo [INFO] Creating required folders...
echo ""
mkdir -p $csl_dp_dir/
mkdir -p $csl_dp_dir/src
mkdir -p $csl_dp_dir/include
mkdir -p $csl_phy_dir
mkdir -p $csl_phy_dir/src
mkdir -p $csl_phy_dir/include
mkdir -p $csl_phy_dir/src/specific

############## Copy files from Cadence driver ##############
echo [INFO] Copying Torrent PHY files from cadence to folder...
cp -r $cadence_phy_dir/common/cdn_errno.h                         $csl_phy_dir/include
cp -r $cadence_phy_dir/common/cdn_log.h                           $csl_phy_dir/src
cp -r $cadence_phy_dir/common/cps.h                               $csl_phy_dir/src
cp -r $cadence_phy_dir/common/cps_drv.h                           $csl_phy_dir/src
cp -r $cadence_phy_dir/include/dp_sd0801_if.h                     $csl_phy_dir/include
cp -r $cadence_phy_dir/include/dp_sd0801_structs_if.h             $csl_phy_dir/include
cp -r $cadence_phy_dir/src/specific/*                             $csl_phy_dir/src/specific/
cp -r $cadence_phy_dir/src/dp_regs.h                              $csl_phy_dir/include
cp -r $cadence_phy_dir/src/dp_regs_macros.h                       $csl_phy_dir/include
cp -r $cadence_phy_dir/src/dp_sd0801.c                            $csl_phy_dir/src
cp -r $cadence_phy_dir/src/dp_sd0801_internal.h                   $csl_phy_dir/src
cp -r $cadence_phy_dir/src/dp_sd0801_priv.h                       $csl_phy_dir/src
cp -r $cadence_phy_dir/src/dp_sd0801_sanity.c                     $csl_phy_dir/src
cp -r $cadence_phy_dir/src/dp_sd0801_sanity.h                     $csl_phy_dir/src
mv    $cadence_phy_dir/src_files_sd0801.mk                        $csl_phy_dir/src
mv    $cadence_phy_dir/csl_dp_sd0801.h                            $csl_phy_dir/
mv    $cadence_phy_dir/cslr_dp_sd0801.h                           $csl_phy_dir/

############## Replace/Remove unwanted include paths ##############
sed -i 's|cdn_stdtypes\.h|stdint.h|g'                             $csl_phy_dir/include/dp_sd0801_structs_if.h
sed -i 's|cdn_errno\.h|src\/csl\/dp_sd0801\/csl_dp_sd0801.h|g'    $csl_phy_dir/src/dp_sd0801_sanity.c
sed -i 's|dp_sd0801_if\.h|src\/csl\/dp_sd0801\/csl_dp_sd0801.h|g' $csl_phy_dir/src/dp_sd0801.c
sed -i '/dp_regs\.h/d'                                            $csl_phy_dir/src/dp_sd0801.c
sed -i '/cdn_log\.h/d'                                            $csl_phy_dir/src/dp_sd0801.c
sed -i 's|CPS_BufferCopy|memcpy|g'                                $csl_phy_dir/src/dp_sd0801.c
sed -i 's|dp_sd0801_if\.h|src\/csl\/dp_sd0801\/csl_dp_sd0801.h|g' $csl_phy_dir/src/specific/dp_dummy_spec.c
sed -i '/dp_regs\.h/d'                                            $csl_phy_dir/src/specific/dp_dummy_spec.c
sed -i 's|dp_sd0801_if\.h|src\/csl\/dp_sd0801\/csl_dp_sd0801.h|g' $csl_phy_dir/src/specific/dp_sd0801_spec.c
sed -i '/dp_regs\.h/d'                                            $csl_phy_dir/src/specific/dp_sd0801_spec.c
sed -i '/cdn_log\.h/d'                                            $csl_phy_dir/src/specific/dp_sd0801_spec.c
sed -i 's|CPS_DelayNs(100)|Osal_delay(1) \/** Min 100 ns **\/|g'  $csl_phy_dir/src/specific/dp_sd0801_spec.c
sed -i 's|CPS_DelayNs(200)|Osal_delay(1) \/** Min 200 ns **\/|g'  $csl_phy_dir/src/specific/dp_sd0801_spec.c
sed -i 's|CPS_DelayNs(900)|Osal_delay(1) \/** Min 900 ns **\/|g'  $csl_phy_dir/src/specific/dp_sd0801_spec.c
sed -i -e 's|CPS_ReadReg32|CSL_REG32_RD_RAW|g'                    $csl_phy_dir/src/cps_drv.h
sed -i -e 's|CPS_WriteReg32|CSL_REG32_WR_RAW|g'                   $csl_phy_dir/src/cps_drv.h
find $csl_phy_dir/ -type f -exec sed -i '/cdn_stdtypes\.h/d' {} \;
find $csl_phy_dir/ -type f -exec sed -i '/custom_types\.h/d' {} \;
find $csl_phy_dir/ -type f -exec sed -i '/cdn_assert\.h/d' {} \;
find $csl_phy_dir/ -type f -exec sed -i '/cdn_inttypes\.h/d' {} \;
find $csl_phy_dir/ -type f -exec sed -i '/cdn_math\.h/d' {} \;
find $csl_phy_dir/ -type f -exec sed -i '/cps_ei\.h/d' {} \;
find $csl_phy_dir/ -type f -exec sed -i 's|float64_t|Float64|g' {} \;
find $csl_phy_dir/ -type f -exec sed -i 's|float32_t|Float32|g' {} \;

#########################################################
################## Copy eDP driver ######################
#########################################################

echo [INFO] Copying eDP files from cadence to folder...
cp -r $cadence_dp_dir/include/dp_aux_if.h                       $csl_dp_dir/src
cp -r $cadence_dp_dir/include/dp_aux_structs_if.h               $csl_dp_dir/include
cp -r $cadence_dp_dir/include/dp_if.h                           $csl_dp_dir/include
cp -r $cadence_dp_dir/include/dp_mst_if.h                       $csl_dp_dir/src
cp -r $cadence_dp_dir/include/dp_mst_structs_if.h               $csl_dp_dir/include
cp -r $cadence_dp_dir/include/dp_sideband_msg_if.h              $csl_dp_dir/src
cp -r $cadence_dp_dir/include/dp_sideband_msg_structs_if.h      $csl_dp_dir/include
cp -r $cadence_dp_dir/include/dp_structs_if.h                   $csl_dp_dir/include
cp -r $cadence_dp_dir/common/cdn_errno.h                        $csl_dp_dir/include
cp -r $cadence_dp_dir/common/cps_drv.h                          $csl_dp_dir/src
cp -r $cadence_dp_dir/common/cdn_log.h                          $csl_dp_dir/src
cp -r $cadence_dp_dir/src/mhdp_apb_regs.h                       $csl_dp_dir/include
cp -r $cadence_dp_dir/src/mhdp_apb_regs_macros.h                $csl_dp_dir/include
cp -r $cadence_dp_dir/src/dp_aux.c                              $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_aux.h                              $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_dsc.c                              $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_dsc.h                              $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_hdcp.c                             $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_hdcp.h                             $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_if.c                               $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_internal.h                         $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_link_policy.c                      $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_link_policy.h                      $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_mailbox.c                          $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_mailbox.h                          $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_mst.c                              $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_mst.h                              $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_priv.h                             $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_register.c                         $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_register.h                         $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_sanity.c                           $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_sanity.h                           $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_sdp.c                              $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_sideband_msg.c                     $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_sideband_msg.h                     $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_topology_mgr.c                     $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_topology_mgr.h                     $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_topology_utils.c                   $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_topology_utils.h                   $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_transaction.c                      $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_transaction.h                      $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_utils.c                            $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dp_utils.h                            $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dsc_utils.c                           $csl_dp_dir/src
cp -r $cadence_dp_dir/src/dsc_utils.h                           $csl_dp_dir/src

############## Replace/Remove unwanted include paths ##############
find $csl_dp_dir/ -type f -exec sed -i '/cdn_stdtypes\.h/d' {} \;
find $csl_dp_dir/ -type f -exec sed -i '/mhdp_apb_regs_macros\.h/d' {} \;
find $csl_dp_dir/ -type f -exec sed -i '/custom_types\.h/d' {} \;
find $csl_dp_dir/ -type f -exec sed -i '/cdn_assert\.h/d' {} \;
find $csl_dp_dir/ -type f -exec sed -i '/cdn_inttypes\.h/d' {} \;
find $csl_dp_dir/ -type f -exec sed -i '/cdn_math\.h/d' {} \;
find $csl_dp_dir/ -type f -exec sed -i 's|float64_t|Float64|g' {} \;
find $csl_dp_dir/ -type f -exec sed -i 's|float32_t|Float32|g' {} \;
sed -i '/cdn_math\.h/d'                                                  $csl_dp_dir/src/dp_mst_if.h
sed -i 's|cdn_errno\.h|src\/csl\/dp\/csl_dp.h|g'                         $csl_dp_dir/src/dp_aux.c
sed -i '/dp_if\.h/d'                                                     $csl_dp_dir/src/dp_aux.c
sed -i '/dp_structs_if\.h/d'                                             $csl_dp_dir/src/dp_aux.c
sed -i '/dp_aux_structs_if\.h/d'                                         $csl_dp_dir/src/dp_aux.c
sed -i '/dp_aux_if\.h/d'                                                 $csl_dp_dir/src/dp_aux.c
sed -i '/cps\.h/d'                                                       $csl_dp_dir/src/dp_aux.c
sed -i 's|dp_if\.h|src\/csl\/dp\/csl_dp.h|g'                             $csl_dp_dir/src/dp_dsc.c
sed -i '/mhdp_apb_regs\.h/d'                                             $csl_dp_dir/src/dp_dsc.c
sed -i '/cps_drv\.h/d'                                                   $csl_dp_dir/src/dp_dsc.c
sed -i 's|dp_if\.h|src\/csl\/dp\/csl_dp.h|g'                             $csl_dp_dir/src/dp_hdcp.c
sed -i '/dp_structs_if\.h/d'                                             $csl_dp_dir/src/dp_hdcp.c
sed -i '/cdn_errno\.h/d'                                                 $csl_dp_dir/src/dp_hdcp.c
sed -i 's|dp_structs_if\.h|src\/csl\/dp\/csl_dp.h|g'                     $csl_dp_dir/src/dp_if.c
sed -i 's|mhdp_apb_regs\.h|math\.h|g'                                    $csl_dp_dir/src/dp_if.c
sed -i '/cps_drv\.h/d'                                                   $csl_dp_dir/src/dp_if.c
sed -i '/dp_aux_structs_if\.h/d'                                         $csl_dp_dir/src/dp_if.c
sed -i '/cdn_errno\.h/d'                                                 $csl_dp_dir/src/dp_if.c
sed -i 's|CPS_BufferCopy|memcpy|g'                                       $csl_dp_dir/src/dp_if.c
sed -i 's|dp_if\.h|src\/csl\/dp\/csl_dp.h|g'                             $csl_dp_dir/src/dp_link_policy.c
sed -i '/dp_structs_if\.h/d'                                             $csl_dp_dir/src/dp_link_policy.c
sed -i '/mhdp_apb_regs\.h/d'                                             $csl_dp_dir/src/dp_link_policy.c
sed -i '/cps_drv\.h/d'                                                   $csl_dp_dir/src/dp_link_policy.c
sed -i '/cdn_errno\.h/d'                                                 $csl_dp_dir/src/dp_link_policy.c
sed -i '/cdn_log\.h/d'                                                   $csl_dp_dir/src/dp_link_policy.c
sed -i 's|CPS_BufferCopy|memcpy|g'                                       $csl_dp_dir/src/dp_link_policy.c
sed -i 's|dp_if\.h|src\/csl\/dp\/csl_dp.h|g'                             $csl_dp_dir/src/dp_mailbox.c
sed -i '/mhdp_apb_regs\.h/d'                                             $csl_dp_dir/src/dp_mailbox.c
sed -i '/cdn_stdint\.h/d'                                                $csl_dp_dir/src/dp_mailbox.c
sed -i '/cdn_errno\.h/d'                                                 $csl_dp_dir/src/dp_mailbox.c
sed -i '/cps_drv\.h/d'                                                   $csl_dp_dir/src/dp_mailbox.c
sed -i 's|CPS_BufferCopy|memcpy|g'                                       $csl_dp_dir/src/dp_mailbox.c
sed -i 's|dp_if\.h|src\/csl\/dp\/csl_dp.h|g'                             $csl_dp_dir/src/dp_mst.c
sed -i 's|mhdp_apb_regs\.h|math\.h|g'                                    $csl_dp_dir/src/dp_mst.c
sed -i '/cdn_errno\.h/d'                                                 $csl_dp_dir/src/dp_mst.c
sed -i '/cdn_log\.h/d'                                                   $csl_dp_dir/src/dp_mst.c
sed -i '/cps_drv\.h/d'                                                   $csl_dp_dir/src/dp_mst.c
sed -i 's|dp_register\.h|src\/csl\/dp\/csl_dp.h|g'                       $csl_dp_dir/src/dp_register.c
sed -i 's|dp_if\.h|dp_register\.h|g'                                     $csl_dp_dir/src/dp_register.c
sed -i 's|mhdp_apb_regs\.h|math\.h|g'                                    $csl_dp_dir/src/dp_register.c
sed -i '/cdn_errno\.h/d'                                                 $csl_dp_dir/src/dp_register.c
sed -i '/cps_drv\.h/d'                                                   $csl_dp_dir/src/dp_register.c
sed -i 's|cdn_errno\.h|src\/csl\/dp\/csl_dp.h|g'                         $csl_dp_dir/src/dp_sanity.c
sed -i '/dp_structs_if\.h/d'                                             $csl_dp_dir/src/dp_sanity.c
sed -i 's|dp_if\.h|src\/csl\/dp\/csl_dp.h|g'                             $csl_dp_dir/src/dp_sdp.c
sed -i 's|mhdp_apb_regs\.h|math\.h|g'                                    $csl_dp_dir/src/dp_sdp.c
sed -i '/cdn_errno\.h/d'                                                 $csl_dp_dir/src/dp_sdp.c
sed -i '/cdn_stdint\.h/d'                                                $csl_dp_dir/src/dp_sdp.c
sed -i 's|cdn_errno\.h|src\/csl\/dp\/csl_dp.h|g'                         $csl_dp_dir/src/dp_sideband_msg.c
sed -i '/cdn_log\.h/d'                                                   $csl_dp_dir/src/dp_sideband_msg.c
sed -i '/dp_sideband_msg_structs_if\.h/d'                                $csl_dp_dir/src/dp_sideband_msg.c
sed -i '/stddef\.h/d'                                                    $csl_dp_dir/src/dp_topology_mgr.c
sed -i '/stdlib\.h/d'                                                    $csl_dp_dir/src/dp_topology_mgr.c
sed -i 's|cps\.h|src\/csl\/dp\/csl_dp.h|g'                               $csl_dp_dir/src/dp_topology_mgr.c
sed -i 's|cdn_log\.h|math\.h|g'                                          $csl_dp_dir/src/dp_topology_mgr.c
sed -i 's|CPS_DelayNs(100000)|Osal_delay(100) \/** Min 100 us **\/|g'    $csl_dp_dir/src/dp_topology_mgr.c
sed -i 's|CPS_DelayNs(10000000)|Osal_delay(10000) \/** Min 10 ms **\/|g' $csl_dp_dir/src/dp_topology_mgr.c
sed -i 's|stdlib\.h|src\/csl\/dp\/csl_dp.h|g'                            $csl_dp_dir/src/dp_topology_utils.c
sed -i '/cdn_log\.h/d'                                                   $csl_dp_dir/src/dp_topology_utils.c
sed -i 's|stdlib\.h|src\/csl\/dp\/csl_dp.h|g'                            $csl_dp_dir/src/dp_transaction.c
sed -i '/cdn_log\.h/d'                                                   $csl_dp_dir/src/dp_transaction.c
sed -i '/cps\.h/d'                                                       $csl_dp_dir/src/dp_transaction.c
sed -i 's|CPS_DelayNs(1000000)|Osal_delay(1000) \/** Min 1 ms **\/|g'    $csl_dp_dir/src/dp_transaction.c
sed -i 's|dp_if\.h|src\/csl\/dp\/csl_dp.h|g'                             $csl_dp_dir/src/dp_utils.h
sed -i '/mhdp_apb_regs\.h/d'                                             $csl_dp_dir/src/dp_utils.h
sed -i 's|dp_if\.h|src\/csl\/dp\/csl_dp.h|g'                             $csl_dp_dir/src/dsc_utils.h
sed -i 's|cdn_log\.h|math\.h|g'                                          $csl_dp_dir/src/dsc_utils.c

############## Replace macro name change ##############
sed -i -e 's|CPS_ReadReg32|CSL_REG32_RD_RAW|g'          $csl_dp_dir/src/cps_drv.h
sed -i -e 's|CPS_WriteReg32|CSL_REG32_WR_RAW|g'         $csl_dp_dir/src/cps_drv.h

############## Copy DP CSLR files ##############
mv    $cadence_dp_dir/src_files_dp.mk                           $csl_dp_dir/src
mv    $cadence_dp_dir/csl_dp.h                                  $csl_dp_dir/
mv    $cadence_dp_dir/cslr_dp.h                                 $csl_dp_dir/

############## Checkout not to be copied files ##############
cd $csl_dp_dir

cd $working_dir

exit
