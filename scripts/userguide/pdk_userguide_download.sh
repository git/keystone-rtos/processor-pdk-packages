#!/bin/bash
#   ============================================================================
#   @file   pdk_userguide_download.sh
#
#   @desc   Script to download the PDK uderguide from processor wiki and
#           create offline documents
#
#   ============================================================================
#   Revision History
#   30-Jun-2011 Sivaraj         Initial draft.
#
#   ============================================================================
#Usage
# ./pdk_userguide_download.sh

#Get user input
while [ $# -gt 0 ]; do
  case "$1" in
    --pdk_base_dir=*)
      pdk_base_dir="${1#*=}"
      ;;
    *)
      printf "Error: Invalid argument $1!!\n"
  esac
  shift
done
#Default value if not provided
cur_dir=`pwd`
: ${pdk_base_dir:="$cur_dir/../.."}

export no_proxy=

rm -rf $pdk_base_dir/docs/userguide/html
mkdir -p $pdk_base_dir/docs/userguide/html
cd $pdk_base_dir/docs/userguide/html
wget_options="-q -nd --page-requisites --html-extension --convert-links --restrict-file-names=windows"
wget $wget_options -i $pdk_base_dir/scripts/userguide/input.txt

#Remove long filename files - problem in windows download/install
rm -rf $pdk_base_dir/docs/userguide/html/load.php*

export no_proxy=.ti.com

exit
