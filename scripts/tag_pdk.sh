#!/bin/bash
#   ============================================================================
#   @file   tag_pdk.sh
#
#   @desc   Script to do tagging of PDK repos and push to origin
#
#   ============================================================================
#   Revision History
#   08-Jun-2017 Sivaraj         Initial draft.
#   04-Jul-2018 Sivaraj         Updated for j6_master
#
#   ============================================================================
#Usage
# ./tag_pdk.sh --pdk_base_dir="/adasuser/sivaraj/pdk" --pdk_tag="REL.PDK.TDA.01.07.00.02" --pdk_tag_msg="RC2/Final release build"

#Get user input
while [ $# -gt 0 ]; do
  case "$1" in
    --pdk_base_dir=*)
      pdk_base_dir="${1#*=}"
      ;;
    --pdk_tag=*)
      pdk_tag="${1#*=}"
      ;;
    --pdk_tag_msg=*)
      pdk_tag_msg="${1#*=}"
      ;;
    --pdk_branch=*)
      pdk_branch="${1#*=}"
      ;;
    --csl_branch=*)
      csl_branch="${1#*=}"
      ;;
    *)
      printf "Error: Invalid argument $1!!\n"
  esac
  shift
done

#Default value if not provided
: ${pdk_base_dir:="/adasuser/sivaraj/pdk"}
: ${pdk_tag:="REL.PDK.TDA.01.10.01.01"}
: ${pdk_tag_msg:="RC/Final release build"}
: ${pdk_branch:="j6_master"}
: ${csl_branch:="j6_master"}

git_tag() {
  git_dir=$1
  branch=$2
  tag=$3
  tag_msg="$4"
  if [ ! -d "$git_dir" ]; then
    echo "Skipping $git_dir as this is not present!!!"
    return
  fi

  cd $git_dir
  git fetch -q origin                       #Fetch first so that repos we get latest commit
  #Get origin commit ID of the branch
  commit_id=`git log origin/$branch -n 1 --pretty=oneline | cut -d " " -f 1`
  git tag $tag $commit_id -m "$tag_msg"     #Tag

  #Cross verify
  new_commit_id=`git log $tag -n 1 --pretty=oneline | cut -d " " -f 1`
  if [ "$commit_id" != "$new_commit_id" ]; then
    echo "$git_dir: $commit_id and $new_commit_id don't match. Tag not pushed!!!"
  else
    git push origin $tag                    #Push tag to remote
  fi
}

git_tag "$pdk_base_dir"                                     $pdk_branch $pdk_tag "$pdk_tag_msg"
git_tag "$pdk_base_dir/docs"                                $pdk_branch $pdk_tag "$pdk_tag_msg"
git_tag "$pdk_base_dir/packages/ti/build"                   $pdk_branch $pdk_tag "$pdk_tag_msg"
git_tag "$pdk_base_dir/packages/ti/csl"                     $csl_branch $pdk_tag "$pdk_tag_msg"
git_tag "$pdk_base_dir/packages/ti/diag"                    $pdk_branch $pdk_tag "$pdk_tag_msg"
git_tag "$pdk_base_dir/packages/ti/boot/sbl_auto"           $pdk_branch $pdk_tag "$pdk_tag_msg"
git_tag "$pdk_base_dir/packages/ti/boot/sbl_auto/security"  $pdk_branch $pdk_tag "$pdk_tag_msg"
git_tag "$pdk_base_dir/packages/ti/drv/bsp_lld"             $pdk_branch $pdk_tag "$pdk_tag_msg"
git_tag "$pdk_base_dir/packages/ti/drv/fw_l3l4"             $pdk_branch $pdk_tag "$pdk_tag_msg"
git_tag "$pdk_base_dir/packages/ti/drv/ipc_lite"            $pdk_branch $pdk_tag "$pdk_tag_msg"
git_tag "$pdk_base_dir/packages/ti/drv/pm"                  $pdk_branch $pdk_tag "$pdk_tag_msg"
git_tag "$pdk_base_dir/packages/ti/drv/stw_lld"             $pdk_branch $pdk_tag "$pdk_tag_msg"
git_tag "$pdk_base_dir/packages/ti/drv/vps"                 $pdk_branch $pdk_tag "$pdk_tag_msg"
