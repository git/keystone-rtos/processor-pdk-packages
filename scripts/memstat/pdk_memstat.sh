#!/bin/bash
#   ============================================================================
#   @file   pdk_memstat.sh
#
#   @desc   Script to do PDK packaging
#
#   ============================================================================
#   Revision History
#   31-May-2017 Sivaraj         Initial draft.
#
#   ============================================================================
# Example: ./pdk_memstat.sh --generate --pdk_dir=/data/datalocal1_videoapps01/user/pdp_jenkin_build/pdk_release_build/package/final

start_time=`date +%s`
: ${OS:="linux"}

TOOLCHAIN_PATH_GCC="/datalocal/ti_components/cg_tools/linux/gcc-arm-none-eabi-4_9-2015q3"
TOOLCHAIN_PATH_TI_CGT="/datalocal/ti_components/cg_tools/linux/ti-cgt-arm_16.9.1.LTS"
C6X_GEN_INSTALL_PATH="/datalocal/ti_components/cg_tools/linux/C6000_7.4.2"
TOOLCHAIN_PATH_EVE="/datalocal/ti_components/cg_tools/linux/arp32_1.0.7"

SIZE_aa15fg=$TOOLCHAIN_PATH_GCC"/bin/arm-none-eabi-size"
SIZE_aem4=$TOOLCHAIN_PATH_TI_CGT"/bin/armofd"
SIZE_ae66=$C6X_GEN_INSTALL_PATH"/bin/ofd6x"
SIZE_aearp32F=$TOOLCHAIN_PATH_EVE"/bin/ofd-arp32"
SECTTI="/data/datalocal1_videoapps01/ti_components/cg_tools/linux/cg_xml_2_41_00/bin/sectti"

#Get user input
while [ $# -gt 0 ]; do
  case "$1" in
    --pdk_dir=*)
      pdk_dir="${1#*=}"
      ;;
    --generate*)
      generate="yes"
      ;;
    *)
      printf "Error: Invalid argument $1!!\n"
  esac
  shift
done

#Default value if not provided
if [ "$pdk_dir" == "" ]; then
  pdk_dir=`pwd`/../../
fi
: ${generate:="no"}

print_memstat() {
    size_file=$1
    lib_name=$2
    core_ext=$3

    # Extract size
    if [ "$core_ext" == "aa15fg" ]; then
        code_size=`grep  "(TOTALS)" $size_file | sed -e 's/ \{1,\}/:/g' | cut -d ":" -f 2`
        data_size=`grep  "(TOTALS)" $size_file | sed -e 's/ \{1,\}/:/g' | cut -d ":" -f 3`
        udata_size=`grep "(TOTALS)" $size_file | sed -e 's/ \{1,\}/:/g' | cut -d ":" -f 4`
    else
        code_size=`grep  -i -A 6 "Totals by section type" $size_file | grep "Code"               | cut -d ":" -f 2 | sed -e 's/^[[:space:]]*//' | cut -d " " -f 1`
        data_size=`grep  -i -A 6 "Totals by section type" $size_file | grep "Initialized Data"   | cut -d ":" -f 2 | sed -e 's/^[[:space:]]*//' | cut -d " " -f 1`
        udata_size=`grep -i -A 6 "Totals by section type" $size_file | grep "Uninitialized Data" | cut -d ":" -f 2 | sed -e 's/^[[:space:]]*//' | cut -d " " -f 1`
    fi

    printf "%-35s: %12d %12d %12d\n" $lib_name $code_size $data_size $udata_size
}

print_memstat_all() {
    soc=$1
    core_name=$2
    core_ext=$3
    profile=$4
    sizelist_file="temp_sizelist.txt"
    liblist_file="temp_listlist.txt"
    output_file="memstat_"$soc"_"$core_name"_"$profile".txt"
    output_file_temp="memstat_"$soc"_"$core_name"_"$profile"temp.txt"

    if [ "$generate" == "yes" ]; then
        #List of all libraries
        find $pdk_dir -wholename "*/"$soc"*/"$profile"*."$core_ext > $liblist_file
        #Remove example utility
        sed -i '/vps_examples_utility/d' $liblist_file

        #Generate size files for all libraries
        while read line
        do
            lib_name=`echo $line | sed 's:.*/::'`
            SIZE="SIZE_"$core_ext
            if [ "$core_ext" == "aa15fg" ]; then
                $SIZE_aa15fg -t $line > $lib_name"_size.txt"
            else
                if [ "$core_ext" == "aem4" ]; then
                    $SIZE_aem4 -x $line > $lib_name"temp"
                else
                    if [ "$core_ext" == "ae66" ]; then
                        $SIZE_ae66  -x $line > $lib_name"temp"
                    else
                        if [ "$core_ext" == "aearp32F" ]; then
                            $SIZE_aearp32F  -x $line > $lib_name"temp"
                        fi
                    fi
                fi
                $SECTTI $lib_name"temp" > $lib_name"_size.txt"
                rm -f $lib_name"temp"
            fi

            print_memstat $lib_name"_size.txt" $lib_name $core_ext >> $output_file_temp
            rm -f $lib_name"_size.txt"
        done <$liblist_file
    else
        #List of all size files
        find $pdk_dir -wholename "*/"$soc"*/"$profile"*."$core_ext"_size.txt" > $sizelist_file
        #Remove example utility
        sed -i '/vps_examples_utility/d' $sizelist_file

        while read line
        do
            # Extract library name from filepath
            lib_name=`echo $line | sed 's:.*/::' | sed 's:_size.txt::'`
            print_memstat $line $lib_name $core_ext >> $output_file_temp
        done <$sizelist_file
    fi

    #Sort and remove duplicate
    sort -u $output_file_temp -o $output_file_temp

    #Calculate total
    total_code_size=`cat $output_file_temp | cut -d ":" -f 2 | sed -e 's/ \{1,\}/:/g' | cut -d ":" -f 2 | awk '{ SUM += $1} END { print SUM }'`
    total_data_size=`cat $output_file_temp | cut -d ":" -f 2 | sed -e 's/ \{1,\}/:/g' | cut -d ":" -f 3 | awk '{ SUM += $1} END { print SUM }'`
    total_bss_size=`cat  $output_file_temp | cut -d ":" -f 2 | sed -e 's/ \{1,\}/:/g' | cut -d ":" -f 4 | awk '{ SUM += $1} END { print SUM }'`

    #Create the final output file with header and total
    rm -f $output_file
    printf "\nMemstat for %s %s %s build...\n\n" $soc $core_name $profile                           >> $output_file
    printf "%-35s: %12s %12s %12s\n" "Library Name" "CODE" "DATA" "BSS"                             >> $output_file
    printf "%s\n" "----------------------------------------------------------------------------"    >> $output_file
    cat $output_file_temp                                                                           >> $output_file
    printf "%s\n" "----------------------------------------------------------------------------"    >> $output_file
    printf "%-35s: %12s %12s %12s\n" "Total Size" $total_code_size $total_data_size $total_bss_size >> $output_file
    printf "%s\n" "----------------------------------------------------------------------------"    >> $output_file

    #Print generated file to console for FYI
    cat $output_file

    #clean-up
    rm -f $sizelist_file
    rm -f $liblist_file
    rm -f $output_file_temp
}

print_memstat_all "tda2xx" "A15"   "aa15fg"     "release"
print_memstat_all "tda2xx" "IPU"   "aem4"       "release"
print_memstat_all "tda2xx" "C66x"  "ae66"       "release"
print_memstat_all "tda2xx" "ARP32" "aearp32F"   "release"
mkdir -p ../../docs/memstat/tda2xx
mv memstat_tda2xx* ../../docs/memstat/tda2xx

print_memstat_all "tda2ex" "A15"   "aa15fg"     "release"
print_memstat_all "tda2ex" "IPU"   "aem4"       "release"
print_memstat_all "tda2ex" "C66x"  "ae66"       "release"
mkdir -p ../../docs/memstat/tda2ex
mv memstat_tda2ex* ../../docs/memstat/tda2ex

print_memstat_all "tda3xx" "IPU"   "aem4"       "release"
print_memstat_all "tda3xx" "C66x"  "ae66"       "release"
print_memstat_all "tda3xx" "ARP32" "aearp32F"   "release"
mkdir -p ../../docs/memstat/tda3xx
mv memstat_tda3xx* ../../docs/memstat/tda3xx

print_memstat_all "tda2px" "A15"   "aa15fg"     "release"
print_memstat_all "tda2px" "IPU"   "aem4"       "release"
print_memstat_all "tda2px" "C66x"  "ae66"       "release"
print_memstat_all "tda2px" "ARP32" "aearp32F"   "release"
mkdir -p ../../docs/memstat/tda2px
mv memstat_tda2px* ../../docs/memstat/tda2px

###################### End of build ######################

end_time=`date +%s`
let deltatime=end_time-start_time
let hours=deltatime/3600
let minutes=deltatime/60
let minutes=minutes%60
let seconds=deltatime%60
printf "\nExecution Time: %d:%02d:%02d\n" $hours $minutes $seconds

exit
