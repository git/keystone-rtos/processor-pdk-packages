#!/bin/bash
# ******************************************************************************
# * FILE PURPOSE: Environment Setup for building PDK
# ******************************************************************************
# * FILE NAME: pdksetupenv.sh
# *
# * DESCRIPTION: 
# *  Configures and sets up the Build Environment for PDK. 
# *
# *  The batch file expects an optional argument:PDK_INSTALL_PATH: Location
# *  of the PDK package.  If the argument is not specified the batch file
# *  assumes that the PDK is installed in the same location where the batch
# *  file is located and is being executed.
# *
# * USAGE:
# *  source ./pdksetupenv.sh ~/ti/pdk_<device>_<version>/packages
# *     --- OR ---
# *  source ./pdksetupenv.sh
# *
# * Copyright (C) 2012-2018, Texas Instruments, Inc.
# *****************************************************************************

# *******************************************************************************
# ******************* CHECK IF SCRIPT WAS SOURCED OR SIMPLY RUN   ***************
# *******************************************************************************
# pdksetupenv.sh must always be sourced. Sometimes, peole forget this and can run
# it. We display and error and a prompt whe we detect this.
if [[ "$(basename -- "$0")" == "pdksetupenv.sh" ]]; then
    echo "Error!! Don't run $0, source it" >&2
    echo "USAGE:" >&2
    echo "    source $0" >&2
    exit 1
fi

# *******************************************************************************
# ********************** GET PARAMETERS PASSED THROUGH ARGUMENT   ***************
# *******************************************************************************
# Parameter Validation: Check if the argument was passed to the batch file and
# if so we use that else we default to the working directory where the batch 
# file was invoked from

tempVar=$1
if [ ! -z $tempVar ];then
    export PDK_INSTALL_PATH=$tempVar
else
    export PDK_INSTALL_PATH=${PWD}
fi

# TI SDK root directory
if [ -z $SDK_INSTALL_PATH ]; then
    export SDK_INSTALL_PATH=__SDK__INSTALL_DIR__
fi

# Tools root directory
if [ -z $TOOLS_INSTALL_PATH ]; then
    export TOOLS_INSTALL_PATH=__SDK__INSTALL_DIR__
fi

# *******************************************************************************
# ********************** CHECK REQUIRED ENVIRONMENT DEFINES BEGIN ***************
# *******************************************************************************

# PDK SoC and version
if [ -z "$PDK_SOC" ]; then
    export PDK_SOC="__PDK_SOC__"
fi

if [ -z "$PDK_VERSION" ]; then
    export PDK_VERSION="__PDK_VER__"
fi

# Rules.make location. Uncomment the following line to set the
# top level Rules.make location
export RULES_MAKE="${SDK_INSTALL_PATH}/pdk_${PDK_SOC}_${PDK_VERSION}/packages/Rules.make"

echo "**************************************************************************"
echo "Environment Configuration:"
echo "**************************************************************************"
echo "    SDK_INSTALL_PATH        : $SDK_INSTALL_PATH"
echo "    GMAKE_INSTALL_PATH      : $GMAKE_INSTALL_PATH"
echo "    RULES_MAKE              : $RULES_MAKE"
echo "**************************************************************************"