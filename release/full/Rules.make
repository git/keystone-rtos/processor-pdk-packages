#
# Copyright (c) 2017 - 2018, Texas Instruments Incorporated
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# *  Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# *  Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# *  Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

################################################################################
# Environment Setup
################################################################################

LIBDIR ?= ./lib
export LIBDIR

export OS ?= linux

SDK_INSTALL_PATH ?= __SDK__INSTALL_DIR__

export LIMIT_SOCS ?= __LIMIT_SOCS__
export LIMIT_BOARDS ?= __LIMIT_BOARDS__
export LIMIT_CORES ?= __LIMIT_CORES__

export PDK_SOC ?= __PDK_SOC__
export PDK_VERSION ?= __PDK_VER__

# Version of IPC
export IPC_VERSION=__IPC_VER__

# Version of the IMGLIB
export IMGLIB_VERSION=c66x_3_1_1_0

# Version of the DSPLIB
export DSPLIB_VERSION=c66x_3_4_0_4

# Version of the MATHLIB
export MATHLIB_VERSION=c66x_3_1_2_4

# Version of AERLIB
export VOLIB_VERSION=C66_2_1_0_1

export IPC_INSTALL_PATH     = $(SDK_INSTALL_PATH)/ipc_$(IPC_VERSION)
export IMGLIB_INSTALL_PATH  = $(SDK_INSTALL_PATH)/imglib_$(IMGLIB_VERSION)
export DSPLIB_INSTALL_PATH  = $(SDK_INSTALL_PATH)/dsplib_$(DSPLIB_VERSION)
export MATHLIB_INSTALL_PATH = $(SDK_INSTALL_PATH)/mathlib_$(MATHLIB_VERSION)
export VOLIB_INSTALL_PATH   = $(SDK_INSTALL_PATH)/volib_$(VOLIB_VERSION)

ifeq ($(PDK_INSTALL_PATH),)
  export PDK_INSTALL_PATH  = $(SDK_INSTALL_PATH)/pdk_$(PDK_SOC)_$(PDK_VERSION)/packages
endif

RULES_MAKE=$(PDK_INSTALL_PATH)/ti/build/Rules.make

include $(RULES_MAKE)

################################################################################
# XDC PATH Configuration
################################################################################

export XDCPATH := ../../..;$(XDC_INSTALL_PATH)/packages;$(PDK_INSTALL_PATH);$(EDMA3LLD_BIOS6_INSTALLDIR)/packages;$(BIOS_INSTALL_PATH)/packages;$(IPC_INSTALL_PATH)/packages;$(NDK_INSTALL_PATH)/packages;$(UIA_INSTALL_PATH)/packages

################################################################################
# Build Tools Configuration
################################################################################

ifeq ($(OS),Windows_NT)
  PATH := $(PATH)
endif

# Compiler Tools:
PATH := $(C6X_GEN_INSTALL_PATH)/bin;$(PATH)

# XDC Tools location:
PATH := $(XDC_INSTALL_PATH);$(XDC_INSTALL_PATH)/bin;$(XDC_INSTALL_PATH)/packages/xdc/services/io/release;$(PATH)

ifeq ($(OS),Windows_NT)
  PATH := $(subst /,\,$(PATH))
else
  PATH := $(subst ;,:,$(PATH))
endif

export PATH

.PHONY: .env
.env:
	@echo "**************************************************************************"
	@echo "Tools Configuration:"
	@echo "    CROSS_TOOL_PRFX           : $(CROSS_TOOL_PRFX)"
	@echo "    C6X_GEN_INSTALL_PATH      : $(C6X_GEN_INSTALL_PATH)"
	@echo "    CL_PRU_INSTALL_PATH       : $(CL_PRU_INSTALL_PATH)"
	@echo "    HARDLIB_PATH              : $(HARDLIB_PATH)"
	@echo "    TOOLCHAIN_PATH_A8         : $(TOOLCHAIN_PATH_A8)"
	@echo "    TOOLCHAIN_PATH_A9         : $(TOOLCHAIN_PATH_A9)"
	@echo "    TOOLCHAIN_PATH_Arm9       : $(TOOLCHAIN_PATH_Arm9)"
	@echo "    TOOLCHAIN_PATH_A15        : $(TOOLCHAIN_PATH_A15)"
	@echo "    TOOLCHAIN_PATH_EVE        : $(TOOLCHAIN_PATH_EVE)"
	@echo "    TOOLCHAIN_PATH_M4         : $(TOOLCHAIN_PATH_M4)"
	@echo "Component Configuration:"
	@echo "    BIOS_INSTALL_PATH         : $(BIOS_INSTALL_PATH)"
	@echo "    DSPLIB_INSTALL_PATH       : $(DSPLIB_INSTALL_PATH)"
	@echo "    EDMA3LLD_BIOS6_INSTALLDIR : $(EDMA3LLD_BIOS6_INSTALLDIR)"
	@echo "    IMGLIB_INSTALL_PATH       : $(IMGLIB_INSTALL_PATH)"
	@echo "    IPC_INSTALL_PATH          : $(IPC_INSTALL_PATH)"
	@echo "    MATHLIB_INSTALL_PATH      : $(MATHLIB_INSTALL_PATH)"
	@echo "    NDK_INSTALL_PATH          : $(NDK_INSTALL_PATH)"
	@echo "    PDK_INSTALL_PATH          : $(PDK_INSTALL_PATH)"
	@echo "    PROC_SDK_INSTALL_PATH     : $(PROC_SDK_INSTALL_PATH)"
	@echo "    UIA_INSTALL_PATH          : $(UIA_INSTALL_PATH)"
	@echo "    XDC_INSTALL_PATH          : $(XDC_INSTALL_PATH)"
	@echo "Remaining SDK Configuration:"
	@echo "    LIMIT_BOARDS              : $(LIMIT_BOARDS)"
	@echo "    LIMIT_SOCS                : $(LIMIT_SOCS)"
	@echo "    IPC_PLATFORM              : $(IPC_PLATFORM)"
	@echo "    IPC_ALT_PLATFORM          : $(IPC_ALT_PLATFORM)"
	@echo "**************************************************************************"

