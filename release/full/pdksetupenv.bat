@REM ******************************************************************************
@REM * FILE PURPOSE: Environment Setup for building PDK
@REM ******************************************************************************
@REM * FILE NAME: pdksetupenv.bat
@REM *
@REM * DESCRIPTION: 
@REM *  Configures and sets up the Build Environment for PDK.
@REM *
@REM *  The batch file expects an optional argument:PDK_INSTALL_PATH: Location
@REM *  of the PDK package.  If the argument is not specified the batch file
@REM *  assumes that the PDK is installed in the same location where the batch
@REM *  file is located and is being executed.
@REM *
@REM * USAGE:
@REM *  pdksetupenv.bat "C:\ti\pdk_<device>_<version>\packages"
@REM *   --- OR ---
@REM *  pdksetupenv.bat
@REM *
@REM * Copyright (C) 2012-2018, Texas Instruments, Inc.
@REM *****************************************************************************
@echo off
@REM *******************************************************************************
@REM ********************** GET PARAMETERS PASSED THROUGH ARGUMENT   ***************
@REM *******************************************************************************
@REM Parameter Validation: Check if the argument was passed to the batch file and
@REM if so we use that else we default to the working directory where the batch 
@REM file was invoked from

if not defined SDK_INSTALL_PATH (
    set SDK_INSTALL_PATH=__SDK__INSTALL_DIR__
)

@REM PDK SoC and version
if not defined PDK_SOC set PDK_SOC=__PDK_SOC__
if not defined PDK_VERSION set PDK_VERSION=__PDK_VER__

@REM Version of XDC
set XDC_VERSION=3_55_02_22_core

if not defined GMAKE_INSTALL_PATH (
    set GMAKE_INSTALL_PATH=%SDK_INSTALL_PATH%/xdctools_%XDC_VERSION%
)
set GMAKE_INSTALL_PATH=%GMAKE_INSTALL_PATH:\=/%

@REM Windows Path; Uncomment the following lines to set environemnt PATH
@REM set PATH=C:/Windows/System32

@REM GMAKE Tool location; Uncomment the following line to set the 
@REM gmake install path into env PATH variable.
set PATH=%GMAKE_INSTALL_PATH:/=\%;%PATH%

@REM Following line is to set the top level Rules.make location.
set RULES_MAKE=%SDK_INSTALL_PATH%/pdk_%PDK_SOC%_%PDK_VERSION%/packages/Rules.make

@REM Print the environmental variables
@echo ***************************************************
@echo Environment Configuration:
@echo ***************************************************
@echo     SDK_INSTALL_PATH        : %SDK_INSTALL_PATH%
@echo     GMAKE_INSTALL_PATH      : %GMAKE_INSTALL_PATH%
@echo     RULES_MAKE              : %RULES_MAKE%
@echo ***************************************************
