@REM ******************************************************************************
@REM * FILE PURPOSE: SDK install path auto set script
@REM ******************************************************************************
@REM * FILE NAME: sdk_install.bat
@REM *
@REM * DESCRIPTION: This file will execute at the time of post installation of pdk
@REM * package. It will auto set the SDK_INSTALL_PATH macro with the user installed
@REM * directory.
@REM *
@REM * Copyright (C) 2018, Texas Instruments, Inc.
@REM *****************************************************************************
@echo off

@REM SoC for SDK environment
set SOC=__PDK_SOC__

@REM Version of Processor SDK
set PROC_SDK_VERSION=__PROC_SDK_VER__

@REM Version of PDK
set PDK_VERSION=__PDK_VER__

set PDK_PACKAGE_PATH=pdk_%SOC%_%PDK_VERSION%/packages

set SDK_INSTALL_PATH=%~dp0

if exist %SDK_INSTALL_PATH%%PDK_PACKAGE_PATH%\Rules.make (
set install_dir=%SDK_INSTALL_PATH%
) else (
echo Installation directory does not exist
goto :endFile
)

setlocal enableextensions disabledelayedexpansion

set install_dir=%install_dir:\=/%

if %install_dir:~-1% == / (
    set install_dir=%install_dir:~0,-1%
)

set "search=__SDK__INSTALL_DIR__"
set "replace=%install_dir%"

@REM List of files which updates __SDK__INSTALL_DIR_WIN__ macro
set "textFileArray=( %install_dir%\pdk_%SOC%_%PDK_VERSION%\packages\Rules.make %install_dir%\pdk_%SOC%_%PDK_VERSION%\packages\ti\build\Rules.make %install_dir%\pdk_%SOC%_%PDK_VERSION%\packages\pdksetupenv.bat %install_dir%\pdk_%SOC%_%PDK_VERSION%\packages\pdkProjectCreate.bat %install_dir%\processor_sdk_rtos_%SOC%_%PROC_SDK_VERSION%\setupenv.bat %install_dir%\processor_sdk_rtos_%SOC%_%PROC_SDK_VERSION%\Rules.make )"

@REM Loop textFileArray to replace the macro with the installation directory.
for %%F in %textFileArray% do (
    if exist %%F (
        echo %%F
        for /f "delims=" %%i in ('type "%%F" ^| find /v /n "" ^& break ^> "%%F"') do (
            set "line=%%i"
            setlocal enabledelayedexpansion
	        set "line=!line:*]=!"
            if defined line set "line=!line:%search%=%replace%!"
            >>"%%F" echo(!line!
            endlocal
        )
    ) else (
        echo %%F is not installed in this location
    )
)

:endFile
DEL "%~f0" & EXIT