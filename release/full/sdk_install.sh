#!/bin/bash

# ******************************************************************************
# * FILE PURPOSE: SDK install path auto set script
# ******************************************************************************
# * FILE NAME: sdk_install.sh
# *
# * DESCRIPTION: This file will execute at the time of post installation of pdk
# * package. It will auto set the SDK_INSTALL_PATH macro with the user installed
# * directory.
# *
# * Copyright (C) 2018, Texas Instruments, Inc.
# *****************************************************************************

# Process command line...
while [ $# -gt 0 ]; do
  case $1 in
    --install_dir)
      shift;
      install_dir=$1;
      ;;
     *)
      shift;
      ;;
  esac
done

currentscript="$0"

# Function that is called when the script exits:
function finish {
    echo "Securely Exiting ${currentscript}"; shred -u ${currentscript};
}

# SoC for SDK environment
SOC=__PDK_SOC__

# Version of Processor SDK
PROC_SDK_VERSION=__PROC_SDK_VER__

# Version of PDK
PDK_VERSION=__PDK_VER__

PDK_PACKAGE_PATH=pdk_${SOC}_${PDK_VERSION}/packages

echo "${PDK_PACKAGE_PATH}"

# Get full path to SDK directory
cd "$install_dir"
install_dir="$PWD"
cd -

echo "$install_dir"

if [ ! -d $install_dir ]; then
        echo "Installation directory does not exist"
        exit 1
fi

# Update SDK_INSTALL_DIR_LINUX variables
fileList=("${install_dir}/${PDK_PACKAGE_PATH}/ti/build/Rules.make" "${install_dir}/${PDK_PACKAGE_PATH}/Rules.make" "${install_dir}/${PDK_PACKAGE_PATH}/pdksetupenv.sh" "${install_dir}/${PDK_PACKAGE_PATH}/pdkProjectCreate.sh" "${install_dir}/processor_sdk_rtos_${SOC}_${PROC_SDK_VERSION}/setupenv.sh" "${install_dir}/processor_sdk_rtos_${SOC}_${PROC_SDK_VERSION}/Rules.make")

# Loop textFileArray to replace the macro with the installation directory.
for file in ${fileList[@]};
do
	if [ -e ${file} ]
	then
		sed -i -e s=__SDK__INSTALL_DIR__=$install_dir= $file
	else
		echo "${file} is not installed in this location"
	fi
done

# Exit with a call to the function, "finish":
trap finish EXIT